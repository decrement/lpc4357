/*! @file CAMYACB21.h
 * @brief pin&gpio defines and function prototypes
*
* pin definitions are described by 6 aspects:
*
*		SCU port number
*		SCU pin number
*		GPIO port number
*		GPIO pin number
*		alternate function index for GPIO mode
*		alternate function index for required mode, sometimes it is the same as GPIO mode
*/
#ifndef _CAMYACB21_H_
#define _CAMYACB21_H_

#define _BV(n) (1UL<<(n))

#define _BV00	(1UL<<0)
#define _BV01	(1UL<<1)
#define _BV02	(1UL<<2)
#define _BV03	(1UL<<3)
#define _BV04	(1UL<<4)
#define _BV05	(1UL<<5)
#define _BV06	(1UL<<6)
#define _BV07	(1UL<<7)


/*! @name PinDefMacros
 * @brief pin definition macros. Every pin has up to 6 aspects, refer to comments for "IMGD0" pin
*/
///@{

//! port number in SCU
#define SPT_IMGD0		0xC		// SCU Port
//! pin number in SCU
#define SPN_IMGD0		1		// SCU Pin
//! port number in GPIO
#define GPT_IMGD0		6		// GPIO Port
//! pin number in GPIO
#define GPN_IMGD0		0		// GPIO Pin
//! alternate function index of GPIO mode
#define GALT_IMGD0		4		// Alternative function for GPIO
//! alternate function index needed for IMGD0
#define ALT_IMGD0		GALT_IMGD0	// Alternative function for use as IMGD0


#define SPT_FLHLIGHT	0x2
#define SPN_FLHLIGHT	3
#define GPT_FLHLIGHT	5
#define GPN_FLHLIGHT	3
#define GALT_FLHLIGHT	4
#define ALT_FLHLIGHT	GALT_FLHLIGHT

#define SPT_VSYNC		0xF
#define SPN_VSYNC		8
#define GPT_VSYNC		7
#define GPN_VSYNC		22
#define ALT_VSYNC		2
//! SCT.Input0
#define CTIN_VSYNC		2	// SCT.IN2   vsync

#define SPT_HSYNC		0xD
#define SPN_HSYNC		10
#define GPT_HSYNC		6
#define GPN_HSYNC		24
#define ALT_HSYNC		1
#define GALT_HSYNC		0
//! SCT.Input3
#define CTIN_HSYNC		1	// SCT.IN1   hsync


#define SPT_PXCLK		0xD
#define SPN_PXCLK		13
#define GPT_PXCLK		6
#define GPN_PXCLK		27
#define GALT_PXCLK		1
#define ALT_PXCLK		1
//! SCT.Input5
#define CTIN_PXCLK		0	// SCT.IN0    pclk



#define SPT_LDO_EN		0xA          //   pwn
#define SPN_LDO_EN		3
#define GPT_LDO_EN		4
#define GPN_LDO_EN		10
#define GALT_LDO_EN		0
#define ALT_LDO_EN		GALT_LDO_EN

#define SPT_CHIP_ENB	0xA           //  reset
#define SPN_CHIP_ENB	4
#define GPT_CHIP_ENB	5
#define GPN_CHIP_ENB	19
#define GALT_CHIP_ENB	4
#define ALT_CHIP_ENB	GALT_CHIP_ENB



//! MCLK hasn't GPIO relationship
#define SPT_MCLK		0x1
#define SPN_MCLK		19
#define ALT_MCLK		4
///@}

//					  DisPullDn | DisPullUp | NromalSlew| InputBufNCare	| DisGlitchFilter
#define SPNCFG_OUT_GNRC	0UL<<3 	| 1UL<<4 	| 0UL<<5 	| 0UL<<6 		| 1UL<<7

//					  DisPullDn | DisPullUp | HiSpdSlew | InputBufNCare	| DisGlitchFilter
#define SPNCFG_OUT_HSPD	0UL<<3 	| 1UL<<4 	| 1UL<<5 	| 0UL<<6 		| 1UL<<7


//					  DisPullDn | DisPullUp | N.C. 		| InputBufEn	| N.C.
#define SPNCFG_IN_GNRC	0UL<<3 	| 1UL<<4 	| 0UL<<5 	| 1UL<<6 		| 0UL<<7


//					  DisPullDn | EnPullUp  | N.C. 		| InputBufEn	| N.C.
#define SPNCFG_IN_PUP	0UL<<3 	| 0UL<<4 	| 0UL<<5 	| 1UL<<6 		| 0UL<<7

//! signal is direct through
#define GIMACFG_DIRECT	0

#define YACB_LDO_EN_H			LPC_GPIO_PORT->SET[GPT_LDO_EN] = 1UL<<GPN_LDO_EN
#define YACB_LDO_EN_L			LPC_GPIO_PORT->CLR[GPT_LDO_EN] = 1UL<<GPN_LDO_EN
#define YACB_CHIP_ENB_H		LPC_GPIO_PORT->SET[GPT_CHIP_ENB] = 1UL<<GPN_CHIP_ENB
#define YACB_CHIP_ENB_L		LPC_GPIO_PORT->CLR[GPT_CHIP_ENB] = 1UL<<GPN_CHIP_ENB

#define YACB_SCTDMAREQNUM	1	// DMA AHB master can only access memory
#define YACB_SCTDMAPERIPNUM	9UL
#define YACB_DMAMUXNUM		2

#define MCLK_ON		1
#define MCLK_OFF	0

#define YACB_I2C_ADDR	0x21//0x30

#define MICROSEC_PER_TICK	250
#define MS2TICK(n) (1000 / MICROSEC_PER_TICK * n)

/*
 * define the camera register for debug view
*/
typedef struct _DS_YACBReg
{
	unsigned short val;
	unsigned char adr;
	unsigned char pg:6;
	unsigned char isInUse:1;	// After 1st time scan it becomes in use, first time just read value
	unsigned char is16b:1;		// organize register pairs representing 16bit value as single 16b reg
}DS_YACBReg;

// define the camera register bank for debug view
typedef union _US_YACBRegs
{
	volatile DS_YACBReg regs[64];
	volatile unsigned int d32[64];	// for ease debug
}US_YACBRegs;

typedef enum
{
	CAMDATAFMT_YUV422 = 0,
	CAMDATAFMT_RGB565 = 4,
	CAMDATAFMT_RGB422 = 7,
}enum_YACBDataFormat;


//! enumeration of states used
typedef enum
{
    ctst_wantV = 0,	// wait for VSYNC, occured only the first time
    ctst_wantH = 1,	// wait for HSYNC, after vsync fall or prev hsync fall
    ctst_inH = 2,	// in a HSYNC pulse, fetching pixel data
    ctst_inHSkipUV = 3,
    ctst_skipV = 4,	// if used, a frame is skipped after last frame
}enum_CamTimSt;

//! enumeration of events used
typedef enum
{
    ctev_vsyncRise = 0,
    ctev_vsyncRiseInSkip,	// 
    ctev_vsyncFall,
    ctev_hsyncRise,
    ctev_hsyncFall,
    ctev_pxclkRiseE,
    ctev_pxclkRiseO,
}enum_ctev;

#define IRAM1_SECT __attribute__((section("SECT_IRAM1"), zero_init))

typedef struct
{
	__IO uint32_t  SRCADDR;				  /*!< (@ 0x40002100) DMA Channel Source Address Register */
	__IO uint32_t  DSTADDR;				  /*!< (@ 0x40002104) DMA Channel Destination Address Register */
	__IO uint32_t  LLI;					  /*!< (@ 0x40002108) DMA Channel Linked List Item Register */
	__IO uint32_t  CONTROL;				  /*!< (@ 0x4000210C) DMA Channel Control Register */
	__IO uint32_t  CONFIG;				  /*!< (@ 0x40002110) DMA Channel Configuration Register */
	__I  uint32_t  RESERVED1[3];

}LPC_GPDMACHN_Type;

/*! @brief main control block for driving the camera
* Brief description continued.
* this block contains most parameters required for operation, some members are
* not mandatory but just for test.
*/
typedef struct
{
	unsigned int y; //! JustForTest: current line in current frame
	
	//volatile unsigned char *pFB;	//! current destination receiving address for next DMA xfer
	LPC_GPDMA_Type *pDMA;	//! pointer to DMA peripheral common registers
	LPC_GPDMACHN_Type *pCHN;  //! pointer to the register bank of used DMA channel
	//US_SnapDblBuf *pSnaps;
	//unsigned char isRdyToShow;
#if (COLOR_CHOOSE == GRAY)
	unsigned char lineBuf[CAMERA_X_SIZE];
#else
	unsigned short lineBuf[CAMERA_X_SIZE];
#endif
}DS_CamCB;


/*! @defgroup YACBA21 camera register low level access routines
  * @brief camera low level register access routines, do not call them directly
  * @{
  */
void DelayTicks(unsigned int cnt);

// Read one register in current page
uint8_t YACBprmRdReg(unsigned char addr);

/**
  * @brief  Write one register in current page.
  * @param  addr
  *   This parameter can be any combination of the following values:
  *     @arg RTC_IT_OW: Overflow interrupt
  *     @arg RTC_IT_ALR: Alarm interrupt
  *     @arg RTC_IT_SEC: Second interrupt
  * @param  NewState new state of the specified RTC interrupts.
  *   This parameter can be: ENABLE or DISABLE.
  * @retval None
  * @remark Don't forget to select page first!
  */
signed int YACBprmWrReg(unsigned char addr, unsigned char value);
/*! @brief select 'pg' as page and write 'value' to register who has address 'addr'
 * @param pg page of dest. register
 * @param addr address of dest. register within the page
 * @param value value to write
 * @retval >=0 on success
*/
signed int YACBprmWrPgAndReg(unsigned int pg, unsigned char addr, unsigned char value);
/*! @brief select 'pg' as page and read from register who has address 'addr'
 * @param pg page of source register
 * @param addr address of source register within the page
 * @retval <0 on error ; >=0 read value
*/
unsigned char YACBprmRdPgAndReg(unsigned int pg, unsigned char addr);
/*! @brief select 'pg' as page and write 'value' to register pair who has address 'addr'
 * @param pg page of dest. register pair
 * @param addr address of dest. register pair within the page
 * @param v16 16-bit value to write
 * @retval >=0 on success
*/
signed int YACBprmWrPgAndReg16(unsigned int pg, unsigned char addr, unsigned short v16);
/*! @brief select 'pg' as page and read from register pair who has address 'addr'
 * @param pg page of source register pair
 * @param addr address of source register pair within the page
 * @retval <0 on error ; >=0 read value (16-bit)
*/
unsigned short YACBprmRdPgAndReg16(unsigned int pg, unsigned char addr);
/*! @brief turn on/off MCLK to camera
 * @param isOn 0=turnoff, !0=turnon
 * @retval none
*/
void YACBprm_MCLKCtl(unsigned int isOn);
/*! @brief generate power down timing sequence to camera
*/
void YACBprm_PwrDnSeq(void);
/*! @brief generate power on timing sequence to camera
*/
void YACBprm_PwrOnSeq(void);
/*! @brief Select currrent working page of camera
 *  @param newPg new page to select
*/
signed int YACBprm_PageSel(unsigned int newPg);
/*! @brief Enter/exit sleep mode of camera
 *	@param isSlp: 0=no sleep ; other = sleep
*/
signed int YACBprm_PowerSleepCtl(unsigned int isSlp);

/*! @brief Configure pins, clock, and GPIO of MCU
* 
* @param none
*/
void YACBprm_MCUCfg(void);

#define ATR_RAMCODE	__attribute__((section("SECT_RAMCODE")))
extern void ATR_RAMCODE YACB_RfrsRegs(void);


//! @}

/*! @defgroup YACBCamAPI Camera API functions
* @{
*/
/*! @brief Initialize camera
*
* Call this to initialize the camera, including :
*
*		MCU pin intialize, 
*		power on the camera
*		camera register initialize
*		control block variable initialize
*		SCT and FSM initialize
*/
signed int YACB_Init(void);
/*! @brief Read camera device ID
 * @retval DvcID >=0: read device ID ; <0 : error
*/
unsigned int YACB_ReadDvcID(void);
/*!
 * @brief  Set output data format
 * @param  fmt
 *   This parameter can be any combination of the following values:
 *     @arg CAMDATAFMT_YUV422
 *     @arg CAMDATAFMT_RGB565
 *     @arg CAMDATAFMT_RGB422
 * @param  variant
 *   variant (mainly color order) in the current format
 * @retval errCode >=0 if success
 * @remark Don't forget to select page first!
 */
signed int YACB_SetOutputDataFormat(enum_YACBDataFormat fmt, unsigned int variant);
/*! Refresh latest captured image, and refresh camera register values
*
* it calls YACB_RefreshColor or YACB_RefreshGray according to macro YUV_Y
* if YUV_Y is defined, then gray mode is selected
* Since LCD orientation is 90 degree rotated compared with camera output
* row-column swap is done by CPU */
signed int YACB_Refresh(void);
//! @}
#endif

