
#ifndef __CAMERA_H
#define __CAMERA_H

#define COLOR  1
#define GRAY   2

#define COLOR_CHOOSE   COLOR
//#define COLOR_CHOOSE GRAY

#define CAMERA_X_SIZE 640
#define CAMERA_Y_SIZE 480

// 初始化摄像头接口
extern void CAMERA_init(void);

// 摄像头捕获到一行数据（中断调用）
#if (COLOR_CHOOSE == GRAY)
// 黑白模式，每个相素点数据为一个字节
extern void CAMERA_lineData(int line, unsigned char * data);
#else
// 彩色模式，每个相素点数据为二个字节
extern void CAMERA_lineData(int line, unsigned short * data);
#endif

// 摄像头完成捕获一帧（中断调用）
extern void CAMERA_frameComplete(void);

#endif


