
#include "board.h"
#include "uart1_rs485.h"
#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h"

#define IER_RBR		0x01
#define IER_THRE	0x02
#define IER_RLS		0x04

#define IIR_PEND	0x01
#define IIR_RLS		0x03
#define IIR_RDA		0x02
#define IIR_CTI		0x06
#define IIR_THRE	0x01

#define LSR_RDR		0x01
#define LSR_OE		0x02
#define LSR_PE		0x04
#define LSR_FE		0x08
#define LSR_BI		0x10
#define LSR_THRE	0x20
#define LSR_TEMT	0x40
#define LSR_RXFE	0x80

#define UART1_INT_Rx_DISABLE (LPC_UART1->IER &= ~(0x01))
#define UART1_INT_Rx_ENABLE  (LPC_UART1->IER |= 0x01)

#define UART1_INT_Tx_DISABLE (LPC_UART1->IER &= ~(0x02))
#define UART1_INT_Tx_ENABLE  (LPC_UART1->IER |= 0x02)

#define UART1_INT_RLS_DISABLE (LPC_UART1->IER &= ~(0x04))
#define UART1_INT_RLS_ENABLE  (LPC_UART1->IER |= 0x04)

#define UART1_Tx(byte)   (LPC_UART1->THR = byte & 0xFF)
#define UART1_Rx         (LPC_UART1->RBR & 0xFF)

// 判断接收缓冲区是否为空
#define UART1_FIFO_Rx_EMPTY       (!(LPC_UART1->LSR & 0x01))
// 判断发送缓冲区是否为空
#define UART1_FIFO_Tx_EMPTY       (LPC_UART1->LSR & 0x020)

#define UART_Rx()         (LPC_UART1->RBR & 0xFF)

#define RS485_SND_ENABLE()    GPIO_SetValue(6, 1 << 11)
#define RS485_SND_DISABLE()   GPIO_ClearValue(6, 1 << 11)


int32_t g_uart1TxBufferHead = 0;
int32_t g_uart1TxBufferTail = 0;

#define UART1_Tx_BUFFER_LEN   256
uint8_t g_uart1TxBuffer[UART1_Tx_BUFFER_LEN];  

void uart1Read(void)
{
    uint8_t buffer[16];
    uint32_t i;

    i = 0;

    while ((!UART1_FIFO_Rx_EMPTY) && (i < 16))
    {
        buffer[i] = UART_Rx();
        i++;
    }
    
    if (i)
    {
        UART1_rcv(buffer, i);
    }
}

void uart1Write(void)
{
    if (g_uart1TxBufferHead == g_uart1TxBufferTail)
	{
        RS485_SND_DISABLE();

		return;
	}

	RS485_SND_ENABLE();

    while ((UART1_FIFO_Tx_EMPTY) && (g_uart1TxBufferHead != g_uart1TxBufferTail))
    {
        UART1_Tx(g_uart1TxBuffer[g_uart1TxBufferHead]);

        g_uart1TxBufferHead++;
        if (g_uart1TxBufferHead >= UART1_Tx_BUFFER_LEN)
        {
            g_uart1TxBufferHead = 0;
        }
    }

//	if (g_uart1TxBufferHead == g_uart1TxBufferTail)
//	{
//        RS485_SND_DISABLE();
//
//		return;
//	}
}

// 根据外设频率和波特率得到寄存器设置需要的除数
uint16_t uart1GetDivisorByBaudrate(uint32_t clk, uint32_t baudrate)
{
    float divisor;
    uint16_t divisorInt;
    
    divisor = (float)clk/(baudrate*16);
    
    divisorInt = (uint16_t)(divisor/256);
    
    divisor-=(divisorInt*256);
    
    if ((divisor - (uint32_t)divisor) > 0.5F)
    {
        divisorInt = (divisorInt << 8) | ((uint8_t)divisor + 1);
    }
    else
    {
        divisorInt = (divisorInt << 8) | (uint8_t)divisor;
    }
    
    return divisorInt;  // 高八位为DLM 低八位为DLL
}
  
// 入参clk：UART2模块工作的系统频率
// 入参baudrate：UART2工作的波特率
void UART1_init(unsigned long sysClk, unsigned long baudrate)
{
    uint16_t divisor;
	uint32_t tempReg;

	/* UART1 使用PLL1的120M时钟 */
	tempReg = LPC_CGU2->BASE_UART1_CLK;
    tempReg &=	~(0xF<<24);
	tempReg |= ((0x0009 << 24) | (0x0001 << 11));
	LPC_CGU2->BASE_UART1_CLK = tempReg;

	scu_pinmux(0xC ,13 , MD_PDN, FUNC2); 	            // UART1_TXD
	scu_pinmux(0xC ,14 , MD_PLN|MD_EZI|MD_ZI, FUNC2); 	// UART1_RXD
	scu_pinmux(0xC ,12 , MD_PDN, FUNC4); 				// UART1_DTR as gpio

	GPIO_SetDir(6, 1 << 11, 1);
	GPIO_ClearValue(6, 11);	

    LPC_UART1->LCR  = 0x83;                      /* 允许设置波特率               */

    // 外设频率60M. 256*DLM+DLL = 3750000/baudrate  
    // baudrate设为9600. 则DLM = 1 DLL = 135

    divisor = uart1GetDivisorByBaudrate(sysClk, baudrate);

    LPC_UART1->DLM  = ((divisor >> 8) & 0x00FF);
    LPC_UART1->DLL  = (divisor & 0x00FF);
    LPC_UART1->LCR  = 0x03;                      /* 锁定波特率                   */
    LPC_UART1->FCR  = 0x87;                      // RX FIFO在收到8个字节时产生中断。 
	
    g_uart1TxBufferHead = 0;
    g_uart1TxBufferTail = 0;
	
    //串口中断初始化

    NVIC_EnableIRQ(UART1_IRQn);	 
    LPC_UART1->IER = IER_RBR | IER_THRE | IER_RLS;	/* Enable UART2 interrupt */		   
}

 /*****************************************************************************
** Function name:		UART2_IRQHandler
**
** Descriptions:		UART2 interrupt handler
**
** parameters:			None
** Returned value:		None
** 
*****************************************************************************/
void UART1_IRQHandler(void)
{
    uint8_t u0lsr, IIRValue;
	     
    IIRValue = LPC_UART1->IIR;
    					   
    IIRValue >>= 1;			/* skip pending bit in IIR */
    IIRValue &= 0x07;			/* check bit 1~3, interrupt identification */

    switch (IIRValue)
    {
    case IIR_RLS:
        u0lsr = LPC_UART1->LSR;
    
        /* Receive Line Status */
        if (u0lsr&(LSR_OE|LSR_PE|LSR_FE|LSR_RXFE|LSR_BI))
        {
            /* There are errors or break interrupt */
            /* Read LSR will clear the interrupt */
            LPC_UART1->RBR;		/* Dummy read on RX to clear interrupt, then bail out */
        }	
	
        break;
    case IIR_RDA:	/* Receive Data Available */
        uart1Read();	 
        break;
    case IIR_CTI:	/* Character timeout indicator */
        uart1Read();	
        break;
    case IIR_THRE:	/* THRE, transmit holding register empty */
        uart1Write();
        break;
    default:
        break;
  }
}
 
// 应用层通过UART接口发送数据
// 入参 data：发送的数据空间
// 入参 len：发送的数据长度
// 返回：实际成功发送的数据长度（如果没有完全发送，请稍作延迟后再发）
unsigned long UART1_snd(unsigned char * data, unsigned long len)
{
    int32_t i, next;

    UART1_INT_Tx_DISABLE;			  //关闭中断

    for (i = 0; i < len; i++)
	{
        next = g_uart1TxBufferTail + 1;
	    if (next >= UART1_Tx_BUFFER_LEN)
	    {
	        next = 0;
	    }

		if (next == g_uart1TxBufferHead)
		{
		    break;
		}
	    g_uart1TxBuffer[g_uart1TxBufferTail] = data[i];

        g_uart1TxBufferTail = next;
	}

    uart1Write();

    UART1_INT_Tx_ENABLE;			   //打开中断

	return i;
}

void UART1_intRxCtl(unsigned long enable)
{
    if (enable)
    {
        UART1_INT_Rx_ENABLE;
    }
    else
    {
        UART1_INT_Rx_DISABLE;   
    }
}

/******************************************************************************
**                            End Of File
******************************************************************************/

