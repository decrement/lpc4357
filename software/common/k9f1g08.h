#ifndef K9F1G08_H
#define K9F1G08_H


void k9f_init(void);
RAW_U32 k9f_id_read(void);
RAW_U8 k9f_block_erase(RAW_U32 block_number);
RAW_U8 k9f_page_read(RAW_U32 page_number, RAW_U8 *nand_buffer);
RAW_U8 k9f_block_read(RAW_U32 block, RAW_U8 *buf);
RAW_U8 k9f_bad_block_check_product(RAW_U32 block);
RAW_U8 k9f_page_write(RAW_U32 page_number, RAW_U8 *pbuf);
RAW_U8 k9f_random_read(RAW_U32 page_number, RAW_U32 addr);
RAW_U8 k9f_random_write(RAW_U32 page_number, RAW_U32 addr, RAW_U8 dat);
RAW_U8 k9f_random_continue_read(RAW_U32 page_number,RAW_U32 add, RAW_U8 *dat, RAW_U8 count);
RAW_U8 k9f_random_continue_write(RAW_U32 page_number, RAW_U32 addr, RAW_U8 *dat, RAW_U8 count);


#endif

