/**********************************************************************
* $Id$		sgpio_pwm.h	2012-07-20
*//**
* @file		sgpio_pwm.h
* @brief	The library for using SGPIO to generate PWM signals
* @version	1.0
* @date		20. July. 2012
* @author	NXP MCU SW Application Team
*
* Copyright(C) 2012, NXP Semiconductor
* All rights reserved.
*
***********************************************************************
* Software that is described herein is for illustrative purposes only
* which provides customers with programming information regarding the
* products. This software is supplied "AS IS" without any warranties.
* NXP Semiconductors assumes no responsibility or liability for the
* use of the software, conveys no license or title under any patent,
* copyright, or mask work right to the product. NXP Semiconductors
* reserves the right to make changes in the software without
* notification. NXP Semiconductors also make no representation or
* warranty that such application will be suitable for the specified
* use without further datasendcountering or modification.
* Permission to use, copy, modify, and distribute this software and its
* documentation is hereby granted, under NXP Semiconductors'
* relevant copyright in the software, without fee, provided that it
* is used in conjunction with NXP Semiconductors microcontrollers.  This
* copyright, permission, and disclaimer notice must appear in all copies of
* this code.
**********************************************************************/

#ifndef __SGPIO_PWM_H_
#define __SGPIO_PWM_H_

void SGPIOPWMValue(uint8_t value, uint8_t channel);
void SGPIOPWMchaninit(uint32_t freq, uint8_t channel);
void SGPIOPWMinit(void);
uint8_t SGPIOPWMstatus(void);


#endif

/*****************************************************************************
**                            End Of File
******************************************************************************/
