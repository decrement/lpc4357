
/**********************************************************************
* $Id$		sgpio_pwm.c	2012-07-20
*//**
* @file		sgpio_pwm.c
* @brief	The library for using SGPIO to generate PWM signals
* @version	1.0
* @date		20. July. 2012
* @author	NXP MCU SW Application Team
*
* Copyright(C) 2012, NXP Semiconductor
* All rights reserved.
*
***********************************************************************
* Software that is described herein is for illustrative purposes only
* which provides customers with programming information regarding the
* products. This software is supplied "AS IS" without any warranties.
* NXP Semiconductors assumes no responsibility or liability for the
* use of the software, conveys no license or title under any patent,
* copyright, or mask work right to the product. NXP Semiconductors
* reserves the right to make changes in the software without
* notification. NXP Semiconductors also make no representation or
* warranty that such application will be suitable for the specified
* use without further datasendcountering or modification.
* Permission to use, copy, modify, and distribute this software and its
* documentation is hereby granted, under NXP Semiconductors'
* relevant copyright in the software, without fee, provided that it
* is used in conjunction with NXP Semiconductors microcontrollers.  This
* copyright, permission, and disclaimer notice must appear in all copies of
* this code.
**********************************************************************/

/* Includes ------------------------------------------------------------------- */
#include "board.h"                    /* lpc43xx definitions                */
#include "lpc_types.h"
#include "sgpio_pwm.h"
#include "lpc43xx_scu.h"
//#include "lpc43xx_cgu.h"

/* Variables ------------------------------------------------------------------*/
uint8_t chan = 0;															//Variable used to get settings from the SGPIOPWMValue to the interrupt handler
uint8_t val = 0;															//Variable used to get settings from the SGPIOPWMValue to the interrupt handler
uint8_t intcount = 0;													//Variable used to count how many times REG_SS was updated
uint32_t PWMValues[0x21] = {									//All the 33 PWM values used for PWM
	0x00000000, 0x00000001, 0x00000003, 0x00000007, 0x0000000F,
	0x0000001F, 0x0000003F, 0x0000007F, 0x000000FF, 0x000001FF,
	0x000003FF, 0x000007FF, 0x00000FFF, 0x00001FFF, 0x00003FFF,
	0x00007FFF, 0x0000FFFF, 0x0001FFFF, 0x0003FFFF, 0x0007FFFF,
	0x000FFFFF, 0x001FFFFF, 0x003FFFFF, 0x007FFFFF, 0x00FFFFFF,
	0x01FFFFFF, 0x03FFFFFF, 0x07FFFFFF, 0x0FFFFFFF, 0x1FFFFFFF,
	0x3FFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF};

/*********************************************************************//**
 * @brief 		Change PWM value of an SGPIO pin
 * @param[in]	value			Should be between 0 and 33, 0 is off, 33 is on, 16 is ~50% PWM
 * @param[in]	channel		SGPIO channel that should be changed
 * @return		None
 **********************************************************************/
void SGPIOPWMValue(uint8_t value, uint8_t channel)
{
	chan = channel;												//Put the values into globab variables so the interrupt handler can reach it
	val = value;													
	LPC_SGPIO->CTR_STATUS_1 =  0xFFFF;		//Clear the interrupts
	LPC_SGPIO->SET_EN_1 = 0x8000;					//interrupt when slice 15 switches data register
}

/*********************************************************************//**
 * @brief 		Initialize an SGPIO channel for PWM use
 * @param[in]	freq			The frequency that PWM should work on
 * @param[in]	channel		SGPIO channel that should be initialised
 * @return		None
 **********************************************************************/
void SGPIOPWMchaninit(uint32_t freq, uint8_t channel)
{
	//Calculate the correct divider setting
	uint32_t calceddivider = 0;													//Variable to calculate the PWM frequency
	calceddivider = SystemCoreClock;
	calceddivider = (calceddivider / freq) -1;
	calceddivider = calceddivider / 0x20;
	
	//Setup an SGPIO channel for PWM
	LPC_SGPIO->OUT_MUX_CFG[channel] = 0x00000000;				//one bit mode
  LPC_SGPIO->SGPIO_MUX_CFG[channel] = (0x1 << 11);		//internal clock, concave on
  LPC_SGPIO->SLICE_MUX_CFG[channel] = 0x0;						//Interrupt on match on
	LPC_SGPIO->PRESET[channel] = calceddivider;					//clock speed = ((supplied clock / 2) / (preset + 1))
  LPC_SGPIO->COUNT[channel] = 0x0;
  LPC_SGPIO->POS[channel] = 0x1F00;										//after 32 clocks, switch data registers,
	LPC_SGPIO->REG[channel] = 0xFFFFF;									//Data in normal register, not important for now
  LPC_SGPIO->REG_SS[channel] = 0xFFFFF;								//Data in shadow register, used for match
}

/*********************************************************************//**
 * @brief 		Initialize the SGPIO channels for PWM use
 * @return		None
 **********************************************************************/
void SGPIOPWMinit(void)
{
  LPC_SGPIO->CLR_EN_0	= 0xffff;									// disable interrupting on clock
  LPC_SGPIO->CLR_EN_1	= 0xffff;									// disable interrupting on clock
	LPC_SGPIO->CLR_EN_2	= 0xffff;									// disable interrupting on clock
	LPC_SGPIO->CLR_EN_3	= 0xffff;									// disable interrupting on clock

	LPC_SGPIO->GPIO_OENREG = 0xFFFF;							//All SGPIO's as output
	LPC_SGPIO->CTRL_ENABLED = 0xFFFF;							//Enable all SGPIO clocks.
  NVIC_EnableIRQ(31);        			//Enable SGPIO Interrupt
}

void SGPIO_IRQHandler (void)                    //Handles all SGPIO interrupts
{
	uint32_t interruptvar = 0;										//Variable used to store the interrupt value, what slice had an interrupt.
	interruptvar = LPC_SGPIO->STATUS_1;	
	
	if (interruptvar & (1 << chan))								//Check if the correct channel got an data swap interrupt
	{
		LPC_SGPIO->REG_SS[chan] = PWMValues[val];		//Place the new value in the REG_SS rgister
		intcount += 1;															//Add one to the counter
		LPC_SGPIO->CTR_STATUS_1 =  0xFFFF;					//Clear the interrupts
		if(intcount >= 2)														//When the counter is 2 the REG_SS register has been updated twice
		{
			LPC_SGPIO->SET_EN_1 = 0x0000;							//Stop the interrupts
			LPC_SGPIO->CLR_EN_1	= 0xffff;							//And clear the interrupt
			intcount = 0;															//Clear the counter
		}
	}
}

uint8_t SGPIOPWMstatus(void)
{
	return intcount;
}

/**
 * @}
 */
