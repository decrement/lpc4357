/*----------------------------------------------------------------------------
 *      RL-ARM - TCPnet
 *----------------------------------------------------------------------------
 *      Name:    EMAC_LPC17xx.h
 *      Purpose: NXP LPC1768 EMAC Ethernet Controller Driver definitions
 *      Rev.:    V4.05
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2009 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#ifndef __EMAC_LPC17XX_H
#define __EMAC_LPC17XX_H

typedef void (* ETH_READ_READY_CB)(void);
extern uint8_t ethInit (ETH_READ_READY_CB ethReadReadyCb, uint8_t * mac);

// 应用层通过EMAC接口发送数据
// data:数据区，数据必须存放在四字节对齐的空间
// len: 数据长度（字节）
extern uint32_t ethWrite(uint8_t * data, uint32_t len);

// 应用层通过UART接口接收数据
// buf:缓存区，数据必须存放在四字节对齐的空间
// len: 缓存区长度（字节）
extern uint32_t ethRead( uint8_t * buffer, uint32_t len, uint32_t timeout);

// 以太网是否在线（是否插入网线连接了网络）
extern uint32_t ethIsLink(void);

// 打开以太网中断
extern void ethIrqEnable(void);

// 关闭以太网中断
extern void ethIrqDisable(void);


#endif

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
