
/*! @file SCTColor.c 
* @brief Color mode SCT driver module.
*
* This file contains definitions and routines to use SCT to recognize camera timing
* and fetch video data. For demonstrating purpose, it also convert the data format and
* show the captured video on TFT panel.
* to ease debugging, a camera register bank view is also simulated, you can write new values
* in watch window, and the view engine then write values to camera register via I2C bus
*/


#define _CAMYACBA21_C_
#include "board.h"
#include "camera.h"

#if (COLOR_CHOOSE == COLOR)

#include "CamYACB21.h"

//! instance of the main control block structure
DS_CamCB IRAM1_SECT s_ccb;
//! Instance of camera register bank view
US_YACBRegs s_regs;

// extern const unsigned int cg_regTabEleCnt;
// extern const unsigned short cg_regTab[];

/*! @brief Program SCT registers to run the state machine, control block variable is also initialized
*
* use SCT for camera timing
*
* 3 states
*
* 		want VSYNC (wantV, initial state)
*		want HSYNC (wantH)
*		in a HSYNC (inH)
*
* 4 events
*
*		VSYNC fall
*		HSYNC rise
*		HSYNC fall
*		pxclk rise
*
* Transitions
*
* 		wantV:
*			in: 	SysInit, +VSync@wantH, +VSyn@inH
*			out: 	-VSync->wantH
*		wantH:
*			in:		-VSync@wantV ; -HSync@inH, -VSync@wantV
*			out: 	+HSync->inH, +VSync->wantV
*		inH:
*			in:		+HSync@wantH
*			self:	+PxClk
*			out:	-HSync->wantH, +VSync->wantV
*/
#define LCD_FONT_TYPE     16

void YACBprm_Tim(void)
{
	LPC_SCT_T *pSCT = LPC_SCT;               //  设置指针，映射地址
	LPC_GIMA_Type *pGIMA = LPC_GIMA2;            
	LPC_CCU1_T1 *pCCU1 = LPC_CCU11;        
	LPC_CREG_T *pCREG = LPC_CREG;          

	//! configure DMAMUX to select SCT as the request source    通道联通
	pCREG->DMAMUX &= ~(3UL<<(YACB_SCTDMAPERIPNUM<<1));   // 屏蔽设置DMA用于外围设备9
	pCREG->DMAMUX |=   2UL<<(YACB_SCTDMAPERIPNUM<<1);    // 设置DMA用于外围设备9且为SCT匹配输出1
	
	pCCU1->CLK_M4_DMA_CFG = 1UL<<0 | 1UL<<1;          //  使能分支时钟并且使能AHB总线为自动方式
	s_ccb.pDMA = LPC_GPDMA2;                           //! pointer to DMA peripheral common registers
	s_ccb.pDMA->CONFIG = 1UL<<0 | 0UL<<1 ;            //  大小端模式配置
// 	s_ccb.pDMA->CONFIG = 1UL<<0 | 0UL<<1 | 0UL<<2;            //  大小端模式配置
	s_ccb.pDMA->SYNC = 0;
	
	//! select DMA channel 0 and configure it               //  映射DMA通道0地址
	s_ccb.pCHN = (LPC_GPDMACHN_Type*)(&s_ccb.pDMA->C0SRCADDR); 	// use DMACHN.00 to xfer camera data to buffer
	s_ccb.pCHN->CONFIG =                             //  配置DMA0通道寄存器
	//	!En		| SrcPerip=SCTDMA1			| DstPrepNCr| M>P		| IRQDis 
		0UL<<0 	| YACB_SCTDMAPERIPNUM<<1 	| 0UL<<6 	| 2UL<<11 	| 0UL<<15;     //  
    //DMA0通道禁能	| 源外围设备为SCT匹配1	| 目的外围设备为存储器忽略该位，直接指向存储器 | 流控制为外围设备到存储器 |掩码相关通道的终结计数中断
		
	//! other channel initialization is performed when VSYnc rise is found


	//! mux pins to CTIN for SCT     //  见数据手册第15章        
	{
		__IO uint32_t *pCTINReg = &pGIMA->CTIN_0_IN;      //  映射SCT寄存器地址   /SCT CTIN_0 capture input multiplexer (GIMA output 16) */
		pCTINReg[CTIN_VSYNC] = GIMACFG_DIRECT | 0;        //  设置SCT.IN2 为 vsync的捕获输入   且信号输入不反转
		pCTINReg[CTIN_HSYNC] = GIMACFG_DIRECT | 0;        //  设置SCT.IN1 为 hsync的捕获输入    且信号输入不反转
		pCTINReg[CTIN_PXCLK] = GIMACFG_DIRECT | 0;        //  设置SCT.IN0 为  pclk的捕获输入    且信号输入不反转
	}

	//! enable SCT clock and reset SCT
	pCCU1->CLK_M4_SCT_CFG = 1UL<<0 | 1UL<<1;     // 使能SCT时钟和SCT的AHB时钟
	/* I don't know why, but reset SCT may cause unexpected debug abort
	pRGU->RESET_CTRL1 = 1UL<<5;
	while (0 == (pRGU->RESET_ACTIVE_STATUS1 & (1UL<<5)))
		;
	*/
	pSCT->CONFIG =                 //  配置SCT的配置寄存器
	//	unify	| busClk  | InSync:All8 
		0UL<<1 	| 0UL<< 1 | 0x00UL<<9;
	//SCT作为两个16位计数器（这个是当SCT坐计数器输出时用到）作为捕获功能时用不到|总线时钟为 SCT和预分频器计时 | 输入选择SCT0（本例程是输出的所以这位也没啥影响）
	
	                                     // 事件状态掩码寄存器，如果相应的位为0那么在符合条件应该发生事
	// config events in states           件响应的情况下也不会发生，改为1则在符合条件的情况下才会发生事件响应
	
	//  这些下边事件的掩码的设置会影响黑白和彩色时DMA送给CPU的数据量即一行信号过来时是否乘2有关，彩色在上升沿和下降沿都采数据需乘2
	pSCT->EVENT[ctev_vsyncRise].STATE = 1UL<<ctst_wantV |  1UL<<ctst_wantH | 0UL<<ctst_inH;   //  设置各个事件发生的掩码
	pSCT->EVENT[ctev_vsyncFall].STATE = 1UL<<ctst_wantV | 1UL<<ctst_wantH | 0UL<<ctst_inH;
	pSCT->EVENT[ctev_hsyncRise].STATE = 0UL<<ctst_wantV | 1UL<<ctst_wantH | 0UL<<ctst_inH;
	pSCT->EVENT[ctev_hsyncFall].STATE = 0UL<<ctst_wantV | 0UL<<ctst_wantH | 1UL<<ctst_inH | 1UL<<ctst_inHSkipUV; //
// 	pSCT->EVENT[ctev_pxclkRise].STATE = 0UL<<ctst_wantV | 0UL<<ctst_wantH | 1UL<<ctst_inH;
//     pSCT->EVENT[ctev_pxclkRiseE].STATE = 0UL<<ctst_wantV | 0UL<<ctst_wantH | 1UL<<ctst_inH;
	pSCT->EVENT[ctev_pxclkRiseO].STATE = 0UL<<ctst_wantV | 0UL<<ctst_wantH | 1UL<<ctst_inH;

    pSCT->EVENT[ctev_vsyncRise].CTRL = 
    //	SelIn  | 			   | Rise	 | IO only | StateSet| NewState
        0UL<<5 | CTIN_VSYNC<<6 | 1UL<<10 | 2UL<<12 | 1UL<<14 | ctst_wantV<<15;
    //  设置有事件CTIN_VSYNC响应时SCT为输出|设置响应CTIN_VSYNC事件|上升沿响应		

	pSCT->EVENT[ctev_vsyncFall].CTRL = 
	//	SelIn  | 			   | Fall	 | IO only | StateSet| NewState
		0UL<<5 | CTIN_VSYNC<<6 | 2UL<<10 | 2UL<<12 | 1UL<<14 | ctst_wantH<<15;

	pSCT->EVENT[ctev_hsyncRise].CTRL =         
	//	SelIn  | 			   | Rise	 | IO only | StateSet| NewState
		0UL<<5 | CTIN_HSYNC<<6 | 1UL<<10 | 2UL<<12 | 1UL<<14 | ctst_inH<<15;            //  ctst_inHSkipUV<<15

	pSCT->EVENT[ctev_hsyncFall].CTRL = 
	//	SelIn  | 			   | Fall	 | IO only | StateSet| NewState
		0UL<<5 | CTIN_HSYNC<<6 | 2UL<<10 | 2UL<<12 | 1UL<<14 | ctst_wantH<<15;
	
	pSCT->EVENT[ctev_pxclkRiseO].CTRL = 
	//	SelIn  | 			   | Rise	 | IO only | StateSet| NewState
		0UL<<5 | CTIN_PXCLK<<6 | 1UL<<10 | 2UL<<12 | 1UL<<14 | ctst_inH<<15;		


	pSCT->EVFLAG = 0xFFFF;	// clear evt flags
	pSCT->EVEN = 1UL<<ctev_vsyncRise | 1UL<<ctev_hsyncFall ;
	// SCT标志使能寄存器  1使能相应的事件中断 0屏蔽相应的事件中断

	
	pSCT->DMA1REQUEST = 1UL<<ctev_pxclkRiseO;       //  这个比较重要  如果ctev_pxclkRiseO这个事件发生将设置SCT的DMA1请求
	
	// start SCT
	pSCT->CTRL_U &=                //  允许事件发生使能
	//	 !Halt	
		~(1UL<<2);
	NVIC_SetPriority(SCT_IRQn, 1);    //  设置SCT中断优先级
	NVIC_EnableIRQ(SCT_IRQn);         //  使能SCT中断
// 	NVIC_EnableIRQ(DMA_IRQn);
}




#endif

