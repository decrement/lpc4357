#include <raw_api.h>

#ifndef __LCD_TOUCH_H
#define __LCD_TOUCH_H


typedef struct{
	int x[5]; 	   //0-4 点的x 逻辑值
	int xfb[5];	   //0-4 点的x 物理值
	int y[5]; 	   //0-4 点的y 逻辑值
	int yfb[5];	   //0-4 点的y 物理值
	int a[7];
}CALIBRATION;


// 对触摸屏进行初始化
// xSize, ySize：屏幕的尺寸（单位为相素点）
// sensitity：灵敏度，共10级，范围（0--9），0的灵敏度最低
//            触摸屏的灵敏度设置越高，则精度越低。
extern void touchInit(unsigned short xSize, unsigned short ySize,  unsigned short sensitity);


// 触摸屏检测到按下操作，此函数由使用者编写，触摸驱动中调用。
extern void touchEventPressOn(void);

// 得到触摸屏按点的物理值
// 成功返回1  失败返回0
extern unsigned long touchGetPhyValue(unsigned short * x, unsigned short  * y);

// 将触摸屏的物理值转化为对应的象素点
extern void touchGetPixelByPhy(unsigned short xPhy, unsigned short yPhy, 
                               unsigned short * xPixel, unsigned short * yPixel);

// 对触摸屏幕进行五点校正
extern int touchCheck(CALIBRATION cal);


RAW_S32 touch_x_y_get(RAW_S32 *x, RAW_S32 *y);
void touch_x_y_raw_get(RAW_S32 *x, RAW_S32 *y);
RAW_S32 touch_phy_to_pixsel(RAW_S32 x_phy, RAW_S32 y_phy, RAW_S32 *x_pixsel, RAW_S32 *y_pixsel);


int pen_is_down(void);
void touch_init(RAW_S32 x_size, RAW_S32 y_size);


#endif


