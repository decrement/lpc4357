/*----------------------------------------------------------------------------
 *      RL-ARM - TCPnet
 *----------------------------------------------------------------------------
 *      Name:    EMAC_LPC17xx.c
 *      Purpose: Driver for NXP LPC1768 EMAC Ethernet Controller
 *      Rev.:    V4.05
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2009 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include <board.h>
#include "lpc43xx_emac.h"
#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h"
#include  <raw_api.h>

#define MYMAC_1         0x10            /* our ethernet (MAC) address        */
#define MYMAC_2         0x1F            /* (MUST be unique in LAN!)          */
#define MYMAC_3         0xE0
#define MYMAC_4         0x12
#define MYMAC_5         0x1E
#define MYMAC_6         0x0F


#define ETH_FRAG_SIZE		1536		
#define NUM_RX_DESC			10
#define NUM_TX_DESC			10
	

/*  Descriptors Fields bits       */
#define OWN_BIT				(1U<<31)	/*  Own bit in RDES0 & TDES0              */
#define RX_END_RING			(1<<15)		/*  Receive End of Ring bit in RDES1      */
#define RX_NXTDESC_FLAG		(1<<14)		/*  Second Address Chained bit in RDES1   */
#define TX_LAST_SEGM		(1<<29)		/*  Last Segment bit in TDES0             */
#define TX_FIRST_SEGM		(1<<28)		/*  First Segment bit in TDES0            */
#define TX_END_RING			(1<<21)		/*  Transmit End of Ring bit in TDES0     */
#define TX_NXTDESC_FLAG		(1<<20)		/*  Second Address Chained bit in TDES0   */


/* EMAC Control and Status bits   */
#define MAC_RX_ENABLE	 (1<<2)			/*  Receiver Enable in MAC_CONFIG reg      */
#define MAC_TX_ENABLE	 (1<<3)			/*  Transmitter Enable in MAC_CONFIG reg   */
#define MAC_PADCRC_STRIP (1<<7)			/*  Automatic Pad-CRC Stripping in MAC_CONFIG reg   */
#define MAC_DUPMODE		 (1<<11)		/*  Duplex Mode in  MAC_CONFIG reg         */
#define MAC_100MPS		 (1<<14)		/*  Speed is 100Mbps in MAC_CONFIG reg     */
#define MAC_PROMISCUOUS  (1U<<0)		/*  Promiscuous Mode bit in MAC_FRAME_FILTER reg    */
#define MAC_DIS_BROAD    (1U<<5)		/*  Disable Broadcast Frames bit in	MAC_FRAME_FILTER reg    */
#define MAC_RECEIVEALL   (1U<<31)       /*  Receive All bit in MAC_FRAME_FILTER reg    */
#define DMA_SOFT_RESET	  0x01          /*  Software Reset bit in DMA_BUS_MODE reg */
#define DMA_SS_RECEIVE   (1<<1)         /*  Start/Stop Receive bit in DMA_OP_MODE reg  */
#define DMA_SS_TRANSMIT  (1<<13)        /*  Start/Stop Transmission bit in DMA_OP_MODE reg  */
#define DMA_INT_TRANSMIT (1<<0)         /*  Transmit Interrupt Enable bit in DMA_INT_EN reg */
#define DMA_INT_OVERFLOW (1<<4)         /*  Overflow Interrupt Enable bit in DMA_INT_EN reg */ 
#define DMA_INT_UNDERFLW (1<<5)         /*  Underflow Interrupt Enable bit in DMA_INT_EN reg */
#define DMA_INT_RECEIVE  (1<<6)         /*  Receive Interrupt Enable bit in DMA_INT_EN reg */
#define DMA_INT_ABN_SUM  (1<<15)        /*  Abnormal Interrupt Summary Enable bit in DMA_INT_EN reg */
#define DMA_INT_NOR_SUM  (1<<16)        /*  Normal Interrupt Summary Enable bit in DMA_INT_EN reg */

/* MII Management Command Register */
#define GMII_READ           (0<<1)		/* GMII Read PHY                     */
#define GMII_WRITE          (1<<1)      /* GMII Write PHY                    */
#define GMII_BUSY           0x00000001  /* GMII is Busy / Start Read/Write   */
#define MII_WR_TOUT         0x00050000  /* MII Write timeout count           */
#define MII_RD_TOUT         0x00050000  /* MII Read timeout count            */

/* MII Management Address Register */
#define MADR_PHY_ADR        0x00001F00  /* PHY Address Mask                  */

/* DP83848C PHY Registers */
#define PHY_REG_BMCR        0x00        /* Basic Mode Control Register       */
#define PHY_REG_BMSR        0x01        /* Basic Mode Status Register        */
#define PHY_REG_IDR1        0x02        /* PHY Identifier 1                  */
#define PHY_REG_IDR2        0x03        /* PHY Identifier 2                  */
#define PHY_REG_ANAR        0x04        /* Auto-Negotiation Advertisement    */
#define PHY_REG_ANLPAR      0x05        /* Auto-Neg. Link Partner Abitily    */
#define PHY_REG_ANER        0x06        /* Auto-Neg. Expansion Register      */
#define PHY_REG_ANNPTR      0x07        /* Auto-Neg. Next Page TX            */

/* PHY Extended Registers */
#define PHY_REG_STS         0x10        /* Status Register                   */
#define PHY_REG_MICR        0x11        /* MII Interrupt Control Register    */
#define PHY_REG_MISR        0x12        /* MII Interrupt Status Register     */
#define PHY_REG_FCSCR       0x14        /* False Carrier Sense Counter       */
#define PHY_REG_RECR        0x15        /* Receive Error Counter             */
#define PHY_REG_PCSR        0x16        /* PCS Sublayer Config. and Status   */
#define PHY_REG_RBR         0x17        /* RMII and Bypass Register          */
#define PHY_REG_LEDCR       0x18        /* LED Direct Control Register       */
#define PHY_REG_PHYCR       0x19        /* PHY Control Register              */
#define PHY_REG_10BTSCR     0x1A        /* 10Base-T Status/Control Register  */
#define PHY_REG_CDCTRL1     0x1B        /* CD Test Control and BIST Extens.  */
#define PHY_REG_EDCR        0x1D        /* Energy Detect Control Register    */
#define PHY_REG_PSCS        0x1F

/* PHY Control and Status bits  */
#define PHY_FULLD_100M      0x2100      /* Full Duplex 100Mbit               */
#define PHY_HALFD_100M      0x2000      /* Half Duplex 100Mbit               */
#define PHY_FULLD_10M       0x0100      /* Full Duplex 10Mbit                */
#define PHY_HALFD_10M       0x0000      /* Half Duplex 10MBit                */
#define PHY_AUTO_NEG        0x1000      /* Select Auto Negotiation           */	  
#define PHY_AUTO_NEG_RES    0x0200  
#define PHY_AUTO_NEG_DONE   0x0020		/* AutoNegotiation Complete in BMSR PHY reg  */
#define PHY_BMCR_RESET		0x8000		/* Reset bit at BMCR PHY reg         */
#define LINK_VALID_STS		0x0001		/* Link Valid Status at REG_STS PHY reg	 */
#define FULL_DUP_STS		0x0004		/* Full Duplex Status at REG_STS PHY reg */
#define SPEED_10M_STS		0x0002		/* 10Mbps Status at REG_STS PHY reg */

#define LAN8720_DEF_ADR    0x01        /* Default PHY device address        */
#define LAN8720_ID          0x7c0f0     /* PHY Identifier (without Rev. info */

/*  Misc    */
#define ETHERNET_RST		22			/* 	Reset Output for EMAC at RGU     */
#define RMII_SELECT			0x04		/*  Select RMII in EMACCFG           */


ETH_READ_READY_CB g_ethReadReadyCb = 0;



unsigned long FLAG_ETHINIT = 0;


/*--------------------------- write_PHY -------------------------------------*/

static void write_PHY (unsigned int PhyReg, unsigned short Value) 
{   
    unsigned int tout;

    /* Write a data 'Value' to PHY register 'PhyReg'. */
    while(LPC_ETHERNET->MAC_MII_ADDR & GMII_BUSY);			// Check GMII busy bit
    LPC_ETHERNET->MAC_MII_ADDR = (LAN8720_DEF_ADR<<11) | (PhyReg<<6) | GMII_WRITE;
    LPC_ETHERNET->MAC_MII_DATA = Value;
    LPC_ETHERNET->MAC_MII_ADDR |= GMII_BUSY;				// Start PHY Write Cycle

    /* Wait utill operation completed */
    for (tout = 0; tout < MII_WR_TOUT; tout++) 
	{
        if ((LPC_ETHERNET->MAC_MII_ADDR & GMII_BUSY) == 0) 
		{
            break;
        }
    }
}

static unsigned short read_PHY (unsigned int PhyReg) 
{   
    unsigned int tout, val;

    /* Read a PHY register 'PhyReg'. */
    while(LPC_ETHERNET->MAC_MII_ADDR & GMII_BUSY);			// Check GMII busy bit
    LPC_ETHERNET->MAC_MII_ADDR = (LAN8720_DEF_ADR<<11) | (PhyReg<<6) | GMII_READ;
    LPC_ETHERNET->MAC_MII_ADDR |= GMII_BUSY;				// Start PHY Read Cycle

    /* Wait until operation completed */
    for (tout = 0; tout < MII_RD_TOUT; tout++) 
	{
        if ((LPC_ETHERNET->MAC_MII_ADDR & GMII_BUSY) == 0) 
		{
            break;
        }
    }

    val = LPC_ETHERNET->MAC_MII_DATA;

    return (val);
}


void phy_reset(void)
{
	uint32_t regv,tout,id1,id2;
	RAW_U16 temp = 0;

	temp = read_PHY (PHY_REG_BMCR);

	printf("temp is %x\r\n", temp);

	/* Put the DP83848C in reset mode */
	write_PHY (PHY_REG_BMCR, PHY_BMCR_RESET | temp);

	/* Wait for hardware reset to end. */
	for (tout = 0; tout < 0x400000; tout++) 
	{
		regv = read_PHY (PHY_REG_BMCR);

	if (!(regv & 0x8800)) 
	{
		/* Reset complete, device not Power Down. */
		break;
	}
	}

	if (tout == 0x400000)
	{
		RAW_ASSERT(0);
	}

	/*should be 0x3000?*/
	printf("basic control register is %x\r\n", read_PHY (PHY_REG_BMCR));
		
	/* Check if this is a lan8720 PHY. */
	id1 = read_PHY (PHY_REG_IDR1);
	id2 = read_PHY (PHY_REG_IDR2);


	if (((id1 << 16) | (id2 & 0xFFF0)) != LAN8720_ID ) 
	{
		RAW_ASSERT(0);

	}

}


STATIC INLINE void reset(LPC_ENET_T *pENET)
{
    Chip_RGU_TriggerReset(RGU_ETHERNET_RST);
	while (Chip_RGU_InReset(RGU_ETHERNET_RST)) 
    {}

	/* Reset ethernet peripheral */
	Chip_ENET_Reset(pENET);
	raw_sleep(10);
	while(LPC_ETHERNET->DMA_BUS_MODE & DMA_SOFT_RESET);	     // Wait for software reset completion
	raw_sleep(10);
}


#define ENET_NUM_TX_DESC 4
#define ENET_NUM_RX_DESC 4

static ENET_ENHTXDESC_T TXDescs[ENET_NUM_TX_DESC];
static ENET_ENHRXDESC_T RXDescs[ENET_NUM_RX_DESC];


static uint8_t RXBuffer[ENET_NUM_RX_DESC][EMAC_ETH_MAX_FLEN];
static int32_t rxFill, rxGet, rxAvail, rxNumDescs;
static int32_t txFill, txGet, txUsed, txNumDescs;

/* Local index and check function */
static __INLINE int32_t incIndex(int32_t index, int32_t max)
{
	index++;
	if (index >= max) {
		index = 0;
	}

	return index;
}


/* Initialize MAC descriptors for simple packet receive/transmit */
void InitDescriptors(
	ENET_ENHTXDESC_T *pTXDescs, int32_t numTXDescs,
	ENET_ENHRXDESC_T *pRXDescs, int32_t numRXDescs)
{
	int i;

	/* Setup the descriptor list to a default state */
	raw_memset(pTXDescs, 0, numTXDescs * sizeof(*pTXDescs));
	raw_memset(pTXDescs, 0, numRXDescs * sizeof(*pRXDescs));
	rxFill = rxGet = 0;
	rxAvail = rxNumDescs = numRXDescs;
	txNumDescs = numTXDescs;
	txUsed = txGet = txFill = 0;

	/* Build linked list, CPU is owner of descriptors */
	for (i = 0; i < numTXDescs; i++) {
		pTXDescs[i].CTRLSTAT = 0;
		pTXDescs[i].B2ADD = (uint32_t) &pTXDescs[i + 1];
	}
	pTXDescs[numTXDescs - 1].B2ADD = (uint32_t) &pTXDescs[0];
	for (i = 0; i < numRXDescs; i++) {
		pRXDescs[i].STATUS = 0;
		pRXDescs[i].B2ADD = (uint32_t) &pRXDescs[i + 1];
		pRXDescs[i].CTRL = RDES_ENH_RCH;
	}
	pRXDescs[numRXDescs - 1].B2ADD = (uint32_t) &pRXDescs[0];
	pRXDescs[numRXDescs - 1].CTRL |= RDES_ENH_RER;

	/* Setup list pointers in Ethernet controller */
	Chip_ENET_InitDescriptors(LPC_ETHERNET, pTXDescs, pRXDescs);
}




/* Attach a buffer to a descriptor and queue it for reception */
void ENET_RXQueue(void *buffer, int32_t bytes)
{
	if (rxAvail > 0) {
		/* Queue the next descriptor and start polling */
		RXDescs[rxFill].B1ADD = (uint32_t) buffer;
		RXDescs[rxFill].CTRL = RDES_ENH_BS1(bytes) | RDES_ENH_RCH;
		if (rxFill == (rxNumDescs - 1)) {
			RXDescs[rxFill].CTRL |= RDES_ENH_RER;
		}
		RXDescs[rxFill].STATUS = RDES_OWN;
		rxAvail--;
		rxFill = incIndex(rxFill, rxNumDescs);

		/* Start polling */
		Chip_ENET_RXStart(LPC_ETHERNET);
	}
}

/* Returns a pointer to a filled ethernet buffer or NULL if none are available */
void *ENET_RXGet(int32_t *bytes)
{
	void *buffer;

	/* This doesn't check status of the received packet */
	if ((rxAvail < rxNumDescs) && (!(RXDescs[rxGet].STATUS & RDES_OWN))) {
		/* CPU owns descriptor, so a packet was received */
		buffer = (void *) RXDescs[rxGet].B1ADD;
		*bytes = (int32_t) ((RXDescs[rxGet].STATUS  >> 16) & 0x03FFF); 
		rxGet = incIndex(rxGet, rxNumDescs);
		rxAvail++;
	}
	else {
		/* Nothing received */
		*bytes = 0;
		buffer = NULL;
	}

	return buffer;
}

/* Attaches a buffer to a transmit descriptor and queues it for transmit */
void ENET_TXQueue(void *buffer, int32_t bytes)
{
	if (txUsed < txNumDescs) {
		/* Queue the next descriptor and start polling */
		TXDescs[txFill].B1ADD = (uint32_t) buffer;
		TXDescs[txFill].BSIZE = TDES_ENH_BS1(bytes);
		TXDescs[txFill].CTRLSTAT = TDES_ENH_FS | TDES_ENH_LS | TDES_ENH_TCH;
		if (txFill == (txNumDescs - 1)) {
			TXDescs[txFill].CTRLSTAT |= TDES_ENH_TER;
		}
		TXDescs[txFill].CTRLSTAT |= TDES_OWN;
		txUsed++;
		txFill = incIndex(txFill, txNumDescs);

		/* Start polling */
		Chip_ENET_TXStart(LPC_ETHERNET);
	}
}


int enet_tx_busy(void)
{

	if (txUsed < txNumDescs) {

		return 0;
	}

	return 1;
	
}
/* Returns a pointer to a buffer that has been transmitted */
void *ENET_TXBuffClaim(void)
{
	void *buffer;

	/* Is packet done sending? */
	if ((txUsed > 0) && (!(TXDescs[txGet].CTRLSTAT & TDES_OWN))) {
		/* CPU owns descriptor, so the packet completed transmit */
		buffer = (void *) TXDescs[txGet].B1ADD;
		txGet = incIndex(txGet, txNumDescs);
		txUsed--;
		//Board_LED_Set(2, false);
	}
	else {
		buffer = NULL;
	}

	return buffer;
}







RAW_SEMAPHORE eth_sema;

uint8_t ethInit(ETH_READ_READY_CB ethReadReadyCb, uint8_t * mac)
{
  	int i;
	
	Chip_Clock_EnableOpts(CLK_MX_ETHERNET, true, true, 1);

	/*	 Select RMII interface	   */
	LPC_CREG->CREG6 |= RMII_SELECT;

	
	/* Ethernet pins configuration		*/
	scu_pinmux(0x0 ,0 ,  (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC2); 	// ENET_RXD1:  P0_0 -> FUNC2	
    scu_pinmux(0x0 ,1 ,  (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC6); 	// ENET_TX_EN: P0_1 -> FUNC6
    scu_pinmux(0x1 ,15 , (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC3); 	// ENET_RXD0:  P1_15 -> FUNC3
    scu_pinmux(0x1 ,16 , (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC7); 	// ENET_CRS:   P1_16 -> FUNC7
    scu_pinmux(0x1 ,17 , (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC3); 	// ENET_MDIO:  P1_17 -> FUNC3 
    scu_pinmux(0x1 ,18 , (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC3); 	// ENET_TXD0:  P1_18 -> FUNC3
    scu_pinmux(0x1 ,19 , (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC0); 	// ENET_REF:   P1_19 -> FUNC0 (default)
    scu_pinmux(0x1 ,20 , (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC3); 	// ENET_TXD1:  P1_20 -> FUNC3
    scu_pinmux(0x7 ,7 ,  (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC6); 	// ENET_MDC:   P7_7 -> FUNC6   :changed;
	scu_pinmux(0xC ,9 ,  (MD_EHS | MD_PLN | MD_EZI | MD_ZI), FUNC3); 	// ENET_RX_ER: PC_9 -> FUNC3

	scu_pinmux(0xF ,2 ,  MD_PUP, FUNC4);	  // PF_2 -> GPIO7[17]
	GPIO_SetDir(7, 1 << 17, 1);				  // Output
	GPIO_ClearValue(7, 1 << 17);
	GPIO_SetValue(7, 1 << 17);

	reset(LPC_ETHERNET);

	LPC_ETHERNET->DMA_INT_EN = 0;

   // LPC_ETHERNET->MAC_FRAME_FILTER = MAC_PROMISCUOUS | MAC_RECEIVEALL;
   //LPC_ETHERNET->MAC_CONFIG    |= MAC_DUPMODE | MAC_100MPS;

	/* Configure Filter           */  
    LPC_ETHERNET->MAC_FRAME_FILTER = MAC_FF_PR | MAC_FF_RA;

	/* Initial MAC configuration for checksum offload, full duplex,
	 100Mbps, disable receive own in half duplex, inter-frame gap
	 of 64-bits */
	LPC_ETHERNET->MAC_CONFIG = MAC_CFG_BL(0) | MAC_CFG_IPC | MAC_CFG_DM |
						MAC_CFG_DO | MAC_CFG_FES | MAC_CFG_PS | MAC_CFG_IFG(0);
			
	
    /* Set the Ethernet MAC Address registers */
    LPC_ETHERNET->MAC_ADDR0_HIGH = (mac[5] << 8) | mac[4];
    LPC_ETHERNET->MAC_ADDR0_LOW =	(mac[3] << 24) | (mac[2] << 16) | (mac[1] << 8) | mac[0];

	/* Setup DMA to flush receive FIFOs at 32 bytes, service TX FIFOs at
	   64 bytes */
	LPC_ETHERNET->DMA_OP_MODE |= DMA_OM_RTC(1) | DMA_OM_TTC(0);
	
	/* Clear all MAC interrupts */
	LPC_ETHERNET->DMA_STAT = DMA_ST_ALL;
	
    /* Enable interrupts    */
    //LPC_ETHERNET->DMA_INT_EN =  DMA_INT_NOR_SUM | DMA_INT_RECEIVE | DMA_INT_TRANSMIT;	
   // LPC_ETHERNET->DMA_INT_EN =  DMA_INT_RECEIVE | DMA_INT_NOR_SUM ;	
    //NVIC_EnableIRQ(ETHERNET_IRQn); 
    
	LPC_ETHERNET->DMA_INT_EN = 0;
	
	
	phy_reset();
	
	/* Setup descriptors */
	InitDescriptors(TXDescs, ENET_NUM_TX_DESC, RXDescs, ENET_NUM_RX_DESC);


	/* Attach a buffer to a RX descriptor and queue it for receive */
	i = 0;
	while (i < ENET_NUM_RX_DESC) {
		ENET_RXQueue(RXBuffer[i], EMAC_ETH_MAX_FLEN);
		i++;
	}
  
	/* Enable RX/TX after descriptors are setup */
	
	Chip_ENET_TXEnable(LPC_ETHERNET);
	Chip_ENET_RXEnable(LPC_ETHERNET);

	return 1;
}




void ETH_IRQHandler (void) 
{
   

}

