
/*! @file SCTYUV.c 
* @brief grayscale mode SCT driver module (use YUV format and extract only Y).
*
* This file contains definitions and routines to use SCT to recognize camera timing
* and fetch video data. For demonstrating purpose, it also convert the data format and
* show the captured video on TFT panel.
* to ease debugging, a camera register bank view is also simulated, you can write new values
* in watch window, and the view engine then write values to camera register via I2C bus
*/



#define _CAMYACBA21_C_
#include "board.h"
#include "camera.h"

#if (COLOR_CHOOSE == GRAY)

#include "CamYACB21.h"

//! instance of the main control block structure
DS_CamCB IRAM1_SECT s_ccb;
//! Instance of camera register bank view
US_YACBRegs s_regs;

// extern const unsigned int cg_regTabEleCnt;
// extern const unsigned short cg_regTab[];

/*! @brief Program SCT registers to run the state machine, control block variable is also initialized
*
* use SCT for camera timing
*
* 4 states
*
* 		want VSYNC (wantV, initial state)
*		want HSYNC (wantH)
*		in a HSYNC, sending U or V byte (inH_UV)
*		in a HSYNC, sending Y byte      (inH_Y)
*
* 5 events
*
*		VSYNC fall
*		HSYNC rise
*		HSYNC fall
*		even number pxclk rise
*		odd  number pxclk rise
*
* Transitions
*
* 		wantV:
*			in: 	SysInit, +VSync@(wantH|inH_UV|inH_Y)
*			out: 	-VSync->wantH
*		wantH:
*			in:		-VSync@wantV, -HSync@(inH_UV|inH_Y)
*			out: 	+HSync->inH_UV
*		inH_UV:
*			in:		+HSync@wantH, +PxClk@inH_Y
*			out:	+VSync->wantV, -HSync->wantH, +PxClk->inH_Y
*		inH_Y:
*			in:		+PxClk@inH_UV
*			out:	+VSync->wantV, -HSync->wantH, +PxClk->inH_UV
*/

void YACBprm_Tim(void)
{
	LPC_SCT_T  *pSCT  = LPC_SCT;
	LPC_GIMA_Type *pGIMA = LPC_GIMA2;
	LPC_CCU1_T1 *pCCU1 = LPC_CCU11;
	LPC_CREG_T *pCREG = LPC_CREG;

	//! configure DMAMUX to select SCT as the request source
	pCREG->DMAMUX &= ~(3UL<<(YACB_SCTDMAPERIPNUM<<1)); 
	pCREG->DMAMUX |= 2UL<<(YACB_SCTDMAPERIPNUM<<1);
	
	pCCU1->CLK_M4_DMA_CFG = 1UL<<0 | 1UL<<1;	
	s_ccb.pDMA = LPC_GPDMA2;
	s_ccb.pDMA->CONFIG = 1UL<<0 | 0UL<<1;
	s_ccb.pDMA->SYNC = 0;

	//! select DMA channel 0 and configure it
	s_ccb.pCHN = (LPC_GPDMACHN_Type*)(&s_ccb.pDMA->C0SRCADDR);	// use DMACHN.00 to xfer camera data to buffer
	s_ccb.pCHN->CONFIG = 
	//	!En		| SrcPerip=SCTDMA1			| DstPrepNCr| M>P		| IRQDis 
		0UL<<0 	| YACB_SCTDMAPERIPNUM<<1 	| 0UL<<6 	| 2UL<<11 	| 0UL<<15;
	//! other channel initialization is performed when VSYnc rise is found

	//! mux pins to CTIN for SCT
	{
		__IO uint32_t *pCTINReg = &pGIMA->CTIN_0_IN;
		pCTINReg[CTIN_VSYNC] = GIMACFG_DIRECT | 0;
		pCTINReg[CTIN_HSYNC] = GIMACFG_DIRECT | 0;
		pCTINReg[CTIN_PXCLK] = GIMACFG_DIRECT | 0;
	}
	
	//! enable SCT clock and reset SCT
	pCCU1->CLK_M4_SCT_CFG = 1UL<<0 | 1UL<<1;
	/* I don't know why, but reset SCT may cause unexpected debug abort
	pRGU->RESET_CTRL1 = 1UL<<5;
	while (0 == (pRGU->RESET_ACTIVE_STATUS1 & (1UL<<5)))
		;
	*/
	pSCT->CONFIG = 
	//	unify	| busClk  | InSync:All8
		1UL<<0 	| 0UL<< 1 | 0x00UL<<9;

	pSCT->CTRL_U = 
	//	Stop   | Halt	| ClrCnt | !BiDir | Prsc
		1UL<<1 | 1UL<<2 | 1UL<<3 | 0UL<<4 | 0x01<<5;
	
	pSCT->LIMIT_L = 0xFFFF;	// EvtClrCnt
	// event doesn't affect cnt
	pSCT->HALT_L = 0, pSCT->STOP_L = 0, pSCT->START_L = 0;
	pSCT->STATE_L = ctst_wantV;
	//! config events in states
	pSCT->EVENT[ctev_vsyncRise].STATE  = 1UL<<ctst_wantV | 1UL<<ctst_wantH | 0UL<<ctst_inH;
	pSCT->EVENT[ctev_vsyncFall].STATE  = 1UL<<ctst_wantV | 1UL<<ctst_wantH | 0UL<<ctst_inH;
	pSCT->EVENT[ctev_hsyncRise].STATE  = 0UL<<ctst_wantV | 1UL<<ctst_wantH | 0UL<<ctst_inH;
	pSCT->EVENT[ctev_hsyncFall].STATE  = 0UL<<ctst_wantV | 0UL<<ctst_wantH | 1UL<<ctst_inH | 1UL<<ctst_inHSkipUV; 
	pSCT->EVENT[ctev_pxclkRiseE].STATE = 0UL<<ctst_wantV | 0UL<<ctst_wantH | 1UL<<ctst_inHSkipUV;
	pSCT->EVENT[ctev_pxclkRiseO].STATE = 0UL<<ctst_wantV | 0UL<<ctst_wantH | 1UL<<ctst_inH;

    pSCT->EVENT[ctev_vsyncRise].CTRL = 
    //	SelIn  | 			   | Rise	 | IO only | StateSet| NewState
        0UL<<5 | CTIN_VSYNC<<6 | 1UL<<10 | 2UL<<12 | 1UL<<14 | ctst_wantV<<15;

	pSCT->EVENT[ctev_vsyncFall].CTRL = 
	//	SelIn  | 			   | Fall	 | IO only | StateSet| NewState
		0UL<<5 | CTIN_VSYNC<<6 | 2UL<<10 | 2UL<<12 | 1UL<<14 | ctst_wantH<<15;

	pSCT->EVENT[ctev_hsyncRise].CTRL = 
	//	SelIn  | 			   | Rise	 | IO only | StateSet| NewState
		0UL<<5 | CTIN_HSYNC<<6 | 1UL<<10 | 2UL<<12 | 1UL<<14 | ctst_inHSkipUV<<15;

	pSCT->EVENT[ctev_hsyncFall].CTRL = 
	//	SelIn  | 			   | Fall	 | IO only | StateSet| NewState
		0UL<<5 | CTIN_HSYNC<<6 | 2UL<<10 | 2UL<<12 | 1UL<<14 | ctst_wantH<<15;

	pSCT->EVENT[ctev_pxclkRiseE].CTRL = 
	//	SelIn  | 			   | Rise	 | IO only | StateSet| NewState
		0UL<<5 | CTIN_PXCLK<<6 | 1UL<<10 | 2UL<<12 | 1UL<<14 | ctst_inH<<15;

	pSCT->EVENT[ctev_pxclkRiseO].CTRL = 
	//	SelIn  | 			   | Rise	 | IO only | StateSet| NewState
		0UL<<5 | CTIN_PXCLK<<6 | 1UL<<10 | 2UL<<12 | 1UL<<14 | ctst_inHSkipUV<<15;


	pSCT->EVFLAG = 0xFFFF;	// clear evt flags
	pSCT->EVEN = 1UL<<ctev_vsyncRise | 1UL<<ctev_hsyncFall;
	pSCT->DMA1REQUEST = 1UL<<ctev_pxclkRiseO;
	// start SCT
	pSCT->CTRL_U &= 
	//	 !Halt	
		~(1UL<<2);
	NVIC_SetPriority(SCT_IRQn, 1);
	NVIC_EnableIRQ(SCT_IRQn);
}

#endif
