#include "board.h"
#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h" 

/* bit definitions for register SSPCR0. */
#define SSPCR0_DSS      0
#define SSPCR0_CPOL     6
#define SSPCR0_CPHA     7
#define SSPCR0_SCR      8
/* bit definitions for register SSPCR1. */
#define SSPCR1_SSE      1
/* bit definitions for register SSPSR. */
#define SSPSR_TFE       0
#define SSPSR_TNF       1
#define SSPSR_RNE       2
#define SSPSR_RFF       3
#define SSPSR_BSY       4

void spi1_master_init(void)
{
	volatile uint32_t dummy;

	dummy = dummy; // avoid warning

    /* Set up clock and power for SSP1 module */
	LPC_CGU2->BASE_SSP1_CLK = (0x09 << 24) | (1 << 11);

	/* Configure SSP1 pins*/
   	scu_pinmux(0xF,4,MD_PLN_FAST,FUNC0);	// Pf.4 connected to SCL/SCLK	func2=SSP1 SCK0
 	scu_pinmux(0xF,6,MD_PLN_FAST,FUNC2);	// Pf.6 connected to SO			func2=SSP1 MISO0
 	scu_pinmux(0xF,7,MD_PLN_FAST,FUNC2);	// Pf.7 connected to nSI		func2=SSP1 MOSI0

//    LPC_SSP1->CR0  = 0x0107;                    /* 8Bit, CPOL=0, CPHA=0         */
//	LPC_SSP1->CR1  = 0x0002;                    /* SSP1 enable, master          */
//
//	LPC_SSP1->CPSR = 200;    // 60000000 / 200 = 300000HZ
	LPC_SSP1->CR0  = 0x0007 | (2 << 8);         /* 8Bit, CPOL=0, CPHA=0         */
	LPC_SSP1->CR1  = 0x0002;                    /* SSP1 enable, master          */

	LPC_SSP1->CPSR = 100;    // 180M / 3 / 100 = 600KHZ

	/* wait for busy gone */
	while( LPC_SSP1->SR & ( 1 << SSPSR_BSY ) );

	/* drain SPI RX FIFO */
	while( LPC_SSP1->SR & ( 1 << SSPSR_RNE ) )
	{
		dummy = LPC_SSP1->DR;
	}
}

/* Send one byte then recv one byte of response. */
uint8_t SSP1_SendRecvByte (uint8_t byte_s)
{
	uint8_t byte_r;

	while (LPC_SSP1->SR & (1 << SSPSR_BSY) /*BSY*/); 	/* Wait for transfer to finish */
	LPC_SSP1->DR = byte_s;
	while (LPC_SSP1->SR & (1 << SSPSR_BSY) /*BSY*/); 	/* Wait for transfer to finish */
	while( !( LPC_SSP1->SR & ( 1 << SSPSR_RNE ) ) );	/* Wait untill the Rx FIFO is not empty */
	byte_r = LPC_SSP1->DR;
	return byte_r;                      /* Return received value */
}

