#include "LPC43xx.h"                    /* LPC18xx definitions                */
#include "lpc43xx_scu.h"


/*********************************************************************//**
 * @brief 		Configure pin function
 * @param[in]	port	Port number, should be: 0..15
 * @param[in]	pin		Pin number, should be: 0..31
 * @param[in]	mode	Pin mode, should be:
 * 					- MD_PUP	:Pull-up enabled
 * 					- MD_BUK	:Plain input
 * 					- MD_PLN	:Repeater mode
 * 					- MD_PDN	:Pull-down enabled
 * @param[in]	func 	Function mode, should be:
 * 					- FUNC0		:Function 0
 * 					- FUNC1		:Function 1
 * 					- FUNC2		:Function 2
 * 					- FUNC3		:Function 3
 * @return		None
 **********************************************************************/
void scu_pinmux(uint8_t port, uint8_t pin, uint8_t mode, uint8_t func)
{
 volatile  uint32_t *scu_base=(uint32_t *)(LPC_SCU_BASE);
  scu_base[(0x80*port+0x4*pin)/4]=mode+func;
} /* scu_pinmux */


