#include "board.h"
#include "lpc43xx_rtc.h"



/**********************************************************************
* Time Counter Group and Alarm register group
**********************************************************************/
/** SEC register mask */
#define RTC_SEC_MASK			(0x0000003F)
/** MIN register mask */
#define RTC_MIN_MASK			(0x0000003F)
/** HOUR register mask */
#define RTC_HOUR_MASK			(0x0000001F)
/** DOM register mask */
#define RTC_DOM_MASK			(0x0000001F)
/** DOW register mask */
#define RTC_DOW_MASK			(0x00000007)
/** DOY register mask */
#define RTC_DOY_MASK			(0x000001FF)
/** MONTH register mask */
#define RTC_MONTH_MASK			(0x0000000F)
/** YEAR register mask */
#define RTC_YEAR_MASK			(0x00000FFF)

#define RTC_SECOND_MAX		59 /*!< Maximum value of second */
#define RTC_MINUTE_MAX		59 /*!< Maximum value of minute*/
#define RTC_HOUR_MAX		23 /*!< Maximum value of hour*/
#define RTC_MONTH_MIN		1 /*!< Minimum value of month*/
#define RTC_MONTH_MAX		12 /*!< Maximum value of month*/
#define RTC_DAYOFMONTH_MIN 	1 /*!< Minimum value of day of month*/
#define RTC_DAYOFMONTH_MAX 	31 /*!< Maximum value of day of month*/
#define RTC_DAYOFWEEK_MAX	6 /*!< Maximum value of day of week*/
#define RTC_DAYOFYEAR_MIN	1 /*!< Minimum value of day of year*/
#define RTC_DAYOFYEAR_MAX	366 /*!< Maximum value of day of year*/
#define RTC_YEAR_MAX		4095 /*!< Maximum value of year*/


/********************************************************************//**
 * @brief		Initializes the RTC peripheral.
 * @param[in]	RTCx	RTC peripheral selected, should be LPC_RTC2
 * @return 		None
 *********************************************************************/
void RTC_Init(void)
{
	// Configure clock to RTC
	LPC_CREG->CREG0 &= ~((1 << 3) | (1 << 2));			// Reset 32Khz oscillator
	LPC_CREG->CREG0 |= (1 << 0);						// Enable 1 kHz on osc32k and release reset
	
	/* Reset RTC clock*/
	LPC_RTC2->CCR = RTC_CCR_CTCRST;
	while(LPC_RTC2->CCR != RTC_CCR_CTCRST);

	/* Clear counter increment and alarm interrupt */
	LPC_RTC2->ILR = (1 << 0) | (1 << 1);
	while(LPC_RTC2->ILR != 0);

	// Clear all register to be default

	LPC_RTC2->CIIR = 0x00;

	LPC_RTC2->AMR = 0xFF;

	LPC_RTC2->CALIBRATION = 0x00;
  
	LPC_RTC2->CCR |= RTC_CCR_CLKEN;
}

void RTC_getTime(unsigned char * hour, unsigned char * min, unsigned char * sec)
{
    *hour = (LPC_RTC2->HRS & RTC_HOUR_MASK);
    *min = (LPC_RTC2->MIN & RTC_MIN_MASK);
    *sec = (LPC_RTC2->SEC & RTC_SEC_MASK);     
}

void RTC_getDate(unsigned short * year, unsigned char * mon, unsigned char * day)
{
    *year = (LPC_RTC2->YEAR & RTC_YEAR_MASK);
    *mon = (LPC_RTC2->MONTH & RTC_MONTH_MASK);
    *day = (LPC_RTC2->DOM & RTC_DOM_MASK);
}

void RTC_setTime(unsigned char hour, unsigned char min, unsigned char sec)
{
    if ((hour > RTC_HOUR_MAX)  || (min > RTC_MINUTE_MAX) || (sec > RTC_SECOND_MAX))
    {
        return;
    }

    LPC_RTC2->HRS = (hour & RTC_HOUR_MASK);
    LPC_RTC2->MIN = (min & RTC_MIN_MASK);
    LPC_RTC2->SEC = (sec & RTC_SEC_MASK);     
}

void RTC_setDate(unsigned short year, unsigned char mon, unsigned char day)
{
    if ( (year > RTC_YEAR_MAX) 
      || (mon < RTC_MONTH_MIN) 
      || (mon > RTC_MONTH_MAX) 
      || (day < RTC_DAYOFMONTH_MIN)
      || (day > RTC_DAYOFMONTH_MAX))
	{
	    return;
	}

    LPC_RTC2->YEAR = (year & RTC_YEAR_MASK);
    LPC_RTC2->MONTH = (mon & RTC_MONTH_MASK);
    LPC_RTC2->DOM = (day & RTC_DOM_MASK);
      
    return;
}
