#include <string.h>
#include "board.h"
#include "lpc43xx_i2s0.h"
#include "lpc43xx_i2c0.h"
#include "lpc43xx_scu.h"
#include <raw_api.h>


#define CODEC_I2S_BUS   LPC_I2S0
#define CODEC_INPUT_DEVICE UDA1380_LINE_IN



/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
/*********************************************************************//**
 * @brief		I2S IRQ Handler, call to send data to transmit buffer
 * @param[in]	None
 * @return 		None
 **********************************************************************/


#define TX_BUFFER_SIZE   64
uint32_t g_txLeftBuffer[TX_BUFFER_SIZE];
uint32_t g_txRightBuffer[TX_BUFFER_SIZE];

#define RX_BUFFER_SIZE   64
uint32_t g_rxLeftBuffer[RX_BUFFER_SIZE];
uint32_t g_rxRightBuffer[TX_BUFFER_SIZE];

uint32_t g_txBufferPtrl = 0;
uint32_t g_rxBufferPtrl = 0;


static T_I2S_CONFIG grxConfig, gtxConfig;


void I2S0_IRQHandler(void)
{
	uint32_t state, i;

	state = 1;
	
    while (state)
	{
		switch (gtxConfig.wordWidth)
		{
	    case SOUND_SAMPLE_8BITS :
		     if(gtxConfig.channel == SOUND_CHNL_STERO)						  //   立体声
			 {     
                 for (i = 0; i < 4; i++)
                 {
		             LPC_I2S0->TXFIFO = (g_txLeftBuffer[g_txBufferPtrl + i*2]
				                  |(g_txRightBuffer[g_txBufferPtrl + i*2]<<8)
								  |(g_txLeftBuffer[g_txBufferPtrl+ i*2 +1]<<16)
								  |(g_txRightBuffer[g_txBufferPtrl+ i*2 + 1]<<24));
                 }
				 g_txBufferPtrl +=8;
			 }
			 else														 //   单声道
			 {
                 for (i = 0; i < 4; i++)
                 {
		             LPC_I2S0->TXFIFO = (g_txLeftBuffer[g_txBufferPtrl + i*4]
				                  |(g_txLeftBuffer[g_txBufferPtrl+ i*4 + 1]<<8)
								  |(g_txLeftBuffer[g_txBufferPtrl+ i*4 + 2]<<16)
								  |(g_txLeftBuffer[g_txBufferPtrl+ i*4 + 3]<<24));
                 }
				 g_txBufferPtrl +=16;				 
			 }
			 break;              
		case SOUND_SAMPLE_16BITS :
		     if(gtxConfig.channel == SOUND_CHNL_STERO)						//   立体声
			 { 
                 for (i = 0; i < 4; i++)
                 {
                     LPC_I2S0->TXFIFO = g_txLeftBuffer[g_txBufferPtrl + i] |(g_txRightBuffer[g_txBufferPtrl + i]<<16);
					 //LPC_I2S0->TXFIFO = g_txLeftBuffer[g_txBufferPtrl + i];
			     }
				 g_txBufferPtrl +=4;
			 }
			 else														//   单声道
			 {
                 for (i = 0; i < 4; i++)
                 {
		             LPC_I2S0->TXFIFO = (g_txLeftBuffer[g_txBufferPtrl + i*2]|(g_txLeftBuffer[g_txBufferPtrl + i*2 + 1]<<16));
                 }
				 g_txBufferPtrl +=8;				 
			 }
			 break;
		case SOUND_SAMPLE_32BITS :
		     if(gtxConfig.channel == SOUND_CHNL_STERO)				 //   立体声
			 {
				LPC_I2S0->TXFIFO = (g_txLeftBuffer[g_txBufferPtrl]);
				LPC_I2S0->TXFIFO = (g_txRightBuffer[g_txBufferPtrl]);
				
				LPC_I2S0->TXFIFO = (g_txLeftBuffer[g_txBufferPtrl + 1]);
				LPC_I2S0->TXFIFO = (g_txRightBuffer[g_txBufferPtrl + 1]);
				
				g_txBufferPtrl+=2;
			 }
			 else												  //   单声道
			 {
                 for (i = 0; i < 4; i++)
                 {
    			 	 LPC_I2S0->TXFIFO = (g_txLeftBuffer[g_txBufferPtrl + i]);
			     }
				 g_txBufferPtrl +=4;
			 }				 
			 break;
		default :
		     // 不可能进入的分支
	         break;
		}
		

		if (g_txBufferPtrl >= TX_BUFFER_SIZE)
		{
			if (gtxConfig.pFunc)
			{
				gtxConfig.pFunc((unsigned long *)g_txLeftBuffer, (unsigned long *)g_txRightBuffer, TX_BUFFER_SIZE);
			}

			g_txBufferPtrl = 0;
		}

		state = 0;
		
    }   
}




static void I2S_ClearFifo (uint8_t TRMode)
{
	uint32_t i;

	if (TRMode == I2S_TX_MODE)
	{
		for (i = 0; i < TX_BUFFER_SIZE; i++)
		{
		    g_txLeftBuffer[i] = 0;
		    g_txRightBuffer[i] = 0;
		}

		g_txBufferPtrl = 0;
	} else
	{
		for (i = 0; i < RX_BUFFER_SIZE; i++)
		{
		    g_rxLeftBuffer[i] = 0;
		    g_rxRightBuffer[i] = 0;
		}

		g_rxBufferPtrl = 0;
	}
}

void I2S_Start(uint8_t TRMode)
{
	//Clear STOP,RESET and MUTE bit
	if (TRMode == I2S_TX_MODE) //Transmit mode
	{
		LPC_I2S0->DAO &= ~(1 << 4); //	I2S_DAI_RESET
	
		LPC_I2S0->DAO &= ~(1 << 3);	 //	I2S_DAI_STOP
		
		LPC_I2S0->DAO &= ~(1 << 15);	 //	I2S_DAI_MUTE
		LPC_I2S0->IRQ |= (1 << 1);
	}
	else
	{
		LPC_I2S0->DAI &= ~(1 << 4);  // I2S_DAI_RESET
		LPC_I2S0->DAI &= ~(1 << 3);	  // I2S_DAI_STOP
		LPC_I2S0->IRQ |= (1 << 0);
	}

	I2S_ClearFifo(TRMode);
}

void I2S_Stop(uint8_t TRMode) 
{
	if (TRMode == I2S_TX_MODE) //Transmit mode
	{
		LPC_I2S0->IRQ &= ~(1 << 1);
		LPC_I2S0->DAO &= ~(1 << 15);	    // I2S_DAO_MUTE
		LPC_I2S0->DAO |= (1 << 3);	   // I2S_DAO_STOP
		LPC_I2S0->DAO |= (1 << 4);		//I2S_DAO_RESET
	} else //Receive mode
	{
		LPC_I2S0->IRQ &= ~(1 << 0);
		LPC_I2S0->DAI |= (1 << 3);   // I2S_DAI_STOP
		LPC_I2S0->DAI |= (1 << 4);	 //	I2S_DAI_RESET
	}
}



void I2S_init (T_I2S_CONFIG *rxConfig, T_I2S_CONFIG *txConfig)
{
	
	uint8_t wordwidth;

	/*SET I2S_CLOCK to pll1 192M or 204M or whatever*/
	LPC_CGU2->BASE_APB1_CLK = (1UL << 11) | (0x09 << 24);

	/*for our new board enable the following*/
	#if 1
	/* Initialize I2S peripheral ------------------------------------*/
	scu_pinmux(3,0, MD_PLN_FAST, 2);   //I2STX_CLK  OK 
	scu_pinmux(9,1, MD_PLN_FAST, 4);   //I2STX_WS   p9_1 4.13
	scu_pinmux(9,2, MD_PLN_FAST, 4);   //I2STX_SDA  p9_2 4.14


	scu_pinmux(6,0, MD_PLN_FAST, 4);   //I2SRX_CLK
	scu_pinmux(6,1, MD_PLN_FAST, 3);   //I2SRX_WS
	scu_pinmux(6,2, MD_PLN_FAST, 3);   //I2SRX_SDA
	#endif

	Board_Audio_Init(CODEC_I2S_BUS, CODEC_INPUT_DEVICE);

	
	LPC_I2S0->DAO = LPC_I2S0->DAI = 0x00;

	/*softResest I2S*/
    LPC_I2S0->DAO |= (1<<4) | (1<<3);
	LPC_I2S0->DAI |= (1<<4) | (1<<3);

	if (txConfig) 
	{
	
		memcpy(&gtxConfig, (char *)txConfig, sizeof(T_I2S_CONFIG));


		/* Audio Config*/
		wordwidth = (gtxConfig.wordWidth + 1) * 8;
		LPC_I2S0->DAO = (wordwidth - 1) << 6      |
						(0 << 5)				  |	  // master mode
						(1 << 4)                  |	  // reset enable
						(1 << 3)  			      |		  // stop enable
						(gtxConfig.channel << 2)  |	  // mono
						(gtxConfig.wordWidth);
	

        
        
		/* Clock Mode Config*/
		LPC_I2S0->TXMODE &= ~0x0F;

		/*1411200 / 44100 = 32*/
		/*1411200 vs our 1417322 for 44100 hz, data 16, stero sample, this is for frequency 180M*/
  		//LPC_I2S0->TXRATE = 127 | (2<<8);	

		/* for 22050 hz, data 16, stero sample, this is for frequency 180M*/
		//LPC_I2S0->TXRATE = 254 | (2<<8);	

		#if (AUDIO_FREQ == 44100)
		
		/* for 44100 hz, data 16, stero sample, this is for frequency 204M*/
		LPC_I2S0->TXRATE = 72 | (1 << 8);
		
		#elif(AUDIO_FREQ == 22050)
		
		/* for 22050 hz, data 16, stero sample, this is for frequency 204M*/
		LPC_I2S0->TXRATE = 144 | (1 << 8);	
		
		#endif

		LPC_I2S0->TXBITRATE = 0;

		
		I2S_Stop(I2S_TX_MODE);
	
		/* TX FIFO depth is 4 */
		LPC_I2S0->IRQ &= ~((1 << 1) | (0x0F << 16));
		LPC_I2S0->IRQ |= 0x02 | (4 << 16);
	
		I2S_Start(I2S_TX_MODE);
	}	

	if (rxConfig)
	{
		memcpy(&grxConfig, (char *)rxConfig, sizeof(T_I2S_CONFIG));
		wordwidth = (grxConfig.wordWidth + 1) * 8;
		LPC_I2S0->DAI = (wordwidth - 1) << 6      |
						(0 << 5)				  |	  // master mode
						(1 << 4)                  |	  // reset enable
						(1 << 3)  			      |		  // stop enable
						(gtxConfig.channel << 2)  |	  // mono
						(gtxConfig.wordWidth);
	
		LPC_I2S0->RXMODE &= ~0x0F;
	
		LPC_I2S0->RXRATE = 9  | (2<<8);
		LPC_I2S0->RXBITRATE = 20000000 /(grxConfig.smpFreq * 2  * wordwidth) - 1;
	
		I2S_Stop(I2S_RX_MODE);
	
		LPC_I2S0->IRQ = 0;
		
		LPC_I2S0->IRQ |= (1 << 1);
		
		LPC_I2S0->IRQ &= ~(1 << 16);
		LPC_I2S0->IRQ &= ~(1 << 17);
		LPC_I2S0->IRQ &= ~(1 << 18);
		LPC_I2S0->IRQ &= ~(1 << 19);
	
		I2S_Start(I2S_RX_MODE);
	}

	NVIC_EnableIRQ(I2S0_IRQn);
}

/**
 * @}
 */
