#include <raw_api.h>
#include <board.h>
#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h"

/*****************************************************************************
 * Defines and typedefs
 ****************************************************************************/
volatile RAW_U8 *K9F_NF_CLE = (volatile  RAW_U8 *)0x1E100000;
volatile RAW_U8 *K9F_NF_ALE = ((volatile RAW_U8 *)0x1E080000);
volatile RAW_U8 *K9F_NF_DATA =((volatile RAW_U8 *)0x1E000000);


#define K9Fxx08_BLOCKNUM                0x800	        /* NAND FLASH共有2048个BLOCK */
#define K9Fxx08_PAGE_PER_BLOCK	        0x40            /* 一个BLOCK上共有多少页 */

#define K9Fxx08_PAGE_SIZE               2112            /* 页大小（包含64个管理字节） */
#define K9Fxx08_PAGE_USR_SIZE	        2048            /* 用户能存储数据的页空间大小 */

#define K9Fxx_READ_1            	0x00                
#define K9Fxx_READ_2            	0x30                
#define K9Fxx_READ_ID           	0x90                
#define K9Fxx_RESET             	0xFF                
#define K9Fxx_BLOCK_PROGRAM_1   	0x80                
#define K9Fxx_BLOCK_PROGRAM_2   	0x10                
#define K9Fxx_BLOCK_ERASE_1     	0x60                
#define K9Fxx_BLOCK_ERASE_2     	0xD0                
#define K9Fxx_READ_STATUS       	0x70                
#define K9Fxx_BUSY              	(1 << 6)            
#define K9Fxx_OK                	(1 << 0)    

#define FIO_BASE_ADDR		0x20098000
#define FIO0DIR        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x00))
#define FIO0MASK       (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x10))
#define FIO0PIN        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x14))
#define FIO0SET        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x18))
#define FIO0CLR        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x1C))

#define FIO3DIR        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x60))
#define FIO3MASK       (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x70))
#define FIO3PIN        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x74))
#define FIO3SET        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x78))
#define FIO3CLR        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x7C))

#define FIO_BASE_ADDR	0x20098000
#define FIO0DIR        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x00))
#define FIO0MASK       (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x10))
#define FIO0PIN        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x14))
#define FIO0SET        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x18))
#define FIO0CLR        (*(volatile unsigned long *)(FIO_BASE_ADDR + 0x1C))


#if 0
static void k9f_busy_wait(void)
{	 
    unsigned long i;

    for (i = 0x00800000; i; i--)
    {
        if (!(GPIO_ReadValue(2) & (1ul<< 0)))
        {
            break;
        }
    }

    if (i == 0)
    {
    	printf("timeout\r\n");
        return;
    }

    for (i = 0x00800000; i; i--)
    {
        if (GPIO_ReadValue(2) & (1ul<< 0))
        {
            break;
        }
    }

    if (i == 0)
    {
    	printf("timeout\r\n");
        return;
    }

    return;
       
}

#else 

static void k9f_busy_wait(void)
{
	RAW_U8 stat;
	
	*K9F_NF_CLE = 0x70;

	//判断状态值的第6位是否为1，即是否在忙，该语句的作用与NF_DETECT_RB();相同
	do{
		
		stat= *K9F_NF_DATA;
	}while(!(stat & (1 << 6)));

	*K9F_NF_CLE = 0x0;
}

#endif



void k9f_init(void)
{
	LPC_EMC_T2 *g_pEMC = LPC_EMC2;

	scu_pinmux(4,0,MD_PUP|MD_EZI,FUNC0);	//	p4_0
	GPIO_SetDir(2, (1ul<<0), 0);	

	g_pEMC->CONTROL = 0x00000001;	 // 使能EMC
	g_pEMC->CONFIG	= 0x00000000;	 // 使用小端模式

	g_pEMC->STATICCONFIG2	= 0x00000080;  // 8位总线
	g_pEMC->STATICWAITWEN2	= 0x00000001; 
	g_pEMC->STATICWAITOEN2	= 0x00000001; 
	g_pEMC->STATICWAITRD2	= 0x00000004;
	g_pEMC->STATICWAITPAG2	= 0x00000001;
	g_pEMC->STATICWAITWR2	= 0x00000001; 
	g_pEMC->STATICWAITTURN2 = 0x00000001; 


	/* 通过NAND FLASH命令使其复位 */
	*K9F_NF_CLE = K9Fxx_RESET;
	/*need some time to delay*/
	raw_sleep(1);
	k9f_busy_wait();

}

RAW_U32 k9f_id_read( void )
{
	RAW_U8 a, b, c, d, e;

	*K9F_NF_CLE = K9Fxx_READ_ID;
	*K9F_NF_ALE = 0;
	
	a = *K9F_NF_DATA;

	b = *K9F_NF_DATA;
	
	c = *K9F_NF_DATA;

	d = *K9F_NF_DATA;
	e = *K9F_NF_DATA;
	e = e;
	
  	return ((a << 24) | (b << 16) | (c << 8) | d);
  
}


static RAW_U32 k9f_state_read(RAW_U32 Cmd)
{
    RAW_U8 StatusData;

    *K9F_NF_CLE = K9Fxx_READ_STATUS;
    /* 等待直到bit6和bit6置1，则NAND状态为ready,bit7应该同时为1，表示无写保护 */
    /* 如果READY位未置1,则停留在此 */
    do
    {
        StatusData = *K9F_NF_DATA;
    }
    while(!(StatusData & 0x40));
    
    switch (Cmd)
    {
	    case K9Fxx_BLOCK_PROGRAM_1:
	    case K9Fxx_BLOCK_ERASE_1:
	        if (StatusData & 0x01) {
	            return 0;
	        }
			
	        else {
	            return 1;
	        }
			
	    default:
	        break;
    } 
	
    return 0;
}


RAW_U8 k9f_bad_block_check_product(RAW_U32 block)
{
    RAW_U32 page;

    page = block * K9Fxx08_PAGE_PER_BLOCK;

    // 先读第0页中第2048字节的值
    *K9F_NF_CLE = K9Fxx_READ_1;
    *K9F_NF_ALE = (RAW_U8)(K9Fxx08_PAGE_USR_SIZE & 0x00FF);		/* 页内 地址低位 */
    *K9F_NF_ALE = (RAW_U8)((K9Fxx08_PAGE_USR_SIZE & 0xFF00) >> 8);	/* 页内 地址高位 */
    *K9F_NF_ALE = (RAW_U8)(page & 0x00FF);	        /* 页 地址低位 */
    *K9F_NF_ALE = (RAW_U8)((page & 0xFF00) >> 8);	/* 页 地址高位 */
    *K9F_NF_CLE = K9Fxx_READ_2;
    
   	k9f_busy_wait();
  
    /*invalid block here*/
    if (*K9F_NF_DATA != 0xFF)
    {
        return 0;
    }

    page++;
    
    // 再读第1页中第2048字节的值
    *K9F_NF_CLE = K9Fxx_READ_1;
    *K9F_NF_ALE = (RAW_U8)(K9Fxx08_PAGE_USR_SIZE & 0x00FF);		/* 页内 地址低位 */
    *K9F_NF_ALE = (RAW_U8)((K9Fxx08_PAGE_USR_SIZE & 0xFF00) >> 8);	/* 页内 地址高位 */
    *K9F_NF_ALE = (RAW_U8)(page & 0x00FF);	        /* 页 地址低位 */
    *K9F_NF_ALE = (RAW_U8)((page & 0xFF00) >> 8);	/* 页 地址高位 */
    *K9F_NF_CLE = K9Fxx_READ_2;
    
	k9f_busy_wait();
 
    /*invalid block here*/
    if (*K9F_NF_DATA != 0xFF)
    {
        return 0;
    }
    
    return 1;
}



RAW_U8 k9f_block_erase(RAW_U32 block_number)
{

	RAW_U32  page;
	RAW_U32 res_erase;
	
    page = block_number * K9Fxx08_PAGE_PER_BLOCK;  
    
    // 先擦除块

    *K9F_NF_CLE = K9Fxx_BLOCK_ERASE_1;
    *K9F_NF_ALE = (RAW_U8)((page & 0x00FF));       /* row 地址低位 */
    *K9F_NF_ALE = (RAW_U8)((page & 0xFF00) >> 8);  /* row 地址高位 */
    *K9F_NF_CLE = K9Fxx_BLOCK_ERASE_2;
    
    k9f_busy_wait();

	res_erase = k9f_state_read(K9Fxx_BLOCK_ERASE_1);

	return res_erase;

}



RAW_U8 k9f_page_read(RAW_U32 page_number, RAW_U8 *nand_buffer)
{
	RAW_U32 i, len;

	*K9F_NF_CLE = K9Fxx_READ_1;
	*K9F_NF_ALE = 0;    /* column 地址低位 */
	*K9F_NF_ALE = 0;    /* column 地址高位 */
	*K9F_NF_ALE = (RAW_U8)(page_number & 0x00FF);        /* row 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((page_number & 0xFF00) >> 8); /* row 地址高位 */
	*K9F_NF_CLE = K9Fxx_READ_2;

	k9f_busy_wait();

	len = 0;
	for(i = 0; i < K9Fxx08_PAGE_USR_SIZE; i++ )		//读出一页
	{
		nand_buffer[len] = *K9F_NF_DATA;
		len++;
	}

	return 1;

}

RAW_U8 k9f_block_read(RAW_U32 block, RAW_U8 *buf)
{
    RAW_U32 i, page, len;
    
    len = 0;

    page = block * K9Fxx08_PAGE_PER_BLOCK;

    for (page = block * K9Fxx08_PAGE_PER_BLOCK; page < ((block + 1) * K9Fxx08_PAGE_PER_BLOCK); page++)
    {
        *K9F_NF_CLE = K9Fxx_READ_1;
        *K9F_NF_ALE = 0;    /* column 地址低位 */
        *K9F_NF_ALE = 0;    /* column 地址高位 */
        *K9F_NF_ALE = (RAW_U8)(page & 0x00FF);        /* row 地址低位 */
        *K9F_NF_ALE = (RAW_U8)((page & 0xFF00) >> 8); /* row 地址高位 */
        *K9F_NF_CLE = K9Fxx_READ_2;
        
        k9f_busy_wait();
        
        for(i = 0; i < K9Fxx08_PAGE_USR_SIZE; i++ )		//读出一页
        {
            buf[len] = *K9F_NF_DATA;
            len++;
        }
    }
    
    return 1;
}


RAW_U8 k9f_page_write(RAW_U32 page_number, RAW_U8 *pbuf)
{
	RAW_U32 len, i;
	RAW_U32 res_erase;

	len = 0;
	
	*K9F_NF_CLE = K9Fxx_BLOCK_PROGRAM_1;
	*K9F_NF_ALE = 0;	                   /* column 地址低位 */
	*K9F_NF_ALE = 0;                         /* column 地址高位 */
	*K9F_NF_ALE = (RAW_U8)((page_number & 0x00FF));       /* row 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((page_number & 0xFF00) >> 8);  /* row 地址高位 */

	for (i = 0; i < K9Fxx08_PAGE_USR_SIZE; i++) {

		*K9F_NF_DATA = pbuf[len];		
		len++;
	}

	*K9F_NF_CLE = K9Fxx_BLOCK_PROGRAM_2;

	k9f_busy_wait();

	res_erase = k9f_state_read(K9Fxx_BLOCK_PROGRAM_1);

	return res_erase;
   
	

}

RAW_U32 k9f_block_write(RAW_U32 block, RAW_U8 *data)
{
    RAW_U32 i;
    RAW_U32  page, len;

    page = block*K9Fxx08_PAGE_PER_BLOCK; 

    // 先擦除块

    *K9F_NF_CLE = K9Fxx_BLOCK_ERASE_1;
    *K9F_NF_ALE = (RAW_U8)((page & 0x00FF));       /* row 地址低位 */
    *K9F_NF_ALE = (RAW_U8)((page & 0xFF00) >> 8);  /* row 地址高位 */
    *K9F_NF_CLE = K9Fxx_BLOCK_ERASE_2;
    
    k9f_busy_wait();
  
    // 再写数据

    len = 0;

    for ( ;page < (block+1)*K9Fxx08_PAGE_PER_BLOCK; page++)
    {
        *K9F_NF_CLE = K9Fxx_BLOCK_PROGRAM_1;
        *K9F_NF_ALE = 0;	                   /* column 地址低位 */
        *K9F_NF_ALE = 0;                         /* column 地址高位 */
        *K9F_NF_ALE = (RAW_U8)((page & 0x00FF));       /* row 地址低位 */
        *K9F_NF_ALE = (RAW_U8)((page & 0xFF00) >> 8);  /* row 地址高位 */
        
        for (i = 0; i < K9Fxx08_PAGE_USR_SIZE; i++)
        {
            *K9F_NF_DATA = data[len];		//  填写数据
            len++;
        }
        
        *K9F_NF_CLE = K9Fxx_BLOCK_PROGRAM_2;
        
        k9f_busy_wait();
    }
    
    return k9f_state_read(K9Fxx_BLOCK_PROGRAM_1);
}


RAW_U8 k9f_random_read(RAW_U32 page_number, RAW_U32 addr)
{
	RAW_U8 nand_data;
	
	*K9F_NF_CLE = K9Fxx_READ_1;
	*K9F_NF_ALE = (RAW_U8)(addr & 0x00FF); 	/* 页内 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((addr & 0xFF00) >> 8);	/* 页内 地址高位 */
	*K9F_NF_ALE = (RAW_U8)(page_number & 0x00FF);			/* 页 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((page_number & 0xFF00) >> 8);	/* 页 地址高位 */
	*K9F_NF_CLE = K9Fxx_READ_2;

	k9f_busy_wait();

	nand_data = *K9F_NF_DATA;

	return nand_data;

}

RAW_U8 k9f_random_continue_read(RAW_U32 page_number,RAW_U32 addr, RAW_U8 *dat, RAW_U8 count)
{

	RAW_U32 i;
	
	*K9F_NF_CLE = K9Fxx_READ_1;
	*K9F_NF_ALE = (RAW_U8)(addr & 0x00FF); 	/* 页内 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((addr & 0xFF00) >> 8);	/* 页内 地址高位 */
	*K9F_NF_ALE = (RAW_U8)(page_number & 0x00FF);			/* 页 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((page_number & 0xFF00) >> 8);	/* 页 地址高位 */
	*K9F_NF_CLE = K9Fxx_READ_2;

	k9f_busy_wait();
	
	for (i = 0; i < count; i++) {
		
	 	dat[i] = *K9F_NF_DATA;

	}

	return 1;
	
}

RAW_U8 k9f_random_write(RAW_U32 page_number, RAW_U32 addr, RAW_U8 dat)
{

	*K9F_NF_CLE =  0x80;
	*K9F_NF_ALE = (RAW_U8)(addr & 0x00FF); 	/* 页内 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((addr & 0xFF00) >> 8);	/* 页内 地址高位 */
	*K9F_NF_ALE = (RAW_U8)(page_number & 0x00FF);			/* 页 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((page_number & 0xFF00) >> 8);	/* 页 地址高位 */
	
	*K9F_NF_CLE =0x85;
	*K9F_NF_ALE = (RAW_U8)(addr & 0x00FF); 	/* 页内 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((addr & 0xFF00) >> 8);	/* 页内 地址高位 */
	
	*K9F_NF_DATA = dat;

	*K9F_NF_CLE =0x10;
	
	k9f_busy_wait();

	return k9f_state_read(K9Fxx_BLOCK_PROGRAM_1);

}


RAW_U8 k9f_random_continue_write(RAW_U32 page_number, RAW_U32 addr, RAW_U8 *dat, RAW_U8 count)
{
	RAW_U32 i;
	
	*K9F_NF_CLE =  0x80;
	*K9F_NF_ALE = (RAW_U8)(addr & 0x00FF); 	/* 页内 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((addr & 0xFF00) >> 8);	/* 页内 地址高位 */
	*K9F_NF_ALE = (RAW_U8)(page_number & 0x00FF);			/* 页 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((page_number & 0xFF00) >> 8);	/* 页 地址高位 */
	
	*K9F_NF_CLE =0x85;
	*K9F_NF_ALE = (RAW_U8)(addr & 0x00FF); 	/* 页内 地址低位 */
	*K9F_NF_ALE = (RAW_U8)((addr & 0xFF00) >> 8);	/* 页内 地址高位 */

	for (i = 0; i < count; i++) {
		
	 	*K9F_NF_DATA = dat[i];
	}

	*K9F_NF_CLE =0x10;
	
	k9f_busy_wait();

	return k9f_state_read(K9Fxx_BLOCK_PROGRAM_1);

}


