
#include "board.h"
#include "key.h"

#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h"
#include <raw_api.h>

typedef struct __PIN_INFO
{
	uint32_t port;
	uint32_t pinMask;
}PIN_INFO;


PIN_INFO gkeyX[KEY_NUM] = {{6, 1ul << 2}, {6, 1ul << 24}, {6, 1ul << 25}, {6, 1ul << 27},{6, 1ul << 28}};

extern void DelayMs (uint32_t ms);
// 对lED使用的GPIO管脚进行初始化
void keyInit(void)
{
    uint8_t i;

	scu_pinmux(0x0C, 3, MD_PUP| MD_EZI, FUNC4);		              // PD_1   ---> keyport1
	scu_pinmux(0x0D, 10, MD_PUP| MD_EZI, FUNC4);		              // P4_0   ---> keyport2
	scu_pinmux(0x0D, 11, MD_PUP| MD_EZI, FUNC4);		          // PD_11  ---> keyport3
	scu_pinmux(0x0D, 13, MD_PUP| MD_EZI, FUNC4);		  		  // PD_13  ---> keyport4 
	scu_pinmux(0x0D, 14, MD_PUP| MD_EZI, FUNC4);		  // PD_14  ---> keyport5

	for (i = 0; i < KEY_NUM; i++)						    
	{
		GPIO_SetDir(gkeyX[i].port, gkeyX[i].pinMask, 0);
	}
}

// 对键盘进行扫描，获得按键值。返回的BIT0-15分别对应16个按键是否按下。1为按下。
uint32_t keyScan(void)
{
    uint32_t i, ret;

    ret = 0;

    for (i = 0; i < KEY_NUM; i++)						   /* Raw scan                         */
    {       
		if ((GPIO_ReadValue(gkeyX[i].port) & gkeyX[i].pinMask) == 0)
        {											   /* Get key state                    */						      
			raw_sleep(5);  /* Debounce                         */
			if ((GPIO_ReadValue(gkeyX[i].port) & gkeyX[i].pinMask) == 0)
			{										   /* Get key state again              */
                ret |= 1UL << i;	       /* Mark key state                   */
			}
        }
    }													   /* Restore pins state               */

    return ret;
}

