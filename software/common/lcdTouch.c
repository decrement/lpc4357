
#include <stdio.h>
#include "board.h"
#include "lpc43xx_ssp1.h"
#include "lcdtouch.h"
#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h"


/*
7  6 - 4  3      2     1-0
s  A2-A0 MODE SER/DFR PD1-PD0
*/
#define TOUCH_MSR_X  0xD4   //读Y轴坐标指令
#define TOUCH_MSR_Y  0x94   //读X轴坐标指令


#define TP_CS_LOW()          GPIO_ClearValue(7,1<<19);
#define TP_CS_HIGH()         GPIO_SetValue(7,1<<19);	

// 读取TP_INT引脚状态,0为有按下,1为释放
// 这里为了使用方便,TP_DOWN()返回1为有按下,0为释放.
#define TP_DOWN()                  (!(LPC_GPIO_PORT->PIN[3] &(1<<8)))


uint32_t g_touchXSize, g_touchYSize;
uint32_t g_touchPhyLeftUpX,  g_touchPhyLeftUpY;
uint32_t g_touchPhyRightUpX,  g_touchPhyRightUpY;
uint32_t g_touchPhyLeftDownX,  g_touchPhyLeftDownY;
uint32_t g_touchPhyRightDownX,  g_touchPhyRightDownY;

uint16_t g_touchSensitity;
uint16_t g_touch_attribute;

volatile int g_touchState;
int xPhys, yPhys;



int pen_is_down(void)
{


	return TP_DOWN();

}

						
uint8_t SPI_WriteByte(unsigned char data)			  
{
    return SSP1_SendRecvByte(data);		
}

void touchInit(unsigned short xSize, unsigned short ySize, unsigned short sensitity)
//void touchInit(void)
{
    g_touchXSize = xSize;
	g_touchYSize = ySize;

    g_touchSensitity = sensitity > 9 ? 55 : (sensitity + 1)*5;

	spi1_master_init();

	scu_pinmux(0xF,5,MD_PLN_FAST,FUNC4);	// Pf.5 as GPIO7[19]	 ssp_cs pin	
	GPIO_SetDir(7,1<<19, 1);			   //P3.6 as GPIO0[6]	 ssp_cs pin	  output
	GPIO_SetValue(7,1<<19);		//cs as high

    scu_pinmux(0x7,0,MD_PLN_FAST, FUNC0);	//   P7.0 as GPIO3[8]    Lcd_int
	GPIO_SetDir(3,1<<8, 0);			        //P7.0 as GPIO3[8]	 Lcd_int
	
 	LPC_SCU->PINTSEL0 |= (8 << 0)     // Pin0  pin is  8
	                  |  (3 << 5);	 // Pin0  port is 3
	                  

   
    LPC_GPIO_PIN_INT->ISEL  |= (1 << 0);	 // Port3 level sensitive
	LPC_GPIO_PIN_INT->IENR |= (1 << 0);	     // Enable Port3  interrupt 
	LPC_GPIO_PIN_INT->CIENF |= (1 << 0);     //low active
	
	NVIC_EnableIRQ(PIN_INT0_IRQn);
}



unsigned long touchGetClickPos(unsigned short * x, unsigned short * y)
{
    uint16_t xPos, yPos;

	if (pen_is_down() == 0) {

		return 0;

	}
     
    TP_CS_LOW();
    SPI_WriteByte(TOUCH_MSR_X);                  /* read X */
    xPos = (SPI_WriteByte(0x00)&0x7F)<<5;     /* read MSB bit[11:8] */
    xPos |= SPI_WriteByte(TOUCH_MSR_Y)>>3;    /* read LSB bit[7:0] and prepare read Y */
    yPos = (SPI_WriteByte(0x00)&0x7F)<<5;     /* read MSB bit[11:8] */
    yPos |= SPI_WriteByte(0x00)>>3;           /* read LSB bit[7:0] */
    //SPI_WriteByte( 1<<7 ); /* 再次打开中断 */
    TP_CS_HIGH();	
				
    *x = xPos ;
    *y = yPos ;

     return 1;	 
}



#define TOUCH_POS_NUM  4
uint16_t g_touchPosX[TOUCH_POS_NUM];
uint16_t g_touchPosY[TOUCH_POS_NUM];

unsigned long touchGetPhyValue(unsigned short * x, unsigned short  * y)
{
    uint16_t xRaw, yRaw, xMax, yMax, xMin, yMin;
    volatile uint32_t touchNum, i;
	uint32_t xTemp, yTemp;

    touchNum = 0;
    xTemp = 0;
	  yTemp = 0;
    while (touchGetClickPos(&xRaw, &yRaw))
    {
        // 得到一个新的值
        if (touchNum == 0)
        {
            xMax = xRaw;
            xMin = xRaw;
            yMax = yRaw;
            yMin = yRaw;

            g_touchPosX[0] = xRaw; 
            g_touchPosY[0] = yRaw; 

            touchNum = 1;

            continue;
        }

        // 判断一下新的输入点与原有输入是否相近。
        if (xRaw < xMin)
        {
            xMin = xRaw;
        }
        else if (xRaw > xMax)
        {
            xMax = xRaw;
        }

        if (yRaw < yMin)
        {
            yMin = yRaw;
        }
        else if (yRaw > yMax)
        {
            yMax = yRaw;
        }

        if (((xMax - xMin) > g_touchSensitity) || ((yMax - yMin) > g_touchSensitity))
        {
            // 范围不集中，重新采集。
            touchNum = 0;

            continue;
        }

        // 记录当前的值		
        g_touchPosX[touchNum] = xRaw; 
        g_touchPosY[touchNum] = yRaw; 

        touchNum++;

        if (touchNum >= TOUCH_POS_NUM)
        {
            // 已成功采集中足够点，计算一下。
			for(i = 0 ;i < TOUCH_POS_NUM; i++)
		    {
			   xTemp += g_touchPosX[i];
			   yTemp += g_touchPosY[i];
		    }
			*x = xTemp >> 3;
			*y = yTemp >> 3;
            //*x = (xMax + xMin) >> 1;
            //*y = (yMax + yMin) >> 1;
	        return 1;
        }

        // 两点之间，稍作延迟。
        for (i = 10; i; i--);
        //raw_sleep(1);
    }

    return 0;
}



void GPIO0_IRQHandler(void)
{
	LPC_GPIO_PIN_INT->CIENR  |= (1 << 0);	 // Port3 low level interrupt

	
    if (g_touchState == 0)
	{
		
     	g_touchState = 1;
	}


	
}

int g_touchCalA[7];

#if 0
void touchGetPixelByPhy(unsigned short xPhy, unsigned short yPhy,unsigned short * xPixel, unsigned short * yPixel)
{
	  unsigned short xLog, yLog;
	  
	  xLog = (g_touchCalA[2] + g_touchCalA[0]*xPhy + g_touchCalA[1]*yPhy ) / g_touchCalA[6];
	
	  yLog = (g_touchCalA[5] + g_touchCalA[3]*xPhy + g_touchCalA[4]*yPhy ) / g_touchCalA[6]; 
	  
	  *xPixel =  xLog;

	  *yPixel = yLog;
}

#else 

void touchGetPixelByPhy(unsigned short xPhy, unsigned short yPhy,unsigned short * xPixel, unsigned short * yPixel)
{
	  unsigned short xLog, yLog;
	  
	  xLog = (g_touchCalA[0] + g_touchCalA[1]*xPhy + g_touchCalA[2]*yPhy ) / g_touchCalA[6];
	
	  yLog = (g_touchCalA[3] + g_touchCalA[4]*xPhy + g_touchCalA[5]*yPhy ) / g_touchCalA[6]; 
	  
	  *xPixel =  xLog;

	  *yPixel = yLog;
}

#endif


void GUI_TOUCH_Measure(void) 
{
   // Coordinate * Ptr;
	uint16_t  xLog, yLog;
   	uint16_t xPhy, yPhy;

	xPhys = -1; yPhys = -1;

	if(g_touchState == 1)
	{
		if (touchGetPhyValue(&xPhy, &yPhy))
		{
            touchGetPixelByPhy(xPhy, yPhy, &xLog, &yLog);
		
			xPhys = xLog;
			yPhys = yLog;
		
			if ((xPhys > g_touchXSize) || (yPhys > g_touchYSize)) {

				xPhys = -1; 
				yPhys = -1;
			}
			
			g_touchState = 0;	   
			LPC_GPIO_PIN_INT->IENR  |= (1 << 0);
        }

		 else
		 {	
			g_touchState = 0;
			LPC_GPIO_PIN_INT->IENR  |= (1 << 0);
		 }	

		
 	}
	else
	{
		//LPC_GPIO_PIN_INT->IENR  |= (1 << 0);
	}

}




int touchCheck(CALIBRATION cal)
{
	int j;
	float n,x,y,x2,y2,xy,z,zx,zy;
	float det,det1,det2,det3;
	float scaling = 65536.0;	

	n = x = y = x2 = y2 = xy = 0;
	for(j = 0; j < 5 ; j++)
	{
		n += 1.0;
		x += (float)cal.x[j];
		y += (float)cal.y[j];
		x2 += (float)(cal.x[j] * cal.x[j]);
		y2 += (float)(cal.y[j] * cal.y[j]);
		xy += (float)(cal.x[j] * cal.y[j]);
	} 
	
	  det = n * (x2*y2 - xy*xy) + x*(xy*y - x*y2) + y*(x*xy - y*x2);
	  
	  if(det < 0.1 && det > -0.1)
	  {
	  		return 1;
	  }
	  
	  z = zx = zy = 0;
	  
	  for(j = 0; j < 5;j++)
	  {
	  	z += (float)cal.xfb[j];
	  	zx += (float)(cal.xfb[j] * cal.x[j]);
	  	zy += (float)(cal.xfb[j] * cal.y[j]);
	  }
	  det1 = n * (zx*y2 - xy * zy) + z*(xy*y - x*y2) + y*(x*zy - y*zx);
	  det2 = n*(x2*zy - zx*xy) + x*(zx*y - x*zy) + z*(x*xy - y*x2);
	  det3 = z*(x2*y2 - xy*xy) + x*(xy*zy - zx*y2) + y*(zx*xy - zy*x2);
	  
	  g_touchCalA[0] = (int)((det1/det)*scaling);
	  g_touchCalA[1] = (int)((det2/det)*scaling);
	  g_touchCalA[2] = (int)((det3/det)*scaling);
	  	  
	  z = zx = zy = 0;

	  for(j = 0; j < 5;j++)
	  {
	  	z += (float)cal.yfb[j];
	  	zx += (float)(cal.yfb[j] * cal.x[j]);
	  	zy += (float)(cal.yfb[j] * cal.y[j]);
	  }

	  det1 = n * (zx*y2 - xy * zy) + z*(xy*y - x*y2) + y*(x*zy - y*zx);
	  det2 = n*(x2*zy - zx*xy) + x*(zx*y - x*zy) + z*(x*xy - y*x2);
	  det3 = z*(x2*y2 - xy*xy) + x*(xy*zy - zx*y2) + y*(zx*xy - zy*x2);
	  
	  g_touchCalA[3] = (int)((det1/det) * scaling);
	  g_touchCalA[4] = (int)((det2/det) * scaling);
	  g_touchCalA[5] = (int)((det3/det) * scaling);
	  
	  g_touchCalA[6] = (int)scaling;

	  return 0;
}




