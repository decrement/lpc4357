#ifndef __SYSTEM_LPC18xx_H
#define __SYSTEM_LPC18xx_H


/**
 * Initialize the system
 *
 * @param  none
 * @return none
 *
 * @brief  Setup the microcontroller system.
 *         Initialize the System and update the SystemCoreClock variable.
 */
extern void SystemInit (void);

extern uint32_t GetCoreClock (void);


#endif /* __SYSTEM_LPC18xx_H */

