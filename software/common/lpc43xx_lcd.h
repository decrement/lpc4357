#ifndef __LPC18XX_LCD_H_
#define __LPC18XX_LCD_H_

#include <raw_api.h>

#define LCD_COLOR_BLACK        0x00000000  // 黑色
#define LCD_COLOR_WHITE        0x00FFFFFF  // 白色
#define LCD_COLOR_GRAY         0x007F7F7F  // 灰色（50%灰度）
#define LCD_COLOR_DARK_GRAY    0x003F3F3F  // 深灰色（75%灰度）
#define LCD_COLOR_LIGHT_GRAY   0x00C3C3C3  // 浅灰色（25%灰度）
#define LCD_COLOR_RED          0x000000FF  // 红色
#define LCD_COLOR_DARK_RED     0x0000007F  // 深红色（酒红色）
#define LCD_COLOR_LIGHT_RED    0x007F7FFF  // 浅红色（粉红色）
#define LCD_COLOR_GREEN        0x0000FF00  // 绿色
#define LCD_COLOR_DARK_GREEN   0x00007F00  // 深绿色
#define LCD_COLOR_LIGHT_GREEN  0x007FFF7F  // 浅绿色
#define LCD_COLOR_BLUE         0x00FF0000  // 蓝色
#define LCD_COLOR_DARK_BLUE    0x007F0000  // 深蓝色（海蓝色）
#define LCD_COLOR_LIGHT        0x00FF7F7F  // 浅蓝色（天蓝色）
#define LCD_COLOR_YELLOW       0x0000FFFF  // 黄色
#define LCD_COLOR_DART_YELLOW  0x00007F7F  // 深黄色（橄榄色）
#define LCD_COLOR_LIGHT_YELLOW 0x007FFFFF  // 浅黄色
#define LCD_COLOR_PURPLE       0x00FF00FF  // 紫色
#define LCD_COLOR_DART_PURPLE  0x007F007F  // 深紫色
#define LCD_COLOR_LIGHT_PURPLE 0x00FF7FFF  // 浅紫色
#define LCD_COLOR_CYAN         0x00FFFF00  // 青色
#define LCD_COLOR_DARK_CYAN    0x007F7F00  // 深青色
#define LCD_COLOR_LIGHT_CYAN   0x00FFFF7F  // 浅青色（天青色）
#define LCD_COLOR_ORANGE       0x00007FFF  // 橙色
#define LCD_COLOR_GOLD         0x0000D7FF  // 金色

// 对LCD进行初始化
// 入参 xSize,ySize: LCD屏幕的大小
extern void LCD_init(int xSize, int ySize);

// 设置显存的地址，地址空间为xSize*ySize*2字节（RGB565模式）
extern void LCD_displayMemorySet(unsigned short * addr);

// LCD使能
extern void LCD_enable(void);

// 设置画图笔
// 入参 color：画笔的颜色（RGB888格式），BIT31-24:保留 BIT23-16:B BIT15-8:G BIT7-0:R 
// 入参 size：画笔的大小（以相素点为单位）
extern void LCD_paintPenSet(unsigned long color, int size);

// 在指定的位置画点
// 入参 x,y: 所画点的位置
extern void LCD_paintPoint(int x0, int y0);

// 在指定的位置画水平线
// 入参 x0, y0, y1: 所画线的位置
extern void LCD_paintLineHorizontal(int x0, int y0, int x1);

// 在指定的位置画垂直线
// 入参 x0, y0, x1: 所画线的位置
extern void LCD_paintLineVertical(int x0, int y0, int y1);

// 在指定的位置画斜线
// 入参 x0, y0, x1, y1: 所画线的位置
extern void LCD_paintLineOblique(int x0, int y0, int x1, int y1);

// 在指定的位置画圆弧线
// 入参 x0, y0, x1, y1：圆弧线的起始点和终点（从起始点顺时针到终点）
// 入参 r: 圆弧线所在圆的圆心点
// 入参 isChord：是否为弦（角度小于180度的弧形）  1:为弦  0：为弓
extern void LCD_paintLineArc(int x0, int y0, int x1, int y1, int r, int isChord);

// 在指定的位置画圆形
// 入参 x0, y0: 圆心点
// 入参 r：半径
extern void LCD_paintCircle(int x0, int y0, int r);

// 在指定的位置画椭圆
// 入参 x0, y0: 椭圆圆心点
// 入参 rX, rY: X和Y方向的半径
extern void LCD_paintEllipse(int x0, int y0, int rX, int rY);

// 在指定的区域进行填充
// 入参 x0, y0：填充的位置（所有与指定点相连并且颜色相同的点都将被填色）
// 入参 color：填充的颜色（RGB888格式），BIT31-24:保留 BIT23-16:B BIT15-8:G BIT7-0:R 
extern void LCD_fillColor(int x0, int y0, unsigned long color);

// 在指定的位置导入图片
// 入参 x0, y0：图片导入的左上角位置
// 入参 xSize, ySize：图片的大小
// 入参 data：图片的数据（RBG565格式，2个字节表示一个相素，）
extern void LCD_loadPicture_16(int x0, int y0, int xSize, int ySize, const unsigned short * data);

// 在指定的位置导入图片
// 入参 x0, y0：图片导入的左上角位置
// 入参 xSize, ySize：图片的大小
// 入参 data：图片的数据（RBG888格式，三个字节表示一个相素，三个字节分别为R G B）
extern void LCD_loadPicture_24(int x0, int y0, int xSize, int ySize, const unsigned char * data);

// 在指定的位置清除画面
// 入参 x0, y0：清除画面左上角位置
// 入参 xSize, ySize：清除画面的大小
// 入参 color：画面清除后的背景色
extern void LCD_clear(int x0, int y0, int xSize, int ySize, unsigned long color);

// LCD修改完成后，需要调用本函数才能展示在显示屏上。 
extern void LCD_show(void);

// LCD背光控制
// 入参 intension：背光强度，取值0 -- 100
extern void LCD_backLightCtl(unsigned int intension);

void LCD_one_pixel_write( RAW_U32 pos_x, RAW_U32 pos_y, RAW_U32 color);
RAW_U32 LCD_one_pixel_read( RAW_U32 pos_x, RAW_U32 pos_y);



#endif /* __LPC18XX_LCD_H_ */


/* --------------------------------- End Of File ------------------------------ */

