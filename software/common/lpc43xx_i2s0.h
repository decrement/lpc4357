#ifndef LPC18XX_I2S_H_
#define LPC18XX_I2S_H_

/** I2S Transmit/Receive bit */
#define I2S_TX_MODE			((uint8_t)(0))
#define I2S_RX_MODE			((uint8_t)(1))

/** I2S Clock Select bit */
#define I2S_CLKSEL_FRDCLK	((uint8_t)(0))
#define I2S_CLKSEL_MCLK		((uint8_t)(2))

/** I2S 4-pin Mode bit */
#define I2S_4PIN_ENABLE 	((uint8_t)(1))
#define I2S_4PIN_DISABLE 	((uint8_t)(0))
/** I2S MCLK Enable bit */
#define I2S_MCLK_ENABLE		((uint8_t)(1))
#define I2S_MCLK_DISABLE	((uint8_t)(0))


typedef unsigned long (*CALL_BACK)(unsigned long *, unsigned long *, unsigned long);


typedef enum
{
    SOUND_FREQ_8_0_K     = 8000,    
    SOUND_FREQ_11_025_K  = 11025,    
    SOUND_FREQ_22_05_K   = 22050,    
    SOUND_FREQ_44_1_K    = 44100   
}T_SOUND_FREQ;

typedef enum
{
    SOUND_CHNL_MONO      = 1,
    SOUND_CHNL_STERO     = 0
}T_SOUND_CHNL;

typedef enum
{
    SOUND_SAMPLE_8BITS   = 0,
    SOUND_SAMPLE_16BITS  = 1,
    SOUND_SAMPLE_32BITS  = 3
}T_SOUND_SAMPLE_BITS;



typedef struct _I2S_CONFIG
{
	T_SOUND_SAMPLE_BITS wordWidth;
	T_SOUND_CHNL channel;
	T_SOUND_FREQ smpFreq;
	CALL_BACK pFunc;
}T_I2S_CONFIG;





/* Public Types --------------------------------------------------------------- */
/** @defgroup I2S_Public_Types I2S Public Types
 * @{
 */

/**
 * @brief I2S configuration structure definition
 */
typedef struct {
	uint8_t wordwidth;		/** the number of bytes in data as follow:
							-I2S_WORDWIDTH_8: 8 bit data
							-I2S_WORDWIDTH_16: 16 bit data
							-I2S_WORDWIDTH_32: 32 bit data */
	uint8_t	mono; 			/** Set mono/stereo mode, should be:
							- I2S_STEREO: stereo mode
							- I2S_MONO: mono mode */
	uint8_t stop;			/** Disables accesses on FIFOs, should be:
							- I2S_STOP_ENABLE: enable stop mode
							- I2S_STOP_DISABLE: disable stop mode */
	uint8_t reset;			/** Asynchronously reset tje transmit channel and FIFO, should be:
							- I2S_RESET_ENABLE: enable reset mode
							- I2S_RESET_DISABLE: disable reset mode */
	uint8_t ws_sel;			/** Set Master/Slave mode, should be:
							- I2S_MASTER_MODE: I2S master mode
							- I2S_SLAVE_MODE: I2S slave mode */
	uint8_t mute;			/** MUTE mode: when true, the transmit channel sends only zeroes, shoule be:
							- I2S_MUTE_ENABLE: enable mute mode
							- I2S_MUTE_DISABLE: disable mute mode */
	uint8_t Reserved0[2];
} I2S_CFG_Type;

/**
 * @brief I2S DMA configuration structure definition
 */
typedef struct {
	uint8_t DMAIndex;		/** Select DMA1 or DMA2, should be:
							- I2S_DMA_1: DMA1
							- I2S_DMA_2: DMA2 */
	uint8_t depth;			/** FIFO level that triggers a DMA request */
	uint8_t Reserved0[2];
}I2S_DMAConf_Type;

/**
 * @brief I2S mode configuration structure definition
 */
typedef struct{
	uint8_t clksel;			/** Clock source selection, should be:
							- I2S_CLKSEL_FRDCLK: Select the fractional rate divider clock output
							- I2S_CLKSEL_MCLK: Select the MCLK signal as the clock source */
	uint8_t fpin;			/** Select four pin mode, should be:
							- I2S_4PIN_ENABLE: 4-pin enable
							- I2S_4PIN_DISABLE: 4-pin disable */
	uint8_t mcena;			/** Select MCLK mode, should be:
							- I2S_MCLK_ENABLE: MCLK enable for output
							- I2S_MCLK_DISABLE: MCLK disable for output */
	uint8_t Reserved;
}I2S_MODEConf_Type;

extern void I2S_init (T_I2S_CONFIG *rxConfig, T_I2S_CONFIG *txConfig);

extern void I2S_Start(uint8_t TRMode);

extern void I2S_Stop(uint8_t TRMode);


#endif /* LPC18XX_I2S_H_ */

/* --------------------------------- End Of File ------------------------------ */
