/*****************************************************************************
 *   uart.h:  Header file for NXP LPC17xx Family Microprocessors
 *
 *   Copyright(C) 2008, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2008.08.21  ver 1.00    Prelimnary version, first Release
 *
******************************************************************************/
#ifndef __UART1_H 
#define __UART1_H

// 入参clk：UART1模块工作的系统频率
// 入参baudrate：UART1工作的波特率
extern void UART1_init(unsigned long sysClk, unsigned long baudrate);

// 应用层通过UART接口发送数据
// 入参 data：发送的数据空间
// 入参 len：发送的数据长度
// 返回：实际成功发送的数据长度（如果没有完全发送，请稍作延迟后再发）
extern unsigned long UART1_snd(unsigned char * data, unsigned long len);

// UART1驱动收到数据（此函数由UART1的使用者编写，在驱动程序中断中调用。）
// 入参 data：收到的数据究竟
// 入参 len：收到的数据长度
// 返回：无
extern void UART1_rcv(unsigned char * data, unsigned long len);

// 控制接收中断使能
// 入参 enable:
//          0：禁能
//          1：使能
extern void UART1_intRxCtl(unsigned long enable);

#endif /* end __UART_H */
/*****************************************************************************
**                            End Of File
******************************************************************************/
