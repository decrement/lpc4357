#ifndef LPC18XX_RTC_H_
#define LPC18XX_RTC_H_

extern void RTC_Init(void);

extern void RTC_getTime(unsigned char * hour, unsigned char * min, unsigned char * sec);

extern void RTC_setTime(unsigned char hour, unsigned char min, unsigned char sec);

extern void RTC_getDate(unsigned short * year, unsigned char * mon, unsigned char * day);

extern void RTC_setDate(unsigned short year, unsigned char mon, unsigned char day);


#endif /* LPC18XX_RTC_H_ */

