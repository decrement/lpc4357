#include <raw_api.h>
#include "lpc43xx_lcd.h"


#define  LCD_MAX_LOG_COLORS (256)

static RAW_U16 read_16(const RAW_U8 ** ppData) {
  const RAW_U8 * pData;
  RAW_U16  Value;
  
  pData = *ppData;
  Value = *pData;
  Value |= (RAW_U16)(*(pData + 1) << 8);
  pData += 2;
  *ppData = pData;
  return Value;
}

/*********************************************************************
*
*       GUI__Read32
*/
static RAW_U32 read_32(const RAW_U8 ** ppData) {
  const RAW_U8 * pData;
  RAW_U32  Value;
  
  pData = *ppData;
  Value = *pData;
  Value |= (     *(pData + 1) << 8);
  Value |= ((RAW_U32)*(pData + 2) << 16);
  Value |= ((RAW_U32)*(pData + 3) << 24);
  pData += 4;
  
  *ppData = pData;
  
  return Value;
}

static int _GetStep(int * pYSize, int * pY) {
  if (*pYSize > 0) {
    *pY = *pYSize - 1;
    return -1;
  } else if (*pYSize < 0) {
    *pYSize = -*pYSize;
    *pY = 0;
    return 1;
  } else {
    return 0;
  }
}
#define COLOR_TO_MTK_COLOR_SIMUL(color) ((((color) >> 19) & 0x1f) << 11) \
                                            |((((color) >> 10) & 0x3f) << 5) \
                                            |(((color) >> 3) & 0x1f) 



/*********************************************************************
*
*       _DrawBitmap_24bpp
*/
static int _DrawBitmap_24bpp(const RAW_U8 * pData, int x0, int y0, int XSize, int YSize) {
  int x, y, BytesPerLine, Step;
  Step = _GetStep(&YSize, &y);
  if (!Step) {
    return 1;
  }
  BytesPerLine = ((24 * XSize + 31) >> 5) << 2;
  for (; (y < YSize) && (y >= 0); y += Step) {
    for (x = 0; x < XSize; x++) {
      const RAW_U8 * pColor = pData + 3 * x;
      RAW_U8 r, g, b;
      b = *(pColor);
      g = *(pColor + 1);
      r = *(pColor + 2);
     LCD_one_pixel_write(x0 + x, y0 + y, COLOR_TO_MTK_COLOR_SIMUL( (r << 16) | (g << 8) | (b) ));
    }
    pData += BytesPerLine;
  }
  return 0;
}


int bmp_draw(const void * pBMP, int x0, int y0) 
{
  
  int Ret = 0;
  RAW_S32 Width, Height;
  RAW_U16 BitCount, Type;
  RAW_U32 ClrUsed, Compression;
  
  int NumColors;
  const RAW_U8 * pSrc = (const RAW_U8 *)pBMP;
  Type        = read_16(&pSrc); /* get type from BITMAPFILEHEADER */
  pSrc += 12;                   /* skip rest of BITMAPFILEHEADER */
  /* get values from BITMAPINFOHEADER */
  pSrc += 4;
  
  Width       = read_32(&pSrc);
  Height      = read_32(&pSrc);
  pSrc += 2;
  BitCount    = read_16(&pSrc);
  Compression = read_32(&pSrc);
  pSrc += 12;
  ClrUsed     = read_32(&pSrc);
  pSrc += 4;
  
  /* calculate number of colors */
  switch (BitCount) {
    case 0:   return 1; /* biBitCount = 0 (JPEG format) not supported. Please convert image ! */
    case 1:   NumColors = 2;   break;
    case 4:   NumColors = 16;  break;
    case 8:   NumColors = 256; break;
    case 24:  NumColors = 0;   break;
    default:
      return 1; /* biBitCount should be 1, 4, 8 or 24 */
  }
  if (NumColors && ClrUsed) {
    NumColors = ClrUsed;
  }
  /* check validity of bmp */
  if ((NumColors > LCD_MAX_LOG_COLORS) ||
      (Type != 0x4d42)                 || /* 'BM' */
      (Compression)                    || /* only uncompressed bitmaps */
      (Width  > 1024)                  ||
      (Height > 1024)) {
    return 1;
  }

  
 
  /* Show bitmap */
  switch (BitCount) {
    case 1:
    case 4:
    case 8:
      //Ret = _DrawBitmap_Pal(pSrc, x0, y0, Width, Height, BitCount, NumColors);
      /*to do*/
	  RAW_ASSERT(0);
      break;
    case 24:
      Ret = _DrawBitmap_24bpp(pSrc, x0, y0, Width, Height);
      break;
  }
  
 
 
  return Ret;
}

