/*************************************************************************
 *
*    Used with ICCARM and AARM.
 *
 *    (c) Copyright IAR Systems 2008
 *
 *    File name   : drv_glcd.c
 *    Description : Graphical LCD driver
 *
 *    History :
 *    1. Date        : 6, March 2008
 *       Author      : Stanimir Bonev
 *       Description : Create
 *
 *
 *    $Revision: 24636 $
 *
 *    @Modify: NXP MCU Application Team - NguyenCao
 *    @Date: 04. March. 2011
 **************************************************************************/

#include "board.h"
#include "lpc43xx_lcd.h"
#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h" 

#define LCD_MODEL_AT043TN24   1  // 群创4.3寸屏
#define LCD_MODEL_AT070TN92   2  // 群创7寸屏
#define LCD_MODEL_KTM101CM01  3  // 维信诺（visionox）10.1寸屏




// #define LCD_MODEL_INDEX   LCD_MODEL_AT043TN24
// #define LCD_MODEL_INDEX   LCD_MODEL_AT070TN92

#if defined(LCD_MODEL_AT043)

    #define C_GLCD_H_PULSE          41
    #define C_GLCD_H_FRONT_PORCH    10
    #define C_GLCD_H_BACK_PORCH     10

    #define C_GLCD_V_PULSE          10
    #define C_GLCD_V_FRONT_PORCH    2
    #define C_GLCD_V_BACK_PORCH     2

    #define C_GLCD_CLK_DIV          44  //180/(n+1)M

#elif defined(LCD_MODEL_AT070)

    #define C_GLCD_H_PULSE          20
    #define C_GLCD_H_FRONT_PORCH    210
    #define C_GLCD_H_BACK_PORCH     46

    #define C_GLCD_V_PULSE          10
    #define C_GLCD_V_FRONT_PORCH    22
    #define C_GLCD_V_BACK_PORCH     23
	#define C_GLCD_CLK_DIV          5  //204/(5+1) = 34M

#else

    #define C_GLCD_H_PULSE          20
    #define C_GLCD_H_FRONT_PORCH    250
    #define C_GLCD_H_BACK_PORCH     50

    #define C_GLCD_V_PULSE          5
    #define C_GLCD_V_FRONT_PORCH    5
    #define C_GLCD_V_BACK_PORCH     5
//	#define C_GLCD_CLK_DIV          5  //180/(5+1) = 30M

#endif

#define C_GLCD_PWR_ENA_DIS_DLY  10000
#define C_GLCD_ENA_DIS_DLY      10000

static RAW_U32 g_lcdHSize, g_lcdVSize;

static RAW_U16 *lcd_frame_buffer;


#define LCD_STATE_UPDATE      0x0001  // 当前帧是否需要更换

//const uint16_t g_lcdCircleSize = 9;
//const uint16_t g_lcdCircleLine[] = {0x0038, 0x00C6, 0x0082, 0x0101, 0x0101,0x0101, 0x0082, 0x00C6, 0x0038};

//#define MCPMW_LIM_MAX        18000

/*Make pwm peroid 1KHZ at 204M*/
#define MCPMW_LIM_MAX        204000


void MCPMW_Init (void)
{
	scu_pinmux(0x09, 4, MD_PUP, 1);  // MCOB0
	
	LPC_MCPWM->CON_CLR = 0x1F;
	LPC_MCPWM->CNTCON_CLR = 1 << 29;
	LPC_MCPWM->LIM[0] = MCPMW_LIM_MAX;
	LPC_MCPWM->MAT[0] = MCPMW_LIM_MAX;

#if defined(LCD_MODEL_AT043)	
	LPC_MCPWM->CON_SET = 0x01;
#else
	LPC_MCPWM->CON_SET = 0x01 | (1 << 2);
#endif
}

void MCPWM_Set_Ratio (unsigned char radio)
{
	LPC_MCPWM->MAT[0] = MCPMW_LIM_MAX / 100 * radio;
}
// 对LCD进行初始化
// 入参 xSize,ySize: LCD屏幕的大小
void LCD_init(int xSize, int ySize)
{
    uint32_t i;
	uint32_t *pPal;

    g_lcdHSize = xSize;
    g_lcdVSize = ySize;

	#if defined(LCD_MODEL_AT070)
	LPC_CGU2->IDIVE_CTRL = ((0x0009 << 24) | (0x0001 << 11)) | (C_GLCD_CLK_DIV<< 2);			
	LPC_CGU2->BASE_LCD_CLK = ((0x10 << 24) | (0x0001 << 11));  
	#endif
	
	LPC_CGU2->BASE_APB1_CLK = ((0x0009 << 24) | (0x0001 << 11));
	
	raw_sleep(1);
	
	Chip_SCU_PinMuxSet(1, 5, FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 8);
	
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 8, 0);
	raw_sleep(1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 8, 1);
	raw_sleep(1);

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 15, 1);
	
    // Assign pins
	//scu_pinmux(0X01, 4, MD_PUP, FUNC0);	
	//GPIO_SetDir(1, 1<<4, 1);	
	//GPIO_SetValue(0X01, 1<<4);
	//GPIO_ClearValue(0X01, 1<<4);
//     scu_pinmux(0x07, 7, MD_PUP, FUNC3); 	/* LCD_PWR @ P7.7 */	  
 	scu_pinmux(0x04, 7, MD_PUP, FUNC0);		/* LCD_DCLK @ P4.7 */	 //CP
	scu_pinmux(0x04, 5, MD_PUP, FUNC2);		/* LCD_FP @ P4.5 */		//FP
	scu_pinmux(0x04, 6, MD_PUP, FUNC2); 	/* LCD_ENAB_M @ P4.6 */	  //AC
	scu_pinmux(0x07, 6, MD_PUP, FUNC3);		/* LCD_LP @ P7.6 */	  //LP

	scu_pinmux(0x04, 1, MD_PUP, FUNC2);		/* LCD_VD_0 @ P4.1 */
	scu_pinmux(0x04, 4, MD_PUP, FUNC2);		/* LCD_VD_1 @ P4.4 */
	scu_pinmux(0x04, 3, MD_PUP, FUNC2);		/* LCD_VD_2 @ P4.3 */
	scu_pinmux(0x04, 2, MD_PUP, FUNC2);		/* LCD_VD_3 @ P4.2 */
	scu_pinmux(0x08, 7, MD_PUP, FUNC3);		/* LCD_VD_4 @ P8.7 */
	scu_pinmux(0x08, 6, MD_PUP, FUNC3);		/* LCD_VD_5 @ P8.6 */
	scu_pinmux(0x08, 5, MD_PUP, FUNC3);		/* LCD_VD_6 @ P8.5 */
	scu_pinmux(0x08, 4, MD_PUP, FUNC3);		/* LCD_VD_7 @ P8.4 */
	scu_pinmux(0x04, 10, MD_PUP, FUNC2);	/* LCD_VD_10 @ P4.10 */
	scu_pinmux(0x04, 9, MD_PUP, FUNC2); 	/* LCD_VD_11 @ P4.9 */
	scu_pinmux(0x08, 3, MD_PUP, FUNC3); 	/* LCD_VD_12 @ P8.3 */
	scu_pinmux(0x0B, 6, MD_PUP, FUNC2); 	/* LCD_VD_13 @ PB.6 */
	scu_pinmux(0x0B, 5, MD_PUP, FUNC2); 	/* LCD_VD_14 @ PB.5 */
	scu_pinmux(0x0B, 4, MD_PUP, FUNC2); 	/* LCD_VD_15 @ PB.4 */
	scu_pinmux(0x07, 2, MD_PUP, FUNC3); 	/* LCD_VD_18 @ P7.2 */
	scu_pinmux(0x07, 1, MD_PUP, FUNC3); 	/* LCD_VD_19 @ P7.1 */
	scu_pinmux(0x0B, 3, MD_PUP, FUNC2); 	/* LCD_VD_20 @ PB.3 */
	scu_pinmux(0x0B, 2, MD_PUP, FUNC2); 	/* LCD_VD_21 @ PB.2 */
	scu_pinmux(0x0B, 1, MD_PUP, FUNC2); 	/* LCD_VD_22 @ PB.1 */
	scu_pinmux(0x0B, 0, MD_PUP, FUNC2); 	/* LCD_VD_23 @ PB.0 */

 //  	scu_pinmux(0x07, 0, MD_PUP, FUNC0);		/* LCD_LE @ P7.0 */	  //LE & CLKIN not used


	/*unused reserved gpio pin*/
    scu_pinmux(0x0f,8, MD_PUP, FUNC4); 	
	scu_pinmux(0x0f,9, MD_PUP, FUNC4); 	
	GPIO_SetDir(7,1<<22, 1);			  
	GPIO_SetDir(7,1<<23, 1);			   
 	GPIO_ClearValue(7, 1<<22);
	GPIO_ClearValue(7, 1<<23);

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 18, 1);

		/* Reset LCD and wait for reset to complete */
	Chip_RGU_TriggerReset(RGU_LCD_RST);
	while (Chip_RGU_InReset(RGU_LCD_RST)) {}
	
	LPC_LCD->CTRL &= ~(1<<0);
  
	LPC_LCD->CRSR_CTRL &=~(1<<0);				  //disable curser;



 	LPC_LCD->CTRL |=  (6<<1);						 //565

	LPC_LCD->CTRL |= (1<<5);				  	// TFT panel

	LPC_LCD->CTRL &= ~(1<<7);				    // single panel
	
	LPC_LCD->CTRL |= (1<<8);					// notmal output rgb
	
	LPC_LCD->CTRL &= ~(1<<9);					// little endian byte order

	LPC_LCD->CTRL &= ~(1<<10);				   	// little endian pix order
	// disable power
	LPC_LCD->CTRL &= ~(1<<11);


	LPC_LCD->CTRL |=  (6<<1);						 //565

	LPC_LCD->CTRL |= (1<<5);				  	// TFT panel

	LPC_LCD->CTRL &= ~(1<<7);				    // single panel
	
	LPC_LCD->CTRL |= (1<<8);					// notmal output rgb
	
	LPC_LCD->CTRL &= ~(1<<9);					// little endian byte order

	LPC_LCD->CTRL &= ~(1<<10);				   	// little endian pix order
	// disable power
	LPC_LCD->CTRL &= ~(1<<11);
	

		// init Horizontal Timing
	LPC_LCD->TIMH = 0; //reset TIMH before set value
	LPC_LCD->TIMH |= (C_GLCD_H_BACK_PORCH - 1)<<24;
	LPC_LCD->TIMH |= (C_GLCD_H_FRONT_PORCH - 1)<<16;
	LPC_LCD->TIMH |= (C_GLCD_H_PULSE - 1)<<8;
	LPC_LCD->TIMH |= ((g_lcdHSize/16) - 1)<<2;
	
	// init Vertical Timing
	LPC_LCD->TIMV = 0;  //reset TIMV value before setting
	LPC_LCD->TIMV |= (C_GLCD_V_BACK_PORCH)<<24;
	LPC_LCD->TIMV |= (C_GLCD_V_FRONT_PORCH)<<16;
	LPC_LCD->TIMV |= (C_GLCD_V_PULSE - 1)<<10;
	LPC_LCD->TIMV |= g_lcdVSize-1;

	LPC_LCD->LE = 0;
	LPC_LCD->INTMSK = 0;

    LPC_LCD->POL = 0;
	LPC_LCD->POL |= 0x01 << 26;
	LPC_LCD->POL |= (g_lcdHSize-1)<< 16;
																
// #if defined(LCD_MODEL_AT070)
// 	LPC_LCD->POL |= 0x01 << 12;			//invert H sync
// 	LPC_LCD->POL |= 0x01 << 11;		  //invert v sync
// #endif
											  
	/* clear palette */
	pPal = (uint32_t*) (&(LPC_LCD->PAL));

	for(i = 0; i < 128; i++)
	{
		*pPal = 0;
		pPal++;
	} 

    //LPC_LCD->UPBASE = (uint32_t)g_lcdFrame;
    
    // 使用LCD控制器
    //LPC_LCD->CTRL |= (1<<0);
    //for(i = C_GLCD_PWR_ENA_DIS_DLY; i; i--);
    //LPC_LCD->CTRL |= (1<<11);
}

void LCD_enable(void)
{
    volatile int i;

    // 使用LCD控制器
    LPC_LCD->CTRL |= (1<<0);

	raw_sleep(2);

	for (i = 0; i < 10; i++) {
    	LPC_LCD->CTRL |= (1<<11);
	}

	raw_sleep(2);

	MCPMW_Init();
	
}


// 设置显存的地址，地址空间为xSize*ySize*2字节（RGB565模式）
void LCD_displayMemorySet(unsigned short * addr)
{	   
    //g_lcdFrame = addr;

    LPC_LCD->UPBASE = (uint32_t)addr; //(uint32_t)g_lcdFrame;
    lcd_frame_buffer = addr;
}

// LCD背光控制
// 入参 intension：背光强度，取值0 -- 100
void LCD_backLightCtl(unsigned int intension)
{
	if (intension > 100)
	{
		intension = 100;
	}
	
	MCPWM_Set_Ratio(intension);
}


void LCD_one_pixel_write( RAW_U32 pos_x, RAW_U32 pos_y, RAW_U32 color)
{

	lcd_frame_buffer[pos_y * g_lcdHSize + pos_x] = (color&0xffff);

}

RAW_U32 LCD_one_pixel_read( RAW_U32 pos_x, RAW_U32 pos_y)
{
	RAW_U16 test;

	test = lcd_frame_buffer[pos_y * g_lcdHSize + pos_x];
	return test;
} 


