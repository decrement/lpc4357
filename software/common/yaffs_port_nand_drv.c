/*
 * YAFFS: Yet Another Flash File System. A NAND-flash specific file system.
 *
 * Copyright (C) 2002-2011 Aleph One Ltd.
 *   for Toby Churchill Ltd and Brightstar Engineering
 *
 * Created by Charles Manning <charles@aleph1.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/*
 * This is an interface module for handling NAND in yaffs2 mode.
 */

/* This code calls a driver that accesses "generic" NAND. In the simulator
 * this is direceted at a file-backed NAND simulator. The NAND access functions
 * should also work with real NAND.
 *
 * This driver is designed for use in yaffs2 mode with a 2k page size and
 * 64 bytes of spare.
 *
 * The spare ares is used as follows:
 * offset 0: 2 bytes bad block marker.
 * offset 2: 8x3 bytes of ECC over the data.
 * offset 26: rest available to store Yaffs tags etc.
 */
#include <raw_api.h>
#include "yaffs_nand_drv.h"
#include "yportenv.h"
#include "yaffs_trace.h"
#include "nand_store.h"
#include "yaffs_flashif.h"
#include "yaffs_guts.h"
#include "nanddrv.h"
#include "yaffs_ecc.h"
#include <k9f1g08.h>


struct nand_context {
	struct nand_chip *chip;
	u8 *buffer;
};


static inline struct nand_chip *dev_to_chip(struct yaffs_dev *dev)
{
	struct nand_context *ctxt =
		(struct nand_context *)(dev->driver_context);
	return ctxt->chip;
}

static inline u8 *dev_to_buffer(struct yaffs_dev *dev)
{
	struct nand_context *ctxt =
		(struct nand_context *)(dev->driver_context);
	return ctxt->buffer;
}

static int yaffs_nand_drv_WriteChunk(struct yaffs_dev *dev, int nand_chunk,
				   const u8 *data, int data_len,
				   const u8 *oob, int oob_len)
{
	struct nand_chip *chip = dev_to_chip(dev);
	u8 *buffer = dev_to_buffer(dev);


	RAW_U8 result;
	
	if(!data || !oob)
		return YAFFS_FAIL;

	if (data_len > 2048)
		RAW_ASSERT(0);

	if (oob_len > 40)
		RAW_ASSERT(0);


	if (data) {
		
		result = k9f_page_write(nand_chunk, (RAW_U8 *)data);

		if (result == 0)
			return YAFFS_FAIL;
	}

	if (oob) {
	
		result = k9f_random_continue_write(nand_chunk, 2068, (RAW_U8 *)oob, oob_len);

		if (result == 0)
			return YAFFS_FAIL;
	}
	
	return YAFFS_OK;
	
}

static int yaffs_nand_drv_ReadChunk(struct yaffs_dev *dev, int nand_chunk,
				   u8 *data, int data_len,
				   u8 *oob, int oob_len,
				   enum yaffs_ecc_result *ecc_result_out)
{
	struct nand_chip *chip = dev_to_chip(dev);
	u8 *buffer = dev_to_buffer(dev);



	enum yaffs_ecc_result ecc_result;



	RAW_U8 result;
	
	if (data_len > 2048)
		RAW_ASSERT(0);

	if (oob_len > 40)
		RAW_ASSERT(0);
	
	if (data) {

		result = k9f_page_read(nand_chunk, data);
		
		if (result == 0x0)
			return YAFFS_FAIL;
	}

	/* Do ECC and marshalling */
	if(oob) {
		
		
		result =  k9f_random_continue_read(nand_chunk, 2068, oob, oob_len);
		
		if (result == 0x1)
			return YAFFS_FAIL;

	}
		
	ecc_result = YAFFS_ECC_RESULT_NO_ERROR;

	
	*ecc_result_out = ecc_result;

	return YAFFS_OK;
}




static int yaffs_nand_drv_EraseBlock(struct yaffs_dev *dev, int block_no)
{
	struct nand_chip *chip = dev_to_chip(dev);

	RAW_U8 result;
	
	result = k9f_block_erase(block_no);

	if (result == 0x0)
		return YAFFS_FAIL;

	
		return YAFFS_OK;
	
}

static int yaffs_nand_drv_MarkBad(struct yaffs_dev *dev, int block_no)
{
	RAW_U8 result;
	
	struct nand_chip *chip = dev_to_chip(dev);
	u8 *buffer = dev_to_buffer(dev);

	result = k9f_random_write(block_no * 64, 2054, 0xA7);

	if (result == 0x0) {
		
		return YAFFS_FAIL;
	}

	return YAFFS_OK;
	
}

static int yaffs_nand_drv_CheckBad(struct yaffs_dev *dev, int block_no)
{
	RAW_U8 check_data;
	RAW_U8 factory_result;
	
	struct nand_chip *chip = dev_to_chip(dev);
	u8 *buffer = dev_to_buffer(dev);

	
	factory_result = k9f_bad_block_check_product(block_no);

	if (factory_result == 0) {
		
		return YAFFS_FAIL;

	}

	check_data = k9f_random_read(block_no * 64, 2054);

	if (check_data == 0xA7) {
		
		return YAFFS_FAIL;

	}
	
	return YAFFS_OK;

}

static int yaffs_nand_drv_Initialise(struct yaffs_dev *dev)
{
	struct nand_chip *chip = dev_to_chip(dev);

	(void)chip;
	return YAFFS_OK;
}

static int yaffs_nand_drv_Deinitialise(struct yaffs_dev *dev)
{
	struct nand_chip *chip = dev_to_chip(dev);

	(void) chip;
	return YAFFS_OK;
}


int yaffs_nand_install_drv(struct yaffs_dev *dev, struct nand_chip *chip)
{
	struct yaffs_driver *drv = &dev->drv;

	drv->drv_write_chunk_fn = yaffs_nand_drv_WriteChunk;
	drv->drv_read_chunk_fn = yaffs_nand_drv_ReadChunk;
	drv->drv_erase_fn = yaffs_nand_drv_EraseBlock;
	drv->drv_mark_bad_fn = yaffs_nand_drv_MarkBad;
	drv->drv_check_bad_fn = yaffs_nand_drv_CheckBad;
	drv->drv_initialise_fn = yaffs_nand_drv_Initialise;
	drv->drv_deinitialise_fn = yaffs_nand_drv_Deinitialise;

	
	return YAFFS_OK;

}

