
/*! @file Register access and power control routines of YACBA21 CMOS image sensor
*/
#include <string.h>
#include "board.h"
#include "lpc43xx_i2c0.h"
#include "camera.h"
#include "CamYACB21.h"
#include <raw_api.h>

union Cmb32b
{
	unsigned char ui8Buf[4];
	unsigned int ui32Buf[1];
};

// #define OV7670  0 

extern volatile unsigned int g_250usTick;
extern DS_CamCB IRAM1_SECT s_ccb;

void DelayTicks(unsigned int cnt)
{
	unsigned int tick;
	tick = g_250usTick;
	while (g_250usTick - tick < cnt);
}

uint8_t ov7670_read_reg(uint8_t Addr, uint8_t *Data)
{
	uint8_t  writeBuf[3];
	
#ifdef OV7670
	
	writeBuf[0] = 0x42;
	writeBuf[1] = Addr;
	I2CXfer(writeBuf, 2, 0, 0);
	
	writeBuf[0] = 0x43;
	I2CXfer(writeBuf, 1, Data, 1);
	
	
	return 1;	
#endif
	
#ifdef OV9650	
	
	writeBuf[0] = 0x60;
	writeBuf[1] = Addr;
	I2CXfer(writeBuf, 2, 0, 0);
	
	writeBuf[0] = 0x61;
	I2CXfer(writeBuf, 1, Data, 1);
	return 1;

#endif
}

uint8_t ov7670_write_reg(uint8_t Addr, uint8_t Data)
{
	uint8_t  writeBuf[3];
	
#ifdef OV7670
	
	writeBuf[0] = 0x42;
	writeBuf[1] = Addr;
	writeBuf[2] = Data;		
	
	I2CXfer(writeBuf, 3, 0, 0);
	return 1;	

#endif

#ifdef OV9650	
	writeBuf[0] = 0x60;
	writeBuf[1] = Addr;
	writeBuf[2] = Data;		
	
	I2CXfer(writeBuf, 3, 0, 0);	
	return 1;
#endif
}

// Gate MCLK, 0 = Off, 1 = On
void YACBprm_MCLKCtl(unsigned int isOn)
{
	LPC_CGU_T2 *pCGU = LPC_CGU2;
	if (isOn)
		pCGU->IDIVC_CTRL &= ~1;
	else
		pCGU->IDIVC_CTRL |= 1;
}

// power down sequence
void YACBprm_PwrDnSeq(void)
{
	YACB_CHIP_ENB_L;
	raw_sleep(1);
	YACBprm_MCLKCtl(MCLK_OFF);
	raw_sleep(1);
	YACB_LDO_EN_L;
	raw_sleep(1);
}

// Power on sequence
void YACBprm_PwrOnSeq(void)
{
#ifdef OV7670
	YACBprm_PwrDnSeq();					// 1st power down camera module
	DelayTicks(MS2TICK(30));
	YACB_LDO_EN_H;							// 2nd pull reset high for OV7670
	DelayTicks(3);
	
	YACB_CHIP_ENB_H;						// 2nd entern power down mode if OV7670
	DelayTicks(MS2TICK(1+1));
	YACBprm_MCLKCtl(MCLK_ON);		// 3nd Open MCLK
	DelayTicks(MS2TICK(1+1));
	YACB_LDO_EN_L;
	DelayTicks(MS2TICK(10+1));
	
#endif
	
#ifdef OV9650	

	YACBprm_MCLKCtl(MCLK_ON);
	raw_sleep(10);
	
	/*power enable*/
	YACB_LDO_EN_L; 
	raw_sleep(10);

	/*reset 9650*/
	YACB_CHIP_ENB_H;
	raw_sleep(10);
	YACB_CHIP_ENB_L;
	raw_sleep(20);

#endif
}


//! 2D array presentation of pin muxing
typedef struct
{
	volatile unsigned int a2d[26][32];
}DS_PinMux2D;


void YACBprm_MCUCfg(void)
{
	DS_PinMux2D *pm = (DS_PinMux2D*) LPC_SCU;
	LPC_CGU_T2 *pCGU = LPC_CGU2;
	LPC_GPIO_T2 *pGP = LPC_GPIO_PORT2;
	unsigned int pllM;
	unsigned int t1;
	
	//  设置一些系统时钟及管脚功能

	/*CGU_OUT1 XCLK config*/
	LPC_SCU2->SFSPA_0							= (0x01<<7)|(0x00<<6)|(0x01<<5)|(0x01<<4)|(0x00<<3)|(0x06<<0);
	/*choose clock  idivc*/
	LPC_CGU2->BASE_CGU_OUT1_CLK					= (0x0E<<24)|(0x00<<11)|(0x00<<0);    
	
	
	// 选择引脚功能
	//pm->a2d[SPT_MCLK][SPN_MCLK]					= ALT_MCLK | SPNCFG_OUT_HSPD;	
	pm->a2d[SPT_LDO_EN][SPN_LDO_EN] 		= ALT_LDO_EN| SPNCFG_OUT_GNRC | /* 20mA drive */ 3UL<<8;
// 	pm->a2d[SPT_FLHLIGHT][SPN_FLHLIGHT] = ALT_FLHLIGHT | SPNCFG_OUT_GNRC | /* 20mA drive */ 3UL<<8;
	pm->a2d[SPT_CHIP_ENB][SPN_CHIP_ENB] = ALT_CHIP_ENB | SPNCFG_OUT_GNRC;
	pm->a2d[SPT_VSYNC][SPN_VSYNC] 			= ALT_VSYNC | SPNCFG_IN_PUP;      
	pm->a2d[SPT_HSYNC][SPN_HSYNC] 			= ALT_HSYNC | SPNCFG_IN_PUP;
	pm->a2d[SPT_PXCLK][SPN_PXCLK] 			= ALT_PXCLK | SPNCFG_IN_PUP;
	
	//! D0-D7, must be pin 0-7 within the same GPIO port, they share the same configuration
	t1 = ALT_IMGD0 | SPNCFG_IN_GNRC;
	pm->a2d[SPT_IMGD0][SPN_IMGD0+0] 		= t1;       // 设置摄像头数据输入管脚，即管脚功能
	pm->a2d[SPT_IMGD0][SPN_IMGD0+1] 		= t1;
	pm->a2d[SPT_IMGD0][SPN_IMGD0+2] 		= t1;
	pm->a2d[SPT_IMGD0][SPN_IMGD0+3] 		= t1;
	pm->a2d[SPT_IMGD0][SPN_IMGD0+4] 		= t1;
	pm->a2d[SPT_IMGD0][SPN_IMGD0+5] 		= t1;
	pm->a2d[SPT_IMGD0][SPN_IMGD0+6] 		= t1;
	pm->a2d[SPT_IMGD0][SPN_IMGD0+7] 		= t1;	
	
	pllM = (pCGU->PLL1_CTRL >> 16 & 0xFF) + 1;     // 得到外设的频率
    printf("pllM  is %d\r\n", pllM);
	
#if (COLOR_CHOOSE == GRAY)                 //  选择给摄像头XCLK输出的频率
	pCGU->IDIVC_CTRL =            //
	//	Disable	| Xtal=24MHz,MCLK 24MHz	| AutoBlock	| ClkSrc=PLL1
		1UL<<0	|  (pllM/2-1) << 2		| 1UL<<11	| 9UL<<24;        // 当黑白时调节XCLK输出频率
#else
	/*actually IDIVC frequency is 180M /7 = 25M*/ 
	pCGU->IDIVC_CTRL =
	//	Disable	| Xtal=24MHz,MCLK 24MHz	| AutoBlock	| ClkSrc=PLL1      
		1UL<<0	| (pllM/2-1) << 2		| 1UL<<11	| 9UL<<24;            // 当彩色时调节XCLK输出频率
#endif
//     pllm1 = pCGU->IDIVC_CTRL;
//     shellPrint("pllM %x \r\n",pllM);
// 	shellPrint("pllM1 %x \r\n",pllm1);

	//! Configure misc GPIO pins     //  设置控制摄像头的一些GPIO
	pGP->DIR[GPT_LDO_EN] 			|= 1UL<<GPN_LDO_EN;              // 电源控制脚          
	pGP->DIR[GPT_CHIP_ENB] 		|= 1UL<<GPN_CHIP_ENB;               //  片选脚
	pGP->DIR[GPT_FLHLIGHT] 		|= 1UL<<GPN_FLHLIGHT;
	pGP->DIR[GPT_IMGD0] 			&= ~(0xFFUL<<GPN_IMGD0);

// 	//! Turn off flash light
	pGP->CLR[GPT_FLHLIGHT] = 1UL<<GPN_FLHLIGHT;
}

extern US_YACBRegs s_regs;

void ov7670_init(void)
{
	
	//for(i=0;i<10000;i++);
	
#ifdef OV7670
	                              //   640*480
	ov7670_write_reg(0x12,0x04);        // 控制图像输出格式是VGA还是QVGA，我们现在用的VGA的
	ov7670_write_reg(0x11,0x03);         //  控制输出图像帧率
	ov7670_write_reg(0x3a, 0x04);
	#if (COLOR_CHOOSE == GRAY)
	{
		ov7670_write_reg(0x40, 0xc0);          //  黑白
	}
	#else
	{
// 		while(1);
		ov7670_write_reg(0x40, 0xd0);          //  彩色
	}
	#endif

	ov7670_write_reg(0x17, 0x14);
	ov7670_write_reg(0x18, 0x02);
	ov7670_write_reg(0x32, 0xb7);
	ov7670_write_reg(0x19, 0x02);
	ov7670_write_reg(0x1a, 0x7a);
	ov7670_write_reg(0x03, 0x00);
	
// 	ov7670_write_reg(0x15, 0x04); 
	
	ov7670_write_reg(0x3e, 0x10);
	ov7670_write_reg(0x70, 0x3a);
	ov7670_write_reg(0x71, 0x35);
	ov7670_write_reg( 0x72, 0x11);
	ov7670_write_reg( 0x73, 0xf0);
	ov7670_write_reg( 0xa2, 0x01);
	ov7670_write_reg(0x13, 0xe0);
	ov7670_write_reg( 0x00, 0x00);
	ov7670_write_reg( 0x10, 0x00);
	ov7670_write_reg( 0x0d, 0x40);
	ov7670_write_reg( 0x14, 0x38);
	ov7670_write_reg( 0xa5 ,0x05);
	ov7670_write_reg( 0xab, 0x07);
	ov7670_write_reg( 0x24, 0x95);
	ov7670_write_reg(0x25, 0x33);
	ov7670_write_reg( 0x26, 0xe3);
	ov7670_write_reg( 0x9f, 0x78);
	ov7670_write_reg( 0xa0, 0x68);
	ov7670_write_reg( 0xa1, 0x0b);
	ov7670_write_reg(0xa6, 0xd8);
	ov7670_write_reg(0xa7, 0xd8);
	ov7670_write_reg(0xa8, 0xf0);
	ov7670_write_reg(0xa9, 0x90);
	ov7670_write_reg(0xaa, 0x94);
	ov7670_write_reg(0x13, 0xe5);
	ov7670_write_reg(0x0e, 0x61);
	ov7670_write_reg(0x0f, 0x4b);
	ov7670_write_reg(0x16, 0x02);
	ov7670_write_reg(0x1e, 0x07 );
	ov7670_write_reg(0x21, 0x02);
	ov7670_write_reg(0x22, 0x91);
	ov7670_write_reg(0x29, 0x07);
	ov7670_write_reg(0x33, 0x0b);         // ;03
	ov7670_write_reg(0x35, 0x0b);
	ov7670_write_reg(0x37, 0x1d);         // ;1c
	ov7670_write_reg(0x38, 0x71);
	ov7670_write_reg(0x39, 0x2a);
	ov7670_write_reg(0x3c, 0x78);
	ov7670_write_reg(0x3d, 0x08);
	ov7670_write_reg(0x41, 0x3a);
	ov7670_write_reg(0x4d, 0x40);
	ov7670_write_reg(0x4e, 0x20);
	ov7670_write_reg(0x69, 0x00);       // ;55
	ov7670_write_reg(0x6b, 0x4a);
	ov7670_write_reg(0x74, 0x19);
	ov7670_write_reg(0x76, 0x61);
	ov7670_write_reg(0x8d, 0x4f);
	ov7670_write_reg(0x8e, 0x00);
	ov7670_write_reg(0x8f, 0x00);
	ov7670_write_reg(0x90, 0x00);
	ov7670_write_reg(0x91, 0x00);
	ov7670_write_reg(0x96,0x00);
	ov7670_write_reg(0x9a, 0x80);
	ov7670_write_reg(0xb0, 0x84 );      //;8c
	ov7670_write_reg(0xb1, 0x0c);
	ov7670_write_reg(0xb2, 0x0e);
	ov7670_write_reg(0xb3, 0x82);
	ov7670_write_reg(0xb8, 0x0a);
	ov7670_write_reg(0x43, 0x14);
	ov7670_write_reg(0x44, 0xf0);
	ov7670_write_reg(0x45, 0x34);
	ov7670_write_reg(0x46, 0x58);
	ov7670_write_reg(0x47, 0x28);
	ov7670_write_reg(0x48, 0x3a);
	ov7670_write_reg(0x59, 0x88);
	ov7670_write_reg(0x5a, 0x88);
	ov7670_write_reg(0x5b, 0x44);
	ov7670_write_reg(0x5c, 0x67);
	ov7670_write_reg(0x5d, 0x49);
	ov7670_write_reg(0x5e, 0x0e);
	ov7670_write_reg(0x6c, 0x0a);
	ov7670_write_reg(0x6d, 0x55);
	ov7670_write_reg(0x6e, 0x11);
	ov7670_write_reg(0x6f, 0x9f);
	ov7670_write_reg(0x6a, 0x40);
	ov7670_write_reg(0x01, 0x40);
	ov7670_write_reg(0x02, 0x40);
	ov7670_write_reg(0x13, 0xe7);
	ov7670_write_reg(0x34, 0x11);
	ov7670_write_reg(0x3b, 0x0a);   // 02
	ov7670_write_reg(0xa4, 0x88);
	ov7670_write_reg(0x96 ,0x00);
	ov7670_write_reg(0x97, 0x30);
	ov7670_write_reg(0x98, 0x20);
	ov7670_write_reg(0x99, 0x20);
	ov7670_write_reg(0x9a, 0x84);
	ov7670_write_reg(0x9b, 0x29);
	ov7670_write_reg(0x9c, 0x03);
	ov7670_write_reg(0x9d, 0x4c);
	ov7670_write_reg(0x9e, 0x3f);
	ov7670_write_reg(0x78, 0x04);
	ov7670_write_reg(0x79, 0x01);
	ov7670_write_reg(0xc8, 0xf0);
	ov7670_write_reg(0x79, 0x0f);
	ov7670_write_reg(0xc8, 0x00 );//;20
	ov7670_write_reg(0x79, 0x10);
	ov7670_write_reg(0xc8, 0x7e);
	ov7670_write_reg(0x79, 0x0a);
	ov7670_write_reg(0xc8, 0x80);
	ov7670_write_reg(0x79, 0x0b);
	ov7670_write_reg(0xc8, 0x01);
	ov7670_write_reg(0x79, 0x0c);
	ov7670_write_reg(0xc8, 0x0f);// ;07
	ov7670_write_reg(0x79, 0x0d);
	ov7670_write_reg(0xc8, 0x20);
	ov7670_write_reg(0x79, 0x09);
	ov7670_write_reg(0xc8, 0x80);
	ov7670_write_reg(0x79, 0x02);
	ov7670_write_reg(0xc8, 0xc0);
	ov7670_write_reg(0x79, 0x03);
	ov7670_write_reg(0xc8, 0x40);
	ov7670_write_reg(0x79, 0x05);
	ov7670_write_reg(0xc8, 0x30);
	ov7670_write_reg(0x79, 0x26);

#endif

#ifdef OV9650

	ov7670_write_reg(0x12,0x80);        // 这个地方要写两遍现在还不知道为什么，
	ov7670_write_reg(0x12,0x80);        // 这个地方要写两遍现在还不知道为什么，

	raw_sleep(1);
	
	//for(i=0;i<400000;i++);
	// delay(100000);

	// ov7670_write_reg(0x12,0x44);
	ov7670_write_reg(0x11,0x00);        //  参数较重要，它和0x12寄存器共同控制每秒的图片的帧率，
	ov7670_write_reg(0x3b,0x00);        //  
	ov7670_write_reg(0x13,0xef);
	ov7670_write_reg(0x01,0x00);
	ov7670_write_reg(0x02,0xff);
	ov7670_write_reg(0x00,0xf9);        
	ov7670_write_reg(0x10,0x00);        
	ov7670_write_reg(0x13,0xe7);
	
	ov7670_write_reg(0x6b,0x0a);		 //  控制帧率   15
	ov7670_write_reg(0x39,0x43); 
	ov7670_write_reg(0x38,0x12); 
	ov7670_write_reg(0x35,0x91);
	ov7670_write_reg(0x2a,0x00);
	ov7670_write_reg(0x2b,0x00);
    ov7670_write_reg(0x6a,0x3e);	
 
// 	ov7670_write_reg(0x6b,0x0a);		//  控制帧率    30
// 	ov7670_write_reg(0x39,0x50); 
// 	ov7670_write_reg(0x38,0x92); 
// 	ov7670_write_reg(0x35,0x81);
// 	ov7670_write_reg(0x2a,0x00);
// 	ov7670_write_reg(0x2b,0x00);
//     ov7670_write_reg(0x6a,0x7d);
	
	ov7670_write_reg(0x37,0x00);
	ov7670_write_reg(0x0e,0xa0);
	ov7670_write_reg(0xa8,0x80);
	ov7670_write_reg(0x12,0x44);
	ov7670_write_reg(0x04,0x00);
	ov7670_write_reg(0x0c,0x04);
	ov7670_write_reg(0x0d,0x80);

// 	 ov7670_write_reg(0x18,0xc4);   //
// 	 ov7670_write_reg(0x17,0x24);
// 	 ov7670_write_reg(0x32,0xad);
// 	 ov7670_write_reg(0x03,0x36);
// 	 ov7670_write_reg(0x1a,0x3d);
// 	 ov7670_write_reg(0x19,0x00);

	ov7670_write_reg(0x17, 0x24);    //    正常
	ov7670_write_reg(0x18, 0x00);
	ov7670_write_reg(0x32, 0xb6);
	ov7670_write_reg(0x19, 0x00);
	ov7670_write_reg(0x1a, 0xff);
	ov7670_write_reg(0x03, 0x36);

	ov7670_write_reg(0x14,0x2e);
	ov7670_write_reg(0x15,0x00);
	ov7670_write_reg(0x1b,0x00);
	ov7670_write_reg(0x16,0x07);
	ov7670_write_reg(0x33,0xe2); 
	ov7670_write_reg(0x34,0xbf);
	ov7670_write_reg(0x41,0x02);
	ov7670_write_reg(0x96,0x04);
	ov7670_write_reg(0x3c,0x00);
	ov7670_write_reg(0x8f,0xdf);
	ov7670_write_reg(0x8b,0x00);
	ov7670_write_reg(0x8c,0x23);

	// ov7670_write_reg(0x8d,0x02);

	ov7670_write_reg(0x94,0x88);
	ov7670_write_reg(0x95,0x88);
#if (COLOR_CHOOSE == GRAY)
	ov7670_write_reg(0x40,0xc0);     // 黑白
#else 
	ov7670_write_reg(0x40,0xd0);     // 彩色
#endif
	ov7670_write_reg(0x29,0x3f); 
	ov7670_write_reg(0x0f,0x00);
	ov7670_write_reg(0xa5,0xd9);
	ov7670_write_reg(0x1e,0x00);
	ov7670_write_reg(0xa9,0xb8);
	ov7670_write_reg(0xaa,0x92);
	ov7670_write_reg(0xab,0x0a);
	ov7670_write_reg(0x90,0x00);
	ov7670_write_reg(0x91,0xff);
	ov7670_write_reg(0x9f,0x00);
	ov7670_write_reg(0xa0,0x00);
	ov7670_write_reg(0x24,0x68);
	ov7670_write_reg(0x25,0x5c);
	ov7670_write_reg(0x26,0xc3);

	ov7670_write_reg(0x3d,0x00);
	ov7670_write_reg(0x69,0x40);
	
	ov7670_write_reg(0x3a,0x0d);
	
	
	ov7670_write_reg(0x8e,0x00);
	ov7670_write_reg(0x5c,0xb9);
	ov7670_write_reg(0x5d,0x96);
	ov7670_write_reg(0x5e,0x10);
	ov7670_write_reg(0x59,0xc0);
	ov7670_write_reg(0x5a,0xaf);
	ov7670_write_reg(0x5b,0x55);
	ov7670_write_reg(0x43,0xf0);
	ov7670_write_reg(0x44,0x10);
	ov7670_write_reg(0x45,0x68);
	ov7670_write_reg(0x46,0x96);
	ov7670_write_reg(0x47,0x60);
	ov7670_write_reg(0x48,0x80);
	ov7670_write_reg(0x5f,0xe0);
	ov7670_write_reg(0x60,0x8c); 
	ov7670_write_reg(0x61,0x20);
	ov7670_write_reg(0xa4,0x74);
	ov7670_write_reg(0x8d,0x02);
	ov7670_write_reg(0x13,0xe7);
	ov7670_write_reg(0x4f,0xb7);
	ov7670_write_reg(0x50,0x2e);
	ov7670_write_reg(0x51,0x19);
	ov7670_write_reg(0x52,0x1f);
	ov7670_write_reg(0x53,0xb1);
	ov7670_write_reg(0x54,0x12);
	ov7670_write_reg(0x55,0x06);
	ov7670_write_reg(0x56,0x55);
	ov7670_write_reg(0x57,0xfb);
	ov7670_write_reg(0x58,0x77);

	ov7670_write_reg(0x3e,0x02);
	
#endif
}


void YACBprm_Tim(void);

extern const unsigned char szASC32[];
extern const unsigned char szASC16[];
extern const unsigned char szASC24[];
extern const unsigned char szASC12[];

#define LCD_FONT_TYPE     16

signed int YACB_Init(void)
{
	uint8_t id[4];
	int ret = 0;
	
	memset(&s_ccb, 0, sizeof(s_ccb));

	YACBprm_MCUCfg();
	raw_sleep(1);
	YACBprm_PwrOnSeq();
		
	//DelayTicks(1);
	raw_sleep(10);
		
	ov7670_read_reg(0x0A, &id[0]);		// 0x76     
	ov7670_read_reg(0x0B, &id[1]);		// 0x73
	ov7670_read_reg(0x1C, &id[2]);		// 0x7F
	ov7670_read_reg(0x1D, &id[3]);		// 0xA2

// 	shellPrint(" id[0] %x,%x, \r\n",id[0],id[1]);
// 	
// 	shellPrint(" id[2] %x,%x, \r\n",id[2],id[3]);

#ifdef OV7670                         // 判断用的那个摄像头
	if (id[0] != 0x76)             // 读摄像头地址0X0A的值正确的话OV7676应为OX76  
	{
		#ifdef LCD_MODEL_AT043TN24
		//LCDPrint(20, 140, LCD_FONT_TYPE, "Camera is not OV7670, please check the camera connection.");
		#endif
		
		#ifdef LCD_MODEL_AT070TN92
		//LCDPrint(130, 260, LCD_FONT_TYPE, "Camera is not OV7670, please check the camera connection is correct.");
		#endif
		
		while(1);
	}
#endif
	
#ifdef OV9650 
	
	if (id[0] != 0x96)             // 读摄像头地址0X0A的值正确的话OV7676应为OX96 
	{
		#ifdef LCD_MODEL_AT043TN24
		//LCDPrint(20, 140, LCD_FONT_TYPE, "Camera is not OV9650, please check the camera connection.");
		#endif
		
		#ifdef LCD_MODEL_AT070TN92
		//LCDPrint(130, 260, LCD_FONT_TYPE, "Camera is not OV9650, please check the camera connection is correct.");
		#endif
		
		while(1);
	}		
#endif	
	
	ov7670_init();                //  设置摄像头寄存值（比较重要�
	
	YACBprm_Tim();

	return ret;
}


/*! @brief Initialize for next DMA xfer, data flow from GPIO to SDRAM
*
* @ Use of GPDMA is critical for this application to offload CPU. GPDMA has 2 bus masters,
* #0 master can only do memory xfer, while #1 master can do both memory and peripheral xfer
* unlike memory xfer, peripheral xfer do only 1 bust per request, while the former stops only all done
* so we need to use peripheral -> memory xfer, and give #1 master to source, #0 master to dest (SDRAM).
*/
void _prvInitCamDMAXferDir(unsigned long dstAddr)
{
	s_ccb.pCHN->CONFIG =             //  在启用前在配置DMA 通道配置寄存器 见第17章
	//  Dis		| SrcPerp=SCT			 | DstRAM | P->M 	| XferDoneIrqEn
		0UL<<0  | YACB_SCTDMAPERIPNUM<<1 | 0UL<<6 | 2UL<<11 | 1UL<<15;	
	
	
	s_ccb.pCHN->SRCADDR = (unsigned int )(LPC_GPIO_PORT->PIN + GPT_IMGD0);    // 设置DMA1的源地址
	s_ccb.pCHN->DSTADDR = dstAddr;                                            // 设置DMA1的目的地址
	s_ccb.pCHN->CONTROL = 
	//	size   				| SBurst=1| DBurst=8| SWidth=8| DWidth=32| SMstr=1 | DMstr=0 | !SrcInc | DstInc  | Prvlg   | B,C	  | EnInt
		2*CAMERA_X_SIZE<<0 	| 0UL<<12 | 2UL<<15 | 0UL<<18 | 2UL<<21  | 1UL<<24 | 0UL<<25 | 0UL<<26 | 1UL<<27 | 1UL<<28 | 1UL<<29  | 1UL<<31;
	// 设置DMA传输的字节长度(只能大不能小但不能太大不能大于7？)|源地址联发大小传输量数据为这里只能为1 | 目的地址联发传输量数据为8字节 | 源传输宽度为1字节 | 目的传输字节为4字节  
    //  |源选择AHB主机1  | 目的选择AHB主机0 （因为仅主机1可以访问外设主机0仅可访问存储器） | 每次传输后源地址不增加 |  每次传输后目标地址增加 |  访问可以缓冲  | 终结计数中断使能
    
    s_ccb.pCHN->LLI = 0; //(unsigned int) s_ccb.lliAry[0].pNext;
	
	s_ccb.pCHN->CONFIG |= 1UL<<0;	// 使能DMA通道
}

/*! @brief SCT IRQ handler, mainly for initializing the next DMA trnafer
*
* Respond to events:
*
*		HSync falling edge ( prepare for next DMA xfer)
*		VSync rising edge ( reset rx buffer and prepare for next (1st) DMA xfer)
*/

void ATR_RAMCODE SCT_IRQHandler(void)             //  有事件过来发生的中断  
{
	unsigned int flgs;

	LPC_SCT_T *pSCT = LPC_SCT;

	flgs = pSCT->EVFLAG;                   //  得到那个产生中断了
	pSCT->EVFLAG = pSCT->EVFLAG;           //  清中断
	
	if (flgs & 1UL<<ctev_hsyncFall)        //  行信号的下降沿
	{
        _prvInitCamDMAXferDir((unsigned long)s_ccb.lineBuf);		   //  一行到了用DMA去读写一行所存的数据

        s_ccb.y++;

        CAMERA_lineData(s_ccb.y, s_ccb.lineBuf);
	}
	
	if (flgs & 1UL<<ctev_vsyncRise)      //  场信号的上升沿
	{
		s_ccb.y = 0;

		_prvInitCamDMAXferDir((unsigned long)s_ccb.lineBuf);		   //  一行到了用DMA去读写一行所存的数据

        CAMERA_frameComplete();
	}
}

void CAMERA_init(void)
{
	YACB_Init();         //  初始化摄像头
}
