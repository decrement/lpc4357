
#include <stdio.h>
#include "board.h"
#include "lpc43xx_ssp1.h"
#include "lcdtouch.h"
#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h"
#include <raw_api.h>


/*
7  6 - 4  3      2     1-0
s  A2-A0 MODE SER/DFR PD1-PD0
*/
#define TOUCH_MSR_X  0xD4   //��Y������ָ��
#define TOUCH_MSR_Y  0x94   //��X������ָ��

#define	TS_SAMPLE_COUNT	8

#define TP_CS_LOW()          GPIO_ClearValue(7,1<<19);
#define TP_CS_HIGH()         GPIO_SetValue(7,1<<19);	

RAW_S32 g_touchXSize, g_touchYSize;

static	RAW_S32 ad_buf_x[TS_SAMPLE_COUNT];
static	RAW_S32 ad_buf_y[TS_SAMPLE_COUNT];


static uint8_t SPI_WriteByte(unsigned char data)			  
{
    return SSP1_SendRecvByte(data);		
}


int pen_is_down(void)
{
	int level;
	int i;
	
	for(i = 0; i < 8; i++) {

		level = Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 8);

		if (level == 1) {

			return 0;
		}

	}
	
	return 1;
}

						

void touch_init(RAW_S32 x_size, RAW_S32 y_size)
{
    g_touchXSize = x_size;
	g_touchYSize = y_size;
	
	spi1_master_init();

	scu_pinmux(0xF,5,MD_PLN_FAST,FUNC4);	// Pf.5 as GPIO7[19]	 ssp_cs pin	
	GPIO_SetDir(7,1<<19, 1);			   //P3.6 as GPIO0[6]	 ssp_cs pin	  output
	GPIO_SetValue(7,1<<19);		//cs as high

	Chip_SCU_PinMuxSet(0x7, 0, FUNC0 | MD_PLN_FAST);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 8);
	
}


static	void InsertSort(int *A, int p, int r)
{
    int i,j;
    int key;
    for(i=p+1; i<=r; i++)
    {
        key = A[i];
        j = i-1;
        while (j >= 0 && A[j] > key)
        {
            A[j+1] = A[j];
            j--;
        }
        A[j+1] = key;
    }
}


static void __touch_x_y_get(RAW_S32 *x, RAW_S32 *y)
{
    RAW_S32 xPos, yPos;

    TP_CS_LOW();
	
    SPI_WriteByte(TOUCH_MSR_X);                /* read X */
    xPos = (SPI_WriteByte(0x00)&0x7F)<<5;     /* read MSB bit[11:8] */
    xPos |= SPI_WriteByte(TOUCH_MSR_Y)>>3;    /* read LSB bit[7:0] and prepare read Y */
    yPos = (SPI_WriteByte(0x00)&0x7F)<<5;     /* read MSB bit[11:8] */
    yPos |= SPI_WriteByte(0x00)>>3;           /* read LSB bit[7:0] */
   
    TP_CS_HIGH();	
				
    *x = xPos ;
    *y = yPos ; 
}

#define TOUCH_SAMPLE_ERROR  200

void touch_x_y_raw_get(RAW_S32 *x, RAW_S32 *y)
{
	__touch_x_y_get(x, y);
}


RAW_S32 touch_x_y_get(RAW_S32 *x, RAW_S32 *y)
{
	volatile RAW_S32 i, j;
	RAW_S32 date_temp_x;
	RAW_S32 date_temp_y;
	
	/*if pen is not down just return -1*/
	if (pen_is_down() == 0) {

		*x = -1;
		*y = -1;
		
		return -1;
	}

	for(i=0; i<TS_SAMPLE_COUNT; i++) {

		/*some delay between conversion*/
		for(j=200;j>0;j--);
		
		__touch_x_y_get(&ad_buf_x[i], &ad_buf_y[i]);

	}

	/*to check again make sure the conversion data is valid*/
	if (pen_is_down() == 0) {

		*x = -1;
		*y = -1;
		
		return -1;
	}

	InsertSort(ad_buf_x, 0, TS_SAMPLE_COUNT - 1);
	i = TS_SAMPLE_COUNT >> 1;
	date_temp_x	= 0;
	date_temp_x += ad_buf_x[i++];
	date_temp_x += ad_buf_x[i++];		
	date_temp_x	= date_temp_x >> 1;
	*x = date_temp_x;

	if (abs(ad_buf_x[TS_SAMPLE_COUNT - 1] - ad_buf_x[0]) > TOUCH_SAMPLE_ERROR) {

		//*x = -1;
		//*y = -1;
		
		//return -1;
	}

	InsertSort(ad_buf_y, 0, TS_SAMPLE_COUNT - 1);
	i = TS_SAMPLE_COUNT >> 1;
	date_temp_y	= 0;
	date_temp_y += ad_buf_y[i++];
	date_temp_y += ad_buf_y[i++];		
	date_temp_y	= date_temp_y >> 1;
	*y = date_temp_y;

	if (abs(ad_buf_y[TS_SAMPLE_COUNT - 1] - ad_buf_y[0]) > TOUCH_SAMPLE_ERROR) {

		//*x = -1;
		//*y = -1;
		
		//return -1;
	}

	return 0;

}


RAW_S32 g_touchCalA[7];

#if 0
void touchGetPixelByPhy(unsigned short xPhy, unsigned short yPhy,unsigned short * xPixel, unsigned short * yPixel)
{
	  unsigned short xLog, yLog;
	  
	  xLog = (g_touchCalA[2] + g_touchCalA[0]*xPhy + g_touchCalA[1]*yPhy ) / g_touchCalA[6];
	
	  yLog = (g_touchCalA[5] + g_touchCalA[3]*xPhy + g_touchCalA[4]*yPhy ) / g_touchCalA[6]; 
	  
	  *xPixel =  xLog;

	  *yPixel = yLog;
}

#else 

RAW_S32 touch_phy_to_pixsel(RAW_S32 x_phy, RAW_S32 y_phy, RAW_S32 *x_pixsel, RAW_S32 *y_pixsel)
{
	RAW_S32 xLog, yLog;

	xLog = (g_touchCalA[0] + g_touchCalA[1]*x_phy + g_touchCalA[2]*y_phy ) / g_touchCalA[6];

	yLog = (g_touchCalA[3] + g_touchCalA[4]*x_phy + g_touchCalA[5]*y_phy ) / g_touchCalA[6]; 

	*x_pixsel = xLog;

	*y_pixsel = yLog;

	//printf("%d %d \r\n", *x_pixsel, *y_pixsel);

	if ((*y_pixsel < 0) || (*x_pixsel < 0) || (*x_pixsel > g_touchXSize) || (*y_pixsel > g_touchYSize)) {
		
		return -1;
	}

	return 0;
	
}

#endif



