#include <raw_api.h>
#include <mm/raw_malloc.h>
#include <mm/raw_page.h>
#include <string>


/* Normal operator new. */
void *operator new(std::size_t size) throw(std::bad_alloc)
{

	return raw_malloc(size);

}

/* Normal operator delete. */
void operator delete(void* free_ptr) throw ()
{

	raw_free(free_ptr);

}


/* Nothrow version of operator new. */
void *operator new(std::size_t size, const std::nothrow_t& nt) throw()
{

	return raw_malloc(size);

}

/* Nothrow version of operator delete. */
void operator delete(void* free_ptr, const std::nothrow_t& nt) throw()
{

	raw_free(free_ptr);

}



/* Array new. */
void *operator new[](std::size_t size) throw(std::bad_alloc)
{

	 return raw_malloc(size);

}


/* Array delete. */
void operator delete[](void* free_ptr) throw ()
{

	raw_free(free_ptr);

}


/* Nothrow version of array new. */
void *operator new[](std::size_t size,
                     const std::nothrow_t& nt) throw()
{

	 return raw_malloc(size);

}


/* Nothrow version of array delete. */
void operator delete[](void* free_ptr,
                       const std::nothrow_t& nt) throw()
{

	raw_free(free_ptr);

}


