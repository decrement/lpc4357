#include <string>
#include <stdio.h>

using namespace std;


class Rectangle {
    int width, height;
  public:
    void set_values (int,int);
    int area() {return width*height;}
};

void Rectangle::set_values (int x, int y) {
  width = x;
  height = y;
}



template<class T> class A{
    public:
        T g(T a,T b);
        A();
};


template<class T> A<T>::A(){}

template<class T> T A<T>::g(T a,T b){
    return a+b;
}



extern "C" void ff2();


void ff2()

{
	int *p;

	Rectangle *r;
	Rectangle rect;
	A<int> a;
	
	rect.set_values (3,4);
	printf("area:  is %d\r\n",rect.area());

	p= new  int[0x200];
	p[0]= 0x11;
	delete p;
	
	r =  new (std::nothrow) Rectangle();
	delete p;
	
	p= new (std::nothrow) int[0x200];
	p[0]= 0x11;

	
	printf("p[0]  is %x\r\n", p[0]);

	printf("template is  %d\r\n", a.g(2, 3));

	delete p;

} 



