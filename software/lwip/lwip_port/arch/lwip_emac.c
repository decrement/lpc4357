/*
 * @brief LPC18xx/43xx Ethernet driver
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "chip.h"
#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h"
#include  <raw_api.h>




#define MYMAC_1         0x10            /* our ethernet (MAC) address        */
#define MYMAC_2         0x1F            /* (MUST be unique in LAN!)          */
#define MYMAC_3         0xE0
#define MYMAC_4         0x12
#define MYMAC_5         0x1E
#define MYMAC_6         0x0F


#define ETH_FRAG_SIZE		1536		
#define NUM_RX_DESC			10
#define NUM_TX_DESC			10
	

/*  Descriptors Fields bits       */
#define OWN_BIT				(1U<<31)	/*  Own bit in RDES0 & TDES0              */
#define RX_END_RING			(1<<15)		/*  Receive End of Ring bit in RDES1      */
#define RX_NXTDESC_FLAG		(1<<14)		/*  Second Address Chained bit in RDES1   */
#define TX_LAST_SEGM		(1<<29)		/*  Last Segment bit in TDES0             */
#define TX_FIRST_SEGM		(1<<28)		/*  First Segment bit in TDES0            */
#define TX_END_RING			(1<<21)		/*  Transmit End of Ring bit in TDES0     */
#define TX_NXTDESC_FLAG		(1<<20)		/*  Second Address Chained bit in TDES0   */


/* EMAC Control and Status bits   */
#define MAC_RX_ENABLE	 (1<<2)			/*  Receiver Enable in MAC_CONFIG reg      */
#define MAC_TX_ENABLE	 (1<<3)			/*  Transmitter Enable in MAC_CONFIG reg   */
#define MAC_PADCRC_STRIP (1<<7)			/*  Automatic Pad-CRC Stripping in MAC_CONFIG reg   */
#define MAC_DUPMODE		 (1<<11)		/*  Duplex Mode in  MAC_CONFIG reg         */
#define MAC_100MPS		 (1<<14)		/*  Speed is 100Mbps in MAC_CONFIG reg     */
#define MAC_PROMISCUOUS  (1U<<0)		/*  Promiscuous Mode bit in MAC_FRAME_FILTER reg    */
#define MAC_DIS_BROAD    (1U<<5)		/*  Disable Broadcast Frames bit in	MAC_FRAME_FILTER reg    */
#define MAC_RECEIVEALL   (1U<<31)       /*  Receive All bit in MAC_FRAME_FILTER reg    */
#define DMA_SOFT_RESET	  0x01          /*  Software Reset bit in DMA_BUS_MODE reg */
#define DMA_SS_RECEIVE   (1<<1)         /*  Start/Stop Receive bit in DMA_OP_MODE reg  */
#define DMA_SS_TRANSMIT  (1<<13)        /*  Start/Stop Transmission bit in DMA_OP_MODE reg  */
#define DMA_INT_TRANSMIT (1<<0)         /*  Transmit Interrupt Enable bit in DMA_INT_EN reg */
#define DMA_INT_OVERFLOW (1<<4)         /*  Overflow Interrupt Enable bit in DMA_INT_EN reg */ 
#define DMA_INT_UNDERFLW (1<<5)         /*  Underflow Interrupt Enable bit in DMA_INT_EN reg */
#define DMA_INT_RECEIVE  (1<<6)         /*  Receive Interrupt Enable bit in DMA_INT_EN reg */
#define DMA_INT_ABN_SUM  (1<<15)        /*  Abnormal Interrupt Summary Enable bit in DMA_INT_EN reg */
#define DMA_INT_NOR_SUM  (1<<16)        /*  Normal Interrupt Summary Enable bit in DMA_INT_EN reg */

/* MII Management Command Register */
#define GMII_READ           (0<<1)		/* GMII Read PHY                     */
#define GMII_WRITE          (1<<1)      /* GMII Write PHY                    */
#define GMII_BUSY           0x00000001  /* GMII is Busy / Start Read/Write   */
#define MII_WR_TOUT         0x00050000  /* MII Write timeout count           */
#define MII_RD_TOUT         0x00050000  /* MII Read timeout count            */

/* MII Management Address Register */
#define MADR_PHY_ADR        0x00001F00  /* PHY Address Mask                  */

/* DP83848C PHY Registers */
#define PHY_REG_BMCR        0x00        /* Basic Mode Control Register       */
#define PHY_REG_BMSR        0x01        /* Basic Mode Status Register        */
#define PHY_REG_IDR1        0x02        /* PHY Identifier 1                  */
#define PHY_REG_IDR2        0x03        /* PHY Identifier 2                  */
#define PHY_REG_ANAR        0x04        /* Auto-Negotiation Advertisement    */
#define PHY_REG_ANLPAR      0x05        /* Auto-Neg. Link Partner Abitily    */
#define PHY_REG_ANER        0x06        /* Auto-Neg. Expansion Register      */
#define PHY_REG_ANNPTR      0x07        /* Auto-Neg. Next Page TX            */

/* PHY Extended Registers */
#define PHY_REG_STS         0x10        /* Status Register                   */
#define PHY_REG_MICR        0x11        /* MII Interrupt Control Register    */
#define PHY_REG_MISR        0x12        /* MII Interrupt Status Register     */
#define PHY_REG_FCSCR       0x14        /* False Carrier Sense Counter       */
#define PHY_REG_RECR        0x15        /* Receive Error Counter             */
#define PHY_REG_PCSR        0x16        /* PCS Sublayer Config. and Status   */
#define PHY_REG_RBR         0x17        /* RMII and Bypass Register          */
#define PHY_REG_LEDCR       0x18        /* LED Direct Control Register       */
#define PHY_REG_PHYCR       0x19        /* PHY Control Register              */
#define PHY_REG_10BTSCR     0x1A        /* 10Base-T Status/Control Register  */
#define PHY_REG_CDCTRL1     0x1B        /* CD Test Control and BIST Extens.  */
#define PHY_REG_EDCR        0x1D        /* Energy Detect Control Register    */
#define PHY_REG_PSCS        0x1F

/* PHY Control and Status bits  */
#define PHY_FULLD_100M      0x2100      /* Full Duplex 100Mbit               */
#define PHY_HALFD_100M      0x2000      /* Half Duplex 100Mbit               */
#define PHY_FULLD_10M       0x0100      /* Full Duplex 10Mbit                */
#define PHY_HALFD_10M       0x0000      /* Half Duplex 10MBit                */
#define PHY_AUTO_NEG        0x1000      /* Select Auto Negotiation           */	  
#define PHY_AUTO_NEG_RES    0x0200  
#define PHY_AUTO_NEG_DONE   0x0020		/* AutoNegotiation Complete in BMSR PHY reg  */
#define PHY_BMCR_RESET		0x8000		/* Reset bit at BMCR PHY reg         */
#define LINK_VALID_STS		0x0001		/* Link Valid Status at REG_STS PHY reg	 */
#define FULL_DUP_STS		0x0004		/* Full Duplex Status at REG_STS PHY reg */
#define SPEED_10M_STS		0x0002		/* 10Mbps Status at REG_STS PHY reg */

#define LAN8720_DEF_ADR    0x01        /* Default PHY device address        */
#define LAN8720_ID          0x7c0f0     /* PHY Identifier (without Rev. info */

/*  Misc    */
#define ETHERNET_RST		22			/* 	Reset Output for EMAC at RGU     */
#define RMII_SELECT			0x04		/*  Select RMII in EMACCFG           */



/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

void mac_reset(LPC_ENET_T *pENET)
{
    Chip_RGU_TriggerReset(RGU_ETHERNET_RST);
	while (Chip_RGU_InReset(RGU_ETHERNET_RST)) 
    {}

	/* Reset ethernet peripheral */
	Chip_ENET_Reset(pENET);
	raw_sleep(1);
}


/*--------------------------- write_PHY -------------------------------------*/

static void write_PHY (unsigned int PhyReg, unsigned short Value) 
{   
    unsigned int tout;

    /* Write a data 'Value' to PHY register 'PhyReg'. */
    while(LPC_ETHERNET->MAC_MII_ADDR & GMII_BUSY);			// Check GMII busy bit
    LPC_ETHERNET->MAC_MII_ADDR = (LAN8720_DEF_ADR<<11) | (PhyReg<<6) | GMII_WRITE;
    LPC_ETHERNET->MAC_MII_DATA = Value;
    LPC_ETHERNET->MAC_MII_ADDR |= GMII_BUSY;				// Start PHY Write Cycle

    /* Wait utill operation completed */
    for (tout = 0; tout < MII_WR_TOUT; tout++) 
	{
        if ((LPC_ETHERNET->MAC_MII_ADDR & GMII_BUSY) == 0) 
		{
            break;
        }
    }
}

static unsigned short read_PHY (unsigned int PhyReg) 
{   
    unsigned int tout, val;

    /* Read a PHY register 'PhyReg'. */
    while(LPC_ETHERNET->MAC_MII_ADDR & GMII_BUSY);			// Check GMII busy bit
    LPC_ETHERNET->MAC_MII_ADDR = (LAN8720_DEF_ADR<<11) | (PhyReg<<6) | GMII_READ;
    LPC_ETHERNET->MAC_MII_ADDR |= GMII_BUSY;				// Start PHY Read Cycle

    /* Wait until operation completed */
    for (tout = 0; tout < MII_RD_TOUT; tout++) 
	{
        if ((LPC_ETHERNET->MAC_MII_ADDR & GMII_BUSY) == 0) 
		{
            break;
        }
    }

    val = LPC_ETHERNET->MAC_MII_DATA;

    return (val);
}


void phy_reset(void)
{
	uint32_t regv,tout,id1,id2;
	RAW_U16 temp = 0;

	temp = read_PHY (PHY_REG_BMCR);

	printf("temp is %x\r\n", temp);

	/* Put the DP83848C in reset mode */
	write_PHY (PHY_REG_BMCR, PHY_BMCR_RESET | temp);

	/* Wait for hardware reset to end. */
	for (tout = 0; tout < 0x400000; tout++) 
	{
		regv = read_PHY (PHY_REG_BMCR);

	if (!(regv & 0x8800)) 
	{
		/* Reset complete, device not Power Down. */
		break;
	}
	}

	if (tout == 0x400000)
	{
		RAW_ASSERT(0);
	}

	/*should be 0x3000?*/
	printf("basic control register is %x\r\n", read_PHY (PHY_REG_BMCR));
		
	/* Check if this is a lan8720 PHY. */
	id1 = read_PHY (PHY_REG_IDR1);
	id2 = read_PHY (PHY_REG_IDR2);


	if (((id1 << 16) | (id2 & 0xFFF0)) != LAN8720_ID ) 
	{
		RAW_ASSERT(0);

	}

}

uint32_t ethIsLink(void)
{
 
	if (read_PHY (0x01) & 0x0004)				   //  检测寄存器1看连接上没
	{

		return 1;
	}

	return 0;
  
}



