/*
 * MODBUS Library: AT91SAM7X port
 * Copyright (c) 2010 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbporttimer.c,v 1.1 2010-05-22 22:31:33 embedded-so.embedded-solutions.1 Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <raw_api.h>

#if defined( WITH_LWIP )
#include "lwip/api.h"
#endif

/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbportlayer.h"
#include "common/mbframe.h"
#include "common/mbutils.h"

/* ----------------------- Defines ------------------------------------------*/
#define MB_TIMER_TASK_PRIORITY             ( 3 )
#define TIMER_TASk_STACK_SIZE           256
#define MAX_TIMER_HDLS                  ( 3 )
#define IDX_INVALID                     ( 255 )

#define RESET_HDL( x ) do { \
    ( x )->ubIdx = IDX_INVALID; \
    ( x )->bIsRunning = FALSE; \
    ( x )->xNTimeoutTicks = 0; \
    ( x )->xNExpiryTimeTicks = 0; \
    ( x )->xMBMHdl = MB_HDL_INVALID; \
    ( x )->pbMBPTimerExpiredFN = NULL; \
} while( 0 );

#ifndef MBP_TIMER_DEBUG
#define MBP_TIMER_DEBUG                    ( 0 )
#endif

#if defined( MBP_TIMER_DEBUG ) && ( MBP_TIMER_DEBUG == 1 )
#include <inc/hw_memmap.h>
#include <inc/hw_types.h>
#include <driverlib/gpio.h>

#define DEBUG_INIT                          do { \
} while( 0 )

#define DEBUG_TIMEREXPIRED_ON               do { \
} while( 0 )

#define DEBUG_TIMEREXPIRED_OFF              do { \
} while( 0 )

#define DEBUG_TIMERSTART_ON                 do { \
} while( 0 )

#define DEBUG_TIMERSTART_OFF                do { \
} while( 0 )

#else
#define DEBUG_INIT
#define DEBUG_TIMEREXPIRED_ON
#define DEBUG_TIMEREXPIRED_OFF
#define DEBUG_TIMERSTART_ON
#define DEBUG_TIMERSTART_OFF
#endif

/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    UBYTE           ubIdx;
    BOOL            bIsRunning;
    RAW_SYS_TIME_TYPE    xNTimeoutTicks;
    RAW_SYS_TIME_TYPE    xNExpiryTimeTicks;
    xMBHandle       xMBMHdl;
    pbMBPTimerExpiredCB pbMBPTimerExpiredFN;
} xTimerInternalHandle;


static RAW_TASK_OBJ  timer_task_obj;
static PORT_STACK    timer_task__stack[TIMER_TASk_STACK_SIZE];

/* ----------------------- Static variables ---------------------------------*/
STATIC xTimerInternalHandle arxTimerHdls[MAX_TIMER_HDLS];

STATIC BOOL     bIsInitalized = FALSE;

/* ----------------------- Static functions ---------------------------------*/
void            vMBPTimerTask( void *pvParameters );

/* ----------------------- Start implementation -----------------------------*/

eMBErrorCode
eMBPTimerInit( xMBPTimerHandle * xTimerHdl, USHORT usTimeOut1ms, pbMBPTimerExpiredCB pbMBPTimerExpiredFN,
               xMBHandle xHdl )
{
    eMBErrorCode    eStatus = MB_EPORTERR;
    UBYTE           ubIdx;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( ( NULL != xTimerHdl ) && ( NULL != pbMBPTimerExpiredFN ) && ( MB_HDL_INVALID != xHdl ) )
    {
        if( !bIsInitalized )
        {
            DEBUG_INIT;
            for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
            {
                RESET_HDL( &arxTimerHdls[ubIdx] );
            }

			raw_task_create(&timer_task_obj, "MBP-TIMER",  0, MB_TIMER_TASK_PRIORITY,  0,  timer_task__stack, TIMER_TASk_STACK_SIZE, vMBPTimerTask, 1);
		
			bIsInitalized = TRUE;
        }
        if( bIsInitalized )
        {
            for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
            {
                if( IDX_INVALID == arxTimerHdls[ubIdx].ubIdx )
                {
                    break;
                }
            }
            if( MAX_TIMER_HDLS != ubIdx )
            {
                arxTimerHdls[ubIdx].ubIdx = ubIdx;
                arxTimerHdls[ubIdx].bIsRunning = FALSE;

				/*Be very careful this is only for 10ms tick ,if tick time changes please change followings!!!!!*/
				if (usTimeOut1ms <= 10) {
					
					/*Make the timeout 10ms-20ms*/
					arxTimerHdls[ubIdx].xNTimeoutTicks = 2;
				}
				else {
					/*Make the timeout plus 10ms*/
					arxTimerHdls[ubIdx].xNTimeoutTicks = (usTimeOut1ms / 10) + 1;
				}
				
                arxTimerHdls[ubIdx].xNExpiryTimeTicks = 0;
                arxTimerHdls[ubIdx].xMBMHdl = xHdl;
                arxTimerHdls[ubIdx].pbMBPTimerExpiredFN = pbMBPTimerExpiredFN;

                *xTimerHdl = &arxTimerHdls[ubIdx];
                eStatus = MB_ENOERR;
            }
            else
            {
                eStatus = MB_ENORES;
            }
        }
    }
    else
    {
        eStatus = MB_EINVAL;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

void
vMBPTimerClose( xMBPTimerHandle xTimerHdl )
{
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        RESET_HDL( pxTimerIntHdl );
    }
}

eMBErrorCode
eMBPTimerSetTimeout( xMBPTimerHandle xTimerHdl, USHORT usTimeOut1ms )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) && ( usTimeOut1ms > 0 ) )
    {
		/*Be very careful this is only for 10ms tick ,if tick time changes please change followings!!!!!*/
		if (usTimeOut1ms <= 10) {

			/*Make the timeout 10ms-20ms*/
			pxTimerIntHdl->xNTimeoutTicks = 2;
		}
		else {
			
			/*Make the timeout plus 10ms*/
			pxTimerIntHdl->xNTimeoutTicks = (usTimeOut1ms / 10) + 1;
		}
		
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

eMBErrorCode
eMBPTimerStart( xMBPTimerHandle xTimerHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;

    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    DEBUG_TIMERSTART_ON;
    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        pxTimerIntHdl->bIsRunning = TRUE;
        pxTimerIntHdl->xNExpiryTimeTicks = raw_system_tick_get();
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    DEBUG_TIMERSTART_OFF;
    return eStatus;
}

eMBErrorCode
eMBPTimerStop( xMBPTimerHandle xTimerHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        pxTimerIntHdl->bIsRunning = FALSE;
        pxTimerIntHdl->xNExpiryTimeTicks = 0;
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

void
vMBPTimerTask( void *pvParameters )
{
    UBYTE           ubIdx;
    xTimerInternalHandle *pxTmrHdl;
    
    RAW_SYS_TIME_TYPE    xCurrentTime;

    for( ;; )
    {
    	raw_sleep(1);
		
        xCurrentTime = raw_system_tick_get();
        MBP_ENTER_CRITICAL_SECTION(  );
        for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
        {
            pxTmrHdl = &arxTimerHdls[ubIdx];
            if( ( IDX_INVALID != pxTmrHdl->ubIdx ) && ( pxTmrHdl->bIsRunning ) )
            {
                if( ( xCurrentTime - pxTmrHdl->xNExpiryTimeTicks ) >= pxTmrHdl->xNTimeoutTicks )
                {
                    DEBUG_TIMEREXPIRED_ON;
                    pxTmrHdl->bIsRunning = FALSE;
                    if( NULL != pxTmrHdl->pbMBPTimerExpiredFN )
                    {
                        ( void )pxTmrHdl->pbMBPTimerExpiredFN( pxTmrHdl->xMBMHdl );
                    }
                    DEBUG_TIMEREXPIRED_OFF;
                }
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  );
    }
}
