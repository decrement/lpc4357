/*
 * MODBUS Library: AT91SAM7X port
 * Copyright (c) 2010 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbportevent.c,v 1.1 2010-05-22 22:31:33 embedded-so.embedded-solutions.1 Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>

/* ----------------------- Platform includes --------------------------------*/
#include "raw_api.h"


#include "mbport.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbportlayer.h"
#include "common/mbframe.h"
#include "common/mbutils.h"

/* ----------------------- Defines ------------------------------------------*/
#define MAX_EVENT_HDLS          ( 1 )
#define IDX_INVALID             ( 255 )
#define EV_NONE                 ( 0 )
#define MSG_QUEUE_NUM           5

#define HDL_RESET( x ) do { \
    ( x )->ubIdx = IDX_INVALID; \
    ( x )->xQueueHdl = 0; \
} while( 0 );

/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    UBYTE           ubIdx;
    RAW_QUEUE       *xQueueHdl;
} xEventInternalHandle;

/* ----------------------- Static variables ---------------------------------*/
STATIC BOOL     bIsInitialized = FALSE;
STATIC xEventInternalHandle arxEventHdls[MAX_EVENT_HDLS];
STATIC RAW_QUEUE queue_obj[MAX_EVENT_HDLS];
//void *queue_mem[MSG_QUEUE_NUM];
void *queue_mem[MAX_EVENT_HDLS][MSG_QUEUE_NUM];


/* ----------------------- Static functions ---------------------------------*/

/* ----------------------- Start implementation -----------------------------*/

eMBErrorCode
eMBPEventCreate( xMBPEventHandle * pxEventHdl )
{
    eMBErrorCode    eStatus = MB_ENOERR;
    
    UBYTE           i;

    if( NULL != pxEventHdl )
    {
        MBP_ENTER_CRITICAL_SECTION(  );
        if( !bIsInitialized )
        {
            for( i = 0; i < MAX_EVENT_HDLS; i++ )
            {
                HDL_RESET( &arxEventHdls[i] );
            }
            bIsInitialized = TRUE;
        }
        for( i = 0; i < MAX_EVENT_HDLS; i++ )
        {
            if( IDX_INVALID == arxEventHdls[i].ubIdx )
            {
				raw_queue_create(&queue_obj[i], "mbmaster_queue", (void **)(&queue_mem[i][0]), MSG_QUEUE_NUM);
				arxEventHdls[i].ubIdx = i;
				arxEventHdls[i].xQueueHdl = &queue_obj[i];
				*pxEventHdl = &arxEventHdls[i];
                break;
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  );
    }
    return eStatus;
}


eMBErrorCode
eMBPEventPost( const xMBPEventHandle xEventHdl, xMBPEventType xEvent )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xEventInternalHandle *pxEventHdl = xEventHdl;
	RAW_OS_ERROR  queue_error;
	
    MBP_ENTER_CRITICAL_SECTION(  );
	
    if( MB_IS_VALID_HDL( pxEventHdl, arxEventHdls ) )
    {
		queue_error = raw_queue_end_post(pxEventHdl->xQueueHdl, (void *)xEvent);
		RAW_ASSERT(queue_error == RAW_SUCCESS);
		
        eStatus = MB_ENOERR;
    }
	
    MBP_EXIT_CRITICAL_SECTION(  );
	
    return eStatus;
}

BOOL
bMBPEventGet( const xMBPEventHandle xEventHdl, xMBPEventType * pxEvent )
{
    BOOL            bEventInQueue = FALSE;
    xEventInternalHandle *pxEventHdl = xEventHdl;
	void *msg_receive;
	RAW_OS_ERROR queue_receive_error;

    MBP_ENTER_CRITICAL_SECTION(  );

	if( MB_IS_VALID_HDL( pxEventHdl, arxEventHdls ) )
    {
		/*50ms time out*/
		queue_receive_error = raw_queue_receive(pxEventHdl->xQueueHdl, (RAW_TICKS_PER_SECOND * 50) / 1000, &msg_receive);
		*pxEvent = (xMBPEventType)msg_receive;

		if (queue_receive_error == RAW_SUCCESS)
        {
            bEventInQueue = TRUE;
        }
    }

	
    MBP_EXIT_CRITICAL_SECTION(  );
    return bEventInQueue;
}

void
vMBPEventDelete(xMBPEventHandle xEventHdl)
{
    xEventInternalHandle *pxEventHdl = xEventHdl;

    MBP_ENTER_CRITICAL_SECTION();
	
    if (MB_IS_VALID_HDL( pxEventHdl, arxEventHdls))
    {
        if (0 != pxEventHdl->xQueueHdl)
        {
			raw_queue_delete(pxEventHdl->xQueueHdl);
        }
		
        HDL_RESET(pxEventHdl);
    }
	
    MBP_EXIT_CRITICAL_SECTION();
}

