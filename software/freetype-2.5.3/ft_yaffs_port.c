#include <raw_api.h>
#include <yaffsfs.h>

extern void Uart_Printf(char * fmt,...);



int *ft_yaffs_open(const char *file_read, char *mode)
{

	int f;
	
	//Uart_Printf("file_read is %s\n", file_read);
	f = yaffs_open(file_read, O_RDWR, S_IREAD);


	yaffs_lseek(f,0,SEEK_SET);
	
	return ((int *)(f + 1));
	
}


int ft_yaffs_close(int *file_close)
{
	int ret;

	
	//Uart_Printf("f close is %d\n", (int)file_close);
	ret = yaffs_close((int)(file_close) - 1);
	return ret;
}


int ft_yaffs_read(unsigned char *buffer, int size, int count, int *file)
{
	int size_read;

	//Uart_Printf("count is %d\n", count);
	
	//Uart_Printf("f read is %d\n", (int)file);


	if (count == 0) {
		return 0;

	}
	
	else {
		size_read = yaffs_read((int)(file) - 1, buffer, count);
	}
	
	return size_read;
	
}

int ft_yaffs_seek(int *file, int offset, int flag)
{
	int ret;

	//Uart_Printf("f seek is %d\n", (int)file);
	ret = yaffs_lseek((int)(file) - 1, offset, flag);

	 return ret;
}


int ft_yaffs_ftell(int *file)
{
	int font_size;
	
	yaffs_lseek((int)(file) - 1, 0, SEEK_SET);

	font_size = yaffs_lseek((int)(file) - 1, 0, SEEK_END);
	//Uart_Printf("font_size is %x\n", font_size);
	return font_size;
}

