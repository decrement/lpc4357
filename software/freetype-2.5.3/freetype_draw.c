#include <raw_api.h>
#include <yaffsfs.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include "lpc43xx_lcd.h"



#define COLOR_TO_MTK_COLOR_SIMUL(color) ((((color) >> 19) & 0x1f) << 11) \
                                            |((((color) >> 10) & 0x3f) << 5) \
                                            |(((color) >> 3) & 0x1f)
                                            

FT_Error      error;
FT_Library    library;
FT_Face       face;



static void display_one_byte(RAW_U8 b_data, RAW_U32 x_pos, RAW_U32 y_pos, RAW_U32 font_color)
{
	int y;


	for (y = 0; y < 8; y++) {

		if (b_data & (1 << y)) {

			LCD_one_pixel_write(x_pos + (7 - y), y_pos, COLOR_TO_MTK_COLOR_SIMUL(font_color));

		}

		else {

			//LCD_one_pixel_write(x_pos + (7 - y), y_pos, 0xffff);

		}

		

	}


}


void draw_word(int word, int x, int y, int width, int height, int color)
{
	FT_UInt glyph_index;
	int iLineBytes ;
	int z;
	int i;

	
	//error = FT_Set_Char_Size(face, width * 64, height * 64, 300, 300);

	FT_Set_Pixel_Sizes(face,width, height);

	error =  FT_Select_Charmap(face,FT_ENCODING_UNICODE);

	glyph_index = FT_Get_Char_Index(face, word);

	error = FT_Load_Glyph(face, glyph_index, FT_LOAD_FORCE_AUTOHINT );
 
	error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_MONO);

	iLineBytes =face->glyph->bitmap.pitch;

	for (z = 0; z < face->glyph->bitmap.rows; z++){

		for (i = 0; i < iLineBytes ; i++) {


			display_one_byte(face->glyph->bitmap.buffer[i + z * iLineBytes], x + (i * 8), y + z, color);
		}

	}
	

}





int freetype_init(const RAW_S8 *font_path)
{

	error = FT_Init_FreeType( &library );              /* initialize library */

	printf("error is %d\n", error);

	error = FT_New_Face(library,
	"/nand/simsun.ttc",
	0, &face);

	return error;
}


int freetype_end()
{

	error = FT_Done_Face(face);
	error = FT_Done_FreeType(library);	
	return error;
}



