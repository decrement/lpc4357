#include <raw_api.h>

#define USE_FPU   /* ARMCC */ (  (defined ( __CC_ARM ) && defined ( __TARGET_FPU_VFP )) \
                  /* IAR */   || (defined ( __ICCARM__ ) && defined ( __ARMVFP__ )) \
                  /* GNU */   || (defined ( __GNUC__ ) && defined ( __VFP_FP__ ) && !defined(__SOFTFP__)) )
                  


RAW_VOID  *port_stack_init(PORT_STACK  *p_stk_base, RAW_U32 stk_size,  RAW_VOID   *p_arg, RAW_TASK_ENTRY p_task)
{
	PORT_STACK *stk;
	RAW_U32 temp = (RAW_U32)(p_stk_base + stk_size);

	temp &= 0xfffffff8;
	
	stk = (PORT_STACK  *)temp;

	                                       
	*(--stk)  =  (RAW_U32)0x01000000L;             /* xPSR                                               */
	*(--stk)  = (RAW_U32)p_task;                    /* Entry Point                                        */
	*(--stk)  = (RAW_U32)0xFFFFFFFEL;             /* R14 (LR) (init value will cause fault if ever used)*/
	*(--stk)  = (RAW_U32)0x12121212L;             /* R12                                                */
	*(--stk)  = (RAW_U32)0x03030303L;             /* R3                                                 */
	*(--stk)  = (RAW_U32)0x02020202L;             /* R2                                                 */
	*(--stk)  = (RAW_U32)0x01010101L;             /* R1                                                 */
	*(--stk)  = (RAW_U32)p_arg;                   /* R0 : argument                                      */
	                                                     
	*(--stk)  = (RAW_U32)0x07070707L;             /* R7                                                 */
	*(--stk)  = (RAW_U32)0x06060606L;             /* R6                                                 */
	*(--stk)  = (RAW_U32)0x05050505L;             /* R5                                                 */
	*(--stk)  = (RAW_U32)0x04040404L;             /* R4                                                 */
	 return stk;
	 
}




void port_isr_stack_check(void)
{


}



extern RAW_U32  Stack_Mem ;




RAW_U32 port_isr_stack_free_check(void)
{
	RAW_U32 aligned_int_stack;
	RAW_U32 *aligned_int_stack_addr;
	RAW_U32 free_isr_stk = 0;
	
	aligned_int_stack = ((RAW_U32)(&Stack_Mem) + 32) & 0xffffffe0;

	aligned_int_stack_addr = (RAW_U32 *)aligned_int_stack;
	
	while (*aligned_int_stack_addr++ == 0) {
		
		free_isr_stk++;
	}

	return free_isr_stk;

}




