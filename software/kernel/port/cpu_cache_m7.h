#ifndef CPU_CACHE_M7_H
#define CPU_CACHE_M7_H


#ifdef __cplusplus
extern  "C" {
#endif

void  CPU_Cache_Init       (void);

void  CPU_DCache_RangeFlush(void      *addr_start,
                            RAW_U32   len);

void  CPU_DCache_RangeInv  (void      *addr_start,
                            RAW_U32   len);

#ifdef __cplusplus
}
#endif

#endif


