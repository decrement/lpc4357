
#ifndef RAW_INTERNAL_TYPE_H
#define RAW_INTERNAL_TYPE_H

/*Ported timer type, you may change here depend your hardware timer type*/
typedef  RAW_U32  PORT_TIMER_TYPE;
typedef  RAW_U32  RAW_HARD_TIME_TYPE; /*32 bit or 64 bit unsigned value*/


/*
    Be very careful here, you can modyfy the following code, if only you understand what yor are doing!

 */  

#define         RAW_INTERNAL_NO_WAIT        0u
#define         RAW_INTERNAL_WAIT_FOREVER   0xffffffffu  /*32 bit value, if RAW_TICK_TYPE is 64 bit, you need change it to 64 bit*/
#define         RAW_TASK_STACK_CHECK_WORD   0xdeadbeafu  /*32 bit or 64 bit stack check value*/

typedef RAW_U32 RAW_TICK_TYPE;  /*32 bit or 64 bit unsigned value*/
typedef RAW_U32 MSG_SIZE_TYPE; /*32 bit or 64 bit unsigned value*/
typedef RAW_U64 RAW_IDLE_COUNT_TYPE; /*64 bit unsigned value*/
typedef RAW_U64 RAW_SYS_TIME_TYPE; /*64 bit unsigned value*/
typedef RAW_U32 RAW_MUTEX_NESTED_TYPE; /*8 bit or 16bit or 32bit unsigned value*/
typedef RAW_U8  RAW_SUSPEND_NESTED_TYPE; /*8 bit or 16bit or 32bit unsigned value*/
typedef RAW_U32 RAW_CTX_SWITCH_TYPE;/*32 bit or 64 bit unsigned value*/



#endif

