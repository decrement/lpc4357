#include <raw_api.h>

#define USE_FPU   /* ARMCC */ (  (defined ( __CC_ARM ) && defined ( __TARGET_FPU_VFP )) \
                  /* IAR */   || (defined ( __ICCARM__ ) && defined ( __ARMVFP__ )) \
                  /* GNU */   || (defined ( __GNUC__ ) && defined ( __VFP_FP__ ) && !defined(__SOFTFP__)) )




/* Constants required to access and manipulate the MPU. */
#define portMPU_TYPE							( ( volatile unsigned long * ) 0xe000ed90 )
#define portMPU_REGION_BASE_ADDRESS				( ( volatile unsigned long * ) 0xe000ed9C )
#define portMPU_REGION_ATTRIBUTE				( ( volatile unsigned long * ) 0xe000edA0 )
#define portMPU_CTRL							( ( volatile unsigned long * ) 0xe000ed94 )
#define portMPU_NUMBERS							( ( volatile unsigned long * ) 0xe000ed98 )


#define portEXPECTED_MPU_TYPE_VALUE				( 8UL << 8UL ) /* 8 regions, unified. */
#define portMPU_ENABLE							( 0x01UL )
#define portMPU_BACKGROUND_ENABLE				( 1UL << 2UL )
#define portPRIVILEGED_EXECUTION_START_ADDRESS	( 0UL )
#define portMPU_REGION_VALID					( 0x10UL )
#define portMPU_REGION_ENABLE					( 0x01UL )



#define portMPU_REGION_NO_ACCESS	            ( 0x00UL << 24UL )
#define portMPU_REGION_READ_WRITE				( 0x03UL << 24UL )
#define portMPU_REGION_PRIVILEGED_READ_ONLY		( 0x05UL << 24UL )
#define portMPU_REGION_READ_ONLY				( 0x06UL << 24UL )
#define portMPU_REGION_PRIVILEGED_READ_WRITE	( 0x01UL << 24UL )
#define portMPU_REGION_CACHEABLE_BUFFERABLE		( 0x07UL << 16UL )
#define portMPU_REGION_EXECUTE_NEVER			( 0x01UL << 28UL )


/* Constants required to access and manipulate the NVIC. */
#define portNVIC_SYSTICK_CTRL					( ( volatile unsigned long * ) 0xe000e010 )
#define portNVIC_SYS_CTRL_STATE					( ( volatile unsigned long * ) 0xe000ed24 )
#define portNVIC_MEM_FAULT_ENABLE				( 1UL << 16UL )


                  

static RAW_U32 mpu_region_numbers;
extern RAW_U32  Stack_Mem ;


RAW_VOID  *port_stack_init(PORT_STACK  *p_stk_base, RAW_U32 stk_size,  RAW_VOID   *p_arg, RAW_TASK_ENTRY p_task)
{
	PORT_STACK *stk;
	RAW_U32 temp = (RAW_U32)(p_stk_base + stk_size);

	temp &= 0xfffffff8;
	
	stk = (PORT_STACK  *)temp;

	
	#if USE_FPU
	
	*(--stk)    = (RAW_U32)0xaa;                  /* R? : argument                                      */ 
	*(--stk)  = (RAW_U32)0xa;                   /* FPSCR : argument                                      */
	*(--stk)  = (RAW_U32)0x15;                   /* S15 : argument                                               */        
	*(--stk)  = (RAW_U32)0x14;                   /* S14 : argument                                      */
	*(--stk)  = (RAW_U32)0x13;                   /* S13 : argument                                      */
	*(--stk)  = (RAW_U32)0x12;                   /* S12 : argument                                      */
	*(--stk)  = (RAW_U32)0x11;                   /* S11 : argument                                      */
	*(--stk)  = (RAW_U32)0x10;                   /* S10 : argument                                      */
	*(--stk)  = (RAW_U32)0x9;                   /* S9 : argument                                      */
	*(--stk)  = (RAW_U32)0x8;                   /* S8 : argument                                      */
	*(--stk)  = (RAW_U32)0x7;                   /* S7 : argument                                 */         
	*(--stk)  = (RAW_U32)0x6;                   /* S6 : argument                                      */        
	*(--stk)  = (RAW_U32)0x5;                   /* S5 : argument                                      */
	*(--stk)  = (RAW_U32)0x4;                   /* S4 : argument                                      */
	*(--stk)  = (RAW_U32)0x3;                   /* S3 : argument                                      */
	*(--stk)  = (RAW_U32)0x2;                   /* S2 : argument                                      */
	*(--stk)  = (RAW_U32)0x1;                   /* S1 : argument                                      */
	*(--stk)  = (RAW_U32)0x0;                   /* S0 : argument                                      */

	#endif
	                                       
	*(--stk)  =  (RAW_U32)0x01000000L;             /* xPSR                                               */
	*(--stk)  = (RAW_U32)p_task;                    /* Entry Point                                        */
	*(--stk)  = (RAW_U32)0xFFFFFFFEL;             /* R14 (LR) (init value will cause fault if ever used)*/
	*(--stk)  = (RAW_U32)0x12121212L;             /* R12                                                */
	*(--stk)  = (RAW_U32)0x03030303L;             /* R3                                                 */
	*(--stk)  = (RAW_U32)0x02020202L;             /* R2                                                 */
	*(--stk)  = (RAW_U32)0x01010101L;             /* R1                                                 */
	*(--stk)  = (RAW_U32)p_arg;                   /* R0 : argument                                      */


	#if USE_FPU
	
	/* FPU register s16 ~ s31 */
	*(--stk)  = (RAW_U32)0x31uL; 		   /* S31												  */
	*(--stk)  = (RAW_U32)0x30uL; 		   /* S30												  */
	*(--stk)  = (RAW_U32)0x29uL; 		   /* S29												  */
	*(--stk)  = (RAW_U32)0x28uL; 		   /* S28												  */
	*(--stk)  = (RAW_U32)0x27uL; 		   /* S27												  */
	*(--stk)  = (RAW_U32)0x26uL; 		   /* S26												  */
	*(--stk)  = (RAW_U32)0x25uL; 		   /* S25												  */
	*(--stk)  = (RAW_U32)0x24uL; 		   /* S24												  */
	*(--stk)  = (RAW_U32)0x23uL; 		   /* S23												  */
	*(--stk)  = (RAW_U32)0x22uL; 		   /* S22												  */
	*(--stk)  = (RAW_U32)0x21uL; 		   /* S21												  */
	*(--stk)  = (RAW_U32)0x20uL; 		   /* S20												  */
	*(--stk)  = (RAW_U32)0x19uL; 		   /* S19												  */
	*(--stk)  = (RAW_U32)0x18uL; 		   /* S18												  */
	*(--stk)  = (RAW_U32)0x17uL; 		   /* S17												  */
	*(--stk)  = (RAW_U32)0x16uL; 		   /* S16												  */

	#endif
	                                 		
	*(--stk)  = (RAW_U32)0x11111111L;             /* R11                                                */
	*(--stk)  = (RAW_U32)0x10101010L;             /* R10                                                */
	*(--stk)  = (RAW_U32)0x09090909L;             /* R9                                                 */
	*(--stk)  = (RAW_U32)0x08080808L;             /* R8                                                 */
	*(--stk)  = (RAW_U32)0x07070707L;             /* R7                                                 */
	*(--stk)  = (RAW_U32)0x06060606L;             /* R6                                                 */
	*(--stk)  = (RAW_U32)0x05050505L;             /* R5                                                 */
	*(--stk)  = (RAW_U32)0x04040404L;             /* R4                                                 */
	 return stk;
	 
}


void port_isr_stack_check(void)
{


}


RAW_U32 port_isr_stack_free_check(void)
{
	RAW_U32 aligned_int_stack;
	RAW_U32 *aligned_int_stack_addr;
	RAW_U32 free_isr_stk = 0;
	
	aligned_int_stack = ((RAW_U32)(&Stack_Mem) + 32) & 0xffffffe0;

	aligned_int_stack_addr = (RAW_U32 *)aligned_int_stack;
	
	while (*aligned_int_stack_addr++ == 0) {
		
		free_isr_stk++;
	}

	return free_isr_stk;

}


#if 0

static unsigned long prvGetMPURegionSizeSetting(unsigned long ulActualSizeInBytes)
{
unsigned long ulRegionSize, ulReturnValue = 4;

	/* 32 is the smallest region size, 31 is the largest valid value for
	ulReturnValue. */
	for( ulRegionSize = 32UL; ulReturnValue < 31UL; ( ulRegionSize <<= 1UL ) )
	{
		if( ulActualSizeInBytes <= ulRegionSize )
		{
			break;
		}
		else
		{
			ulReturnValue++;
		}
	}

	/* Shift the code by one before returning so it can be written directly
	into the the correct bit position of the attribute register. */
	return ( ulReturnValue << 1UL );
}

#endif



void system_mpu_enable(void)
{

	RAW_U32 aligned_int_stack;
	RAW_U32 int_stack_base;
	
	mpu_region_numbers = ((*portMPU_TYPE) >> 8) & 0xff;

	if (mpu_region_numbers > 0) {

		int_stack_base = (RAW_U32)(&Stack_Mem);

		/*if int_stack_base is not 32 bytes aligned then up 32 bytes aligned otherwise not change*/
		if ((int_stack_base & 0x1f) > 0) {
			aligned_int_stack = (int_stack_base + 32) & 0xffffffe0;
		}
		else {
			aligned_int_stack = int_stack_base;
		}

		/*protect interrupt stack*/
		*portMPU_REGION_BASE_ADDRESS = ( ( unsigned long )aligned_int_stack) | /* Base address. */
							( portMPU_REGION_VALID ) |
							( 6 );

		*portMPU_REGION_ATTRIBUTE = ( portMPU_REGION_READ_ONLY ) |
							( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
							(4 << 1) | (portMPU_REGION_ENABLE);

		/* Enable the memory fault exception. */
		*portNVIC_SYS_CTRL_STATE |= portNVIC_MEM_FAULT_ENABLE;

		/* Enable the MPU with the background region configured. */
		*portMPU_CTRL |= (portMPU_ENABLE | portMPU_BACKGROUND_ENABLE);
	}

}



void task_stack_mpu_set(void)
{
	RAW_U32 aligned_32_task_stack;
	RAW_U32 task_stack_base;
	

	if (mpu_region_numbers > 0) {

		task_stack_base = (RAW_U32)(high_ready_obj->task_stack_base);

		/*if task_stack_base is not 32 bytes aligned then up 32 bytes aligned otherwise not change*/
		if ((task_stack_base & 0x1f) > 0) {
			aligned_32_task_stack = (task_stack_base + 32) & 0xffffffe0;
		}
		else {
			aligned_32_task_stack = task_stack_base;
		}

		/*7 is the highest region number but 15 is working either why?*/
		*portMPU_REGION_BASE_ADDRESS =	( ( unsigned long )aligned_32_task_stack) | /* Base address. */
							( portMPU_REGION_VALID ) |
							( 7 );

		/*size is 32 bytes which is the smallest size*/
		*portMPU_REGION_ATTRIBUTE =		( portMPU_REGION_READ_ONLY ) |
							( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
							(4 << 1) | (portMPU_REGION_ENABLE);
	}

}




