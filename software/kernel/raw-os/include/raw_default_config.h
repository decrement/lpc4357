/*
     raw os - Copyright (C)  Lingjun Chen(jorya_txj).

    This file is part of raw os.

    raw os is free software; you can redistribute it it under the terms of the 
    GNU General Public License as published by the Free Software Foundation; 
    either version 3 of the License, or  (at your option) any later version.

    raw os is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program. if not, write email to jorya.txj@gmail.com
                                      ---

    A special exception to the LGPL can be applied should you wish to distribute
    a combined work that includes raw os, without being obliged to provide
    the source code for any proprietary components. See the file exception.txt
    for full details of how and when the exception can be applied.
*/

/* 	2012-4  Created by jorya_txj
  *	xxxxxx   please added here
  */


#ifndef RAW_DEFAULT_CONFIG_H
#define RAW_DEFAULT_CONFIG_H

/*to enable system dynamic tick process*/
#ifndef CONFIG_RAW_DYNAMIC_TICK
#define CONFIG_RAW_DYNAMIC_TICK                                     0
#endif

/*to check invalid interrupt priority when interupt zero feature is enabled*/
#ifndef CONFIG_INTERRUPT_PRIORITY_CHECK
#define CONFIG_INTERRUPT_PRIORITY_CHECK                             0
#endif

#ifndef CONFIG_RAW_SYSTEM_STATISTICS
#define CONFIG_RAW_SYSTEM_STATISTICS                                0
#endif

/*enable system embedded trace module*/
#ifndef CONFIG_RAW_TRACE_ENABLE
#define CONFIG_RAW_TRACE_ENABLE                                     0
#endif

/*almost cpu stack is from high to low*/
#ifndef RAW_CPU_STACK_DOWN
#define RAW_CPU_STACK_DOWN                                          1
#endif

/*Max system priority*/
#ifndef CONFIG_RAW_PRIO_MAX
#define CONFIG_RAW_PRIO_MAX                                         62
#endif

/*if cpu is little endian, please set it to 1.If not set it to 0*/
#ifndef CONFIG_RAW_LITTLE_ENDIAN
#define CONFIG_RAW_LITTLE_ENDIAN                                    1
#endif

/*enable SCHED_FIFO and SCHED_RR support*/
#ifndef CONFIG_SCHED_FIFO_RR
#define CONFIG_SCHED_FIFO_RR                                        1
#endif

/*enable system debug*/
#ifndef CONFIG_RAW_DEBUG
#define CONFIG_RAW_DEBUG                                            1
#endif

/*enable user data pointer*/
#ifndef CONFIG_USER_DATA_POINTER
#define	CONFIG_USER_DATA_POINTER                                    1
#endif

/*enable system default memset and memcpy, you can also use your choice*/
#ifndef CONFIG_SYSTEM_MEMOPT
#define CONFIG_SYSTEM_MEMOPT                                        1
#endif

/*tick numbers per second*/
#ifndef RAW_TICKS_PER_SECOND
#define RAW_TICKS_PER_SECOND                                        100
#endif

/*timer frequency = RAW_TICKS_PER_SECOND /  RAW_TIMER_RATE*/
#ifndef RAW_TIMER_RATE
#define RAW_TIMER_RATE                                              1
#endif

/*RAW OS FUNCTION MODULE*/

#ifndef CONFIG_RAW_BYTE
#define CONFIG_RAW_BYTE                                             0
#endif

#ifndef CONFIG_RAW_BLOCK
#define CONFIG_RAW_BLOCK                                            0
#endif

#ifndef CONFIG_RAW_TIMER
#define CONFIG_RAW_TIMER                                            0
#endif

#ifndef CONFIG_RAW_SEMAPHORE
#define CONFIG_RAW_SEMAPHORE                                        1
#endif

#ifndef CONFIG_RAW_EVENT
#define CONFIG_RAW_EVENT                                            0
#endif

#ifndef CONFIG_RAW_QUEUE
#define CONFIG_RAW_QUEUE                                            0
#endif

#ifndef CONFIG_RAW_QUEUE_BUFFER
#define CONFIG_RAW_QUEUE_BUFFER                                     0
#endif

#ifndef CONFIG_RAW_QUEUE_SIZE
#define CONFIG_RAW_QUEUE_SIZE                                       0
#endif

#ifndef CONFIG_RAW_IDLE_EVENT
#define CONFIG_RAW_IDLE_EVENT                                       0
#endif

#ifndef CONFIG_RAW_TASK_QUEUE_SIZE
#define CONFIG_RAW_TASK_QUEUE_SIZE                                  0
#endif

#ifndef CONFIG_RAW_TASK_SEMAPHORE
#define CONFIG_RAW_TASK_SEMAPHORE                                   1
#endif

/*enable different task function*/
#ifndef CONFIG_RAW_TASK_STACK_CHECK
#define CONFIG_RAW_TASK_STACK_CHECK                                 1
#endif

#ifndef CONFIG_RAW_TASK_SLEEP
#define CONFIG_RAW_TASK_SLEEP                                       1
#endif

#ifndef CONFIG_RAW_TASK_SUSPEND
#define CONFIG_RAW_TASK_SUSPEND                                     1
#endif

#ifndef CONFIG_RAW_TASK_RESUME
#define CONFIG_RAW_TASK_RESUME                                      1
#endif

#ifndef CONFIG_RAW_TASK_PRIORITY_CHANGE
#define CONFIG_RAW_TASK_PRIORITY_CHANGE                             1 
#endif

#ifndef CONFIG_RAW_TASK_DELETE
#define CONFIG_RAW_TASK_DELETE                                      1
#endif

#ifndef CONFIG_RAW_TASK_WAIT_ABORT
#define CONFIG_RAW_TASK_WAIT_ABORT                                  1
#endif

#ifndef CONFIG_RAW_TICK_TASK
#define CONFIG_RAW_TICK_TASK                                        1
#endif

#ifndef CONFIG_RAW_ISR_STACK_CHECK
#define CONFIG_RAW_ISR_STACK_CHECK                                  0
#endif



/*enable different semphore function*/

#ifndef CONFIG_RAW_SEMAPHORE_BLOCK_WAY_SET
#define CONFIG_RAW_SEMAPHORE_BLOCK_WAY_SET                          1
#endif

#ifndef CONFIG_RAW_SEMAPHORE_DELETE
#define CONFIG_RAW_SEMAPHORE_DELETE                                 1
#endif

#ifndef CONFIG_RAW_SEMAPHORE_SET
#define CONFIG_RAW_SEMAPHORE_SET                                    1
#endif


/*enable different mutex function*/

#ifndef CONFIG_RAW_MUTEX_DELETE
#define CONFIG_RAW_MUTEX_DELETE                                     1
#endif


/*enable different event function*/

#ifndef CONFIG_RAW_EVENT_DELETE
#define CONFIG_RAW_EVENT_DELETE                                     0
#endif

/*enable different queue function*/

#ifndef CONFIG_RAW_QUEUE_BLOCK_WAY_SET
#define CONFIG_RAW_QUEUE_BLOCK_WAY_SET                              0
#endif

#ifndef CONFIG_RAW_QUEUE_FLUSH
#define CONFIG_RAW_QUEUE_FLUSH                                      0
#endif

#ifndef CONFIG_RAW_QUEUE_DELETE
#define CONFIG_RAW_QUEUE_DELETE                                     0
#endif

#ifndef CONFIG_RAW_QUEUE_GET_INFORMATION
#define CONFIG_RAW_QUEUE_GET_INFORMATION                            0
#endif

/*enable different queue buffer function*/

#ifndef CONFIG_RAW_QUEUE_BUFFER_FLUSH
#define CONFIG_RAW_QUEUE_BUFFER_FLUSH                               0
#endif

#ifndef CONFIG_RAW_QUEUE_BUFFER_DELETE
#define CONFIG_RAW_QUEUE_BUFFER_DELETE                              0
#endif

#ifndef CONFIG_RAW_QUEUE_BUFFER_GET_INFORMATION
#define CONFIG_RAW_QUEUE_BUFFER_GET_INFORMATION                     0
#endif

/*enable different queue size function*/

#ifndef CONFIG_RAW_QUEUE_SIZE_FLUSH
#define CONFIG_RAW_QUEUE_SIZE_FLUSH                                 0
#endif

#ifndef CONFIG_RAW_QUEUE_SIZE_DELETE
#define	CONFIG_RAW_QUEUE_SIZE_DELETE                                0
#endif

#ifndef CONFIG_RAW_QUEUE_SIZE_GET_INFORMATION
#define CONFIG_RAW_QUEUE_SIZE_GET_INFORMATION                       0
#endif

/*enable different timer function*/

#ifndef CONFIG_RAW_TIMER_DEACTIVATE
#define CONFIG_RAW_TIMER_DEACTIVATE                                 1
#endif

#ifndef CONFIG_RAW_TIMER_CHANGE
#define CONFIG_RAW_TIMER_CHANGE                                     1
#endif

#ifndef CONFIG_RAW_TIMER_ACTIVATE
#define CONFIG_RAW_TIMER_ACTIVATE                                   1
#endif

/*enable different user hook function*/
#ifndef CONFIG_RAW_USER_HOOK
#define CONFIG_RAW_USER_HOOK                                        1
#endif

/*enable different module check  function*/

#ifndef RAW_TASK_FUNCTION_CHECK
#define RAW_TASK_FUNCTION_CHECK                                     1
#endif

#ifndef RAW_SEMA_FUNCTION_CHECK
#define RAW_SEMA_FUNCTION_CHECK                                     1
#endif

#ifndef RAW_QUEUE_FUNCTION_CHECK
#define RAW_QUEUE_FUNCTION_CHECK                                    1
#endif

#ifndef RAW_QUEUE_BUFFER_FUNCTION_CHECK
#define RAW_QUEUE_BUFFER_FUNCTION_CHECK                             1
#endif

#ifndef RAW_QUEUE_SIZE_FUNCTION_CHECK
#define RAW_QUEUE_SIZE_FUNCTION_CHECK                               1
#endif

#ifndef RAW_MQUEUE_FUNCTION_CHECK
#define RAW_MQUEUE_FUNCTION_CHECK                                   1
#endif

#ifndef RAW_EVENT_FUNCTION_CHECK
#define RAW_EVENT_FUNCTION_CHECK                                    1
#endif

#ifndef RAW_MUTEX_FUNCTION_CHECK
#define RAW_MUTEX_FUNCTION_CHECK                                    1
#endif

#ifndef RAW_TIMER_FUNCTION_CHECK
#define RAW_TIMER_FUNCTION_CHECK                                    1
#endif

#ifndef RAW_BLOCK_FUNCTION_CHECK
#define RAW_BLOCK_FUNCTION_CHECK                                    1
#endif

#ifndef RAW_BYTE_FUNCTION_CHECK
#define RAW_BYTE_FUNCTION_CHECK                                     1
#endif


/*Set idle task task size, adjust as you need*/

#ifndef IDLE_STACK_SIZE
#define IDLE_STACK_SIZE                                             100
#endif

#if (CONFIG_RAW_TIMER > 0)

/*set timer task stack size, adjust as you need*/
#ifndef TIMER_STACK_SIZE
#define TIMER_STACK_SIZE                                            256
#endif

#ifndef TIMER_TASK_PRIORITY
#define TIMER_TASK_PRIORITY                                         5
#endif

#endif

/*Must be 2^n size!, such as 1, 2, 4, 8, 16,32, etc.......*/
#ifndef TICK_HEAD_ARRAY
#define TICK_HEAD_ARRAY                                             8
#endif

#if (CONFIG_RAW_TICK_TASK > 0)

#ifndef TICK_TASK_STACK_SIZE
#define TICK_TASK_STACK_SIZE                                        256
#endif

#ifndef TICK_TASK_PRIORITY
#define TICK_TASK_PRIORITY                                          1
#endif

#endif

/*default  task time slice*/
#ifndef TIME_SLICE_DEFAULT
#define TIME_SLICE_DEFAULT                                          50
#endif

/*allowed interrupted nested level*/
#ifndef INT_NESTED_LEVEL
#define INT_NESTED_LEVEL                                            100
#endif

#ifndef RAW_CONFIG_CPU_TIME
#define RAW_CONFIG_CPU_TIME                                         0
#endif

#ifndef RAW_SCHE_LOCK_MEASURE_CHECK
#define RAW_SCHE_LOCK_MEASURE_CHECK                                 0
#endif

#ifndef RAW_CPU_INT_DIS_MEASURE_CHECK
#define RAW_CPU_INT_DIS_MEASURE_CHECK                               0
#endif

#ifndef RAW_CONFIG_CPU_TASK
#define RAW_CONFIG_CPU_TASK                                         0
#endif

#ifndef CPU_TASK_PRIORITY
#define CPU_TASK_PRIORITY                                           (CONFIG_RAW_PRIO_MAX - 2)
#endif

#ifndef CPU_STACK_SIZE
#define CPU_STACK_SIZE                                              256
#endif


#if ((CONFIG_RAW_DYNAMIC_TICK >= 1) && (TICK_HEAD_ARRAY != 1))
#error  "TICK_HEAD_ARRAY must be 1 when CONFIG_RAW_DYNAMIC_TICK is enabled."
#endif

#if (CONFIG_RAW_PRIO_MAX >= 256)
#error  "CONFIG_RAW_PRIO_MAX must be <= 255."
#endif

#if ((CONFIG_RAW_QUEUE_SIZE == 0) && (CONFIG_RAW_TASK_QUEUE_SIZE >= 1))
#error  "you need enable CONFIG_RAW_QUEUE_SIZE as well."
#endif

#if ((CONFIG_RAW_SEMAPHORE == 0) && (CONFIG_RAW_TASK_SEMAPHORE >= 1))
#error  "you need enable CONFIG_RAW_SEMAPHORE as well."
#endif

#if ((RAW_CONFIG_CPU_TIME == 0) && (CONFIG_RAW_SYSTEM_STATISTICS >= 1))
#error  "you need enable RAW_CONFIG_CPU_TIME as well."
#endif


#endif

