#ifndef RTC_H_
#define RTC_H_

#if (HAL_USE_RTC > 0) 

#include "rtc_lld.h"

#ifdef __cplusplus
extern "C" {
#endif


HAL_ERROR rtc_init(RTC_DRIVER *rtc);
HAL_ERROR rtc_start(RTC_DRIVER *rtc, RTC_CONFIG *config);
HAL_ERROR rtc_stop(RTC_DRIVER *rtc);
HAL_ERROR rtc_time_set(RTC_DRIVER *rtc, RTC_TIME_DATA *time, RAW_U32 format);
HAL_ERROR rtc_time_get(RTC_DRIVER *rtc, RTC_TIME_DATA *time, RAW_U32 format);
HAL_ERROR rtc_deinit(RTC_DRIVER *rtc);

#ifdef __cplusplus
}

#endif

#endif 
#endif 


