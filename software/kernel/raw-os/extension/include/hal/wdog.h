#ifndef WDOG_H
#define WDOG_H

#if (HAL_USE_WDOG > 0)

#include "wdog_lld.h"

#ifdef __cplusplus
extern "C" {
#endif

HAL_ERROR wdog_init(WDOG_DRIVER *wdog);
HAL_ERROR wdog_start(WDOG_DRIVER *wdog, WDOG_CONFIG *config);
HAL_ERROR wdog_feed(WDOG_DRIVER *wdog);
HAL_ERROR wdog_stop(WDOG_DRIVER *wdog);
HAL_ERROR wdog_deinit(WDOG_DRIVER *wdog);


#ifdef __cplusplus
}


#endif

#endif

#endif

