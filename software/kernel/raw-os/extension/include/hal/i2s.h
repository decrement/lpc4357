#ifndef _I2S_H_
#define _I2S_H_

#if (HAL_USE_I2S > 0)


#define I2S_MODE_SLAVE          0
#define I2S_MODE_MASTER         1

typedef enum {
  I2S_UNINIT = 0,                  
  I2S_STOP = 1,                    
  I2S_READY = 2,                    
  I2S_ACTIVE = 3,                   
  I2S_COMPLETE = 4                  
} i2sstate_t;

#include "i2s_lld.h"


#define i2sStartExchangeI(i2sp) {                                           \
  i2s_lld_start_exchange(i2sp);                                             \
  (i2sp)->state = I2S_ACTIVE;                                               \
}

#define i2sStopExchangeI(i2sp) {                                            \
  i2s_lld_stop_exchange(i2sp);                                              \
  (i2sp)->state = I2S_READY;                                                \
}

#define _i2s_isr_half_code(i2sp) {                                          \
  if ((i2sp)->config->end_cb != NULL) {                                     \
    (i2sp)->config->end_cb(i2sp, 0, (i2sp)->config->size / 2);              \
  }                                                                         \
}

#define _i2s_isr_full_code(i2sp) {                                               \
  if ((i2sp)->config->end_cb) {                                             \
    (i2sp)->state = I2S_COMPLETE;                                           \
    (i2sp)->config->end_cb(i2sp,                                            \
                           (i2sp)->config->size / 2,                        \
                           (i2sp)->config->size / 2);                       \
    if ((i2sp)->state == I2S_COMPLETE)                                      \
      (i2sp)->state = I2S_READY;                                            \
  }                                                                         \
  else                                                                      \
    (i2sp)->state = I2S_READY;                                              \
}

#ifdef __cplusplus
extern "C" {
#endif
  void i2sInit(void);
  void i2sObjectInit(I2SDriver *i2sp);
  void i2sStart(I2SDriver *i2sp, const I2SConfig *config);
  void i2sStop(I2SDriver *i2sp);
  void i2sStartExchange(I2SDriver *i2sp);
  void i2sStopExchange(I2SDriver *i2sp);
#ifdef __cplusplus
}
#endif

#endif 

#endif 

