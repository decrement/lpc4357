#ifndef _DAC_H_
#define _DAC_H_

#if (HAL_USE_DAC > 0) 


typedef enum {
  DAC_UNINIT = 0,                 
  DAC_STOP = 1,                     
  DAC_READY = 2,                  
  DAC_ACTIVE = 3,                  
  DAC_COMPLETE = 4,                
  DAC_ERROR = 5 			       
} dacstate_t;

#include "dac_lld.h"


#ifdef __cplusplus
extern "C" {
#endif
  void dacInit(void);
  void dacObjectInit(DACDriver *dacp);
  void dacStart(DACDriver *dacp, const DACConfig *config);
  void dacStop(DACDriver *dacp);
  void dacPutChannelX(DACDriver *dacp,
                      dacchannel_t channel,
                      dacsample_t sample);
  void dacStartConversion(DACDriver *dacp, const DACConversionGroup *grpp,
                          const dacsample_t *samples, size_t depth);
  void dacStartConversionI(DACDriver *dacp, const DACConversionGroup *grpp,
                           const dacsample_t *samples, size_t depth);
  void dacStopConversion(DACDriver *dacp);
  void dacStopConversionI(DACDriver *dacp);
#if DAC_USE_WAIT
  msg_t dacConvert(DACDriver *dacp, const DACConversionGroup *grpp,
                   const dacsample_t *samples, size_t depth);
#endif

#ifdef __cplusplus
}
#endif

#endif

#endif 


