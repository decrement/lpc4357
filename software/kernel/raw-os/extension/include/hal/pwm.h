
#ifndef _PWM_H_
#define _PWM_H_

#if (HAL_USE_PWM > 0)


#define PWM_OUTPUT_MASK                         0x0FU


#define PWM_OUTPUT_DISABLED                     0x00U


#define PWM_OUTPUT_ACTIVE_HIGH                  0x01U


#define PWM_OUTPUT_ACTIVE_LOW                   0x02U

typedef enum {
  PWM_UNINIT = 0,                  
  PWM_STOP = 1,                     
  PWM_READY = 2                    
} pwmstate_t;


typedef struct PWMDriver PWMDriver;

typedef void (*pwmcallback_t)(PWMDriver *pwmp);

#include "pwm_lld.h"


#define PWM_FRACTION_TO_WIDTH(pwmp, denominator, numerator)                 \
  ((pwmcnt_t)((((pwmcnt_t)(pwmp)->period) *                                 \
               (pwmcnt_t)(numerator)) / (pwmcnt_t)(denominator)))


#define PWM_DEGREES_TO_WIDTH(pwmp, degrees)                                 \
  PWM_FRACTION_TO_WIDTH(pwmp, 36000, degrees)

#define PWM_PERCENTAGE_TO_WIDTH(pwmp, percentage)                           \
  PWM_FRACTION_TO_WIDTH(pwmp, 10000, percentage)

#define pwmChangePeriodI(pwmp, value) {                                     \
  (pwmp)->period = (value);                                                 \
  pwm_lld_change_period(pwmp, value);                                       \
}


#define pwmEnableChannelI(pwmp, channel, width) do {                        \
  (pwmp)->enabled |= ((pwmchnmsk_t)1U << (pwmchnmsk_t)(channel));           \
  pwm_lld_enable_channel(pwmp, channel, width);                             \
} while (false)


#define pwmDisableChannelI(pwmp, channel) do {                              \
  (pwmp)->enabled &= ~((pwmchnmsk_t)1U << (pwmchnmsk_t)(channel));          \
  pwm_lld_disable_channel(pwmp, channel);                                   \
} while (false)


#define pwmIsChannelEnabledI(pwmp, channel)                                 \
  (((pwmp)->enabled & ((pwmchnmsk_t)1U << (pwmchnmsk_t)(channel))) != 0U)


#define pwmEnablePeriodicNotificationI(pwmp)                                \
  pwm_lld_enable_periodic_notification(pwmp)

/
#define pwmDisablePeriodicNotificationI(pwmp)                               \
  pwm_lld_disable_periodic_notification(pwmp)


#define pwmEnableChannelNotificationI(pwmp, channel)                        \
  pwm_lld_enable_channel_notification(pwmp, channel)


#define pwmDisableChannelNotificationI(pwmp, channel)                       \
  pwm_lld_disable_channel_notification(pwmp, channel)

#ifdef __cplusplus
extern "C" {
#endif
  void pwmInit(void);
  void pwmObjectInit(PWMDriver *pwmp);
  void pwmStart(PWMDriver *pwmp, const PWMConfig *config);
  void pwmStop(PWMDriver *pwmp);
  void pwmChangePeriod(PWMDriver *pwmp, pwmcnt_t period);
  void pwmEnableChannel(PWMDriver *pwmp,
                        pwmchannel_t channel,
                        pwmcnt_t width);
  void pwmDisableChannel(PWMDriver *pwmp, pwmchannel_t channel);
  void pwmEnablePeriodicNotification(PWMDriver *pwmp);
  void pwmDisablePeriodicNotification(PWMDriver *pwmp);
  void pwmEnableChannelNotification(PWMDriver *pwmp, pwmchannel_t channel);
  void pwmDisableChannelNotification(PWMDriver *pwmp, pwmchannel_t channel);
#ifdef __cplusplus
}
#endif

#endif 

#endif 

