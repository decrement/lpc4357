#ifndef _CAN_H_
#define _CAN_H_

#if (HAL_USE_CAN > 0)

#include "can_lld.h"

#ifdef __cplusplus
extern "C" {
#endif

	HAL_ERROR can_init(CAN_DRIVER *can);
	HAL_ERROR can_start(CAN_DRIVER *can, CAN_CONFIG *config);
	HAL_ERROR can_transmit(CAN_DRIVER *can, CAN_TX_FRAME *frame, RAW_U32 timeout);
	HAL_ERROR can_receive(CAN_DRIVER *can, CAN_RX_FRAME *frame, RAW_U32 timeout);
	HAL_ERROR can_stop(CAN_DRIVER *can);
	HAL_ERROR can_deinit(CAN_DRIVER *can);
  

#ifdef __cplusplus
}
#endif

#endif 

#endif 


