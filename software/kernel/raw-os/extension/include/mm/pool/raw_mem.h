#ifndef LWIP_HDR_MEM_H
#define LWIP_HDR_MEM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef RAW_U32 mem_size_t;

/** mem_init is not used when using pools instead of a heap */
#define mem_init()
/** mem_trim is not used when using pools instead of a heap:
    we can't free part of a pool element and don't want to copy the rest */
#define mem_trim(mem, size) (mem)

void *mem_malloc(mem_size_t size);
void *mem_calloc(mem_size_t count, mem_size_t size);
void  mem_free(void *mem);


#ifdef __cplusplus
}
#endif

#endif /* LWIP_HDR_MEM_H */

