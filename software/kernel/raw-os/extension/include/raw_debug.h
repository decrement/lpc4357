/*
     raw os - Copyright (C)  Lingjun Chen(jorya_txj).

    This file is part of raw os.

    raw os is free software; you can redistribute it it under the terms of the 
    GNU General Public License as published by the Free Software Foundation; 
    either version 3 of the License, or  (at your option) any later version.

    raw os is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
    without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the GNU General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program. if not, write email to jorya.txj@gmail.com
                                      ---

    A special exception to the LGPL can be applied should you wish to distribute
    a combined work that includes raw os, without being obliged to provide
    the source code for any proprietary components. See the file exception.txt
    for full details of how and when the exception can be applied.
*/

/* 	2016-7  Created by jorya_txj
  *	xxxxxx   please added here
  */


#ifndef RAW_DEBUG_H
#define RAW_DEBUG_H


/** lower two bits indicate debug level
 * - 0 all
 * - 1 warning
 * - 2 serious
 * - 3 severe
 */
#define RAW_DBG_LEVEL_ALL     0x00
#define RAW_DBG_LEVEL_OFF     RAW_DBG_LEVEL_ALL /* compatibility define only */
#define RAW_DBG_LEVEL_WARNING 0x01 /* bad checksums, dropped packets, ... */
#define RAW_DBG_LEVEL_SERIOUS 0x02 /* memory allocation failures, ... */
#define RAW_DBG_LEVEL_SEVERE  0x03
#define RAW_DBG_MASK_LEVEL    0x03

/** flag for RAW_DEBUGF to enable that debug message */
#define RAW_DBG_ON            0x80U
/** flag for RAW_DEBUGF to disable that debug message */
#define RAW_DBG_OFF           0x00U

/** flag for RAW_DEBUGF indicating a tracing message (to follow program flow) */
#define RAW_DBG_TRACE         0x40U
/** flag for RAW_DEBUGF indicating a state debug message (to follow module states) */
#define RAW_DBG_STATE         0x20U
/** flag for RAW_DEBUGF indicating newly added code, not thoroughly tested yet */
#define RAW_DBG_FRESH         0x10U
/** flag for RAW_DEBUGF to halt after printing this debug message */
#define RAW_DBG_HALT          0x08U

/**
 * RAW_NOASSERT: Disable RAW_ASSERT checks.
 * -- To disable assertions define RAW_NOASSERT in your port file
 */
#ifndef RAW_NO_ASSERT
#define RAW_MESSAGE_ASSERT(message, assertion) do { if(!(assertion)) \
  RAW_PLATFORM_ASSERT(message); } while(0)
#ifndef RAW_PLATFORM_ASSERT
#error "If you want to use RAW_ASSERT, RAW_PLATFORM_ASSERT(message) needs to be defined in your port file"
#endif
#else  /* RAW_NOASSERT */
#define RAW_MESSAGE_ASSERT(message, assertion)
#endif /* RAW_NOASSERT */

/** if "expression" isn't true, then print "message" and execute "handler" expression */
#ifndef RAW_MESSAGE_ERROR
#ifndef RAW_NO_ASSERT
#define RAW_PLATFORM_ERROR(message) RAW_PLATFORM_ASSERT(message)
#elif defined RAW_PRINTF_DEBUG
#define RAW_PLATFORM_ERROR(message) RAW_PLATFORM_DIAG((message))
#else
#define RAW_PLATFORM_ERROR(message)
#endif

#define RAW_MESSAGE_ERROR(message, expression, handler) do { if (!(expression)) { \
  RAW_PLATFORM_ERROR(message); handler;}} while(0)
#endif /* RAW_ERROR */

#ifdef RAW_PRINTF_DEBUG
#ifndef RAW_PLATFORM_DIAG
#error "If you want to use RAW_DEBUG, RAW_PLATFORM_DIAG(message) needs to be defined in your port file"
#endif
/** print debug message only if debug message type is enabled...
 *  AND is of correct type AND is at least RAW_DBG_LEVEL
 */
#define RAW_DEBUGF(debug, message) do { \
                               if ( \
                                   ((debug) & RAW_DBG_ON) && \
                                   ((debug) & RAW_DBG_TYPES_ON) && \
                                   ((RAW_U16)((debug) & RAW_DBG_MASK_LEVEL) >= RAW_DBG_MIN_LEVEL)) { \
                                 RAW_PLATFORM_DIAG(message); \
                                 if ((debug) & RAW_DBG_HALT) { \
                                   while(1); \
                                 } \
                               } \
                             } while(0)

#else  /* RAW_DEBUG */
#define RAW_DEBUGF(debug, message)
#endif /* RAW_DEBUG */


#endif

