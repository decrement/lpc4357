#include "hal.h"

#if (HAL_USE_I2S > 0)


void i2sInit(void) {

  i2s_lld_init();
}


void i2sObjectInit(I2SDriver *i2sp) {

  i2sp->state  = I2S_STOP;
  i2sp->config = NULL;
}


void i2sStart(I2SDriver *i2sp, const I2SConfig *config) {

  RAW_ASSERT((i2sp != NULL) && (config != NULL));

  raw_mutex_get(&i2sp->i2sp_lock, RAW_WAIT_FOREVER);
  RAW_ASSERT((i2sp->state == I2S_STOP) || (i2sp->state == I2S_READY),
                "invalid state");
  i2sp->config = config;
  i2s_lld_start(i2sp);
  i2sp->state = I2S_READY;
  raw_mutex_put(&i2sp->i2sp_lock);
}


void i2sStop(I2SDriver *i2sp) {

  RAW_ASSERT(i2sp != NULL);

  raw_mutex_get(&i2sp->i2sp_lock, RAW_WAIT_FOREVER);
  RAW_ASSERT((i2sp->state == I2S_STOP) || (i2sp->state == I2S_READY),
                "invalid state");
  i2s_lld_stop(i2sp);
  i2sp->state = I2S_STOP;
  raw_mutex_put(&i2sp->i2sp_lock);
}


void i2sStartExchange(I2SDriver *i2sp) {

  RAW_ASSERT(i2sp != NULL);

  raw_mutex_get(&i2sp->i2sp_lock, RAW_WAIT_FOREVER);
  RAW_ASSERT(i2sp->state == I2S_READY, "not ready");
  i2sStartExchangeI(i2sp);
  raw_mutex_put(&i2sp->i2sp_lock);
}


void i2sStopExchange(I2SDriver *i2sp) {

  RAW_ASSERT((i2sp != NULL));

  raw_mutex_get(&i2sp->i2sp_lock, RAW_WAIT_FOREVER);
  RAW_ASSERT((i2sp->state == I2S_READY) ||
                (i2sp->state == I2S_ACTIVE) ||
                (i2sp->state == I2S_COMPLETE),
                "invalid state");
  i2sStopExchangeI(i2sp);
  raw_mutex_put(&i2sp->i2sp_lock);
}

#endif 

