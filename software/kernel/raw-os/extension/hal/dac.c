#include "hal.h"

#if (HAL_USE_DAC > 0) 


void dacInit(void) {

  dac_lld_init();
}


void dacObjectInit(DACDriver *dacp) {

  dacp->state = DAC_STOP;
  dacp->config = NULL;

  DAC_DRIVER_EXT_INIT_HOOK(dacp);

}


void dacStart(DACDriver *dacp, const DACConfig *config) {

  RAW_ASSERT(dacp != NULL);

  raw_mutex_get(&dacp->dacp_lock, RAW_WAIT_FOREVER);

  RAW_ASSERT((dacp->state == DAC_STOP) || (dacp->state == DAC_READY),
                "invalid state");

  dacp->config = config;
  dac_lld_start(dacp);
  dacp->state = DAC_READY;

  raw_mutex_put(&dacp->dacp_lock);
}


void dacStop(DACDriver *dacp) {

  RAW_ASSERT(dacp != NULL);

  raw_mutex_get(&dacp->dacp_lock, RAW_WAIT_FOREVER);

  RAW_ASSERT((dacp->state == DAC_STOP) || (dacp->state == DAC_READY),
                "invalid state");

  dac_lld_stop(dacp);
  dacp->state = DAC_STOP;

  raw_mutex_put(&dacp->dacp_lock);
}


void dacPutChannelX(DACDriver *dacp, dacchannel_t channel, dacsample_t sample) {

  RAW_ASSERT(channel < DAC_MAX_CHANNELS);
  RAW_ASSERT(dacp->state == DAC_READY, "invalid state");

  dac_lld_put_channel(dacp, channel, sample);
}


void dacStartConversion(DACDriver *dacp,
                        const DACConversionGroup *grpp,
                        const dacsample_t *samples,
                        size_t depth) {

  raw_mutex_get(&dacp->dacp_lock, RAW_WAIT_FOREVER);
  dacStartConversionI(dacp, grpp, samples, depth);
  raw_mutex_put(&dacp->dacp_lock);
}


void dacStartConversionI(DACDriver *dacp,
                         const DACConversionGroup *grpp,
                         const dacsample_t *samples,
                         size_t depth) {

  RAW_ASSERTClassI();
  RAW_ASSERT((dacp != NULL) && (grpp != NULL) && (samples != NULL) &&
               ((depth == 1) || ((depth & 1) == 0)));
  RAW_ASSERT((dacp->state == DAC_READY) ||
                (dacp->state == DAC_COMPLETE) ||
                (dacp->state == DAC_ERROR),
                "not ready");

  dacp->samples  = samples;
  dacp->depth    = depth;
  dacp->grpp     = grpp;
  dacp->state    = DAC_ACTIVE;
  dac_lld_start_conversion(dacp);
}


void dacStopConversion(DACDriver *dacp) {

  RAW_ASSERT(dacp != NULL);

  raw_mutex_get(&dacp->dacp_lock, RAW_WAIT_FOREVER);

  RAW_ASSERT((dacp->state == DAC_READY) ||
                (dacp->state == DAC_ACTIVE),
                "invalid state");

  if (dacp->state != DAC_READY) {
    dac_lld_stop_conversion(dacp);
    dacp->grpp  = NULL;
    dacp->state = DAC_READY;
    _dac_reset_s(dacp);
  }

  raw_mutex_put(&dacp->dacp_lock);
}


void dacStopConversionI(DACDriver *dacp) {

  RAW_ASSERTClassI();
  RAW_ASSERT(dacp != NULL);
  RAW_ASSERT((dacp->state == DAC_READY) ||
                (dacp->state == DAC_ACTIVE) ||
                (dacp->state == DAC_COMPLETE),
                "invalid state");

  if (dacp->state != DAC_READY) {
    dac_lld_stop_conversion(dacp);
    dacp->grpp  = NULL;
    dacp->state = DAC_READY;
    _dac_reset_i(dacp);
  }
}


#endif 


