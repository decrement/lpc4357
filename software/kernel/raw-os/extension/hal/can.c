#include "hal.h"

#if (HAL_USE_CAN > 0)

HAL_ERROR can_init(CAN_DRIVER *can) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;

	RAW_ASSERT(can != 0);

	mutex_error = raw_mutex_get(&can->can_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = can_lld_init(can);

	mutex_error = raw_mutex_put(&can->can_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;	
}


HAL_ERROR can_start(CAN_DRIVER *can, CAN_CONFIG *config) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT((can != 0) && (config != 0));

	mutex_error = raw_mutex_get(&can->can_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	can->config = config;
	ret = can_lld_start(can, config);
	
	mutex_error = raw_mutex_put(&can->can_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;
}

HAL_ERROR can_transmit(CAN_DRIVER *can, CAN_TX_FRAME *frame, RAW_U32 timeout) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT((can != 0) && (frame != 0));
	      
	mutex_error = raw_mutex_get(&can->can_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = can_lld_transmit(can, frame, timeout);
	
	mutex_error = raw_mutex_put(&can->can_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;
}


HAL_ERROR can_receive(CAN_DRIVER *can, CAN_RX_FRAME *frame, RAW_U32 timeout) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT((can != 0) && (frame != 0));
	          
	mutex_error = raw_mutex_get(&can->can_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = can_lld_receive(can, frame, timeout);

	mutex_error = raw_mutex_put(&can->can_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;
}



HAL_ERROR can_stop(CAN_DRIVER *can) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT(can != 0);

	mutex_error = raw_mutex_get(&can->can_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = can_lld_stop(can);
	
	mutex_error = raw_mutex_put(&can->can_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;
}


HAL_ERROR can_deinit(CAN_DRIVER *can)
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT(can != 0);

	mutex_error = raw_mutex_get(&can->can_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	ret = can_lld_deinit(can);

	mutex_error = raw_mutex_put(&can->can_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;
}



#endif 


