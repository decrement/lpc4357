
#include "hal.h"

#if (HAL_USE_PWM > 0) 


void pwmInit(void) {

  pwm_lld_init();
}


void pwmObjectInit(PWMDriver *pwmp) {

  pwmp->state    = PWM_STOP;
  pwmp->config   = NULL;
  pwmp->enabled  = 0;
  pwmp->channels = 0;
#if defined(PWM_DRIVER_EXT_INIT_HOOK)
  PWM_DRIVER_EXT_INIT_HOOK(pwmp);
#endif
}


void pwmStart(PWMDriver *pwmp, const PWMConfig *config) {

  RAW_ASSERT((pwmp != NULL) && (config != NULL));

  raw_mutex_get(&pwmp->can_lock, RAW_WAIT_FOREVER);
  RAW_ASSERT((pwmp->state == PWM_STOP) || (pwmp->state == PWM_READY),
                "invalid state");
  pwmp->config = config;
  pwmp->period = config->period;
  pwm_lld_start(pwmp);
  pwmp->enabled = 0;
  pwmp->state = PWM_READY;
  raw_mutex_put(&pwmp->can_lock, RAW_WAIT_FOREVER);
}

void pwmStop(PWMDriver *pwmp) {

  RAW_ASSERT(pwmp != NULL);

  raw_mutex_get(&pwmp->can_lock, RAW_WAIT_FOREVER);
  RAW_ASSERT((pwmp->state == PWM_STOP) || (pwmp->state == PWM_READY),
                "invalid state");
  pwm_lld_stop(pwmp);
  pwmp->enabled = 0;
  pwmp->state   = PWM_STOP;
  raw_mutex_put(&pwmp->can_lock, RAW_WAIT_FOREVER);
}

void pwmChangePeriod(PWMDriver *pwmp, pwmcnt_t period) {

  RAW_ASSERT(pwmp != NULL);

  raw_mutex_get(&pwmp->can_lock, RAW_WAIT_FOREVER);
  RAW_ASSERT(pwmp->state == PWM_READY, "invalid state");
  pwmChangePeriodI(pwmp, period);
  raw_mutex_put(&pwmp->can_lock, RAW_WAIT_FOREVER);
}


void pwmEnableChannel(PWMDriver *pwmp,
                      pwmchannel_t channel,
                      pwmcnt_t width) {

  RAW_ASSERT((pwmp != NULL) && (channel < pwmp->channels));

  raw_mutex_get(&pwmp->can_lock, RAW_WAIT_FOREVER);

  RAW_ASSERT(pwmp->state == PWM_READY, "not ready");

  pwmEnableChannelI(pwmp, channel, width);

  raw_mutex_put(&pwmp->can_lock, RAW_WAIT_FOREVER);
}


void pwmDisableChannel(PWMDriver *pwmp, pwmchannel_t channel) {

  RAW_ASSERT((pwmp != NULL) && (channel < pwmp->channels));

  raw_mutex_get(&pwmp->can_lock, RAW_WAIT_FOREVER);

  RAW_ASSERT(pwmp->state == PWM_READY, "not ready");

  pwmDisableChannelI(pwmp, channel);

  raw_mutex_put(&pwmp->can_lock, RAW_WAIT_FOREVER);
}

void pwmEnablePeriodicNotification(PWMDriver *pwmp) {

  RAW_ASSERT(pwmp != NULL);

  raw_mutex_get(&pwmp->can_lock, RAW_WAIT_FOREVER);

  RAW_ASSERT(pwmp->state == PWM_READY, "not ready");
  RAW_ASSERT(pwmp->config->callback != NULL, "undefined periodic callback");

  pwmEnablePeriodicNotificationI(pwmp);

  raw_mutex_put(&pwmp->can_lock, RAW_WAIT_FOREVER);
}

void pwmDisablePeriodicNotification(PWMDriver *pwmp) {

  RAW_ASSERT(pwmp != NULL);

  raw_mutex_get(&pwmp->can_lock, RAW_WAIT_FOREVER);

  RAW_ASSERT(pwmp->state == PWM_READY, "not ready");
  RAW_ASSERT(pwmp->config->callback != NULL, "undefined periodic callback");

  pwmDisablePeriodicNotificationI(pwmp);

  raw_mutex_put(&pwmp->can_lock, RAW_WAIT_FOREVER);
}

void pwmEnableChannelNotification(PWMDriver *pwmp, pwmchannel_t channel) {

  RAW_ASSERT((pwmp != NULL) && (channel < pwmp->channels));

  raw_mutex_get(&pwmp->can_lock, RAW_WAIT_FOREVER);

  RAW_ASSERT(pwmp->state == PWM_READY, "not ready");
  RAW_ASSERT((pwmp->enabled & ((pwmchnmsk_t)1U << (pwmchnmsk_t)channel)) != 0U,
                "channel not enabled");
  RAW_ASSERT(pwmp->config->channels[channel].callback != NULL,
                "undefined channel callback");

  pwmEnableChannelNotificationI(pwmp, channel);

  raw_mutex_put(&pwmp->can_lock, RAW_WAIT_FOREVER);
}

void pwmDisableChannelNotification(PWMDriver *pwmp, pwmchannel_t channel) {

  RAW_ASSERT((pwmp != NULL) && (channel < pwmp->channels));

  raw_mutex_get(&pwmp->can_lock, RAW_WAIT_FOREVER);

  RAW_ASSERT(pwmp->state == PWM_READY, "not ready");
  RAW_ASSERT((pwmp->enabled & ((pwmchnmsk_t)1U << (pwmchnmsk_t)channel)) != 0U,
                "channel not enabled");
  RAW_ASSERT(pwmp->config->channels[channel].callback != NULL,
                "undefined channel callback");

  pwmDisableChannelNotificationI(pwmp, channel);

  raw_mutex_put(&pwmp->can_lock, RAW_WAIT_FOREVER);
}

#endif 


