/*
 * @brief CGU/CCU registers and control functions
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef __CGUCCU_18XX_43XX_H_
#define __CGUCCU_18XX_43XX_H_

#include "chip_clocks.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @ingroup CLOCK_18XX_43XX
 * @{
 */

/**
 * Audio or USB PLL selection
 */
typedef enum CHIP_CGU_USB_AUDIO_PLL {
	CGU_USB_PLL,
	CGU_AUDIO_PLL
} CHIP_CGU_USB_AUDIO_PLL_T;

/**
 * PLL register block
 */
typedef struct {
	__I  uint32_t  PLL_STAT;				/*!< PLL status register */
	__IO uint32_t  PLL_CTRL;				/*!< PLL control register */
	__IO uint32_t  PLL_MDIV;				/*!< PLL M-divider register */
	__IO uint32_t  PLL_NP_DIV;				/*!< PLL N/P-divider register */
} CGU_PLL_REG_T;

/**
 * @brief LPC18XX/43XX CGU register block structure
 */
typedef struct {							/*!< (@ 0x40050000) CGU Structure          */
	__I  uint32_t  RESERVED0[5];
	__IO uint32_t  FREQ_MON;				/*!< (@ 0x40050014) Frequency monitor register */
	__IO uint32_t  XTAL_OSC_CTRL;			/*!< (@ 0x40050018) Crystal oscillator control register */
	CGU_PLL_REG_T  PLL[CGU_AUDIO_PLL + 1];	/*!< (@ 0x4005001C) USB and audio PLL blocks */
	__IO uint32_t  PLL0AUDIO_FRAC;			/*!< (@ 0x4005003C) PLL0 (audio)           */
	__I  uint32_t  PLL1_STAT;				/*!< (@ 0x40050040) PLL1 status register   */
	__IO uint32_t  PLL1_CTRL;				/*!< (@ 0x40050044) PLL1 control register  */
	__IO uint32_t  IDIV_CTRL[CLK_IDIV_LAST];/*!< (@ 0x40050048) Integer divider A-E control registers */
	__IO uint32_t  BASE_CLK[CLK_BASE_LAST];	/*!< (@ 0x4005005C) Start of base clock registers */
} LPC_CGU_T;


/**
  * @brief Product name title=UM10462 Chapter title=LPC18xx Clock Generation Unit (CGU) Modification date=6/1/2011 Major revision=0 Minor revision=1  (CGU)
  */

typedef struct {                            /*!< (@ 0x40050000) CGU Structure          */
  __I  uint32_t  RESERVED0[5];
  __IO uint32_t  FREQ_MON;                  /*!< (@ 0x40050014) Frequency monitor register */
  __IO uint32_t  XTAL_OSC_CTRL;             /*!< (@ 0x40050018) Crystal oscillator control register */
  __I  uint32_t  PLL0USB_STAT;              /*!< (@ 0x4005001C) PLL0 (USB) status register */
  __IO uint32_t  PLL0USB_CTRL;              /*!< (@ 0x40050020) PLL0 (USB) control register */
  __IO uint32_t  PLL0USB_MDIV;              /*!< (@ 0x40050024) PLL0 (USB) M-divider register */
  __IO uint32_t  PLL0USB_NP_DIV;            /*!< (@ 0x40050028) PLL0 (USB) N/P-divider register */
  __I  uint32_t  PLL0AUDIO_STAT;            /*!< (@ 0x4005002C) PLL0 (audio) status register */
  __IO uint32_t  PLL0AUDIO_CTRL;            /*!< (@ 0x40050030) PLL0 (audio) control register */
  __IO uint32_t  PLL0AUDIO_MDIV;            /*!< (@ 0x40050034) PLL0 (audio) M-divider register */
  __IO uint32_t  PLL0AUDIO_NP_DIV;          /*!< (@ 0x40050038) PLL0 (audio) N/P-divider register */
  __IO uint32_t  PLL0AUDIO_FRAC;            /*!< (@ 0x4005003C) PLL0 (audio)           */
  __I  uint32_t  PLL1_STAT;                 /*!< (@ 0x40050040) PLL1 status register   */
  __IO uint32_t  PLL1_CTRL;                 /*!< (@ 0x40050044) PLL1 control register  */
  __IO uint32_t  IDIVA_CTRL;                /*!< (@ 0x40050048) Integer divider A control register */
  __IO uint32_t  IDIVB_CTRL;                /*!< (@ 0x4005004C) Integer divider B control register */
  __IO uint32_t  IDIVC_CTRL;                /*!< (@ 0x40050050) Integer divider C control register */
  __IO uint32_t  IDIVD_CTRL;                /*!< (@ 0x40050054) Integer divider D control register */
  __IO uint32_t  IDIVE_CTRL;                /*!< (@ 0x40050058) Integer divider E control register */
  __IO uint32_t  BASE_SAFE_CLK;             /*!< (@ 0x4005005C) Output stage 0 control register for base clock BASE_SAFE_CLK */
  __IO uint32_t  BASE_USB0_CLK;             /*!< (@ 0x40050060) Output stage 1 control register for base clock BASE_USB0_CLK */
  __IO uint32_t  BASE_PERIPH_CLK;           /*!< (@ 0x40050064) Output stage 2 control register for base clock BASE_PERIPH_CLK */
  __IO uint32_t  BASE_USB1_CLK;             /*!< (@ 0x40050068) Output stage 3 control register for base clock BASE_USB1_CLK */
  __IO uint32_t  BASE_M4_CLK;               /*!< (@ 0x4005006C) Output stage BASE_M4_CLK control register */
  __IO uint32_t  BASE_SPIFI_CLK;            /*!< (@ 0x40050070) Output stage BASE_SPIFI_CLK control register */
  __IO uint32_t  BASE_SPI_CLK;              /*!< (@ 0x40050074) Output stage BASE_SPI_CLK control register */
  __IO uint32_t  BASE_PHY_RX_CLK;           /*!< (@ 0x40050078) Output stage BASE_PHY_RX_CLK control register */
  __IO uint32_t  BASE_PHY_TX_CLK;           /*!< (@ 0x4005007C) Output stage BASE_PHY_TX_CLK control register */
  __IO uint32_t  BASE_APB1_CLK;             /*!< (@ 0x40050080) Output stage BASE_APB1_CLK control register */
  __IO uint32_t  BASE_APB3_CLK;             /*!< (@ 0x40050084) Output stage BASE_APB3_CLK control register */
  __IO uint32_t  BASE_LCD_CLK;              /*!< (@ 0x40050088) Output stage BASE_LCD_CLK control register */
  __I  uint32_t  RESERVED2;
  __IO uint32_t  BASE_SDIO_CLK;             /*!< (@ 0x40050090) Output stage BASE_SDIO_CLK control register */
  __IO uint32_t  BASE_SSP0_CLK;             /*!< (@ 0x40050094) Output stage BASE_SSP0_CLK control register */
  __IO uint32_t  BASE_SSP1_CLK;             /*!< (@ 0x40050098) Output stage BASE_SSP1_CLK control register */
  __IO uint32_t  BASE_UART0_CLK;            /*!< (@ 0x4005009C) Output stage BASE_UART0_CLK control register */
  __IO uint32_t  BASE_UART1_CLK;            /*!< (@ 0x400500A0) Output stage BASE_UART1_CLK control register */
  __IO uint32_t  BASE_UART2_CLK;            /*!< (@ 0x400500A4) Output stage BASE_UART2_CLK control register */
  __IO uint32_t  BASE_UART3_CLK;            /*!< (@ 0x400500A8) Output stage BASE_UART3_CLK control register */
  __IO uint32_t  BASE_OUT_CLK;              /*!< (@ 0x400500AC) Output stage 20 control register for base clock BASE_OUT_CLK */
  __I  uint32_t  RESERVED3[4];
  __IO uint32_t  BASE_APLL_CLK;             /*!< (@ 0x400500C0) Output stage 25 control register for base clock BASE_APLL_CLK */
  __IO uint32_t  BASE_CGU_OUT0_CLK;         /*!< (@ 0x400500C4) Output stage 25 control register for base clock BASE_CGU_OUT0_CLK */
  __IO uint32_t  BASE_CGU_OUT1_CLK;         /*!< (@ 0x400500C8) Output stage 25 control register for base clock BASE_CGU_OUT1_CLK */
} LPC_CGU_T2;

/**
 * @brief CCU clock config/status register pair
 */
typedef struct {
	__IO uint32_t  CFG;						/*!< CCU clock configuration register */
	__I  uint32_t  STAT;					/*!< CCU clock status register */
} CCU_CFGSTAT_T;



/**
 * @brief CCU1 register block structure
 */
typedef struct {							/*!< (@ 0x40051000) CCU1 Structure         */
	__IO uint32_t  PM;						/*!< (@ 0x40051000) CCU1 power mode register */
	__I  uint32_t  BASE_STAT;				/*!< (@ 0x40051004) CCU1 base clocks status register */
	__I  uint32_t  RESERVED0[62];
	CCU_CFGSTAT_T  CLKCCU[CLK_CCU1_LAST];	/*!< (@ 0x40051100) Start of CCU1 clock registers */
} LPC_CCU1_T;

/**
 * @brief CCU2 register block structure
 */
typedef struct {							/*!< (@ 0x40052000) CCU2 Structure         */
	__IO uint32_t  PM;						/*!< (@ 0x40052000) Power mode register    */
	__I  uint32_t  BASE_STAT;				/*!< (@ 0x40052004) CCU base clocks status register */
	__I  uint32_t  RESERVED0[62];
	CCU_CFGSTAT_T  CLKCCU[CLK_CCU2_LAST - CLK_CCU1_LAST];	/*!< (@ 0x40052100) Start of CCU2 clock registers */
} LPC_CCU2_T;



typedef struct {                            /*!< (@ 0x40051000) CCU1 Structure         */
  __IO uint32_t  PM;                        /*!< (@ 0x40051000) CCU1 power mode register */
  __I  uint32_t  BASE_STAT;                 /*!< (@ 0x40051004) CCU1 base clocks status register */
  __I  uint32_t  RESERVED0[62];
  __IO uint32_t  CLK_APB3_BUS_CFG;          /*!< (@ 0x40051100) CLK_APB3_BUS clock configuration register */
  __I  uint32_t  CLK_APB3_BUS_STAT;         /*!< (@ 0x40051104) CLK_APB3_BUS clock status register */
  __IO uint32_t  CLK_APB3_I2C1_CFG;         /*!< (@ 0x40051108) CLK_APB3_I2C1 clock configuration register */
  __I  uint32_t  CLK_APB3_I2C1_STAT;        /*!< (@ 0x4005110C) CLK_APB3_I2C1 clock status register */
  __IO uint32_t  CLK_APB3_DAC_CFG;          /*!< (@ 0x40051110) CLK_APB3_DAC clock configuration register */
  __I  uint32_t  CLK_APB3_DAC_STAT;         /*!< (@ 0x40051114) CLK_APB3_DAC clock status register */
  __IO uint32_t  CLK_APB3_ADC0_CFG;         /*!< (@ 0x40051118) CLK_APB3_ADC0 clock configuration register */
  __I  uint32_t  CLK_APB3_ADC0_STAT;        /*!< (@ 0x4005111C) CLK_APB3_ADC0 clock status register */
  __IO uint32_t  CLK_APB3_ADC1_CFG;         /*!< (@ 0x40051120) CLK_APB3_ADC1 clock configuration register */
  __I  uint32_t  CLK_APB3_ADC1_STAT;        /*!< (@ 0x40051124) CLK_APB3_ADC1 clock status register */
  __IO uint32_t  CLK_APB3_CAN0_CFG;         /*!< (@ 0x40051128) CLK_APB3_CAN0 clock configuration register */
  __I  uint32_t  CLK_APB3_CAN0_STAT;        /*!< (@ 0x4005112C) CLK_APB3_CAN0 clock status register */
  __I  uint32_t  RESERVED1[52];
  __IO uint32_t  CLK_APB1_BUS_CFG;          /*!< (@ 0x40051200) CLK_APB1_BUS clock configuration register */
  __I  uint32_t  CLK_APB1_BUS_STAT;         /*!< (@ 0x40051204) CLK_APB1_BUS clock status register */
  __IO uint32_t  CLK_APB1_MOTOCONPWM_CFG;   /*!< (@ 0x40051208) CLK_APB1_MOTOCONPWM clock configuration register */
  __I  uint32_t  CLK_APB1_MOTOCONPWM_STAT;  /*!< (@ 0x4005120C) CLK_APB1_MOTOCONPWM clock status register */
  __IO uint32_t  CLK_APB1_I2C0_CFG;         /*!< (@ 0x40051210) CLK_APB1_I2C0 clock configuration register */
  __I  uint32_t  CLK_APB1_I2C0_STAT;        /*!< (@ 0x40051214) CLK_APB1_I2C0 clock status register */
  __IO uint32_t  CLK_APB1_I2S_CFG;          /*!< (@ 0x40051218) CLK_APB1_I2S clock configuration register */
  __I  uint32_t  CLK_APB1_I2S_STAT;         /*!< (@ 0x4005121C) CLK_APB1_I2S clock status register */
  __IO uint32_t  CLK_APB1_CAN1_CFG;         /*!< (@ 0x40051220) CLK_APB1_CAN1 clock configuration register */
  __I  uint32_t  CLK_APB1_CAN1_STAT;        /*!< (@ 0x40051224) CLK_APB1_CAN1 clock status register */
  __I  uint32_t  RESERVED2[54];
  __IO uint32_t  CLK_SPIFI_CFG;             /*!< (@ 0x40051300) CLK_SPIFI clock configuration register */
  __I  uint32_t  CLK_SPIFI_STAT;            /*!< (@ 0x40051304) CLK_APB1_SPIFI clock status register */
  __I  uint32_t  RESERVED3[62];
  __IO uint32_t  CLK_M4_BUS_CFG;            /*!< (@ 0x40051400) CLK_M4_BUS clock configuration register */
  __I  uint32_t  CLK_M4_BUS_STAT;           /*!< (@ 0x40051404) CLK_M4_BUSclock status register */
  __IO uint32_t  CLK_M4_SPIFI_CFG;          /*!< (@ 0x40051408) CLK_M4_SPIFI clock configuration register */
  __I  uint32_t  CLK_M4_SPIFI_STAT;         /*!< (@ 0x4005140C) CLK_M4_SPIFI clock status register */
  __IO uint32_t  CLK_M4_GPIO_CFG;           /*!< (@ 0x40051410) CLK_M4_GPIO clock configuration register */
  __I  uint32_t  CLK_M4_GPIO_STAT;          /*!< (@ 0x40051414) CLK_M4_GPIO clock status register */
  __IO uint32_t  CLK_M4_LCD_CFG;            /*!< (@ 0x40051418) CLK_M4_LCD clock configuration register */
  __I  uint32_t  CLK_M4_LCD_STAT;           /*!< (@ 0x4005141C) CLK_M4_LCD clock status register */
  __IO uint32_t  CLK_M4_ETHERNET_CFG;       /*!< (@ 0x40051420) CLK_M4_ETHERNET clock configuration register */
  __I  uint32_t  CLK_M4_ETHERNET_STAT;      /*!< (@ 0x40051424) CLK_M4_ETHERNET clock status register */
  __IO uint32_t  CLK_M4_USB0_CFG;           /*!< (@ 0x40051428) CLK_M4_USB0 clock configuration register */
  __I  uint32_t  CLK_M4_USB0_STAT;          /*!< (@ 0x4005142C) CLK_M4_USB0 clock status register */
  __IO uint32_t  CLK_M4_EMC_CFG;            /*!< (@ 0x40051430) CLK_M4_EMC clock configuration register */
  __I  uint32_t  CLK_M4_EMC_STAT;           /*!< (@ 0x40051434) CLK_M4_EMC clock status register */
  __IO uint32_t  CLK_M4_SDIO_CFG;           /*!< (@ 0x40051438) CLK_M4_SDIO clock configuration register */
  __I  uint32_t  CLK_M4_SDIO_STAT;          /*!< (@ 0x4005143C) CLK_M4_SDIO clock status register */
  __IO uint32_t  CLK_M4_DMA_CFG;            /*!< (@ 0x40051440) CLK_M4_DMA clock configuration register */
  __I  uint32_t  CLK_M4_DMA_STAT;           /*!< (@ 0x40051444) CLK_M4_DMA clock status register */
  __IO uint32_t  CLK_M4_M4CORE_CFG;         /*!< (@ 0x40051448) CLK_M4_M4CORE clock configuration register */
  __I  uint32_t  CLK_M4_M3CORE_STAT;        /*!< (@ 0x4005144C) CLK_M4_M3CORE clock status register */
  __I  uint32_t  RESERVED4[6];
  __IO uint32_t  CLK_M4_SCT_CFG;            /*!< (@ 0x40051468) CLK_M4_SCT clock configuration register */
  __I  uint32_t  CLK_M4_SCT_STAT;           /*!< (@ 0x4005146C) CLK_M4_SCT clock status register */
  __IO uint32_t  CLK_M4_USB1_CFG;           /*!< (@ 0x40051470) CLK_M4_USB1 clock configuration register */
  __I  uint32_t  CLK_M4_USB1_STAT;          /*!< (@ 0x40051474) CLK_M4_USB1 clock status register */
  __IO uint32_t  CLK_M4_EMCDIV_CFG;         /*!< (@ 0x40051478) CLK_M4_EMCDIV clock configuration register */
  __I  uint32_t  CLK_M4_EMCDIV_STAT;        /*!< (@ 0x4005147C) CLK_M4_EMCDIV clock status register */
  __I  uint32_t  RESERVED5[4];
  __IO uint32_t  CLK_M4_M0APP_CFG;          /*!< (@ 0x40051490) CLK_M0APP_CFG clock configuration register */
  __I  uint32_t  CLK_M4_M0APP_STAT;         /*!< (@ 0x40051494) CLK_M4_MOAPP clock status register */
  __I  uint32_t  RESERVED6[26];
  __IO uint32_t  CLK_M4_WWDT_CFG;           /*!< (@ 0x40051500) CLK_M4_WWDT clock configuration register */
  __I  uint32_t  CLK_M4_WWDT_STAT;          /*!< (@ 0x40051504) CLK_M4_WWDT clock status register */
  __IO uint32_t  CLK_M4_USART0_CFG;         /*!< (@ 0x40051508) CLK_M4_USART0 clock configuration register */
  __I  uint32_t  CLK_M4_USART0_STAT;        /*!< (@ 0x4005150C) CLK_M4_USART0 clock status register */
  __IO uint32_t  CLK_M4_UART1_CFG;          /*!< (@ 0x40051510) CLK_M4_UART1 clock configuration register */
  __I  uint32_t  CLK_M4_UART1_STAT;         /*!< (@ 0x40051514) CLK_M4_UART1 clock status register */
  __IO uint32_t  CLK_M4_SSP0_CFG;           /*!< (@ 0x40051518) CLK_M4_SSP0 clock configuration register */
  __I  uint32_t  CLK_M4_SSP0_STAT;          /*!< (@ 0x4005151C) CLK_M4_SSP0 clock status register */
  __IO uint32_t  CLK_M4_TIMER0_CFG;         /*!< (@ 0x40051520) CLK_M4_TIMER0 clock configuration register */
  __I  uint32_t  CLK_M4_TIMER0_STAT;        /*!< (@ 0x40051524) CLK_M4_TIMER0 clock status register */
  __IO uint32_t  CLK_M4_TIMER1_CFG;         /*!< (@ 0x40051528) CLK_M4_TIMER1clock configuration register */
  __I  uint32_t  CLK_M4_TIMER1_STAT;        /*!< (@ 0x4005152C) CLK_M4_TIMER1 clock status register */
  __IO uint32_t  CLK_M4_SCU_CFG;            /*!< (@ 0x40051530) CLK_M4_SCU clock configuration register */
  __I  uint32_t  CLK_M4_SCU_STAT;           /*!< (@ 0x40051534) CLK_SCU_XXX clock status register */
  __IO uint32_t  CLK_M4_CREG_CFG;           /*!< (@ 0x40051538) CLK_M4_CREGclock configuration register */
  __I  uint32_t  CLK_M4_CREG_STAT;          /*!< (@ 0x4005153C) CLK_M4_CREG clock status register */
  __I  uint32_t  RESERVED7[48];
  __IO uint32_t  CLK_M4_RITIMER_CFG;        /*!< (@ 0x40051600) CLK_M4_RITIMER clock configuration register */
  __I  uint32_t  CLK_M4_RITIMER_STAT;       /*!< (@ 0x40051604) CLK_M4_RITIMER clock status register */
  __IO uint32_t  CLK_M4_USART2_CFG;         /*!< (@ 0x40051608) CLK_M4_USART2 clock configuration register */
  __I  uint32_t  CLK_M4_USART2_STAT;        /*!< (@ 0x4005160C) CLK_M4_USART2 clock status register */
  __IO uint32_t  CLK_M4_USART3_CFG;         /*!< (@ 0x40051610) CLK_M4_USART3 clock configuration register */
  __I  uint32_t  CLK_M4_USART3_STAT;        /*!< (@ 0x40051614) CLK_M4_USART3 clock status register */
  __IO uint32_t  CLK_M4_TIMER2_CFG;         /*!< (@ 0x40051618) CLK_M4_TIMER2 clock configuration register */
  __I  uint32_t  CLK_M4_TIMER2_STAT;        /*!< (@ 0x4005161C) CLK_M4_TIMER2 clock status register */
  __IO uint32_t  CLK_M4_TIMER3_CFG;         /*!< (@ 0x40051620) CLK_M4_TIMER3 clock configuration register */
  __I  uint32_t  CLK_M4_TIMER3_STAT;        /*!< (@ 0x40051624) CLK_M4_TIMER3 clock status register */
  __IO uint32_t  CLK_M4_SSP1_CFG;           /*!< (@ 0x40051628) CLK_M4_SSP1 clock configuration register */
  __I  uint32_t  CLK_M4_SSP1_STAT;          /*!< (@ 0x4005162C) CLK_M4_SSP1 clock status register */
  __IO uint32_t  CLK_M4_QEI_CFG;            /*!< (@ 0x40051630) CLK_M4_QEIclock configuration register */
  __I  uint32_t  CLK_M4_QEI_STAT;           /*!< (@ 0x40051634) CLK_M4_QEI clock status register */
  __I  uint32_t  RESERVED8[50];
  __IO uint32_t  CLK_PERIPH_BUS_CFG;        /*!< (@ 0x40051700) CLK_PERIPH_BUS_CFG clock configuration register */
  __I  uint32_t  CLK_PERIPH_BUS_STAT;       /*!< (@ 0x40051704) CLK_PERIPH_BUS_STAT clock status register */
  __I  uint32_t  RESERVED9[2];
  __IO uint32_t  CLK_PERIPH_CORE_CFG;       /*!< (@ 0x40051710) CLK_PERIPH_CORE_CFG clock configuration register */
  __I  uint32_t  CLK_PERIPH_CORE_STAT;      /*!< (@ 0x40051714) CLK_CORE_BUS_STAT clock status register */
  __IO uint32_t  CLK_PERIPH_SGPIO_CFG;      /*!< (@ 0x40051718) CLK_PERIPH_SGPIO_CFG clock configuration register */
  __I  uint32_t  CLK_PERIPH_SGPIO_STAT;     /*!< (@ 0x4005171C) CLK_CORE_SGPIO_STAT clock status register */
  __I  uint32_t  RESERVED10[56];
  __IO uint32_t  CLK_USB0_CFG;              /*!< (@ 0x40051800) CLK_M4_USB0 clock configuration register */
  __I  uint32_t  CLK_USB0_STAT;             /*!< (@ 0x40051804) CLK_USB0 clock status register */
  __I  uint32_t  RESERVED11[62];
  __IO uint32_t  CLK_USB1_CFG;              /*!< (@ 0x40051900) CLK_USB1 clock configuration register */
  __I  uint32_t  CLK_USB1_STAT;             /*!< (@ 0x40051904) CLK_USB1 clock status register */
  __I  uint32_t  RESERVED12[62];
  __IO uint32_t  CLK_SPI_CFG;               /*!< (@ 0x40051A00) CLK_SPI clock configuration register */
  __I  uint32_t  CLK_SPI_STAT;              /*!< (@ 0x40051A04) CLK_SPI clock status register */
  __I  uint32_t  RESERVED13[62];
  __IO uint32_t  CLK_VADC_CFG;              /*!< (@ 0x40051B00) CLK_VADC clock configuration register */
  __I  uint32_t  CLK_VADC_STAT;             /*!< (@ 0x40051B04) CLK_VADC clock status register */
} LPC_CCU1_T1;


// ------------------------------------------------------------------------------------------------
// -----                                         CCU2                                         -----
// ------------------------------------------------------------------------------------------------


/**
  * @brief Product name title=UM10430 Chapter title=LPC18xx Clock Control Unit (CCU) Modification date=1/21/2011 Major revision=0 Minor revision=7  (CCU2)
  */

typedef struct {                            /*!< (@ 0x40052000) CCU2 Structure         */
  __IO uint32_t  PM;                        /*!< (@ 0x40052000) Power mode register    */
  __I  uint32_t  BASE_STAT;                 /*!< (@ 0x40052004) CCU base clocks status register */
  __I  uint32_t  RESERVED0[62];
  __IO uint32_t  CLK_APLL_CFG;              /*!< (@ 0x40052100) CLK_APLL clock configuration register */
  __I  uint32_t  CLK_APLL_STAT;             /*!< (@ 0x40052104) CLK_APLL clock status register */
  __I  uint32_t  RESERVED1[62];
  __IO uint32_t  CLK_APB2_USART3_CFG;       /*!< (@ 0x40052200) CLK_APB2_USART3 clock configuration register */
  __I  uint32_t  CLK_APB2_USART3_STAT;      /*!< (@ 0x40052204) CLK_APB2_USART3 clock status register */
  __I  uint32_t  RESERVED2[62];
  __IO uint32_t  CLK_APB2_USART2_CFG;       /*!< (@ 0x40052300) CLK_APB2_USART2 clock configuration register */
  __I  uint32_t  CLK_APB2_USART2_STAT;      /*!< (@ 0x40052304) CLK_APB2_USART clock status register */
  __I  uint32_t  RESERVED3[62];
  __IO uint32_t  CLK_APB0_UART1_CFG;        /*!< (@ 0x40052400) CLK_APB2_UART1 clock configuration register */
  __I  uint32_t  CLK_APB0_UART1_STAT;       /*!< (@ 0x40052404) CLK_APB0_UART1 clock status register */
  __I  uint32_t  RESERVED4[62];
  __IO uint32_t  CLK_APB0_USART0_CFG;       /*!< (@ 0x40052500) CLK_APB2_USART0 clock configuration register */
  __I  uint32_t  CLK_APB0_USART0_STAT;      /*!< (@ 0x40052504) CLK_APB0_USART0 clock status register */
  __I  uint32_t  RESERVED5[62];
  __IO uint32_t  CLK_APB2_SSP1_CFG;         /*!< (@ 0x40052600) CLK_APB2_SSP1 clock configuration register */
  __I  uint32_t  CLK_APB2_SSP1_STAT;        /*!< (@ 0x40052604) CLK_APB2_SSP1 clock status register */
  __I  uint32_t  RESERVED6[62];
  __IO uint32_t  CLK_APB0_SSP0_CFG;         /*!< (@ 0x40052700) CLK_APB0_SSP0 clock configuration register */
  __I  uint32_t  CLK_APB0_SSP0_STAT;        /*!< (@ 0x40052704) CLK_APB0_SSP0 clock status register */
  __I  uint32_t  RESERVED7[62];
  __IO uint32_t  CLK_SDIO_CFG;              /*!< (@ 0x40052800) CLK_SDIO clock configuration register */
  __I  uint32_t  CLK_SDIO_STAT;             /*!< (@ 0x40052804) CLK_SDIO clock status register */
} LPC_CCU2_T2;

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __CGUCCU_18XX_43XX_H_ */
