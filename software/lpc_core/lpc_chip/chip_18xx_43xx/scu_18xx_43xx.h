/*
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef __SCU_18XX_43XX_H_
#define __SCU_18XX_43XX_H_

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup SCU_18XX_43XX CHIP: LPC18xx/43xx SCU Driver (configures pin functions)
 * @ingroup CHIP_18XX_43XX_Drivers
 * @{
 */


/**
 * @brief Array of pin definitions passed to Chip_SCU_SetPinMuxing() must be in this format
 */
typedef struct {
	uint8_t pingrp;		/* Pin group */
	uint8_t pinnum;		/* Pin number */
	uint16_t modefunc;	/* Pin mode and function for SCU */
} PINMUX_GRP_T;

/**
 * @brief System Control Unit register block
 */
typedef struct {
	__IO uint32_t  SFSP[16][32];
	__I  uint32_t  RESERVED0[256];
	__IO uint32_t  SFSCLK[4];			/*!< Pin configuration register for pins CLK0-3 */
	__I  uint32_t  RESERVED16[28];
	__IO uint32_t  SFSUSB;				/*!< Pin configuration register for USB */
	__IO uint32_t  SFSI2C0;				/*!< Pin configuration register for I2C0-bus pins */
	__IO uint32_t  ENAIO[3];			/*!< Analog function select registerS */
	__I  uint32_t  RESERVED17[27];
	__IO uint32_t  EMCDELAYCLK;			/*!< EMC clock delay register */
	__I  uint32_t  RESERVED18[63];
	__IO uint32_t  PINTSEL0;			/*!< Pin interrupt select register for pin interrupts 0 to 3. */
	__IO uint32_t  PINTSEL1;			/*!< Pin interrupt select register for pin interrupts 4 to 7. */
} LPC_SCU_T;

typedef struct {                            /*!< (@ 0x40086000) SCU Structure          */
  __IO uint32_t  SFSP0_0;                   /*!< (@ 0x40086000) Pin configuration register for pins P0 */
  __IO uint32_t  SFSP0_1;                   /*!< (@ 0x40086004) Pin configuration register for pins P0 */
  __I  uint32_t  RESERVED0[30];
  __IO uint32_t  SFSP1_0;                   /*!< (@ 0x40086080) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_1;                   /*!< (@ 0x40086084) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_2;                   /*!< (@ 0x40086088) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_3;                   /*!< (@ 0x4008608C) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_4;                   /*!< (@ 0x40086090) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_5;                   /*!< (@ 0x40086094) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_6;                   /*!< (@ 0x40086098) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_7;                   /*!< (@ 0x4008609C) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_8;                   /*!< (@ 0x400860A0) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_9;                   /*!< (@ 0x400860A4) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_10;                  /*!< (@ 0x400860A8) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_11;                  /*!< (@ 0x400860AC) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_12;                  /*!< (@ 0x400860B0) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_13;                  /*!< (@ 0x400860B4) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_14;                  /*!< (@ 0x400860B8) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_15;                  /*!< (@ 0x400860BC) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_16;                  /*!< (@ 0x400860C0) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_17;                  /*!< (@ 0x400860C4) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_18;                  /*!< (@ 0x400860C8) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_19;                  /*!< (@ 0x400860CC) Pin configuration register for pins P1 */
  __IO uint32_t  SFSP1_20;                  /*!< (@ 0x400860D0) Pin configuration register for pins P1 */
  __I  uint32_t  RESERVED1[11];
  __IO uint32_t  SFSP2_0;                   /*!< (@ 0x40086100) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_1;                   /*!< (@ 0x40086104) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_2;                   /*!< (@ 0x40086108) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_3;                   /*!< (@ 0x4008610C) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_4;                   /*!< (@ 0x40086110) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_5;                   /*!< (@ 0x40086114) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_6;                   /*!< (@ 0x40086118) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_7;                   /*!< (@ 0x4008611C) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_8;                   /*!< (@ 0x40086120) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_9;                   /*!< (@ 0x40086124) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_10;                  /*!< (@ 0x40086128) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_11;                  /*!< (@ 0x4008612C) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_12;                  /*!< (@ 0x40086130) Pin configuration register for pins P2 */
  __IO uint32_t  SFSP2_13;                  /*!< (@ 0x40086134) Pin configuration register for pins P2 */
  __I  uint32_t  RESERVED2[18];
  __IO uint32_t  SFSP3_0;                   /*!< (@ 0x40086180) Pin configuration register for pins P3 */
  __IO uint32_t  SFSP3_1;                   /*!< (@ 0x40086184) Pin configuration register for pins P3 */
  __IO uint32_t  SFSP3_2;                   /*!< (@ 0x40086188) Pin configuration register for pins P3 */
  __IO uint32_t  SFSP3_3;                   /*!< (@ 0x4008618C) Pin configuration register for pins P3 */
  __IO uint32_t  SFSP3_4;                   /*!< (@ 0x40086190) Pin configuration register for pins P3 */
  __IO uint32_t  SFSP3_5;                   /*!< (@ 0x40086194) Pin configuration register for pins P3 */
  __IO uint32_t  SFSP3_6;                   /*!< (@ 0x40086198) Pin configuration register for pins P3 */
  __IO uint32_t  SFSP3_7;                   /*!< (@ 0x4008619C) Pin configuration register for pins P3 */
  __IO uint32_t  SFSP3_8;                   /*!< (@ 0x400861A0) Pin configuration register for pins P3 */
  __I  uint32_t  RESERVED3[23];
  __IO uint32_t  SFSP4_0;                   /*!< (@ 0x40086200) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_1;                   /*!< (@ 0x40086204) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_2;                   /*!< (@ 0x40086208) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_3;                   /*!< (@ 0x4008620C) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_4;                   /*!< (@ 0x40086210) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_5;                   /*!< (@ 0x40086214) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_6;                   /*!< (@ 0x40086218) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_7;                   /*!< (@ 0x4008621C) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_8;                   /*!< (@ 0x40086220) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_9;                   /*!< (@ 0x40086224) Pin configuration register for pins P4 */
  __IO uint32_t  SFSP4_10;                  /*!< (@ 0x40086228) Pin configuration register for pins P4 */
  __I  uint32_t  RESERVED4[21];
  __IO uint32_t  SFSP5_0;                   /*!< (@ 0x40086280) Pin configuration register for pins P5 */
  __IO uint32_t  SFSP5_1;                   /*!< (@ 0x40086284) Pin configuration register for pins P5 */
  __IO uint32_t  SFSP5_2;                   /*!< (@ 0x40086288) Pin configuration register for pins P5 */
  __IO uint32_t  SFSP5_3;                   /*!< (@ 0x4008628C) Pin configuration register for pins P5 */
  __IO uint32_t  SFSP5_4;                   /*!< (@ 0x40086290) Pin configuration register for pins P5 */
  __IO uint32_t  SFSP5_5;                   /*!< (@ 0x40086294) Pin configuration register for pins P5 */
  __IO uint32_t  SFSP5_6;                   /*!< (@ 0x40086298) Pin configuration register for pins P5 */
  __IO uint32_t  SFSP5_7;                   /*!< (@ 0x4008629C) Pin configuration register for pins P5 */
  __I  uint32_t  RESERVED5[24];
  __IO uint32_t  SFSP6_0;                   /*!< (@ 0x40086300) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_1;                   /*!< (@ 0x40086304) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_2;                   /*!< (@ 0x40086308) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_3;                   /*!< (@ 0x4008630C) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_4;                   /*!< (@ 0x40086310) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_5;                   /*!< (@ 0x40086314) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_6;                   /*!< (@ 0x40086318) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_7;                   /*!< (@ 0x4008631C) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_8;                   /*!< (@ 0x40086320) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_9;                   /*!< (@ 0x40086324) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_10;                  /*!< (@ 0x40086328) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_11;                  /*!< (@ 0x4008632C) Pin configuration register for pins P6 */
  __IO uint32_t  SFSP6_12;                  /*!< (@ 0x40086330) Pin configuration register for pins P6 */
  __I  uint32_t  RESERVED6[19];
  __IO uint32_t  SFSP7_0;                   /*!< (@ 0x40086380) Pin configuration register for pins P7 */
  __IO uint32_t  SFSP7_1;                   /*!< (@ 0x40086384) Pin configuration register for pins P7 */
  __IO uint32_t  SFSP7_2;                   /*!< (@ 0x40086388) Pin configuration register for pins P7 */
  __IO uint32_t  SFSP7_3;                   /*!< (@ 0x4008638C) Pin configuration register for pins P7 */
  __IO uint32_t  SFSP7_4;                   /*!< (@ 0x40086390) Pin configuration register for pins P7 */
  __IO uint32_t  SFSP7_5;                   /*!< (@ 0x40086394) Pin configuration register for pins P7 */
  __IO uint32_t  SFSP7_6;                   /*!< (@ 0x40086398) Pin configuration register for pins P7 */
  __IO uint32_t  SFSP7_7;                   /*!< (@ 0x4008639C) Pin configuration register for pins P7 */
  __I  uint32_t  RESERVED7[24];
  __IO uint32_t  SFSP8_0;                   /*!< (@ 0x40086400) Pin configuration register for pins P8 */
  __IO uint32_t  SFSP8_1;                   /*!< (@ 0x40086404) Pin configuration register for pins P8 */
  __IO uint32_t  SFSP8_2;                   /*!< (@ 0x40086408) Pin configuration register for pins P8 */
  __IO uint32_t  SFSP8_3;                   /*!< (@ 0x4008640C) Pin configuration register for pins P8 */
  __IO uint32_t  SFSP8_4;                   /*!< (@ 0x40086410) Pin configuration register for pins P8 */
  __IO uint32_t  SFSP8_5;                   /*!< (@ 0x40086414) Pin configuration register for pins P8 */
  __IO uint32_t  SFSP8_6;                   /*!< (@ 0x40086418) Pin configuration register for pins P8 */
  __IO uint32_t  SFSP8_7;                   /*!< (@ 0x4008641C) Pin configuration register for pins P8 */
  __IO uint32_t  SFSP8_8;                   /*!< (@ 0x40086420) Pin configuration register for pins P8 */
  __I  uint32_t  RESERVED8[23];
  __IO uint32_t  SFSP9_0;                   /*!< (@ 0x40086480) Pin configuration register for pins P9 */
  __IO uint32_t  SFSP9_1;                   /*!< (@ 0x40086484) Pin configuration register for pins P9 */
  __IO uint32_t  SFSP9_2;                   /*!< (@ 0x40086488) Pin configuration register for pins P9 */
  __IO uint32_t  SFSP9_3;                   /*!< (@ 0x4008648C) Pin configuration register for pins P9 */
  __IO uint32_t  SFSP9_4;                   /*!< (@ 0x40086490) Pin configuration register for pins P9 */
  __IO uint32_t  SFSP9_5;                   /*!< (@ 0x40086494) Pin configuration register for pins P9 */
  __IO uint32_t  SFSP9_6;                   /*!< (@ 0x40086498) Pin configuration register for pins P9 */
  __I  uint32_t  RESERVED9[25];
  __IO uint32_t  SFSPA_0;                   /*!< (@ 0x40086500) Pin configuration register for pins PA */
  __IO uint32_t  SFSPA_1;                   /*!< (@ 0x40086504) Pin configuration register for pins PA */
  __IO uint32_t  SFSPA_2;                   /*!< (@ 0x40086508) Pin configuration register for pins PA */
  __IO uint32_t  SFSPA_3;                   /*!< (@ 0x4008650C) Pin configuration register for pins PA */
  __IO uint32_t  SFSPA_4;                   /*!< (@ 0x40086510) Pin configuration register for pins PA */
  __I  uint32_t  RESERVED10[27];
  __IO uint32_t  SFSPB_0;                   /*!< (@ 0x40086580) Pin configuration register for pins PB */
  __IO uint32_t  SFSPB_1;                   /*!< (@ 0x40086584) Pin configuration register for pins PB */
  __IO uint32_t  SFSPB_2;                   /*!< (@ 0x40086588) Pin configuration register for pins PB */
  __IO uint32_t  SFSPB_3;                   /*!< (@ 0x4008658C) Pin configuration register for pins PB */
  __IO uint32_t  SFSPB_4;                   /*!< (@ 0x40086590) Pin configuration register for pins PB */
  __IO uint32_t  SFSPB_5;                   /*!< (@ 0x40086594) Pin configuration register for pins PB */
  __IO uint32_t  SFSPB_6;                   /*!< (@ 0x40086598) Pin configuration register for pins PB */
  __I  uint32_t  RESERVED11[25];
  __IO uint32_t  SFSPC_0;                   /*!< (@ 0x40086600) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_1;                   /*!< (@ 0x40086604) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_2;                   /*!< (@ 0x40086608) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_3;                   /*!< (@ 0x4008660C) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_4;                   /*!< (@ 0x40086610) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_5;                   /*!< (@ 0x40086614) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_6;                   /*!< (@ 0x40086618) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_7;                   /*!< (@ 0x4008661C) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_8;                   /*!< (@ 0x40086620) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_9;                   /*!< (@ 0x40086624) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_10;                  /*!< (@ 0x40086628) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_11;                  /*!< (@ 0x4008662C) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_12;                  /*!< (@ 0x40086630) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_13;                  /*!< (@ 0x40086634) Pin configuration register for pins PC */
  __IO uint32_t  SFSPC_14;                  /*!< (@ 0x40086638) Pin configuration register for pins PC */
  __I  uint32_t  RESERVED12[17];
  __IO uint32_t  SFSPD_0;                   /*!< (@ 0x40086680) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_1;                   /*!< (@ 0x40086684) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_2;                   /*!< (@ 0x40086688) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_3;                   /*!< (@ 0x4008668C) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_4;                   /*!< (@ 0x40086690) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_5;                   /*!< (@ 0x40086694) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_6;                   /*!< (@ 0x40086698) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_7;                   /*!< (@ 0x4008669C) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_8;                   /*!< (@ 0x400866A0) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_9;                   /*!< (@ 0x400866A4) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_10;                  /*!< (@ 0x400866A8) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_11;                  /*!< (@ 0x400866AC) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_12;                  /*!< (@ 0x400866B0) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_13;                  /*!< (@ 0x400866B4) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_14;                  /*!< (@ 0x400866B8) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_15;                  /*!< (@ 0x400866BC) Pin configuration register for pins PD */
  __IO uint32_t  SFSPD_16;                  /*!< (@ 0x400866C0) Pin configuration register for pins PD */
  __I  uint32_t  RESERVED13[15];
  __IO uint32_t  SFSPE_0;                   /*!< (@ 0x40086700) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_1;                   /*!< (@ 0x40086704) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_2;                   /*!< (@ 0x40086708) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_3;                   /*!< (@ 0x4008670C) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_4;                   /*!< (@ 0x40086710) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_5;                   /*!< (@ 0x40086714) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_6;                   /*!< (@ 0x40086718) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_7;                   /*!< (@ 0x4008671C) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_8;                   /*!< (@ 0x40086720) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_9;                   /*!< (@ 0x40086724) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_10;                  /*!< (@ 0x40086728) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_11;                  /*!< (@ 0x4008672C) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_12;                  /*!< (@ 0x40086730) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_13;                  /*!< (@ 0x40086734) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_14;                  /*!< (@ 0x40086738) Pin configuration register for pins PE */
  __IO uint32_t  SFSPE_15;                  /*!< (@ 0x4008673C) Pin configuration register for pins PE */
  __I  uint32_t  RESERVED14[16];
  __IO uint32_t  SFSPF_0;                   /*!< (@ 0x40086780) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_1;                   /*!< (@ 0x40086784) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_2;                   /*!< (@ 0x40086788) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_3;                   /*!< (@ 0x4008678C) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_4;                   /*!< (@ 0x40086790) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_5;                   /*!< (@ 0x40086794) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_6;                   /*!< (@ 0x40086798) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_7;                   /*!< (@ 0x4008679C) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_8;                   /*!< (@ 0x400867A0) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_9;                   /*!< (@ 0x400867A4) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_10;                  /*!< (@ 0x400867A8) Pin configuration register for pins PF */
  __IO uint32_t  SFSPF_11;                  /*!< (@ 0x400867AC) Pin configuration register for pins PF */
  __I  uint32_t  RESERVED15[276];
  __IO uint32_t  SFSCLK_0;                  /*!< (@ 0x40086C00) Pin configuration register for pin CLK0 */
  __IO uint32_t  SFSCLK_1;                  /*!< (@ 0x40086C04) Pin configuration register for pin CLK1 */
  __IO uint32_t  SFSCLK_2;                  /*!< (@ 0x40086C08) Pin configuration register for pin CLK2 */
  __IO uint32_t  SFSCLK_3;                  /*!< (@ 0x40086C0C) Pin configuration register for pin CLK3 */
  __I  uint32_t  RESERVED16[28];
  __IO uint32_t  SFSUSB;                    /*!< (@ 0x40086C80) Pin configuration register for */
  __IO uint32_t  SFSI2C0;                   /*!< (@ 0x40086C84) Pin configuration register for I 2C0-bus pins */
  __IO uint32_t  ENAIO0;                    /*!< (@ 0x40086C88) ADC0 function select register */
  __IO uint32_t  ENAIO1;                    /*!< (@ 0x40086C8C) ADC1 function select register */
  __IO uint32_t  ENAIO2;                    /*!< (@ 0x40086C90) Analog function select register */
  __I  uint32_t  RESERVED17[27];
  __IO uint32_t  EMCDELAYCLK;               /*!< (@ 0x40086D00) EMC clock delay register */
  __I  uint32_t  RESERVED18[63];
  __IO uint32_t  PINTSEL0;                  /*!< (@ 0x40086E00) Pin interrupt select register for pin interrupts 0 to 3. */
  __IO uint32_t  PINTSEL1;                  /*!< (@ 0x40086E04) Pin interrupt select register for pin interrupts 4 to 7. */
} LPC_SCU_Type;


/**
 * SCU function and mode selection definitions
 * See the User Manual for specific modes and functions supoprted by the
 * various LPC18xx/43xx devices. Functionality can vary per device.
 */
#define SCU_MODE_PULLUP            (0x0 << 3)		/*!< Enable pull-up resistor at pad */
#define SCU_MODE_REPEATER          (0x1 << 3)		/*!< Enable pull-down and pull-up resistor at resistor at pad (repeater mode) */
#define SCU_MODE_INACT             (0x2 << 3)		/*!< Disable pull-down and pull-up resistor at resistor at pad */
#define SCU_MODE_PULLDOWN          (0x3 << 3)		/*!< Enable pull-down resistor at pad */
#define SCU_MODE_HIGHSPEEDSLEW_EN  (0x1 << 5)		/*!< Enable high-speed slew */
#define SCU_MODE_INBUFF_EN         (0x1 << 6)		/*!< Enable Input buffer */
#define SCU_MODE_ZIF_DIS           (0x1 << 7)		/*!< Disable input glitch filter */
#define SCU_MODE_4MA_DRIVESTR      (0x0 << 8)		/*!< Normal drive: 4mA drive strength */
#define SCU_MODE_8MA_DRIVESTR      (0x1 << 8)		/*!< Medium drive: 8mA drive strength */
#define SCU_MODE_14MA_DRIVESTR     (0x2 << 8)		/*!< High drive: 14mA drive strength */
#define SCU_MODE_20MA_DRIVESTR     (0x3 << 8)		/*!< Ultra high- drive: 20mA drive strength */
#define SCU_MODE_FUNC0             0x0				/*!< Selects pin function 0 */
#define SCU_MODE_FUNC1             0x1				/*!< Selects pin function 1 */
#define SCU_MODE_FUNC2             0x2				/*!< Selects pin function 2 */
#define SCU_MODE_FUNC3             0x3				/*!< Selects pin function 3 */
#define SCU_MODE_FUNC4             0x4				/*!< Selects pin function 4 */
#define SCU_MODE_FUNC5             0x5				/*!< Selects pin function 5 */
#define SCU_MODE_FUNC6             0x6				/*!< Selects pin function 6 */
#define SCU_MODE_FUNC7             0x7				/*!< Selects pin function 7 */
#define SCU_PINIO_FAST             (SCU_MODE_INACT | SCU_MODE_HIGHSPEEDSLEW_EN | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS)

/**
 * SCU function and mode selection definitions (old)
 * For backwards compatibility.
 */
#define MD_PUP						(0x0 << 3)		/** Enable pull-up resistor at pad */
#define MD_BUK						(0x1 << 3)		/** Enable pull-down and pull-up resistor at resistor at pad (repeater mode) */
#define MD_PLN						(0x2 << 3)		/** Disable pull-down and pull-up resistor at resistor at pad */
#define MD_PDN						(0x3 << 3)		/** Enable pull-down resistor at pad */
#define MD_EHS						(0x1 << 5)		/** Enable fast slew rate */
#define MD_EZI						(0x1 << 6)		/** Input buffer enable */
#define MD_ZI						(0x1 << 7)		/** Disable input glitch filter */
#define MD_EHD0						(0x1 << 8)		/** EHD driver strength low bit */
#define MD_EHD1						(0x1 << 8)		/** EHD driver strength high bit */
#define MD_PLN_FAST					(MD_PLN | MD_EZI | MD_ZI | MD_EHS)
#define I2C0_STANDARD_FAST_MODE		(1 << 3 | 1 << 11)	/** Pin configuration for STANDARD/FAST mode I2C */
#define I2C0_FAST_MODE_PLUS			(2 << 1 | 1 << 3 | 1 << 7 | 1 << 10 | 1 << 11)	/** Pin configuration for Fast-mode Plus I2C */
#define FUNC0						0x0				/** Pin function 0 */
#define FUNC1						0x1				/** Pin function 1 */
#define FUNC2						0x2				/** Pin function 2 */
#define FUNC3						0x3				/** Pin function 3 */
#define FUNC4						0x4				/** Pin function 4 */
#define FUNC5						0x5				/** Pin function 5 */
#define FUNC6						0x6				/** Pin function 6 */
#define FUNC7						0x7				/** Pin function 7 */

#define PORT_OFFSET					0x80			/** Port offset definition */
#define PIN_OFFSET					0x04			/** Pin offset definition */

/** Returns the SFSP register address in the SCU for a pin and port, recommend using (*(volatile int *) &LPC_SCU->SFSP[po][pi];) */
#define LPC_SCU_PIN(LPC_SCU_BASE, po, pi) (*(volatile int *) ((LPC_SCU_BASE) + ((po) * 0x80) + ((pi) * 0x4))

/** Returns the address in the SCU for a SFSCLK clock register, recommend using (*(volatile int *) &LPC_SCU->SFSCLK[c];) */
#define LPC_SCU_CLK(LPC_SCU_BASE, c) (*(volatile int *) ((LPC_SCU_BASE) +0xC00 + ((c) * 0x4)))

/**
 * @brief	Sets I/O Control pin mux
 * @param	port		: Port number, should be: 0..15
 * @param	pin			: Pin number, should be: 0..31
 * @param	modefunc	: OR'ed values or type SCU_MODE_*
 * @return	Nothing
 * @note	Do not use for clock pins (SFSCLK0 .. SFSCLK4). Use
 * Chip_SCU_ClockPinMux() function for SFSCLKx clock pins.
 */
STATIC INLINE void Chip_SCU_PinMuxSet(uint8_t port, uint8_t pin, uint16_t modefunc)
{
	LPC_SCU->SFSP[port][pin] = modefunc;
}

/**
 * @brief	Configure pin function
 * @param	port	: Port number, should be: 0..15
 * @param	pin		: Pin number, should be: 0..31
 * @param	mode	: OR'ed values or type SCU_MODE_*
 * @param	func	: Pin function, value of type SCU_MODE_FUNC0 to SCU_MODE_FUNC7
 * @return	Nothing
 * @note	Do not use for clock pins (SFSCLK0 .. SFSCLK4). Use
 * Chip_SCU_ClockPinMux() function for SFSCLKx clock pins.
 */
STATIC INLINE void Chip_SCU_PinMux(uint8_t port, uint8_t pin, uint16_t mode, uint8_t func)
{
	Chip_SCU_PinMuxSet(port, pin, (mode | (uint16_t) func));
}

/**
 * @brief	Configure clock pin function (pins SFSCLKx)
 * @param	clknum	: Clock pin number, should be: 0..3
 * @param	modefunc	: OR'ed values or type SCU_MODE_*
 * @return	Nothing
 */
STATIC INLINE void Chip_SCU_ClockPinMuxSet(uint8_t clknum, uint16_t modefunc)
{
	LPC_SCU->SFSCLK[clknum] = (uint32_t) modefunc;
}

/**
 * @brief	Configure clock pin function (pins SFSCLKx)
 * @param	clknum	: Clock pin number, should be: 0..3
 * @param	mode	: OR'ed values or type SCU_MODE_*
 * @param	func	: Pin function, value of type SCU_MODE_FUNC0 to SCU_MODE_FUNC7
 * @return	Nothing
 */
STATIC INLINE void Chip_SCU_ClockPinMux(uint8_t clknum, uint16_t mode, uint8_t func)
{
	LPC_SCU->SFSCLK[clknum] = ((uint32_t) mode | (uint32_t) func);
}

/**
 * @brief	GPIO Interrupt Pin Select
 * @param	PortSel	: GPIO PINTSEL interrupt, should be: 0 to 7
 * @param	PortNum	: GPIO port number interrupt, should be: 0 to 7
 * @param	PinNum	: GPIO pin number Interrupt , should be: 0 to 31
 * @return	Nothing
 */
void Chip_SCU_GPIOIntPinSel(uint8_t PortSel, uint8_t PortNum, uint8_t PinNum);

/**
 * @brief	I2C Pin Configuration
 * @param	I2C0Mode	: I2C0 mode, should be:
 *                  - I2C0_STANDARD_FAST_MODE: Standard/Fast mode transmit
 *                  - I2C0_FAST_MODE_PLUS: Fast-mode Plus transmit
 * @return	Nothing
 */
STATIC INLINE void Chip_SCU_I2C0PinConfig(uint32_t I2C0Mode)
{
	LPC_SCU->SFSI2C0 = I2C0Mode;
}

/**
 * @brief	ADC Pin Configuration
 * @param	ADC_ID	: ADC number
 * @param	channel	: ADC channel
 * @return	Nothing
 */
STATIC INLINE void Chip_SCU_ADC_Channel_Config(uint32_t ADC_ID, uint8_t channel)
{
	LPC_SCU->ENAIO[ADC_ID] |= 1UL << channel;
}

/**
 * @brief	DAC Pin Configuration
 * @return	Nothing
 */
STATIC INLINE void Chip_SCU_DAC_Analog_Config(void)
{
	/*Enable analog function DAC on pin P4_4*/
	LPC_SCU->ENAIO[2] |= 1;
}

/**
 * @brief	Set all I/O Control pin muxing
 * @param	pinArray    : Pointer to array of pin mux selections
 * @param	arrayLength : Number of entries in pinArray
 * @return	Nothing
 */
STATIC INLINE void Chip_SCU_SetPinMuxing(const PINMUX_GRP_T *pinArray, uint32_t arrayLength)
{
	uint32_t ix;
	for (ix = 0; ix < arrayLength; ix++ ) {
		Chip_SCU_PinMuxSet(pinArray[ix].pingrp, pinArray[ix].pinnum, pinArray[ix].modefunc);
	}
}

/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __SCU_18XX_43XX_H_ */
