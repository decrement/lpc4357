/*
 * @brief LPC18xx/43xx GIMA driver
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licenser disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef __GIMA_18XX_43XX_H_
#define __GIMA_18XX_43XX_H_

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup GIMA_18XX_43XX CHIP: LPC18xx/43xx GIMA driver
 * @ingroup CHIP_18XX_43XX_Drivers
 * @{
 */

/**
 * @brief Global Input Multiplexer Array (GIMA) register block structure
 */
typedef struct {						/*!< GIMA Structure */
	__IO uint32_t  CAP0_IN[4][4];		/*!< Timer x CAP0_y capture input multiplexer (GIMA output ((x*4)+y)) */
	__IO uint32_t  CTIN_IN[8];			/*!< SCT CTIN_x capture input multiplexer (GIMA output (16+x)) */
	__IO uint32_t  ADCHS_TRIGGER_IN;	/*!< ADCHS trigger input multiplexer (GIMA output 24) */
	__IO uint32_t  EVENTROUTER_13_IN;	/*!< Event router input 13 multiplexer (GIMA output 25) */
	__IO uint32_t  EVENTROUTER_14_IN;	/*!< Event router input 14 multiplexer (GIMA output 26) */
	__IO uint32_t  EVENTROUTER_16_IN;	/*!< Event router input 16 multiplexer (GIMA output 27) */
	__IO uint32_t  ADCSTART0_IN;		/*!< ADC start0 input multiplexer (GIMA output 28) */
	__IO uint32_t  ADCSTART1_IN;		/*!< ADC start1 input multiplexer (GIMA output 29) */
} LPC_GIMA_T;


/**
  * @brief Product name title=UM10503 Chapter title=LPC43xx Global Input Multiplexer Array (GIMA) Modification date=10/7/2011 Major revision=0 Minor revision=3  (GIMA)
  */

typedef struct {                            /*!< (@ 0x400C7000) GIMA Structure         */
  __IO uint32_t  CAP0_0_IN;                 /*!< (@ 0x400C7000) Timer 0 CAP0_0 capture input multiplexer (GIMA output 0) */
  __IO uint32_t  CAP0_1_IN;                 /*!< (@ 0x400C7004) Timer 0 CAP0_1 capture input multiplexer (GIMA output 1) */
  __IO uint32_t  CAP0_2_IN;                 /*!< (@ 0x400C7008) Timer 0 CAP0_2 capture input multiplexer (GIMA output 2) */
  __IO uint32_t  CAP0_3_IN;                 /*!< (@ 0x400C700C) Timer 0 CAP0_3 capture input multiplexer (GIMA output 3) */
  __IO uint32_t  CAP1_0_IN;                 /*!< (@ 0x400C7010) Timer 1 CAP1_0 capture input multiplexer (GIMA output 4) */
  __IO uint32_t  CAP1_1_IN;                 /*!< (@ 0x400C7014) Timer 1 CAP1_1 capture input multiplexer (GIMA output 5) */
  __IO uint32_t  CAP1_2_IN;                 /*!< (@ 0x400C7018) Timer 1 CAP1_2 capture input multiplexer (GIMA output 6) */
  __IO uint32_t  CAP1_3_IN;                 /*!< (@ 0x400C701C) Timer 1 CAP1_3 capture input multiplexer (GIMA output 7) */
  __IO uint32_t  CAP2_0_IN;                 /*!< (@ 0x400C7020) Timer 2 CAP2_0 capture input multiplexer (GIMA output 8) */
  __IO uint32_t  CAP2_1_IN;                 /*!< (@ 0x400C7024) Timer 2 CAP2_1 capture input multiplexer (GIMA output 9) */
  __IO uint32_t  CAP2_2_IN;                 /*!< (@ 0x400C7028) Timer 2 CAP2_2 capture input multiplexer (GIMA output 10) */
  __IO uint32_t  CAP2_3_IN;                 /*!< (@ 0x400C702C) Timer 2 CAP2_3 capture input multiplexer (GIMA output 11) */
  __IO uint32_t  CAP3_0_IN;                 /*!< (@ 0x400C7030) Timer 3 CAP3_0 capture input multiplexer (GIMA output 12) */
  __IO uint32_t  CAP3_1_IN;                 /*!< (@ 0x400C7034) Timer 3 CAP3_1 capture input multiplexer (GIMA output 13) */
  __IO uint32_t  CAP3_2_IN;                 /*!< (@ 0x400C7038) Timer 3 CAP3_2 capture input multiplexer (GIMA output 14) */
  __IO uint32_t  CAP3_3_IN;                 /*!< (@ 0x400C703C) Timer 3 CAP3_3 capture input multiplexer (GIMA output 15) */
  __IO uint32_t  CTIN_0_IN;                 /*!< (@ 0x400C7040) SCT CTIN_0 capture input multiplexer (GIMA output 16) */
  __IO uint32_t  CTIN_1_IN;                 /*!< (@ 0x400C7044) SCT CTIN_1 capture input multiplexer (GIMA output 17) */
  __IO uint32_t  CTIN_2_IN;                 /*!< (@ 0x400C7048) SCT CTIN_2 capture input multiplexer (GIMA output 18) */
  __IO uint32_t  CTIN_3_IN;                 /*!< (@ 0x400C704C) SCT CTIN_3 capture input multiplexer (GIMA output 19) */
  __IO uint32_t  CTIN_4_IN;                 /*!< (@ 0x400C7050) SCT CTIN_4 capture input multiplexer (GIMA output 20) */
  __IO uint32_t  CTIN_5_IN;                 /*!< (@ 0x400C7054) SCT CTIN_5 capture input multiplexer (GIMA output 21) */
  __IO uint32_t  CTIN_6_IN;                 /*!< (@ 0x400C7058) SCT CTIN_6 capture input multiplexer (GIMA output 22) */
  __IO uint32_t  CTIN_7_IN;                 /*!< (@ 0x400C705C) SCT CTIN_7 capture input multiplexer (GIMA output 23) */
  __IO uint32_t  VADC_TRIGGER_IN;           /*!< (@ 0x400C7060) VADC trigger input multiplexer (GIMA output 24) */
  __IO uint32_t  EVENTROUTER_13_IN;         /*!< (@ 0x400C7064) Event router input 13 multiplexer (GIMA output 25) */
  __IO uint32_t  EVENTROUTER_14_IN;         /*!< (@ 0x400C7068) Event router input 14 multiplexer (GIMA output 26) */
  __IO uint32_t  EVENTROUTER_16_IN;         /*!< (@ 0x400C706C) Event router input 16 multiplexer (GIMA output 27) */
  __IO uint32_t  ADCSTART0_IN;              /*!< (@ 0x400C7070) ADC start0 input multiplexer (GIMA output 28) */
  __IO uint32_t  ADCSTART1_IN;              /*!< (@ 0x400C7074) ADC start1 input multiplexer (GIMA output 29) */
} LPC_GIMA_Type;


/**
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* __GIMA_18XX_43XX_H_ */
