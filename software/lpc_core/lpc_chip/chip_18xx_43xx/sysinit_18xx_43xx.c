/*
 * @brief LPC18xx/LPC43xx Chip specific SystemInit
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "chip.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/* Structure for initial base clock states */
struct CLK_BASE_STATES {
	CHIP_CGU_BASE_CLK_T clk;	/* Base clock */
	CHIP_CGU_CLKIN_T clkin;	/* Base clock source, see UM for allowable souorces per base clock */
	bool autoblock_enab;	/* Set to true to enable autoblocking on frequency change */
	bool powerdn;			/* Set to true if the base clock is initially powered down */
};

static const struct CLK_BASE_STATES InitClkStates[] = {
	{CLK_BASE_SAFE, CLKIN_IRC, true, false},
	{CLK_BASE_APB1, CLKIN_MAINPLL, true, false},
	{CLK_BASE_APB3, CLKIN_MAINPLL, true, false},
	{CLK_BASE_USB0, CLKIN_USBPLL, true, true},
#if defined(CHIP_LPC43XX)
	{CLK_BASE_PERIPH, CLKIN_MAINPLL, true, false},
	{CLK_BASE_SPI, CLKIN_MAINPLL, true, false},
	{CLK_BASE_ADCHS, CLKIN_MAINPLL, true, true},
#endif
	{CLK_BASE_SDIO, CLKIN_MAINPLL, true, false},
	{CLK_BASE_SSP0, CLKIN_MAINPLL, true, false},
	{CLK_BASE_SSP1, CLKIN_MAINPLL, true, false},
	{CLK_BASE_UART0, CLKIN_MAINPLL, true, false},
	{CLK_BASE_UART1, CLKIN_MAINPLL, true, false},
	{CLK_BASE_UART2, CLKIN_MAINPLL, true, false},
	{CLK_BASE_UART3, CLKIN_MAINPLL, true, false},
	{CLK_BASE_OUT, CLKINPUT_PD, true, false},
	{CLK_BASE_APLL, CLKINPUT_PD, true, false},
	{CLK_BASE_CGU_OUT0, CLKINPUT_PD, true, false},
	{CLK_BASE_CGU_OUT1, CLKINPUT_PD, true, false},
};

static void clock_init_delay(void)
{

	volatile unsigned int i;

	for (i = 0; i < 1024;i++) {
		i++;
		i--;
	}
}

static void clock_main_init(void)
{
	int i;
	
	LPC_CGU->XTAL_OSC_CTRL &= ~(1<<2);		  /* 1M -- 20M crystal */
    LPC_CGU->XTAL_OSC_CTRL &= ~(1UL << 0);	  /* Enable crystal */

	clock_init_delay();
	
	/** CGU control clock-source mask bit */
	LPC_CGU->PLL1_CTRL &= ~(0xF<<24);
	LPC_CGU->PLL1_CTRL |= 0x06 <<24;		 /* Clock source: crystal oscillator */

    /** CGU control auto block mask bit */
	LPC_CGU->PLL1_CTRL |= (1<<11);

	/* 
	 * BYPASS = 0, CCO clock send to post-divider 
	 * FBSEL = 1,  PLL output is used as feedback divider 
	 * DIRECT = 0, PLL direct CCO output disable
	 * During this integer mode, the Formula:
	 *       Fout = M * (Fin / N)
	 *       Fcco = Fout * 2          (156M < Fcco < 320M)
	 */
	LPC_CGU->PLL1_CTRL &= ~(  (1 << 6) 
							| (1 << 1) 
							| (1 << 7) 
							| (0x03<<8) 
							| (0xFF<<16) 
							| (0x03<<12));

	/*                      MSEL      NSEL      PSEL            */

	/*204M frequency*/
    LPC_CGU->PLL1_CTRL |= (0x10<<16) | (0<<12) | (1<<8) | (1 << 6) | (1 << 7);

	/*180M frequency*/
	//LPC_CGU->PLL1_CTRL |= (0xE<<16) | (0<<12) | (1<<8) | (1 << 6) | (1 << 7);

	 
	LPC_CGU->PLL1_CTRL &= ~(0x01);
    while((LPC_CGU->PLL1_STAT&1) == 0x0);
	clock_init_delay();
	LPC_CGU2->BASE_M4_CLK = ((0x0009 << 24) | (0x0001 << 11));
	clock_init_delay();

	/* Setup system base clocks and initial states. This won't enable and
	disable individual clocks, but sets up the base clock sources for
	each individual peripheral clock. */
	for (i = 0; i < (sizeof(InitClkStates) / sizeof(InitClkStates[0])); i++) {
		Chip_Clock_SetBaseClock(InitClkStates[i].clk, InitClkStates[i].clkin,
						InitClkStates[i].autoblock_enab, InitClkStates[i].powerdn);
	}
	
}


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
/* Setup Chip Core clock */
void Chip_SetupCoreClock(CHIP_CGU_CLKIN_T clkin, uint32_t core_freq, bool setbase)
{
	#if 1
	
	clock_main_init();

	#else
	
	int i;
	if (clkin == CLKIN_CRYSTAL) {
		/* Switch main system clocking to crystal */
		Chip_Clock_EnableCrystal();
		clock_init_delay();
	}
	
	Chip_Clock_SetBaseClock(CLK_BASE_MX, clkin, true, false);

	if (core_freq > 110000000UL) {
		/* Setup PLL for 100MHz and switch main system clocking */
		Chip_Clock_SetupMainPLLHz(clkin, CGU_IRC_FREQ, 110 * 1000000, 110 * 1000000);
		clock_init_delay();
		Chip_Clock_SetBaseClock(CLK_BASE_MX, CLKIN_MAINPLL, true, false);
	}
	clock_init_delay();
	
	/* Setup PLL for maximum clock */
	Chip_Clock_SetupMainPLLHz(clkin, OscRateIn, core_freq, core_freq);
	clock_init_delay();

	if (setbase) {
		/* Setup system base clocks and initial states. This won't enable and
		   disable individual clocks, but sets up the base clock sources for
		   each individual peripheral clock. */
		for (i = 0; i < (sizeof(InitClkStates) / sizeof(InitClkStates[0])); i++) {
			Chip_Clock_SetBaseClock(InitClkStates[i].clk, InitClkStates[i].clkin,
									InitClkStates[i].autoblock_enab, InitClkStates[i].powerdn);
		}
	}
	
	#endif
	
}

/* Setup system clocking */
void Chip_SetupXtalClocking(void)
{
	Chip_SetupCoreClock(CLKIN_CRYSTAL, MAX_CLOCK_FREQ, true);
}

/* Set up and initialize hardware prior to call to main */
void Chip_SetupIrcClocking(void)
{
	Chip_SetupCoreClock(CLKIN_IRC, MAX_CLOCK_FREQ, true);
}

/* Set up and initialize hardware prior to call to main */
void Chip_SystemInit(void)
{
	/* Initial internal clocking */
	Chip_SetupIrcClocking();
}
