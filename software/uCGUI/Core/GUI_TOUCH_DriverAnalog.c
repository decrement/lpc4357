/*
*********************************************************************************************************
*                                                uC/GUI
*                        Universal graphic software for embedded applications
*
*                       (c) Copyright 2002, Micrium Inc., Weston, FL
*                       (c) Copyright 2002, SEGGER Microcontroller Systeme GmbH
*
*              �C/GUI is protected by international copyright laws. Knowledge of the
*              source code may not be used to write a similar product. This file may
*              only be used in accordance with a license and should not be redistributed
*              in any way. We appreciate your understanding and fairness.
*
----------------------------------------------------------------------
File        : GUITOUCH.C
Purpose     : Touch screen manager
----------------------------------------------------------------------
This module handles the touch screen. It is configured in the file
GUITouch.conf.h (Should be located in the Config\ directory).
----------------------------------------------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LCD_Private.h"      /* private modul definitions & config */
#include "GUI_Protected.h"
#include <raw_api.h>
#include "lcdtouch.h"

/* Generate code only if configuration says so ! */


#include "GUITouchConf.h"    /* Located in GUIx, will include GUITouchConf.h */


/*********************************************************************
*
*       _StoreUnstable
*/
static void _StoreUnstable(int x, int y) {
  static int _xLast = -1;
  static int _yLast = -1;
  int xOut, yOut;

  if ((x != -1) && (y != -1) && (_xLast != -1) && (_yLast != -1)) {
    xOut = _xLast;    
    yOut = _yLast;    
  } else {
    xOut = -1;
    yOut = -1;    
  }
  _xLast = x;
  _yLast = y;
  GUI_TOUCH_StoreUnstable(xOut, yOut);
}

static RAW_S32 last_x_pixsel;
static RAW_S32 last_y_pixsel;

#define PIXCEL_STABLE 15

/*********************************************************************
*
*       GUI_TOUCH_Exec
*/
void GUI_TOUCH_Exec(void) 
{
	RAW_S32 x,y;
	RAW_S32 ret;

	RAW_S32 x_pixsel;
	RAW_S32 y_pixsel;

	ret = touch_x_y_get(&x, &y);

	if (ret == -1) {

		_StoreUnstable(-1, -1);	
		return;
	}

	ret = touch_phy_to_pixsel(x, y, &x_pixsel, &y_pixsel);

	if (ret == -1) {

		_StoreUnstable(-1, -1);	
		return;
	}

	if (  (abs(x_pixsel - last_x_pixsel) < PIXCEL_STABLE) && (abs(y_pixsel - last_y_pixsel) < PIXCEL_STABLE)  ) {

		x_pixsel = last_x_pixsel;
		y_pixsel = last_y_pixsel;
		
		_StoreUnstable(x_pixsel, y_pixsel);
	}
	
	else {
		
		last_x_pixsel = x_pixsel;
		last_y_pixsel = y_pixsel;

		_StoreUnstable(x_pixsel, y_pixsel);
	}
		

	
 
}

/*************************** End of file ****************************/
