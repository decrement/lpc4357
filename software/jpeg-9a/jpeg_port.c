#include <raw_api.h>
#include <yaffsfs.h>

extern void Uart_Printf(char * fmt,...);


int JFWRITE(void *file, unsigned char *buffer, int count)


{
	int bytes_write;
	
	bytes_write = yaffs_write((int)file, buffer, count);

	return bytes_write;

}


int JFREAD(void *file, unsigned char *buffer, int count)
{
	int size_read;

	//Uart_Printf("count is %d\n", count);
	
	//Uart_Printf("f read is %d\n", (int)file);


	if (count == 0) {
		return 0;

	}
	
	else {
		size_read = yaffs_read((int)file, buffer, count);
	}
	
	return size_read;
	
}



int JFSEEK(void *file, int offset, int flag)
{
	int ret;

	ret = yaffs_lseek((int)(file), offset, flag);

	return ret;
}

int JFCLOSE(void *file)
{
	int ret;

	
	//Uart_Printf("f close is %d\n", (int)file_close);
	ret = yaffs_close((int)(file));
	return ret;
}


