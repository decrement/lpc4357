#include <raw_api.h>
#include <cmsis.h>
#include <board.h>


#define SHELL_STACK_SIZE   256

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;




void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

extern void  cpu_timer_init(void);

void shell_task(void *arg)
{
	RAW_U8 old_priority;

	//cpu_timer_init();
	OS_CPU_SysTickInit();
	//measure_overhead();
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	/*led1 ,led2 ,led3 ,led4 are set to gpio output*/
	Chip_SCU_PinMuxSet(0xe, 14, FUNC4);
	Chip_SCU_PinMuxSet(0xe, 15, FUNC4);
	Chip_SCU_PinMuxSet(0xf, 1, FUNC4);
	Chip_SCU_PinMuxSet(0xf, 3, FUNC4);
	
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 7, 14);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 7, 15);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 7, 16);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 7, 18);

	while (1) {

		test_count++;

		/*flashing  led1 ,led2 ,led3, led4*/
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 14, 0);
		raw_sleep(RAW_TICKS_PER_SECOND / 2);
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 14, 1);
		raw_sleep(RAW_TICKS_PER_SECOND / 2);
		
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 15, 0);
		raw_sleep(RAW_TICKS_PER_SECOND / 2);
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 15, 1);
		raw_sleep(RAW_TICKS_PER_SECOND / 2);

		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 16, 0);
		raw_sleep(RAW_TICKS_PER_SECOND / 2);
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 16, 1);
		raw_sleep(RAW_TICKS_PER_SECOND / 2);
		
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 18, 0);
		raw_sleep(RAW_TICKS_PER_SECOND / 2);
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 18, 1);
		raw_sleep(RAW_TICKS_PER_SECOND / 2);

	}
}


int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


