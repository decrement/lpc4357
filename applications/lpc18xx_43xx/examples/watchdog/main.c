#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


static volatile int wdtFeedState;
static volatile bool On = false, On1 = false;


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();

	/*just for watch test not to be used in this way!*/	
	if (wdtFeedState == 0) {
		On = (bool) !On;
		Chip_WWDT_Feed(LPC_WWDT);
	}
	
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}





void register_task_stack_command(void);

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/* WDT interrupt handler */
static void wdt_handle(void) {
	uint32_t wdtStatus = Chip_WWDT_GetStatus(LPC_WWDT);

	On = (bool) !On;

	/* The chip will reset before this happens, but if the WDT doesn't
	   have WWDT_WDMOD_WDRESET enabled, this will hit once */
	if (wdtStatus & WWDT_WDMOD_WDTOF) {
		/* A watchdog feed didn't occur prior to window timeout */
		Chip_WWDT_ClearStatusFlag(LPC_WWDT, WWDT_WDMOD_WDTOF);

		if (wdtFeedState == 2) {
			Chip_WWDT_Start(LPC_WWDT);	/* Needs restart */
		}
	}

	/* Handle warning interrupt */
	if (wdtStatus & WWDT_WDMOD_WDINT) {
		/* A watchdog feed didn't occur prior to warning timeout */
		Chip_WWDT_ClearStatusFlag(LPC_WWDT, WWDT_WDMOD_WDINT);

		if (wdtFeedState == 1) {
			Chip_WWDT_Feed(LPC_WWDT);
		}
	}
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/**
 * @brief	Event router Interrupt Handler
 * @return	Nothing
 * Handles event router events
 */
void EVRT_IRQHandler(void)
{
	On1 = !On1;

	/* Clear watchdog timer interrupt in event router (ok to clear before
	   watchdog clear on edge based event router events) */
	Chip_EVRT_ClrPendIntSrc(EVRT_SRC_WWDT);

	wdt_handle();
}

/**
 * @brief	watchdog timer Interrupt Handler
 * @return	Nothing
 * @note	Handles watchdog timer warning and timeout events
 */
void WDT_IRQHandler(void)
{
	wdt_handle();
}


void shell_task(void *arg)
{
	RAW_U8 old_priority;

	uint8_t ch;
	OS_CPU_SysTickInit();
	
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	//NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	//NVIC_EnableIRQ(UARTx_IRQn);
	//uart3_int_init();

	emc_init();
	sdram_init();
	DEBUGOUT("start watch dog test\r\n");
	
		/* Watchdog will be fed on each watchdog interrupt */
	wdtFeedState = 0;

	/* Initialize WWDT and event router */
	Chip_WWDT_Init(LPC_WWDT);
	Chip_EVRT_Init();

	/* Set watchdog feed time constant to 0.1s
	   Set watchdog warning time to 512 ticks after feed time constant
	   Set watchdog window time to 0.9s */
	Chip_WWDT_SetTimeOut(LPC_WWDT, WDT_OSC / 10);
	Chip_WWDT_SetWarning(LPC_WWDT, 512);
	Chip_WWDT_SetWindow(LPC_WWDT, WDT_OSC - (WDT_OSC / 10));

	/* Configure WWDT to reset on timeout */
	Chip_WWDT_SetOption(LPC_WWDT, WWDT_WDMOD_WDRESET);

	/* Clear watchdog warning and timeout interrupts */
	Chip_WWDT_ClearStatusFlag(LPC_WWDT, WWDT_WDMOD_WDTOF | WWDT_WDMOD_WDINT);

	/* Initiate EVRT, route interrupt signal from WWDT to EVRT,
	   enable EVRT WWDT interrupt */
	Chip_EVRT_ConfigIntSrcActiveType(EVRT_SRC_WWDT, EVRT_SRC_ACTIVE_RISING_EDGE);
	Chip_EVRT_SetUpIntSrc(EVRT_SRC_WWDT, ENABLE);

	/* Start watchdog */
	Chip_WWDT_Start(LPC_WWDT);

	/* Setup Systick for a 10Hz tick rate. Systick clock is clocked at
	   CPU core clock speed */
	//SysTick_Config(Chip_Clock_GetRate(CLK_MX_MXCORE) / 20);

	/* Enable watchdog interrupt */
	NVIC_EnableIRQ(WWDT_IRQn);

	/* Watchdog test options */
	DEBUGOUT("Press '1' to enable watchdog feed on systick interrupt\n\r");
	DEBUGOUT("Press '2' to enable watchdog feed on warning interrupt\n\r");
	DEBUGOUT("Press '3' to disable watchdog feed (will reset device)\n\r");
	DEBUGOUT("Press '4' to switch from WDT interrupt to event router handler\n\r");

	while (1) {
		do {
			ch = (uint8_t) DEBUGIN();
			
		} while ((ch < '1') || (ch > '4'));

		switch (ch) {
		case '1':
		default:
			wdtFeedState = 0;
			DEBUGOUT("Watchdog feed on systick interrupt\r\n");
			break;

		case '2':
			wdtFeedState = 1;
			DEBUGOUT("Watchdog feed on warning interrupt\r\n");
			break;

		case '3':
			wdtFeedState = 2;
			DEBUGOUT("No feeding - board will reset\r\n");
			break;

		case '4':
			DEBUGOUT("using event router instead of WDT interrupt\r\n");
			NVIC_DisableIRQ(WWDT_IRQn);
			NVIC_EnableIRQ(EVENTROUTER_IRQn);
			break;
		}
	}
	

	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


