#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>

#include "can0.h"
#include "lpc43xx_scu.h"


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);

#define RX_MSG_ID			(0x100)
#define TX_MSG_ID			(0x200)

message_object recv_obj;

void RX_callback(uint32_t msg_no)
{
	CAN_ReadMsg(msg_no, &recv_obj);
	recv_obj.id = TX_MSG_ID;

	/* resend message using slot 17 */
	CAN_Send(17, (uint32_t *)&recv_obj);
}

void TX_callback(uint32_t msg_no)
{
}


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

void shell_task(void *arg)
{
	RAW_U8 old_priority;
	message_object send_obj;

	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();
	printf("start can test\r\n");

	/* Setup IO pin */
	scu_pinmux(0x3,2,MD_PLN,FUNC2);		/* PE.3 CAN TD1, FUNC1 */
	scu_pinmux(0x3,1,MD_PLN | MD_EZI ,FUNC2);		/* PE.2	CAN RD1, FUNC1 */

	/** set CAN Peripheral Clock = 12MHz */
	LPC_CGU2->BASE_APB3_CLK = (0x06 << 24) | (1 << 11);
	
	CAN_Init( CAN_BITRATE500K12MHZ, CLKDIV1 , TX_callback, RX_callback);
	NVIC_EnableIRQ(C_CAN0_IRQn);
	
	recv_obj.id = RX_MSG_ID;
	CAN_Recv(1, (uint32_t*)&recv_obj, 0);
	send_obj.id = TX_MSG_ID;
	send_obj.dlc = 8;
	send_obj.data[0] = 0x01;
	send_obj.data[1] = 0x02;
	send_obj.data[2] = 0x03;
	send_obj.data[3] = 0x04;
	send_obj.data[4] = 0x05;
	send_obj.data[5] = 0x06;
	send_obj.data[6] = 0x07;
	send_obj.data[7] = 0x08;
	CAN_Send(17, (uint32_t *)&send_obj);
	
	
	while (1);

	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


