#include <raw_api.h>
#include <cmsis.h>
#include <board.h>


#define SHELL_STACK_SIZE   256

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;


#define __DSB()                           __dsb(0xF)
#define __SEV                             __sev



void RIT_IRQHandler(void)
{
	raw_enter_interrupt();
	Chip_RIT_ClearInt(LPC_RITIMER);
	raw_time_tick();
	raw_finish_int();	
}
	

void ritimer_setup(void)
{
	/* Initialize RITimer */
	Chip_RIT_Init(LPC_RITIMER);

	/* Configure RIT for a 10ms interrupt tick rate */
	Chip_RIT_SetTimerInterval(LPC_RITIMER, 10);

	NVIC_EnableIRQ(RITIMER_IRQn);
}


RAW_U32 test_count;
RAW_SEMAPHORE m0_sema;
void M4_IRQHandler(void)
{
	raw_enter_interrupt();
	Chip_CREG_ClearM4Event();
	raw_semaphore_put(&m0_sema);
	raw_finish_int();

}


void shell_task(void *arg)
{
	RAW_U8 old_priority;
	ritimer_setup();
	raw_semaphore_create(&m0_sema, "m0_sema", 0);
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	/*Enable interrupt from m4*/
	NVIC_EnableIRQ(M4_IRQn);

	
	while (1) {

		raw_semaphore_get(&m0_sema, RAW_WAIT_FOREVER);
		raw_sleep(RAW_TICKS_PER_SECOND);
		test_count++;
		printf("test_count is %d\r\n", test_count);
		__DSB();
		__SEV();

	}

}



int main()
{
	DEBUGSTR("m0 has started\r\n");
	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	DEBUGSTR("m111111\r\n");
	raw_os_start();

	return 0;
	
}


