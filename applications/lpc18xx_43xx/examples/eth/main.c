#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>
#include "lpc43xx_emac.h"

#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

#define ENET_NUM_TX_DESC 4
#define ENET_NUM_RX_DESC 4
#define EMAC_ETH_MAX_FLEN (1536)



uint8_t TXBuffer[ENET_NUM_TX_DESC][EMAC_ETH_MAX_FLEN];


PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;


extern void *ENET_RXGet(int32_t *bytes);
extern void ENET_RXQueue(void *buffer, int32_t bytes);
extern void ENET_TXQueue(void *buffer, int32_t bytes);
extern void *ENET_TXBuffClaim(void);


/* Local index and check function */
static __INLINE int32_t incIndex(int32_t index, int32_t max)
{
	index++;
	if (index >= max) {
		index = 0;
	}

	return index;
}


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}


RAW_U8  g_ethMac[6];


char eth_frame[42] = {
0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 	//destination MAC/*destination ethernet addrress */
0xe0, 0x22, 0x33,0x44, 0x55, 0x66,  	//source MAC	 /* source ethernet addresss */
0x08, 0x06,								//frame format   /*ethernet pachet type*/
0x00, 0x01 , 							//hardware format
0x08, 0x00,								//protocol format
0x06,									//hardware address length
0x04,									//protocol address length
0x00, 0x01 ,							//arp/RARP operation
0xe0, 0x22, 0x33,0x44, 0x55, 0x66,  	//source MAC	 /*sender hardware address*/
0xC0, 0xA8, 0, 0x50 , 					//source ip 	 /*sender protocol address*/
0, 0, 0 ,0 ,0 ,0 ,						//dest mac fill	 /*target hardware address */
0xC0, 0xA8, 0, 0x64						//dest mac		 /*target protocol address */
};




int enet_tx_busy(void);


int control;

void shell_task(void *arg)
{
	RAW_U8 old_priority;
	RAW_U32 count;
	RAW_U8 *workbuff;
	RAW_S32 rxBytes;
	RAW_U32 txNextIndex;
	RAW_U32 physts = 0;
	
	count= 0;
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();

	g_ethMac[0] = 0xe0;
    g_ethMac[1] = 0x22;
    g_ethMac[2] = 0x33;
    g_ethMac[3] = 0x44;
    g_ethMac[4] = 0x55;
    g_ethMac[5] = 0x66;

	printf("start eth test\r\n");

	/*lan8720 must need hard reset or phy does not work*/
	#if 1

	Chip_SCU_PinMuxSet(0x6, 3, FUNC0 | MD_PLN_FAST);
		
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 3, 2);

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 3, 2, 0);
	
	raw_sleep(10);

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 3, 2, 1);
	
	raw_sleep(10);

	#endif
	
	
  	ethInit(0, g_ethMac);

	while (1) {

		/* PHY status state machine, LED on when connected. This function will
		not block. */
		physts = lpcPHYStsPoll();

		/* Only check for connection state when the PHY status has changed */
		if (physts & PHY_LINK_CHANGED) {
			
			if (physts & PHY_LINK_CONNECTED) {
				
				//Board_LED_Set(0, true);

				printf("Link connect success\r\n");
				break;
			
			}
			
			else {
				printf("Link is unconnected, please wait\r\n");
				//Board_LED_Set(0, false);
				
			}

		}

	
	}
	
  	txNextIndex = 0;
	
	while (1) {

		
		if (enet_tx_busy() == 0) {
			workbuff = (uint8_t *) TXBuffer[txNextIndex];
			raw_memcpy(workbuff, eth_frame, 42);

			txNextIndex = incIndex(txNextIndex, ENET_NUM_TX_DESC);
			ENET_TXQueue(workbuff, 42);
			ENET_TXBuffClaim();
		}

		else {

			ENET_TXBuffClaim();
		}


		/* Check for receive packets */
		workbuff = ENET_RXGet(&rxBytes);

		if (workbuff) {

			printf("receive size %d %d\r\n", rxBytes, count++);
			/* Re-queue the (same) packet again */
			ENET_RXQueue(workbuff, EMAC_ETH_MAX_FLEN);
			
		}

		raw_sleep(10);

	}
	
	
}

int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


