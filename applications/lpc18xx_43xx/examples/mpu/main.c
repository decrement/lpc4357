#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

extern const unsigned char successwave[183340];

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


/* Constants required to access and manipulate the MPU. */
#define portMPU_TYPE							( ( volatile unsigned long * ) 0xe000ed90 )
#define portMPU_REGION_BASE_ADDRESS				( ( volatile unsigned long * ) 0xe000ed9C )
#define portMPU_REGION_ATTRIBUTE				( ( volatile unsigned long * ) 0xe000edA0 )
#define portMPU_CTRL							( ( volatile unsigned long * ) 0xe000ed94 )
#define portMPU_NUMBERS							( ( volatile unsigned long * ) 0xe000ed98 )


#define portEXPECTED_MPU_TYPE_VALUE				( 8UL << 8UL ) /* 8 regions, unified. */
#define portMPU_ENABLE							( 0x01UL )
#define portMPU_BACKGROUND_ENABLE				( 1UL << 2UL )
#define portPRIVILEGED_EXECUTION_START_ADDRESS	( 0UL )
#define portMPU_REGION_VALID					( 0x10UL )
#define portMPU_REGION_ENABLE					( 0x01UL )

#define portUNPRIVILEGED_FLASH_REGION		( 0UL )
#define portPRIVILEGED_FLASH_REGION			( 1UL )
#define portPRIVILEGED_RAM_REGION			( 2UL )
#define portGENERAL_PERIPHERALS_REGION		( 3UL )
#define portSTACK_REGION					( 4UL )
#define portFIRST_CONFIGURABLE_REGION	    ( 5UL )
#define portLAST_CONFIGURABLE_REGION		( 7UL )

#define portMPU_REGION_NO_ACCESS	   ( 0x00UL << 24UL )
#define portMPU_REGION_READ_WRITE				( 0x03UL << 24UL )
#define portMPU_REGION_PRIVILEGED_READ_ONLY		( 0x05UL << 24UL )
#define portMPU_REGION_READ_ONLY				( 0x06UL << 24UL )
#define portMPU_REGION_PRIVILEGED_READ_WRITE	( 0x01UL << 24UL )
#define portMPU_REGION_CACHEABLE_BUFFERABLE		( 0x07UL << 16UL )
#define portMPU_REGION_EXECUTE_NEVER			( 0x01UL << 28UL )


/* Constants required to access and manipulate the NVIC. */
#define portNVIC_SYSTICK_CTRL					( ( volatile unsigned long * ) 0xe000e010 )
#define portNVIC_SYSTICK_LOAD					( ( volatile unsigned long * ) 0xe000e014 )
#define portNVIC_SYSPRI2						( ( volatile unsigned long * ) 0xe000ed20 )
#define portNVIC_SYSPRI1						( ( volatile unsigned long * ) 0xe000ed1c )
#define portNVIC_SYS_CTRL_STATE					( ( volatile unsigned long * ) 0xe000ed24 )
#define portNVIC_MEM_FAULT_ENABLE				( 1UL << 16UL )


static unsigned long prvGetMPURegionSizeSetting(unsigned long ulActualSizeInBytes)
{
unsigned long ulRegionSize, ulReturnValue = 4;

	/* 32 is the smallest region size, 31 is the largest valid value for
	ulReturnValue. */
	for( ulRegionSize = 32UL; ulReturnValue < 31UL; ( ulRegionSize <<= 1UL ) )
	{
		if( ulActualSizeInBytes <= ulRegionSize )
		{
			break;
		}
		else
		{
			ulReturnValue++;
		}
	}

	/* Shift the code by one before returning so it can be written directly
	into the the correct bit position of the attribute register. */
	return ( ulReturnValue << 1UL );
}

__align(32) char test_mpu_arry1[35];
__align(32) char test_mpu_arry2[35];
__align(32) char test_mpu_arry3[35];
__align(32) char test_mpu_arry4[35];
__align(32) char test_mpu_arry5[35];
__align(32) char test_mpu_arry6[35];
__align(32) char test_mpu_arry7[35];
__align(32) char test_mpu_arry8[35];








void system_setup_mpu( RAW_U32 int_stack)
{
	int i;

	#if 0
	*portMPU_REGION_BASE_ADDRESS =	( ( unsigned long )int_stack) | /* Base address. */
						( portMPU_REGION_VALID ) |
						( 0 );

	*portMPU_REGION_ATTRIBUTE =		( portMPU_REGION_READ_ONLY ) |
						( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
						(prvGetMPURegionSizeSetting(32)) | (portMPU_REGION_ENABLE);
	#endif

	/* First setup the entire flash for unprivileged read only access. */
	*portMPU_REGION_BASE_ADDRESS =	( ( unsigned long )test_mpu_arry2) | /* Base address. */
						( portMPU_REGION_VALID ) |
						( 1 );

	*portMPU_REGION_ATTRIBUTE =		( portMPU_REGION_PRIVILEGED_READ_WRITE ) |
						( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
						(prvGetMPURegionSizeSetting(32)) | (portMPU_REGION_ENABLE);

	/* First setup the entire flash for unprivileged read only access. */
	*portMPU_REGION_BASE_ADDRESS =	( ( unsigned long )test_mpu_arry3) | /* Base address. */
						( portMPU_REGION_VALID ) |
						( 2 );

	*portMPU_REGION_ATTRIBUTE =		( portMPU_REGION_PRIVILEGED_READ_WRITE ) |
						( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
						(prvGetMPURegionSizeSetting(32)) | (portMPU_REGION_ENABLE);

	/* First setup the entire flash for unprivileged read only access. */
	*portMPU_REGION_BASE_ADDRESS =	( ( unsigned long )test_mpu_arry4) | /* Base address. */
						( portMPU_REGION_VALID ) |
						( 3 );

	*portMPU_REGION_ATTRIBUTE =		( portMPU_REGION_PRIVILEGED_READ_WRITE ) |
						( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
						(prvGetMPURegionSizeSetting(32)) | (portMPU_REGION_ENABLE);

	/* First setup the entire flash for unprivileged read only access. */
	*portMPU_REGION_BASE_ADDRESS =	( ( unsigned long )test_mpu_arry5) | /* Base address. */
						( portMPU_REGION_VALID ) |
						( 4 );

	*portMPU_REGION_ATTRIBUTE =		( portMPU_REGION_PRIVILEGED_READ_WRITE ) |
						( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
						(prvGetMPURegionSizeSetting(32)) | (portMPU_REGION_ENABLE);

	/* First setup the entire flash for unprivileged read only access. */
	*portMPU_REGION_BASE_ADDRESS =	( ( unsigned long )test_mpu_arry6) | /* Base address. */
						( portMPU_REGION_VALID ) |
						( 5 );

	/*region 6 is reserved for interrupt*/
	
	#if 0
	*portMPU_REGION_ATTRIBUTE =		( portMPU_REGION_PRIVILEGED_READ_WRITE ) |
						( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
						(prvGetMPURegionSizeSetting(32)) | (portMPU_REGION_ENABLE);

	/* First setup the entire flash for unprivileged read only access. */
	*portMPU_REGION_BASE_ADDRESS =	( ( unsigned long )test_mpu_arry7) | /* Base address. */
						( portMPU_REGION_VALID ) |
						( 6 );

	*portMPU_REGION_ATTRIBUTE =		( portMPU_REGION_READ_ONLY ) |
						( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
						(prvGetMPURegionSizeSetting(32)) | (portMPU_REGION_ENABLE);
	#endif
	

	/*region 7 is reserved for task stack*/

	#if 0
	*portMPU_REGION_BASE_ADDRESS =	( ( unsigned long )test_mpu_arry8) | /* Base address. */
						( portMPU_REGION_VALID ) |
						( 7);
	
	*portMPU_REGION_ATTRIBUTE =		(portMPU_REGION_READ_ONLY ) |
						( portMPU_REGION_CACHEABLE_BUFFERABLE ) |
						(prvGetMPURegionSizeSetting(32)) | (portMPU_REGION_ENABLE);
	
	#endif


	/*The following has been started at init*/
	#if 0
	/* Enable the memory fault exception. */
	*portNVIC_SYS_CTRL_STATE |= portNVIC_MEM_FAULT_ENABLE;

	/* Enable the MPU with the background region configured. */
	*portMPU_CTRL |= (portMPU_ENABLE | portMPU_BACKGROUND_ENABLE);
	#endif
	
	
}




void SysTick_Handler(void)
{
	/*for test mpu interrupt stack protect*/
	#if 0
	RAW_U8 test[0x600];
	int i;
	raw_memset(test, 0, 0x600);
	#endif
	
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

static void prepare_src_data(uint32_t *src, int sz)
{
	int i;
	for (i = 0; i < sz; i++) {
		src[i] = i | (~i << (16 - (i & 15))); /* Fill it with some pattern */
	}
}



static void check_src_data(uint32_t *src, int sz)
{
	int i;
	RAW_U32 temp;
	
	for (i = 0; i < sz; i++) {

		temp = i | (~i << (16 - (i & 15)));
		
		if (src[i] != temp) {


			RAW_ASSERT(0);
		}
	}
}

extern RAW_U32  Stack_Mem ;

RAW_U32 aligned_32_int_stack;

void shell_task(void *arg)
{
	RAW_U8 old_priority;
	RAW_U32 i;
	
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();

	printf("region number is %x\r\n",  *portMPU_TYPE);
	printf("Stack_Mem is %x\r\n",  &Stack_Mem );

	raw_isr_stack_check(&i);
	printf("is stack size is %d\r\n",i);
	
	aligned_32_int_stack = ((RAW_U32)(&Stack_Mem) + 32) & 0xffffffe0;

	system_setup_mpu(aligned_32_int_stack);

	test_mpu_arry1[31] = 1;
	test_mpu_arry2[31] = 1;
	test_mpu_arry3[31] = 1;
	test_mpu_arry4[31] = 1;

	test_mpu_arry5[31] = 1;
	test_mpu_arry6[31] = 1;

	/*write is not allowed*/
	//test_mpu_arry7[31] = 1;

	raw_sleep(1);

	/*stack has beed protected by MPU*/
	//memset(shell_task_obj, 0, 65);
	
	while (1) {

		raw_sleep(10);

		test_count++;
	
	}
	

	
}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


