#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>

#define debugstr(str)  DEBUGSTR(str)


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


/* Number of sectors to read/write */
#define NUM_SECTORS     256

/* Number of iterations of measurement */
#define NUM_ITER        10

/* Starting sector number for read/write */
#define START_SECTOR    32

/* Buffer size (in bytes) for R/W operations */
#define BUFFER_SIZE     (NUM_SECTORS * MMC_SECTOR_SIZE)

/* Buffers to store original data of SD/MMC card.
 * The data will be stored in this buffer, once read/write measurement
 * completed, the original contents will be restored into SD/MMC card.
 * This is done in order avoid corurupting the SD/MMC card
 */
static uint32_t *Buff_Backup = (uint32_t *) 0x28200000;

/* Buffers for read/write operation */
static uint32_t *Buff_Rd = (uint32_t *) 0x28000000;
static uint32_t *Buff_Wr = (uint32_t *) 0x28100000;

/* Measurement data */
static uint32_t rd_ticks[NUM_ITER];
static uint32_t wr_ticks[NUM_ITER];

/* SD/MMC card information */
/* Number of sectors in SD/MMC card */
static int32_t tot_secs;


/* SDMMC card info structure */
mci_card_struct sdcardinfo;

/* SDIO wait flag */
static volatile int32_t sdio_wait_exit = 0;


/* Delay callback for timed SDIF/SDMMC functions */
static void sdmmc_waitms(uint32_t time)
{
	/* In an RTOS, the thread would sleep allowing other threads to run.
	   For standalone operation, we just spin on RI timer */
	int32_t curr = (int32_t) Chip_RIT_GetCounter(LPC_RITIMER);
	int32_t final = curr + ((SystemCoreClock / 1000) * time);

	if (final == curr) return;

	if ((final < 0) && (curr > 0)) {
		while (Chip_RIT_GetCounter(LPC_RITIMER) < (uint32_t) final) {}
	}
	else {
		while ((int32_t) Chip_RIT_GetCounter(LPC_RITIMER) < final) {}
	}

	return;
}

/**
 * @brief	Sets up the SD event driven wakeup
 * @param	bits : Status bits to poll for command completion
 * @return	Nothing
 */
static void sdmmc_setup_wakeup(void *bits)
{
	uint32_t bit_mask = *((uint32_t *)bits);
	/* Wait for IRQ - for an RTOS, you would pend on an event here with a IRQ based wakeup. */
	NVIC_ClearPendingIRQ(SDIO_IRQn);
	sdio_wait_exit = 0;
	Chip_SDIF_SetIntMask(LPC_SDMMC, bit_mask);
	NVIC_EnableIRQ(SDIO_IRQn);
}

/**
 * @brief	A better wait callback for SDMMC driven by the IRQ flag
 * @return	0 on success, or failure condition (-1)
 */
static uint32_t sdmmc_irq_driven_wait(void)
{
	uint32_t status;

	/* Wait for event, would be nice to have a timeout, but keep it  simple */
	while (sdio_wait_exit == 0) {}

	/* Get status and clear interrupts */
	status = Chip_SDIF_GetIntStatus(LPC_SDMMC);
	Chip_SDIF_ClrIntStatus(LPC_SDMMC, status);
	Chip_SDIF_SetIntMask(LPC_SDMMC, 0);

	return status;
}


/* Initialize SD/MMC */
static void App_SDMMC_Init()
{
	
	raw_memset(&sdcardinfo, 0, sizeof(sdcardinfo));
	sdcardinfo.card_info.evsetup_cb = sdmmc_setup_wakeup;
	sdcardinfo.card_info.waitfunc_cb = sdmmc_irq_driven_wait;
	sdcardinfo.card_info.msdelay_func = sdmmc_waitms;

	/*  SD/MMC initialization */
	Board_SDMMC_Init();

	/* The SDIO driver needs to know the SDIO clock rate */
	Chip_SDIF_Init(LPC_SDMMC);

}

/* Buffer initialisation function */
static void Prepare_Buffer(uint32_t value)
{
    uint32_t i;

    for(i = 0; i < (BUFFER_SIZE/sizeof(uint32_t)); i++)
    {
        Buff_Rd[i] = 0x0;
        Buff_Wr[i] = i + value;
    }
}

void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

/* Print the result of a data transfer */
static void print_meas_data(void)
{
	static char debugBuf[64];
    uint64_t tot_sum_rd, tot_sum_wr;
    uint32_t i, rd_ave, wr_ave, rd_time, wr_time;
	uint32_t clk = SystemCoreClock/1000000;

    /* Print Number of Interations */
	debugstr("\r\n=====================\r\n");
    debugstr("SDMMC Measurements \r\n");
	debugstr("=====================\r\n");
	sprintf(debugBuf, "No. of Iterations: %u \r\n", NUM_ITER);
	debugstr(debugBuf);
    sprintf(debugBuf, "No. of Sectors for R/W: %u \r\n", NUM_SECTORS);
    debugstr(debugBuf);
	sprintf(debugBuf, "Sector size : %u bytes \r\n", MMC_SECTOR_SIZE);
	debugstr(debugBuf);
	sprintf(debugBuf, "Data Transferred : %u bytes\r\n", (MMC_SECTOR_SIZE * NUM_SECTORS));
	debugstr(debugBuf);
    tot_sum_rd = tot_sum_wr = 0;
    for(i = 0; i < NUM_ITER; i++) {
        tot_sum_rd += rd_ticks[i];
        tot_sum_wr += wr_ticks[i];
    }
    rd_ave = tot_sum_rd / NUM_ITER;
    wr_ave = tot_sum_wr / NUM_ITER;
	sprintf(debugBuf, "CPU Speed: %lu.%lu MHz\r\n", clk, (SystemCoreClock / 10000) - (clk * 100));
	debugstr(debugBuf);
    sprintf(debugBuf, "Ave Ticks for Read: %u \r\n", rd_ave);
    debugstr(debugBuf);
    sprintf(debugBuf, "Aver Ticks for Write: %u \r\n", wr_ave);
    debugstr(debugBuf);
	rd_time = (rd_ave / clk);
	wr_time = (wr_ave / clk);
	sprintf(debugBuf, "READ: Ave Time: %u usecs Ave Speed : %u KB/sec\r\n", rd_time, ((NUM_SECTORS * MMC_SECTOR_SIZE * 1000)/rd_time));
    debugstr(debugBuf);
	sprintf(debugBuf, "WRITE:Ave Time: %u usecs Ave Speed : %u KB/sec \r\n", wr_time, ((NUM_SECTORS * MMC_SECTOR_SIZE * 1000)/wr_time));
    debugstr(debugBuf);
}

int32_t act_read, act_written;

void shell_task(void *arg)
{
	RAW_U8 old_priority;
	RAW_U32 pf_10_value;
	
	uint32_t rc;	/* Result code */
    
    uint32_t i, ite_cnt;
	uint32_t start_time;
	uint32_t end_time;
	static char debugBuf[64];
    uint32_t backup = 0;
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();

	/*sd detect pin PF_10 to input*/
	Chip_SCU_PinMuxSet(0xf, 10, FUNC4 | MD_EZI);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 7, 24);

	printf("Please insert the sd card\r\n");
	
	while (1) {

		pf_10_value = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, 7, 24);

		if (pf_10_value == 0) {
			
			printf("Sd card has been found\r\n");
			break;

		}

	}


	/* Disable SD/MMC interrupt */
	NVIC_DisableIRQ(SDIO_IRQn);

    /* Initialise SD/MMC card */
	App_SDMMC_Init();

	printf("\r\n==============================\r\n");
	printf("SDMMC CARD measurement demo\r\n");
	printf("==============================\r\n");

	NVIC_DisableIRQ(SDIO_IRQn);

	scu_pinmux(0xD ,1 , 1<<4, FUNC5);           /* PD.1 SDIO power */
	/* Enable slot power */
	Chip_SDIF_PowerOn(LPC_SDMMC);
	
	/* Enumerate the SDMMC card once detected.
     * Note this function may block for a little while. */
	rc = Chip_SDMMC_Acquire(LPC_SDMMC, &sdcardinfo);
	
	if (!rc) {
	    printf("SD/MMC Card enumeration failed! ..\r\n");
		RAW_ASSERT(0);
	}

	
	/* Check if Write Protected */
	rc = Chip_SDIF_CardWpOn(LPC_SDMMC);
	
	if (rc) {
		printf("SDMMC Card is write protected!, so tests can not continue..\r\n");
		RAW_ASSERT(0);
	}

	/* Read Card information */
	tot_secs = Chip_SDMMC_GetDeviceBlocks(LPC_SDMMC);

	/* Make sure that the sectors are withing the card size */
	if((START_SECTOR + NUM_SECTORS) >= tot_secs) {
		printf("Out of range parameters! ..\r\n");
		RAW_ASSERT(0);
	}

	printf("tot_secs is %d\r\n", tot_secs);
	
	
    /* Take back up of SD/MMC card contents so that
     * it can be restored so that SD/MMC card is not corrupted
     */
    debugstr("\r\nTaking back up of card.. \r\n");
    act_read = Chip_SDMMC_ReadBlocks(LPC_SDMMC, (void *)Buff_Backup, START_SECTOR, NUM_SECTORS);
    if(act_read == 0) {
        debugstr("Taking back up of card failed!.. \r\n");
		RAW_ASSERT(0);
    }

    act_written = Chip_SDMMC_WriteBlocks(LPC_SDMMC, (void *)Buff_Wr, START_SECTOR, NUM_SECTORS);
  
    if(act_written == 0) {
		sprintf(debugBuf, "WriteBlocks failed for Iter: %u! \r\n", ite_cnt);
		debugstr(debugBuf);
		RAW_ASSERT(0);
    }

    act_read = Chip_SDMMC_ReadBlocks(LPC_SDMMC, (void *)Buff_Rd, START_SECTOR, NUM_SECTORS);
    
    if(act_read == 0) {
        sprintf(debugBuf, "ReadBlocks failed for Iter: %u! \r\n", ite_cnt);
        debugstr(debugBuf);
	    RAW_ASSERT(0);
    }

    /* Comapre data */
    for(i = 0; i < (BUFFER_SIZE/sizeof(uint32_t)); i++)
    {
        if(Buff_Rd[i] != Buff_Wr[i])
        {
            sprintf(debugBuf, "Data mismacth: ind: %u Rd: 0x%x Wr: 0x%x \r\n", i, Buff_Rd[i], Buff_Wr[i]);
            RAW_ASSERT(0)
        }
    }

	debugstr("sd card test success\r\n");      
	while (1);	

}

/**
 * @brief	SDIO controller interrupt handler
 * @return	Nothing
 */
void SDIO_IRQHandler(void)
{
	/* All SD based register handling is done in the callback
	   function. The SDIO interrupt is not enabled as part of this
	   driver and needs to be enabled/disabled in the callbacks or
	   application as needed. This is to allow flexibility with IRQ
	   handling for applicaitons and RTOSes. */
	/* Set wait exit flag to tell wait function we are ready. In an RTOS,
	   this would trigger wakeup of a thread waiting for the IRQ. */
	NVIC_DisableIRQ(SDIO_IRQn);
	sdio_wait_exit = 1;
}


int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


