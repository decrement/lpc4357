#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>
#include "lpc43xx_lcd.h"
#include "GUI.h"
#include "lcdtouch.h"


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

#define LCD_X_SIZE	800 											
#define LCD_Y_SIZE  480 

#define LCD_DEMO_TYPE_43  1 // ����4.3����
#define LCD_DEMO_TYPE_70  2	//AT070TN93 �µ�7����

#define LCD_DEMO_TYPE   LCD_DEMO_TYPE_70

CALIBRATION g_calRation;



#if LCD_DEMO_TYPE == LCD_DEMO_TYPE_43

#define LCD_X_SIZE 480
#define LCD_Y_SIZE 272
#define LCD_TOUCH_SENSITITY   5


#elif LCD_DEMO_TYPE == LCD_DEMO_TYPE_70

#define LCD_X_SIZE 800
#define LCD_Y_SIZE 480
#define LCD_TOUCH_SENSITITY   4					  	

#endif

#define CAL_OFFSEX_X  50
#define CAL_OFFSEX_Y  50


PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_TASK_OBJ 		gui_task_obj;

RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


int perform_calibration(CALIBRATION *cal) {
	int j;
	float n, x, y, x2, y2, xy, z, zx, zy;
	float det, a, b, c, e, f, i;
	float scaling = 65536.0;

// Get sums for matrix
	n = x = y = x2 = y2 = xy = 0;
	for(j=0;j<5;j++) {
		n += 1.0;
		x += (float)cal->x[j];
		y += (float)cal->y[j];
		x2 += (float)(cal->x[j]*cal->x[j]);
		y2 += (float)(cal->y[j]*cal->y[j]);
		xy += (float)(cal->x[j]*cal->y[j]);
	}

// Get determinant of matrix -- check if determinant is too small
	det = n*(x2*y2 - xy*xy) + x*(xy*y - x*y2) + y*(x*xy - y*x2);
	if(det < 0.1 && det > -0.1) {
		printf("ts_calibrate: determinant is too small -- %f\n",det);
		RAW_ASSERT(0);
		return 0;
	}

// Get elements of inverse matrix
	a = (x2*y2 - xy*xy)/det;
	b = (xy*y - x*y2)/det;
	c = (x*xy - y*x2)/det;
	e = (n*y2 - y*y)/det;
	f = (x*y - n*xy)/det;
	i = (n*x2 - x*x)/det;

// Get sums for x calibration
	z = zx = zy = 0;
	for(j=0;j<5;j++) {
		z += (float)cal->xfb[j];
		zx += (float)(cal->xfb[j]*cal->x[j]);
		zy += (float)(cal->xfb[j]*cal->y[j]);
	}

// Now multiply out to get the calibration for framebuffer x coord
	cal->a[0] = (int)((a*z + b*zx + c*zy)*(scaling));
	cal->a[1] = (int)((b*z + e*zx + f*zy)*(scaling));
	cal->a[2] = (int)((c*z + f*zx + i*zy)*(scaling));

	printf("%f %f %f\n",(a*z + b*zx + c*zy),
				(b*z + e*zx + f*zy),
				(c*z + f*zx + i*zy));

// Get sums for y calibration
	z = zx = zy = 0;
	for(j=0;j<5;j++) {
		z += (float)cal->yfb[j];
		zx += (float)(cal->yfb[j]*cal->x[j]);
		zy += (float)(cal->yfb[j]*cal->y[j]);
	}

// Now multiply out to get the calibration for framebuffer y coord
	cal->a[3] = (int)((a*z + b*zx + c*zy)*(scaling));
	cal->a[4] = (int)((b*z + e*zx + f*zy)*(scaling));
	cal->a[5] = (int)((c*z + f*zx + i*zy)*(scaling));

	printf("%f %f %f\n",(a*z + b*zx + c*zy),
				(b*z + e*zx + f*zy),
				(c*z + f*zx + i*zy));

// If we got here, we're OK, so assign scaling to a[6] and return
	cal->a[6] = (int)scaling;
	return 1;
/*	
// This code was here originally to just insert default values
	for(j=0;j<7;j++) {
		c->a[j]=0;
	}
	c->a[1] = c->a[5] = c->a[6] = 1;
	return 1;
*/

}


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);



static void RePaint(void *pParam)
{
	
	while(1) 
	{
		/*������*/
		GUI_TOUCH_Exec();
	
		raw_sleep(1);
		
	
	}
}


int g_touchState = 1;
extern void GUIDEMO_main(void);
extern int g_touchCalA[7];

#define TOUCH_CALI_DELAY 200


void touch_cali(void)
{
	RAW_S32 xPhy, yPhy;
	int ret;
	int x_temp, y_temp;
	
	while (1) {

		GUI_SetBkColor(GUI_BLUE);  
		GUI_Clear();
		GUI_SetColor(GUI_WHITE);  GUI_FillCircle(CAL_OFFSEX_X, CAL_OFFSEX_Y, 10);
		GUI_SetColor(GUI_RED);    GUI_FillCircle(CAL_OFFSEX_X, CAL_OFFSEX_Y, 5);
		GUI_SetColor(GUI_WHITE);
		GUI_DispStringAt("Press 1", CAL_OFFSEX_X+20, CAL_OFFSEX_Y); 
	
		do{
			if (pen_is_down() == 1) {

				GUI_Delay (TOUCH_CALI_DELAY);
				
				ret = touch_x_y_get(&xPhy, &yPhy);
				if (ret == 0)
				{
					g_calRation.xfb[0] = CAL_OFFSEX_X;
					g_calRation.yfb[0] = CAL_OFFSEX_Y;

					g_calRation.x[0] =  xPhy;
					g_calRation.y[0] =  yPhy;

					break;
				}
			}
			
	  	} while(1);

	
		GUI_SetBkColor(GUI_BLUE);  
		GUI_Clear();
		GUI_SetColor(GUI_WHITE);  GUI_FillCircle(CAL_OFFSEX_X, CAL_OFFSEX_Y, 10);
		GUI_SetColor(GUI_RED);    GUI_FillCircle(CAL_OFFSEX_X, CAL_OFFSEX_Y, 5);
		GUI_SetColor(GUI_WHITE);
		GUI_DispStringAt("Press 2", CAL_OFFSEX_X+20, CAL_OFFSEX_Y); 
	
		do{
			if (pen_is_down() == 1) {

				GUI_Delay (TOUCH_CALI_DELAY);
				
				ret = touch_x_y_get(&xPhy, &yPhy);
				if (ret == 0)
				{
					g_calRation.xfb[0] = CAL_OFFSEX_X;
					g_calRation.yfb[0] = CAL_OFFSEX_Y;

					x_temp =  xPhy;
					y_temp =  yPhy;
					break;
				}
			}	
	
		}while(1);

		#if 0
		do {

			if (pen_is_down() == 0) {
				break;
			}
			
		} while(1);	

		#endif
		
		if ((abs(x_temp - g_calRation.x[0] ) < 100) && (abs(y_temp - g_calRation.y[0] ) < 100)) {

			break;
		}

	}

	do {

		if (pen_is_down() == 0) {
			break;
		}

	} while(1);
	
	raw_sleep(100);
	
   // second point
   while (1) {
	   	
	   GUI_SetBkColor(GUI_BLUE);  
	   GUI_Clear();
	   GUI_SetColor(GUI_WHITE);  GUI_FillCircle(LCD_X_SIZE - CAL_OFFSEX_X, CAL_OFFSEX_Y, 10);
	   GUI_SetColor(GUI_RED);    GUI_FillCircle(LCD_X_SIZE - CAL_OFFSEX_X, CAL_OFFSEX_Y, 5);
	   GUI_SetColor(GUI_WHITE);
	   GUI_DispStringAt("Press here 1", LCD_X_SIZE - CAL_OFFSEX_X-80, CAL_OFFSEX_Y);  
	   
	   do{
			if (pen_is_down() == 1) {

				GUI_Delay (TOUCH_CALI_DELAY);
				
				ret = touch_x_y_get(&xPhy, &yPhy);

				if (ret == 0)
				{
					g_calRation.xfb[1] = LCD_X_SIZE - CAL_OFFSEX_X;
					g_calRation.yfb[1] = CAL_OFFSEX_Y;

					g_calRation.x[1] =  xPhy;
					g_calRation.y[1] =  yPhy;
					break;

				}
			}
			
			
		} while(1);

		
	
		GUI_SetBkColor(GUI_BLUE);  
		GUI_Clear();
		GUI_SetColor(GUI_WHITE);  GUI_FillCircle(LCD_X_SIZE - CAL_OFFSEX_X, CAL_OFFSEX_Y, 10);
		GUI_SetColor(GUI_RED);    GUI_FillCircle(LCD_X_SIZE - CAL_OFFSEX_X, CAL_OFFSEX_Y, 5);
		GUI_SetColor(GUI_WHITE);
		GUI_DispStringAt("Press here 2", LCD_X_SIZE - CAL_OFFSEX_X-80, CAL_OFFSEX_Y); 
		
		do{
			if (pen_is_down() == 1) {

				GUI_Delay (TOUCH_CALI_DELAY);
				ret = touch_x_y_get(&xPhy, &yPhy);

				if (ret == 0)
				{
					g_calRation.xfb[1] = LCD_X_SIZE - CAL_OFFSEX_X;
					g_calRation.yfb[1] = CAL_OFFSEX_Y;

					x_temp =  xPhy;
					y_temp =  yPhy;

					break;
				}
			}	
			
		} while(1);



		if ((abs(x_temp - g_calRation.x[1] ) < 100) && ((abs(y_temp - g_calRation.y[1] ) < 100))) {

			break;
		}

}

   do {

		if (pen_is_down() == 0) {
			break;
		}

	} while(1);
	
	raw_sleep(100);
	
  while (1) {
  	
		// third point
		GUI_SetBkColor(GUI_BLUE);  
		GUI_Clear();
		GUI_SetColor(GUI_WHITE);  GUI_FillCircle(LCD_X_SIZE - CAL_OFFSEX_X, LCD_Y_SIZE - CAL_OFFSEX_Y, 10);
		GUI_SetColor(GUI_RED);    GUI_FillCircle(LCD_X_SIZE - CAL_OFFSEX_X, LCD_Y_SIZE - CAL_OFFSEX_Y, 5);
		GUI_SetColor(GUI_WHITE);
		GUI_DispStringAt("Press here 1", LCD_X_SIZE - CAL_OFFSEX_X-80, LCD_Y_SIZE - CAL_OFFSEX_Y); 
		
		do { 
			if (pen_is_down() == 1) {

				GUI_Delay (TOUCH_CALI_DELAY);
				ret = touch_x_y_get(&xPhy, &yPhy);

				if (ret == 0)
				{
					g_calRation.xfb[2] = LCD_X_SIZE - CAL_OFFSEX_X;
					g_calRation.yfb[2] = LCD_Y_SIZE - CAL_OFFSEX_Y;

					g_calRation.x[2] =  xPhy;
					g_calRation.y[2] =  yPhy;
					g_touchState = 0;
					break;
				}
			}
			
			
		} while(1);

		

		GUI_SetBkColor(GUI_BLUE);  
		GUI_Clear();
		GUI_SetColor(GUI_WHITE);  GUI_FillCircle(LCD_X_SIZE - CAL_OFFSEX_X, LCD_Y_SIZE - CAL_OFFSEX_Y, 10);
		GUI_SetColor(GUI_RED);    GUI_FillCircle(LCD_X_SIZE - CAL_OFFSEX_X, LCD_Y_SIZE - CAL_OFFSEX_Y, 5);
		GUI_SetColor(GUI_WHITE);
		GUI_DispStringAt("Press here 2", LCD_X_SIZE - CAL_OFFSEX_X-80, LCD_Y_SIZE - CAL_OFFSEX_Y);   
	
		do{

			if (pen_is_down() == 1) {

				GUI_Delay (TOUCH_CALI_DELAY);
				ret = touch_x_y_get(&xPhy, &yPhy);

				if (ret == 0)
				{
					g_calRation.xfb[2] = LCD_X_SIZE - CAL_OFFSEX_X;
					g_calRation.yfb[2] = LCD_Y_SIZE - CAL_OFFSEX_Y;

					x_temp =  xPhy;
					y_temp =  yPhy;
					g_touchState = 0;

					break;
				}
			}
			
			
		} while(1);

		

		if ((abs(x_temp - g_calRation.x[2] ) < 100) && ((abs(y_temp - g_calRation.y[2] ) < 100))) {

			break;
		}
	}

	do {

		if (pen_is_down() == 0) {
			break;
		}

	} while(1);
	
	raw_sleep(100);
	
	while (1) {
		
		// fourth point
		GUI_SetBkColor(GUI_BLUE);  
		GUI_Clear();
		GUI_SetColor(GUI_WHITE);  GUI_FillCircle(CAL_OFFSEX_X, LCD_Y_SIZE - CAL_OFFSEX_Y, 10);
		GUI_SetColor(GUI_RED);    GUI_FillCircle(CAL_OFFSEX_X, LCD_Y_SIZE - CAL_OFFSEX_Y, 5);
		GUI_SetColor(GUI_WHITE);
		GUI_DispStringAt("Press here 1", CAL_OFFSEX_X+20, LCD_Y_SIZE - CAL_OFFSEX_Y);  
		
		do{
			
			if (pen_is_down() == 1) {

				GUI_Delay (TOUCH_CALI_DELAY);
				
				ret = touch_x_y_get(&xPhy, &yPhy);

				if (ret == 0)
				{
					g_calRation.xfb[3] = CAL_OFFSEX_X;
					g_calRation.yfb[3] = LCD_Y_SIZE - CAL_OFFSEX_Y;

					g_calRation.x[3] =  xPhy;
					g_calRation.y[3] =  yPhy;
					g_touchState = 0;

					break;
				}
			}
			
			
		} while(1);

		

	
		GUI_SetBkColor(GUI_BLUE);  
		GUI_Clear();
		GUI_SetColor(GUI_WHITE);  GUI_FillCircle(CAL_OFFSEX_X, LCD_Y_SIZE - CAL_OFFSEX_Y, 10);
		GUI_SetColor(GUI_RED);    GUI_FillCircle(CAL_OFFSEX_X, LCD_Y_SIZE - CAL_OFFSEX_Y, 5);
		GUI_SetColor(GUI_WHITE);
		GUI_DispStringAt("Press here 2", CAL_OFFSEX_X+20, LCD_Y_SIZE - CAL_OFFSEX_Y); 
		
		do{

			if (pen_is_down() == 1) {

					GUI_Delay (TOUCH_CALI_DELAY);

				if (ret == 0)
				{
					g_calRation.xfb[3] = CAL_OFFSEX_X;
					g_calRation.yfb[3] = LCD_Y_SIZE - CAL_OFFSEX_Y;

					x_temp =  xPhy;
					y_temp =  yPhy;
					break;
				}
			}


		} while(1);

		

		if ((abs(x_temp - g_calRation.x[3] ) < 100) && ((abs(y_temp - g_calRation.y[3] ) < 100))) {

			break;
		}

	}

	do {

		if (pen_is_down() == 0) {
			break;
		}

	} while(1);
	
	raw_sleep(100);
	
	while (1) {
		
		// fifth point
		GUI_SetBkColor(GUI_BLUE);  
		GUI_Clear();
		GUI_SetColor(GUI_WHITE);  GUI_FillCircle(LCD_X_SIZE >> 1, LCD_Y_SIZE >> 1, 10);
		GUI_SetColor(GUI_RED);    GUI_FillCircle(LCD_X_SIZE >> 1, LCD_Y_SIZE >> 1, 5);
		GUI_SetColor(GUI_WHITE);
		GUI_DispStringAt("Press here 1", (LCD_X_SIZE >> 1 )+20, LCD_Y_SIZE >> 1); 
		
		do{
			if (pen_is_down() == 1) {

				GUI_Delay (TOUCH_CALI_DELAY);
				ret = touch_x_y_get(&xPhy, &yPhy);

				if (ret == 0)
				{
					g_calRation.xfb[4] = LCD_X_SIZE >> 1;
					g_calRation.yfb[4] = LCD_Y_SIZE >> 1;

					g_calRation.x[4] =  xPhy;
					g_calRation.y[4] =  yPhy;
					break;
				}
			}
			
		} while(1);    

		

		GUI_SetBkColor(GUI_BLUE);  
		GUI_Clear();
		GUI_SetColor(GUI_WHITE);  GUI_FillCircle(LCD_X_SIZE >> 1, LCD_Y_SIZE >> 1, 10);
		GUI_SetColor(GUI_RED);    GUI_FillCircle(LCD_X_SIZE >> 1, LCD_Y_SIZE >> 1, 5);
		GUI_SetColor(GUI_WHITE);
		GUI_DispStringAt("Press here 2", (LCD_X_SIZE >> 1 )+20, LCD_Y_SIZE >> 1);  
	
		do{
			if (pen_is_down() == 1) {

				GUI_Delay (TOUCH_CALI_DELAY);

				if (ret == 0)
				{
					g_calRation.xfb[4] = LCD_X_SIZE >> 1;
					g_calRation.yfb[4] = LCD_Y_SIZE >> 1;

					x_temp =  xPhy;
					y_temp =  yPhy;

					break;
				}
		}
		} while(1);    

		

		if ((abs(x_temp - g_calRation.x[4] ) < 100) && ((abs(y_temp - g_calRation.y[4] ) < 100))) {

			break;
		}
	
	}

	do {

		if (pen_is_down() == 0) {
			break;
		}

	} while(1);

	perform_calibration(&g_calRation);
	memcpy(g_touchCalA, &(g_calRation.a), sizeof(g_calRation.a));

	GUI_SetBkColor(GUI_BLUE);  
	GUI_Clear();
	GUI_SetColor(GUI_WHITE);
	GUI_DispStringAt("Calibrate ok, enter ucgui...", LCD_X_SIZE >> 2, LCD_Y_SIZE >> 1);

	GUI_Delay (100);
	
}



void shell_task(void *arg)
{
	RAW_U8 old_priority;

	/*led1 ,led2 ,led3 ,led4 are set to gpio output*/
	Chip_SCU_PinMuxSet(0xe, 14, FUNC4);
	Chip_SCU_PinMuxSet(0xe, 15, FUNC4);
	Chip_SCU_PinMuxSet(0xf, 1, FUNC4);
	Chip_SCU_PinMuxSet(0xf, 3, FUNC4);
	
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 7, 14);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 7, 15);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 7, 16);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 7, 18);

	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 14, 0);

	emc_init();
	sdram_init();

	
	LCD_init(LCD_X_SIZE,LCD_Y_SIZE); 

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 7, 16, 1);

	LCD_displayMemorySet((unsigned short *)0x28000000);
	LCD_enable();
	
	g_touchState = 0;  // 0:UNPRESS 1:RRESS

	touch_init(LCD_X_SIZE, LCD_Y_SIZE);
	
	GUI_Init();
	GUI_CURSOR_Show();

	touch_cali();
	raw_task_priority_change (raw_task_active, IDLE_PRIORITY - 1, &old_priority);

	raw_task_resume(&gui_task_obj);

	while (1) {

		GUIDEMO_main();
		
		raw_sleep(100);

	}

	


}


PORT_STACK gui_task_stack1[1024];

int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 

	raw_task_create(&gui_task_obj, (RAW_U8 *)"Exec()"	, 0, IDLE_PRIORITY - 2, 0, gui_task_stack1, 1024, RePaint, 0);
	
	raw_os_start();

	return 0;
	
}


