#include <stdio.h>
#include <stdarg.h>

#include <raw_api.h>
#include <lib_string.h>
#include <rsh.h>




RAW_S32 string_to_int (void * input, RAW_S32 len)
{
	RAW_U16 out = 0;
	RAW_U8 * temp = input;
	RAW_S32 i = 0, num = len;
	while (*temp != '\0' && i < num)  {
		out = out * 10 + *(temp + i) - '0';
		i++;
	}
	
	return out;
}



static RAW_S32 command_case_three_parameters(RAW_S8 *pcWriteBuffer, size_t xWriteBufferLen, const RAW_S8 *pcCommandString )
{
	RAW_S8 *pcParameter;
	RAW_S32 lParameterStringLength, xReturn;
	RAW_S32 para2, para3;
	RAW_S8 para1[100];

	/* Obtain the  first parameter string. */
	pcParameter = (RAW_S8 *) rsh_get_parameter
		( 
		 pcCommandString,		/* The command string itself. */
		 1,		/* Return the next parameter. */
		 &lParameterStringLength	/* Store the parameter string length. */
		);
	

	raw_memcpy(para1, pcParameter, lParameterStringLength);
	para1[lParameterStringLength] = '\0';


	/* Obtain the second parameter string. */
	pcParameter = (RAW_S8 *) rsh_get_parameter
		( 
		 pcCommandString,		/* The command string itself. */
		 2,		/* Return the next parameter. */
		 &lParameterStringLength	/* Store the parameter string length. */
		);

	

	para2 = string_to_int(pcParameter, lParameterStringLength);
	/* Obtain the second parameter string. */
	pcParameter = (RAW_S8 *) rsh_get_parameter
		( 
		 pcCommandString,		/* The command string itself. */
		 3,		/* Return the next parameter. */
		 &lParameterStringLength	/* Store the parameter string length. */
		);
	

	para3 = string_to_int(pcParameter, lParameterStringLength);

	printf("\r");
	printf("The first parameters is %s\r\n",  para1);
	printf("The second parameters is %d\r\n", para2);
	printf("The third parameters is %d\r\n",  para3);
	
	xReturn = 1;
	return xReturn;
}

static RAW_S32 command_case_no_parameters(RAW_S8 *pcWriteBuffer,
                                          size_t xWriteBufferLen,
                                          const RAW_S8 *pcCommandString )
{
	/* For simplicity, this function assumes the output buffer is large enough
	   to hold all the text generated by executing the vTaskList() API function,
	   so the xWriteBufferLen parameter is not used. */
	printf("\r");
	printf("command_case_no_parameters running\r\n");
	/* The entire table was written directly to the output buffer.  Execution
	   of this command is complete, so return pdFALSE. */
	return 1;
}



static xCommandLineInputListItem pxNewListItem3;
static xCommandLineInputListItem pxNewListItem1;



static xCommandLineInput test_rsh_command =
{
	"test",
	"test -- only test!\n",
	command_case_no_parameters, /* The function to run. */
	0 /* Three parameters are expected, which can take any value. */
};


static xCommandLineInput three_parameters_case =
{
	"echo_3_parameters",
	"echo_3_parameters:\r\n\t<param1_string> <param2_int> <param3_int>: \r\n\tExpects three parameters, echos each in turn\r\n",
	command_case_three_parameters, /* The function to run. */
	3 /* Three parameters are expected, which can take any value. */
};


static xCommandLineInput test_rsh_command;


void three_parameters_cmd_register(void)
{
	rsh_register_command(&three_parameters_case, &pxNewListItem3);
}



void no_parameters_cmd_register(void)
{
	
	rsh_register_command(&test_rsh_command, &pxNewListItem1);
}


