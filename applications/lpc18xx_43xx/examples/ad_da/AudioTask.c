#include "board.h"
#include "speak.h"
#include <raw_api.h>

/************************** PRIVATE DEFINITIONS ***********************/
#define _ADC_CHANNEL		1


S16 g_speakerTestBuffer[80];

S16 g_speakerTestDegree = 0;
             
uint8_t gFlag = 0;

uint32_t g_countAudio = 0 ;
extern unsigned char audio[];

uint32_t g_adc_value ;

uint32_t g_speakerVolume = 0 ;
uint32_t g_speakerVolumeTmp = 0xFFFFFFFF;

extern void DelayMs (uint32_t ms);

void ADC_Init(uint32_t cclk, uint32_t rate)
{
	uint32_t temp, tmpreg;//, ADCbitrate;

	LPC_ADC0->CR = 0;
    
	//Enable PDN bit
	tmpreg = (1UL << 21);


	/* The APB clock (PCLK_ADC0) is divided by (CLKDIV+1) to produce the clock for
	 * A/D converter, which should be less than or equal to 12.4MHz.
	 * A fully conversion requires 31 of these clocks.
	 * ADC clock = PCLK_ADC0 / (CLKDIV + 1);
	 * ADC rate = ADC clock / 31;
	 */
	temp = (cclk /(rate * 31)) - 1;
	tmpreg |=  (temp << 8);	

	LPC_ADC0->CR = tmpreg;

	LPC_ADC0->INTEN &= ~(1UL << 1);
	LPC_ADC0->CR |= (1UL << 1);
}

uint16_t ADC_GetData(uint8_t channel)
{
	uint32_t temp = 0;

	LPC_ADC0->CR &= ~(7UL << 24);
	LPC_ADC0->CR |= (1 << 24);	    // start now

	while ((temp & (1UL << 31)) == 0)
	{
		temp = LPC_ADC0->DR[channel];
	}

	return ((temp >> 6)& 0x3FF);
}

// 向SPEAKER模块发送一帧数据（10MS）
void speakerSndFrame(void)
{
    U32 i;

    for (i = 0; i < 80; i++)
    {						
    	g_speakerTestBuffer[i] =  (audio[i+g_countAudio*80])*g_speakerVolume;
	}
	g_countAudio++;

	if(g_countAudio*80>=63102)
	{
	    g_countAudio = 0 ;
	}
    speakerOut(g_speakerTestBuffer, 80);
}

void speakerBufferSoonEmpty(void)
{
	gFlag = 1;
}

void taskSpeaker(void)
{
    uint32_t volume;

	static uint32_t counter = 0;


	speakerInit(204000000, speakerBufferSoonEmpty);

	ADC_Init(204000000, 50000);

    speakerSndFrame();
	
	for (;;)
    {																				              
		if (!gFlag)
		{
			raw_sleep(1);
			continue;
		}
		gFlag = 0;
        speakerSndFrame();

		g_adc_value = ADC_GetData(_ADC_CHANNEL);

		volume = g_adc_value*100>>10 ;

		counter++;
		if (counter == 100)
		{
			printf("Volumn value: %d \r\n" , volume);
			counter = 0;				 
		}

		g_speakerVolume = volume;

        // 抗抖动处理
        if (g_speakerVolumeTmp == 0xFFFFFFFF)
        {
            if (volume != g_speakerVolume)  // 有改变
            {
                g_speakerVolumeTmp = volume;
            }

            volume = 0xFFFFFFFF;
        }
        else if (g_speakerVolumeTmp == volume)
        {
            g_speakerVolume = volume;

            g_speakerVolumeTmp = 0xFFFFFFFF;
        }
        else
        {
            if (volume != g_speakerVolume)
            {
                g_speakerVolumeTmp = volume;
            }
            else
            {
                g_speakerVolumeTmp = 0xFFFFFFFF;
            }

            volume = 0xFFFFFFFF;
        }
    }
}



