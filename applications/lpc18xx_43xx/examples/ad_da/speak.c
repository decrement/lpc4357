#include "board.h"
#include "speak.h"

#define SPEAKER_BUFFER_LEN 256
S16 g_speakerBuffer[SPEAKER_BUFFER_LEN+1];

U32 g_speakerBufferHead, g_speakerBufferTail; 


#define SPEAKER_SAMPLE_CLK 8000

unsigned long g_audioOffset;

SPEAKER_BUFFER_SOON_EMPTY g_speakerBufferSoonEmpty = 0;

extern const unsigned char audio[];

void TIMER0_IRQHandler(void) 
{
    U32 bufferLen;
	
    if (g_speakerBufferHead != g_speakerBufferTail)
    {
        LPC_DAC->CR = (g_speakerBuffer[g_speakerBufferHead] + 0x8000) & 0xFFC0;

        g_speakerBufferHead++;
        if (g_speakerBufferHead >= SPEAKER_BUFFER_LEN)
        {
            g_speakerBufferHead = 0;
        }
    }
    else
    {
        LPC_DAC->CR = 0x8000;
    }

    if (g_speakerBufferTail >= g_speakerBufferHead)
    {
        bufferLen = g_speakerBufferTail - g_speakerBufferHead;
    }
    else
    {
        bufferLen = g_speakerBufferTail + SPEAKER_BUFFER_LEN - g_speakerBufferHead;
    }

    if (bufferLen == 64) // 只有8MS的数据了
    {
        if (g_speakerBufferSoonEmpty)
        {
             g_speakerBufferSoonEmpty();
        }
    }

    LPC_TIMER0->IR = 1;                         /* Clear Interrupt Flag */
}



void speakerInit(U32 cclk, SPEAKER_BUFFER_SOON_EMPTY speakerBufferSoonEmpty)
{
	LPC_CGU2->BASE_APB3_CLK = (9UL << 24) | (1UL << 11);

	LPC_DAC->CR = 0x00010000;
    LPC_DAC->CTRL |= (1 << 3);

    LPC_TIMER0->MR[0] = (cclk ) / SPEAKER_SAMPLE_CLK -1;	/* TC0 Match Value 0 */
    LPC_TIMER0->MCR |= 3;					/* TC1 Interrupt and Reset on MR1 */
    NVIC_EnableIRQ(TIMER0_IRQn);
 
	LPC_TIMER0->TCR = 1;					/* TC0 Enable */

	g_audioOffset = 0;
	
    g_speakerBufferHead = 0;
    g_speakerBufferTail = 0;

    g_speakerBufferSoonEmpty = speakerBufferSoonEmpty;
}

// 驱动层空间有限，每次最多输入160个采样值
// sample的输入范围（-32768~~32767）
void speakerOut(S16 * sample, U32 len)
{

    U32  i;

    LPC_TIMER0->TCR = 0;					/* TC0 Disable */

    for (i = 0; i < len; i++)
    {
        if ((g_speakerBufferTail + 1) == g_speakerBufferHead)
        {
            // BUFFER已满
            break;
        }

        g_speakerBuffer[g_speakerBufferTail] = sample[i];
                       
        g_speakerBufferTail++;
        if (g_speakerBufferTail >= SPEAKER_BUFFER_LEN)
        {
            g_speakerBufferTail = 0;
        }
    }

    LPC_TIMER0->TCR = 1;					/* TC0 Enable */
}

