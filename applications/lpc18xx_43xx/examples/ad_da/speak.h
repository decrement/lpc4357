

#ifndef __SPEAKER_H
#define __SPEAKER_H

typedef unsigned long U32;
typedef signed short  S16;

// 驱动模块通知应用层可以补充数据的回调函数（驱动层BUFFER只有不到10MS的语音数据时）
typedef void(* SPEAKER_BUFFER_SOON_EMPTY)(void);

extern void speakerInit(U32 cclk, SPEAKER_BUFFER_SOON_EMPTY speakerBufferSoonEmpty);

// 驱动层空间有限，每次最多输入160个采样值
// sample的输入范围（-32768~~32768），采样频率为8K.
extern void speakerOut(S16 * sample, U32 len);

#endif


