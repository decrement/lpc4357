#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>
#include "lpc43xx_i2c0.h"
#include "lpc43xx_i2s0.h"
#include "ff.h"
#include <mm/raw_malloc.h>
#include <mm/raw_page.h>


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


#define WAV_BUFFER_BASE_ADDR    0x28000000 
unsigned char * g_WaveBuf = (unsigned char *)WAV_BUFFER_BASE_ADDR;

static T_I2S_CONFIG txConfig;

const unsigned char * g_audioData;
unsigned long g_audioDataPtr, g_audioDataLen;


unsigned long I2S_playSoundSet(unsigned long *  leftSound, unsigned long * rightSound, unsigned long len)
{
    uint32_t i;

    if (txConfig.channel == SOUND_CHNL_STERO)
    {																	   //  立体声
        switch (txConfig.wordWidth)
        {
        case SOUND_SAMPLE_8BITS:										 //  立体声	
            for(i=0; i<len; i++)
            {
                leftSound[i] =  g_audioData[g_audioDataPtr];  
                g_audioDataPtr++;

                rightSound[i] =  g_audioData[g_audioDataPtr];  
                g_audioDataPtr++;

				if (g_audioDataPtr >= g_audioDataLen)
				{
				    g_audioDataPtr = 0;
				}
            }
            break;
        case SOUND_SAMPLE_16BITS:										 //  立体声
            for(i=0; i<len; i++)
            {
				
                leftSound[i] =  (unsigned long)g_audioData[g_audioDataPtr] | ((unsigned long)g_audioData[g_audioDataPtr + 1] << 8); 
				//leftSound[i] =0x0;
                g_audioDataPtr+=2;

				rightSound[i] =  (unsigned long)g_audioData[g_audioDataPtr] | ((unsigned long)g_audioData[g_audioDataPtr + 1] << 8);  
				//rightSound[i] =  0x0;  

                g_audioDataPtr+=2;

               

				if (g_audioDataPtr >= g_audioDataLen)
				{
				    g_audioDataPtr = 0;
				}
            }
            break;
        case SOUND_SAMPLE_32BITS:											 //  立体声
            for(i=0; i<len; i++)
            {
                leftSound[i] =  (unsigned long)g_audioData[g_audioDataPtr] 
				             | ((unsigned long)g_audioData[g_audioDataPtr + 1] << 8)
							 | ((unsigned long)g_audioData[g_audioDataPtr + 2] << 16)
							 | ((unsigned long)g_audioData[g_audioDataPtr + 3] << 24);  
                g_audioDataPtr+=4;

                rightSound[i] =  (unsigned long)g_audioData[g_audioDataPtr] 
				              | ((unsigned long)g_audioData[g_audioDataPtr + 1] << 8)
					          | ((unsigned long)g_audioData[g_audioDataPtr + 2] << 16)
							  | ((unsigned long)g_audioData[g_audioDataPtr + 3] << 24);  
                g_audioDataPtr+=4;

				if (g_audioDataPtr >= g_audioDataLen)
				{
				    g_audioDataPtr = 0;
				}
            }
            break;
        default :  
            break ;
        }
    }			 
    else // 双声道
    {
        switch(txConfig.wordWidth)
        {
        case SOUND_SAMPLE_8BITS:
            for(i=0; i<len; i++)
            {
                leftSound[i] =  g_audioData[g_audioDataPtr];  
                g_audioDataPtr++;

				if (g_audioDataPtr >= g_audioDataLen)
				{
				    g_audioDataPtr = 0;
				}
            }
            break;
        case SOUND_SAMPLE_16BITS:
            for(i=0; i<len; i++)
            {
                leftSound[i] =  (unsigned long)g_audioData[g_audioDataPtr] | ((unsigned long)g_audioData[g_audioDataPtr + 1] << 8);  
                g_audioDataPtr+=2;

				if (g_audioDataPtr >= g_audioDataLen)
				{
				    g_audioDataPtr = 0;
				}
            }
            break;
        case SOUND_SAMPLE_32BITS:
            for(i=0; i<len; i++)
            {
                leftSound[i] =  (unsigned long)g_audioData[g_audioDataPtr] 
				             | ((unsigned long)g_audioData[g_audioDataPtr + 1] << 8)
							 | ((unsigned long)g_audioData[g_audioDataPtr + 2] << 16)
							 | ((unsigned long)g_audioData[g_audioDataPtr + 3] << 24);  
                g_audioDataPtr+=4;

				if (g_audioDataPtr >= g_audioDataLen)
				{
				    g_audioDataPtr = 0;
				}
            }
            break;	
        default :  
            break ;
        }				  					 		
    }
    
    return len;
}

uint8_t waveAudioDecode (uint8_t *pData)
{
    uint16_t sampleRate, sampleWidth;
	uint32_t  soundDataSize;

	if ((pData[8] != 'W') || (pData[9] != 'A') || (pData[10] != 'V') || (pData[11] != 'E'))
	{
		printf("Music file format isn't wav\r\n");
		return 1;
	}

    if (pData[0x16] == 1)
	{
	    txConfig.channel = SOUND_CHNL_MONO;
	}
	else if (pData[0x16] == 2)
	{
	    txConfig.channel = SOUND_CHNL_STERO;
	}
	else
	{
		printf("Error channel : %d\r\n", pData[16]);
	    return 2;
	}

    sampleRate =  ((uint16_t)*(pData + 0x18)) |(((uint16_t)*(pData + 0x19)) << 8) ;

	if (sampleRate == 8000)
	{
	    txConfig.smpFreq = SOUND_FREQ_8_0_K;
	}
    else if (sampleRate == 11025)
	{
	    txConfig.smpFreq = SOUND_FREQ_11_025_K;
	}
    else if (sampleRate == 22050)
	{
	    txConfig.smpFreq = SOUND_FREQ_22_05_K;
	}
	else if (sampleRate == 44100)
	{
	    txConfig.smpFreq = SOUND_FREQ_44_1_K;
	}
	else
	{
		printf("Error sampleRate : %d\r\n", sampleRate);
	    return 3;
	}

	sampleWidth =  ((uint16_t)*(pData + 0x22))  |(((uint16_t)*(pData + 0x23)) << 8) ;

	if (sampleWidth == 8)
	{
	 	txConfig.wordWidth = SOUND_SAMPLE_8BITS;
	}
	else if (sampleWidth == 16)
	{
	 	txConfig.wordWidth = SOUND_SAMPLE_16BITS;
	}
	else if (sampleWidth == 32)
	{
	 	txConfig.wordWidth = SOUND_SAMPLE_32BITS;
	}
	else
	{
		printf("Error sampleWidth : %d\r\n", sampleWidth);
	    return 4;
	}

	soundDataSize =  ((uint32_t)*(pData + 0x28))		        //  28H 语音数据长度
            |(((uint32_t)*(pData + 0x29)) << 8)
            |(((uint32_t)*(pData + 0x2a)) << 16)
            |(((uint32_t)*(pData + 0x2b)) << 24) ;

    g_audioData = pData + 44;
	g_audioDataLen = soundDataSize;
	g_audioDataPtr = 0;

	printf("Decode ok\r\n");

	return 0;
}


extern const unsigned char successwave[183340];

extern const unsigned char audio[15658];

void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

/* SD/MMC card information */
/* Number of sectors in SD/MMC card */
static int32_t tot_secs;


/* SDMMC card info structure */
mci_card_struct sdcardinfo;

/* SDIO wait flag */
static volatile int32_t sdio_wait_exit = 0;


/* Delay callback for timed SDIF/SDMMC functions */
static void sdmmc_waitms(uint32_t time)
{
	/* In an RTOS, the thread would sleep allowing other threads to run.
	   For standalone operation, we just spin on RI timer */
	int32_t curr = (int32_t) Chip_RIT_GetCounter(LPC_RITIMER);
	int32_t final = curr + ((SystemCoreClock / 1000) * time);

	if (final == curr) return;

	if ((final < 0) && (curr > 0)) {
		while (Chip_RIT_GetCounter(LPC_RITIMER) < (uint32_t) final) {}
	}
	else {
		while ((int32_t) Chip_RIT_GetCounter(LPC_RITIMER) < final) {}
	}

	return;
}

/**
 * @brief	Sets up the SD event driven wakeup
 * @param	bits : Status bits to poll for command completion
 * @return	Nothing
 */
static void sdmmc_setup_wakeup(void *bits)
{
	uint32_t bit_mask = *((uint32_t *)bits);
	/* Wait for IRQ - for an RTOS, you would pend on an event here with a IRQ based wakeup. */
	NVIC_ClearPendingIRQ(SDIO_IRQn);
	sdio_wait_exit = 0;
	Chip_SDIF_SetIntMask(LPC_SDMMC, bit_mask);
	NVIC_EnableIRQ(SDIO_IRQn);
}

/**
 * @brief	A better wait callback for SDMMC driven by the IRQ flag
 * @return	0 on success, or failure condition (-1)
 */
static uint32_t sdmmc_irq_driven_wait(void)
{
	uint32_t status;

	/* Wait for event, would be nice to have a timeout, but keep it  simple */
	while (sdio_wait_exit == 0) {}

	/* Get status and clear interrupts */
	status = Chip_SDIF_GetIntStatus(LPC_SDMMC);
	Chip_SDIF_ClrIntStatus(LPC_SDMMC, status);
	Chip_SDIF_SetIntMask(LPC_SDMMC, 0);

	return status;
}


/* Initialize SD/MMC */
static void App_SDMMC_Init()
{
	
	raw_memset(&sdcardinfo, 0, sizeof(sdcardinfo));
	sdcardinfo.card_info.evsetup_cb = sdmmc_setup_wakeup;
	sdcardinfo.card_info.waitfunc_cb = sdmmc_irq_driven_wait;
	sdcardinfo.card_info.msdelay_func = sdmmc_waitms;

	/*  SD/MMC initialization */
	Board_SDMMC_Init();

	/* The SDIO driver needs to know the SDIO clock rate */
	Chip_SDIF_Init(LPC_SDMMC);

}

/**
 * @brief	SDIO controller interrupt handler
 * @return	Nothing
 */
void SDIO_IRQHandler(void)
{
	/* All SD based register handling is done in the callback
	   function. The SDIO interrupt is not enabled as part of this
	   driver and needs to be enabled/disabled in the callbacks or
	   application as needed. This is to allow flexibility with IRQ
	   handling for applicaitons and RTOSes. */
	/* Set wait exit flag to tell wait function we are ready. In an RTOS,
	   this would trigger wakeup of a thread waiting for the IRQ. */
	NVIC_DisableIRQ(SDIO_IRQn);
	sdio_wait_exit = 1;
}
FATFS fs; /* 逻辑驱动器的工作区(文件系统对象) */
FIL file;


#define HEAP_ADDRESS_START 0x2bc00000
#define HEAP_ADDRESS_END   0x2c000000



void shell_task(void *arg)
{
	RAW_U8 old_priority;
	uint32_t rc;
	FRESULT res;
	RAW_U32 pf_10_value;
	RAW_U32 bw;
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();
	raw_page_init((RAW_VOID *)HEAP_ADDRESS_START, (RAW_VOID *)HEAP_ADDRESS_END);
	raw_malloc_init();

	#if (AUDIO_FREQ == 44100)
	
	/*sd detect pin PF_10 to input*/
	Chip_SCU_PinMuxSet(0xf, 10, FUNC4 | MD_EZI);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 7, 24);

	printf("Please insert the sd card\r\n");

	while (1) {

		pf_10_value = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, 7, 24);

		if (pf_10_value == 0) {

			printf("Sd card has been found\r\n");
			break;

		}

	}

	/*Need somer delay here otherwise enum fail, due fast sd card inserted.*/
	raw_sleep(200);
	
	/* Disable SD/MMC interrupt */
	NVIC_DisableIRQ(SDIO_IRQn);

	/* Initialise SD/MMC card */
	App_SDMMC_Init();

	NVIC_DisableIRQ(SDIO_IRQn);

	/* Enable slot power */
	Chip_SDIF_PowerOn(LPC_SDMMC);

	/* Enumerate the SDMMC card once detected.
	* Note this function may block for a little while. */
	rc = Chip_SDMMC_Acquire(LPC_SDMMC, &sdcardinfo);

	if (!rc) {

	printf("SD/MMC Card enumeration failed! ..\r\n");
	RAW_ASSERT(0);
	}


	/* Read Card information */
	tot_secs = Chip_SDMMC_GetDeviceBlocks(LPC_SDMMC);


	printf("tot_secs is %d\r\n", tot_secs);

	res = f_mount(&fs, "0:", 1);		//0,要安装/卸载逻辑驱动器号,

	printf("f_mount is	%d\r\n", res);		//fs,新的文件系统对象的指针（NULL要卸载）?


	res = f_open(&file, "0:music3.wav", FA_OPEN_EXISTING | FA_READ);
	printf("the f_open return is %d\r\n",res);

	res = f_read(&file,  g_WaveBuf, 55 * 1024 * 1024,  &bw);

	printf("read result is %d, read bytes %d\r\n", res, bw);

	res = f_close(&file);
	
	res = f_mount(0, "0:", 0);	

	#elif (AUDIO_FREQ == 22050)
	
	raw_memcpy(g_WaveBuf, successwave, 183340);
	
	#endif
	
	
	
	printf("Start to decode wav file\r\n");

	if (waveAudioDecode(g_WaveBuf) > 0)
	{
		printf("Decode error\r\n");
		RAW_ASSERT(0);
	}
	
	txConfig.pFunc = I2S_playSoundSet;

	printf("width = %d, channel = %d, freq = %d\r\n", txConfig.wordWidth, txConfig.channel, txConfig.smpFreq);

	
	I2S_init(0, &txConfig);
	
	while (1);
	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


