#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
/* Auto programming on/off */
#define AUTOPROG_ON     1

/* Page used for storage */
#define PAGE_ADDR       0x01/* Page number */

/* Tag for checking if a string already exists in EEPROM */
#define CHKTAG          "NxP"
#define CHKTAG_SIZE     3

/* ASCII ESC character code */
#define ESC_CHAR        27

/* Read/write buffer (32-bit aligned) */
uint32_t buffer[EEPROM_PAGE_SIZE / sizeof(uint32_t)];

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/* Show current string stored in UART */
static void ShowString(char *str) {
	int stSize;

	/* Is data tagged with check pattern? */
	if (strncmp(str, CHKTAG, CHKTAG_SIZE) == 0) {
		/* Get next byte, which is the string size in bytes */
		stSize = (uint32_t) str[3];
		if (stSize > 32) {
			stSize = 32;
		}

		/* Add terminator */
		str[4 + stSize] = '\0';

		/* Show string stored in EEPROM */
		DEBUGSTR("Stored string found in EEEPROM\r\n");
		DEBUGSTR("-->");
		DEBUGSTR((char *) &str[4]);
		DEBUGSTR("<--\r\n");
	}
	else {
		DEBUGSTR("No string stored in the EEPROM\r\n");
	}
}

/* Get a string to save from the UART */
static uint32_t MakeString(uint8_t *str)
{
	int index, byte;
	char strOut[2];

	/* Get a string up to 32 bytes to write into EEPROM */
	DEBUGSTR("\r\nEnter a string to write into EEPROM\r\n");
	DEBUGSTR("Up to 32 bytes in length, press ESC to accept\r\n");

	/* Setup header */
	strncpy((char *) str, CHKTAG, CHKTAG_SIZE);

	/* Read until escape, but cap at 32 characters */
	index = 0;
	strOut[1] = '\0';
	byte = DEBUGIN();
	while ((index < 32) && (byte != ESC_CHAR)) {
		if (byte != EOF) {
			strOut[0] = str[4 + index] = (uint8_t) byte;
			DEBUGSTR(strOut);
			index++;
		}

		byte = DEBUGIN();
	}

	str[3] = (uint8_t) index;

	return (uint32_t) index;
}

/* Read data from EEPROM */
/* size must be multiple of 4 bytes */
STATIC void EEPROM_Read(uint32_t pageOffset, uint32_t pageAddr, uint32_t* ptr, uint32_t size)
{
	uint32_t i = 0;
	uint32_t *pEepromMem = (uint32_t*)EEPROM_ADDRESS(pageAddr,pageOffset);
	for(i = 0; i < size/4; i++) {
		ptr[i] = pEepromMem[i];
	}
}

/* Erase a page in EEPROM */
STATIC void EEPROM_Erase(uint32_t pageAddr)
{
	uint32_t i = 0;
	uint32_t *pEepromMem = (uint32_t*)EEPROM_ADDRESS(pageAddr,0);
	for(i = 0; i < EEPROM_PAGE_SIZE/4; i++) {
		pEepromMem[i] = 0;
#if AUTOPROG_ON
		Chip_EEPROM_WaitForIntStatus(LPC_EEPROM, EEPROM_INT_ENDOFPROG);
#endif
	}
#if (AUTOPROG_ON == 0)
	Chip_EEPROM_EraseProgramPage(LPC_EEPROM);
#endif
}

/* Write data to a page in EEPROM */
/* size must be multiple of 4 bytes */
STATIC void EEPROM_Write(uint32_t pageOffset, uint32_t pageAddr, uint32_t* ptr, uint32_t size)
{
	uint32_t i = 0;
	uint32_t *pEepromMem = (uint32_t*)EEPROM_ADDRESS(pageAddr,pageOffset);

	if(size > EEPROM_PAGE_SIZE - pageOffset)
		size = EEPROM_PAGE_SIZE - pageOffset;

	for(i = 0; i < size/4; i++) {
		pEepromMem[i] = ptr[i];
#if AUTOPROG_ON
		Chip_EEPROM_WaitForIntStatus(LPC_EEPROM, EEPROM_INT_ENDOFPROG);
#endif
	}

#if (AUTOPROG_ON == 0)
	Chip_EEPROM_EraseProgramPage(LPC_EEPROM);
#endif
}



void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

void shell_task(void *arg)
{
	RAW_U8 old_priority;

	uint32_t stSize;
	uint8_t *ptr = (uint8_t *) buffer;
	volatile int loop = 1;	/* For debug message only */
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	
	emc_init();
	sdram_init();

	printf("start sdram test\r\n");
	
	/* Init EEPROM */
	Chip_EEPROM_Init(LPC_EEPROM);

#if AUTOPROG_ON
	/* Set Auto Programming mode */
	Chip_EEPROM_SetAutoProg(LPC_EEPROM,EEPROM_AUTOPROG_AFT_1WORDWRITTEN);
#else
	/* Set Auto Programming mode */
	Chip_EEPROM_SetAutoProg(LPC_EEPROM,EEPROM_AUTOPROG_OFF);
#endif /*AUTOPROG_ON*/

	/* Read all data from EEPROM page */
	EEPROM_Read(0, PAGE_ADDR, (uint32_t*)ptr, EEPROM_PAGE_SIZE);

	/* Check and display string if it exists */
	ShowString((char *) ptr);

	/* Get a string to save */
	stSize = MakeString(ptr);
	if(stSize % 4)
		stSize =  (stSize/4 + 1)*4;

	/* Erase page */
	EEPROM_Erase(PAGE_ADDR);

	/* Write header + size + data to page */
	DEBUGSTR("\r\nEEPROM write...\r\n");
	EEPROM_Write(0, PAGE_ADDR, (uint32_t*)ptr, (4 + stSize));
	DEBUGSTR("Reading back string...\r\n");

	/* Read all data from EEPROM page */
	EEPROM_Read(0, PAGE_ADDR, (uint32_t*)ptr, EEPROM_PAGE_SIZE);

	/* Check and display string if it exists */
	ShowString((char *) ptr);

	/* Wait forever */
	while (loop) {}
	

}


int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


