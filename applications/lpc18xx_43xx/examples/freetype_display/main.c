#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>
#include <k9f1g08.h>
#include <mm/raw_malloc.h>
#include <mm/raw_page.h>
#include <yaffsfs.h>
#include "lpc43xx_lcd.h"
#include <ft2build.h>
#include FT_FREETYPE_H



#define LCD_X_SIZE	800 											
#define LCD_Y_SIZE  480 

#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   1024

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

#define HEAP_ADDRESS_START 0x2bc00000
#define HEAP_ADDRESS_END   0x2c000000


void draw_word(int word, int x, int y, int width, int height, int color);
int freetype_init(const RAW_S8 *font_path);
int freetype_end(void);

#define COLOR_TO_MTK_COLOR_SIMUL(color) ((((color) >> 19) & 0x1f) << 11) \
                                            |((((color) >> 10) & 0x3f) << 5) \
                                            |(((color) >> 3) & 0x1f) 

RAW_U16 *g_lcdShowMemory;

void shell_task(void *arg)
{
	RAW_U8 old_priority;
	RAW_U32 i;
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();

	
	LCD_init(LCD_X_SIZE,LCD_Y_SIZE); 

	LCD_displayMemorySet((unsigned short *)0x28000000);
	LCD_enable();

	
	g_lcdShowMemory = (RAW_U16 *)0x28000000;

	for (i = 0; i < LCD_X_SIZE * LCD_Y_SIZE; i++) {
	                                                           
			g_lcdShowMemory[i] = COLOR_TO_MTK_COLOR_SIMUL(0x000000);

	}

	
	k9f_init();

	raw_page_init((RAW_VOID *)HEAP_ADDRESS_START, (RAW_VOID *)HEAP_ADDRESS_END);
	raw_malloc_init();

	yaffs_start_up();

	/*if you need format nand , please uncomment the following line*/
	//yaffs_format("nand", 0, 0, 0);

	printf("result is %x\r\n", yaffs_mount("nand"));

	freetype_init("/nand/simsun.ttc");

	draw_word(L'��', 160, 100, 120, 120, 0xcdcd);
	draw_word(L'��', 320, 100, 120,120, 0xff0000);
	draw_word('T', 160, 220, 120,120, 0xffffff);
	draw_word('X', 280, 220, 120,120, 0xffffff);
	draw_word('J', 400, 220, 120,120, 0xffffff);
	

	freetype_end();
	
	while (1);
	
	
	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


