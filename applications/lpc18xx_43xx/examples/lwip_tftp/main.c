#include <stdlib.h>
#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>

#include "lwip/opt.h"
#include "lwip/mem.h"
#include "lwip/raw.h"
#include "lwip/api.h"
#include "lwip/icmp.h"
#include "lwip/netif.h"
#include "lwip/sys.h"
#include "lwip/timers.h"
#include "lwip/inet_chksum.h"
#include "lwip/sockets.h"
#include "lwip/inet.h"
#include "lwip/tcpip.h"



#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400


PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;

extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);


extern err_t lpc_enetif_init(struct netif *netif);

struct netif tftp_client_netif;

ip_addr_t tftp_client_ipaddr, tftp_client_netmask, tftp_client_gw;

static void default_netif_add(void)
{
  IP4_ADDR(&tftp_client_gw, 192,168,0,1);
  IP4_ADDR(&tftp_client_ipaddr, 192,168,0,80);
  IP4_ADDR(&tftp_client_netmask, 255,255,255,0);

  netif_set_default(netif_add(&tftp_client_netif, &tftp_client_ipaddr, &tftp_client_netmask,
                              &tftp_client_gw, NULL, lpc_enetif_init, tcpip_input));
  netif_set_up(&tftp_client_netif);
}

extern void tftp_client_init(void);
extern uint32_t ethIsLink(void);
extern void  cpu_timer_init(void);


void shell_task(void *arg)
{
	RAW_U8 old_priority;
	RAW_U32 physts;

	//cpu_timer_init();
	OS_CPU_SysTickInit();
	//measure_overhead();
	
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();

	printf("start lwip tftp test\r\n");

	/*lan8720 must need hard reset or phy does not work*/
	#if 1

	Chip_SCU_PinMuxSet(0x6, 3, FUNC0 | MD_PLN_FAST);
		
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 3, 2);

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 3, 2, 0);
	
	raw_sleep(10);

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 3, 2, 1);
	
	raw_sleep(10);

	#endif

	tcpip_init(0, 0);                   
	default_netif_add();

	NVIC_EnableIRQ(ETHERNET_IRQn);

	#if 0
	while (1) {
		
		if (ethIsLink() == 1) {
			break;
		}
		
		raw_sleep(1);	
	}
	
	raw_sleep(200);	
	#endif

	while (1) {

		/* PHY status state machine, LED on when connected. This function will
		not block. */
		physts = lpcPHYStsPoll();

		/* Only check for connection state when the PHY status has changed */
		if (physts & PHY_LINK_CHANGED) {
			
			if (physts & PHY_LINK_CONNECTED) {
				
				//Board_LED_Set(0, true);

				printf("Link connect success\r\n");
				break;
			
			}
			
			else {
				printf("Link is unconnected, please wait\r\n");
				//Board_LED_Set(0, false);
				
			}

		}

	
	}
	
	tftp_client_init();

	
	while (1) {

		raw_sleep(100);
	}
	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


