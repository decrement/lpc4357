#include "board.h"
#include "cdjpeg.h"		/* Common decls for cjpeg/djpeg applications */
#include "jversion.h"		/* for version message */

#include <ctype.h>		/* to declare isprint() */
#include <yaffsfs.h>
#include <raw_api.h>


struct jpeg_decompress_struct cinfo;
struct jpeg_error_mgr jerr;
djpeg_dest_ptr dest_mgr;
JDIMENSION num_scanlines;

extern int bmp_data;

void jpeg_demo_decompress(void)
{
	int f1;
	int f2;
	RAW_U32 i;

	
	yaffs_start_up();

	/*if you need format nand , please uncomment the following line*/
	//yaffs_format("nand", 0, 0, 0);

	yaffs_mount("nand");

	f1 = yaffs_open("/nand/test.jpg", O_RDONLY, S_IREAD);
	
	f2 = yaffs_open("/nand/test.bmp", O_CREAT | O_RDWR,S_IREAD | S_IWRITE);


	printf("f1 f2 is %d %d\r\n", f1, f2);
		
	yaffs_lseek(f1,0,SEEK_SET);
	yaffs_lseek(f2,0,SEEK_SET);

	cinfo.err = jpeg_std_error(&jerr);

	 
	jpeg_create_decompress(&cinfo);
	/* Specify data source for decompression */
	jpeg_stdio_src(&cinfo, (void *)f1);

	 /* Read file header, set default decompression parameters */
	jpeg_read_header(&cinfo, TRUE);

	dest_mgr = jinit_write_bmp(&cinfo, FALSE);

	dest_mgr->output_file = (void *)f2;

	/* Start decompressor */
	jpeg_start_decompress(&cinfo);
	
	/* Write output file header */
	(*dest_mgr->start_output) (&cinfo, dest_mgr);


	/* Process data */
	while (cinfo.output_scanline < cinfo.output_height) {
		num_scanlines = jpeg_read_scanlines(&cinfo, dest_mgr->buffer,
		dest_mgr->buffer_height);
		(*dest_mgr->put_pixel_rows) (&cinfo, dest_mgr, num_scanlines);
	}


	(*dest_mgr->finish_output) (&cinfo, dest_mgr);
	
	yaffs_flush(f2);
	
	jpeg_finish_decompress(&cinfo);
	
	jpeg_destroy_decompress(&cinfo);

	yaffs_lseek(f2,0,SEEK_SET);

	i = yaffs_read(f2, (void *)0x28400000, 2 * 1024 * 1024);
		
	printf("i is 0x%x\r\n", i);
	
	bmp_data = i;
	
	
	yaffs_close(f1);
	
	yaffs_close(f2);
	
	yaffs_unmount("nand");



}

