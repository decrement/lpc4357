#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>
#include "spifilib_api.h"
#include "test_suite.h"





#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   2000

extern const unsigned char successwave[183340];

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


#ifndef SPIFLASH_BASE_ADDRESS
#define SPIFLASH_BASE_ADDRESS (0x14000000)
#endif

STATIC const PINMUX_GRP_T spifipinmuxing[] = {
	{0x3, 3,  (SCU_PINIO_FAST | SCU_MODE_FUNC3)},	/* SPIFI CLK */
	{0x3, 4,  (SCU_PINIO_FAST | SCU_MODE_FUNC3)},	/* SPIFI D3 */
	{0x3, 5,  (SCU_PINIO_FAST | SCU_MODE_FUNC3)},	/* SPIFI D2 */
	{0x3, 6,  (SCU_PINIO_FAST | SCU_MODE_FUNC3)},	/* SPIFI D1 */
	{0x3, 7,  (SCU_PINIO_FAST | SCU_MODE_FUNC3)},	/* SPIFI D0 */
	{0x3, 8,  (SCU_PINIO_FAST | SCU_MODE_FUNC3)}	/* SPIFI CS/SSEL */
};

/* Local memory, 32-bit aligned that will be used for driver context (handle) */
static uint32_t lmem[41];

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static SPIFI_HANDLE_T *initializeSpifi(void)
{
	int idx;
	int devIdx;
	uint32_t memSize;
	SPIFI_HANDLE_T *pReturnVal;

	/* Initialize LPCSPIFILIB library, reset the interface */
	spifiInit(LPC_SPIFI_BASE, true);

	/* register support for the family(s) we may want to work with
	     (only 1 is required) */
	spifiRegisterFamily(SPIFI_REG_FAMILY_SpansionS25FLP);
	spifiRegisterFamily(SPIFI_REG_FAMILY_SpansionS25FL1);
	spifiRegisterFamily(SPIFI_REG_FAMILY_MacronixMX25L);
	
	/* Return the number of families that are registered */
	idx = spifiGetSuppFamilyCount();

	/* Show all families that are registered */
	for (devIdx = 0; devIdx < idx; ++devIdx) {
		printf("FAMILY: %s\r\n", spifiGetSuppFamilyName(devIdx));
	}

	/* Get required memory for detected device, this may vary per device family */
	memSize = spifiGetHandleMemSize(LPC_SPIFI_BASE);
	
	if (memSize == 0) {
		/* No device detected, error */
		test_suiteError("spifiGetHandleMemSize", SPIFI_ERR_GEN);
	}

	/* Initialize and detect a device and get device context */
	pReturnVal = spifiInitDevice(&lmem, sizeof(lmem), LPC_SPIFI_BASE, SPIFLASH_BASE_ADDRESS);
	if (pReturnVal == NULL) {
		test_suiteError("spifiInitDevice", SPIFI_ERR_GEN);
	}
	return pReturnVal;
}


static void RunExample(void)
{
	uint32_t spifiBaseClockRate;
	uint32_t maxSpifiClock;
	uint16_t libVersion;
	SPIFI_HANDLE_T *pSpifi;

	/* Report the library version to start with */
	libVersion = spifiGetLibVersion();
	printf("\r\n\r\nSPIFI Lib Version %02d%02d\r\n", ((libVersion >> 8) & 0xff), (libVersion & 0xff));
	
	/* set the blink rate to 1/2 second while testing */
	test_suiteSetErrorBlinkRate(500);
	
	/* Setup SPIFI FLASH pin muxing (QUAD) */
	Chip_SCU_SetPinMuxing(spifipinmuxing, sizeof(spifipinmuxing) / sizeof(PINMUX_GRP_T));

	#if 0
	/* SPIFI base clock will be based on the main PLL rate and a divider */
	spifiBaseClockRate = Chip_Clock_GetClockInputHz(CLKIN_MAINPLL);

	/* Setup SPIFI clock to run around 1Mhz. Use divider E for this, as it allows
	   higher divider values up to 256 maximum) */
	Chip_Clock_SetDivider(CLK_IDIV_E, CLKIN_MAINPLL, 3);
	Chip_Clock_SetBaseClock(CLK_BASE_SPIFI, CLKIN_IDIVE, true, false);

	spifiBaseClockRate = Chip_Clock_GetClockInputHz(CLKIN_IDIVE);
	printf("SPIFI clock rate %d\r\n", spifiBaseClockRate);

	//raw_sleep(400);
	#endif
	
	/* initialize and get a handle to the spifi lib */
	pSpifi = initializeSpifi();

	
	/* Get info */
	printf("Device family       = %s\r\n", spifiDevGetFamilyName(pSpifi));
	printf("Capabilities        = 0x%x\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_CAPS));
	printf("Device size         = %d\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_DEVSIZE));
	printf("Max Clock Rate      = %d\r\n", maxSpifiClock);
	printf("Erase blocks        = %d\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_ERASE_BLOCKS));
	printf("Erase block size    = %d\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_ERASE_BLOCKSIZE));
	printf("Erase sub-blocks    = %d\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_ERASE_SUBBLOCKS));
	printf("Erase sub-blocksize = %d\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_ERASE_SUBBLOCKSIZE));
	printf("Write page size     = %d\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_PAGESIZE));
	printf("Max single readsize = %d\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_MAXREADSIZE));
	printf("Current dev status  = 0x%x\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_STATUS));
	printf("Current options     = %d\r\n", spifiDevGetInfo(pSpifi, SPIFI_INFO_OPTIONS));
	
	/* Test the helper functions first */
	test_suiteLibHelperBattery(pSpifi);

	/* Now test the erase functions first in block mode, then in address mode */
	test_suiteLibEraseBattery(pSpifi, false);
	

	test_suiteLibEraseBattery(pSpifi, true);

	/* test data integrity */
	test_suiteDataBattery(pSpifi, false, false);
	test_suiteDataBattery(pSpifi, true, false);
	test_suiteDataBattery(pSpifi, true, true);

	/* Done, de-init will enter memory mode */
	spifiDevDeInit(pSpifi);

	/* Indicate success */
	printf("All spifi test has completed.\r\n");

	while (1) {
		__WFI();
	}
	
}



void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

void shell_task(void *arg)
{
	RAW_U8 old_priority;

	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	//emc_init();
	//sdram_init();

	printf("start spifi test\r\n");
	
	/* Run the example code */
	RunExample();
	
	while (1);
	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


