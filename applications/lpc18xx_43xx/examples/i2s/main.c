#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   800



PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);


#define CODEC_I2S_BUS   LPC_I2S0
#define I2S_IRQ         I2S0_IRQn
#define AUDCFG_SAMPLE_RATE 48000
#define I2S_IRQHandler  I2S0_IRQHandler
#define CODEC_INPUT_DEVICE UDA1380_LINE_IN
#define I2S_DMA_TX_CHAN GPDMA_CONN_I2S_Tx_Channel_0
#define I2S_DMA_RX_CHAN GPDMA_CONN_I2S_Rx_Channel_1


/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define BUFFER_FULL 0
#define BUFFER_EMPTY 1
#define BUFFER_AVAILABLE 2

typedef struct ring_buff {
	uint32_t buffer[256];
	uint8_t read_index;
	uint8_t write_index;
} Ring_Buffer_t;

static char WelcomeMenu[] = "\r\nHello NXP Semiconductors \r\n"
							"I2S DEMO : Connect audio headphone out from computer to line-in on tested board to get audio signal\r\n"
							"Please press \'1\' to test Polling mode\r\n"
							"Please press \'2\' to test Interrupt mode\r\n"
							"Please press \'3\' to test DMA mode\r\n"
							"Please press \'x\' to exit test mode\r\n"
							"Please press \'m\' to DISABLE/ENABLE mute\r\n";

static Ring_Buffer_t ring_buffer;

static uint8_t send_flag;
static uint8_t channelTC;
static uint8_t dmaChannelNum_I2S_Tx, dmaChannelNum_I2S_Rx;
static uint8_t dma_send_receive;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
uint8_t mute_status = 0;

/* Get input from console */
int Con_GetInput(void)
{
#ifdef DEBUG_ENABLE
	return DEBUGIN();
#else
	return '3';
#endif
}

static void mute_toggle()
{
	mute_status = !mute_status;
	if (mute_status) {
		Chip_I2S_EnableMute(CODEC_I2S_BUS);
		DEBUGOUT("MUTE ENABLE\n\r");
	}
	else {
		Chip_I2S_DisableMute(CODEC_I2S_BUS);
		DEBUGOUT("MUTE DISABLE\n\r");
	}
}

/* Get status of the ring buffer */
static uint8_t ring_buff_get_status(Ring_Buffer_t *ring_buff)
{
	if (ring_buff->read_index == ring_buff->write_index) {
		return BUFFER_EMPTY;
	}
	else if (ring_buff->read_index == (ring_buff->write_index) + 1) {
		return BUFFER_FULL;
	}
	else {return BUFFER_AVAILABLE; }
}

/* Interrupt routine for I2S example */
static void App_Interrupt_Test(void)
{
	uint8_t bufferUART, continue_Flag = 1;
	DEBUGOUT("I2S Interrupt mode\r\n");
	Chip_I2S_Int_RxCmd(CODEC_I2S_BUS, ENABLE, 4);
	Chip_I2S_Int_TxCmd(CODEC_I2S_BUS, ENABLE, 4);
	NVIC_EnableIRQ(I2S_IRQ);
	while (continue_Flag) {
		bufferUART = 0xFF;
		bufferUART = Con_GetInput();
		switch (bufferUART) {
		case 'x':
			continue_Flag = 0;
			Chip_I2S_Int_RxCmd(CODEC_I2S_BUS, DISABLE, 4);
			NVIC_DisableIRQ(I2S_IRQ);
			DEBUGOUT(WelcomeMenu);
			break;

		case 'm':
			mute_toggle();
			break;

		default:
			break;
		}
	}
}

/* Polling routine for I2S example */
static void App_Polling_Test(void)
{
	uint32_t polling_data = 0;
	uint8_t bufferUART, continue_Flag = 1;
	DEBUGOUT("I2S Polling mode\r\n");
	while (continue_Flag) {
		bufferUART = 0xFF;
		bufferUART = Con_GetInput();
		switch (bufferUART) {
		case 'x':
			continue_Flag = 0;
			DEBUGOUT(WelcomeMenu);
			break;

		case 'm':
			mute_toggle();
			break;

		default:
			break;
		}

		if (Chip_I2S_GetRxLevel(CODEC_I2S_BUS) > 0) {
			polling_data = Chip_I2S_Receive(CODEC_I2S_BUS);
			send_flag = 1;
		}
		if ((Chip_I2S_GetTxLevel(CODEC_I2S_BUS) < 4) && (send_flag == 1)) {
			Chip_I2S_Send(CODEC_I2S_BUS, polling_data);
			send_flag = 0;
		}
	}
}

/* DMA routine for I2S example */
static void App_DMA_Test(void)
{
	uint8_t continue_Flag = 1, bufferUART = 0xFF;
	Chip_I2S_DMA_TxCmd(CODEC_I2S_BUS, I2S_DMA_REQUEST_CHANNEL_1, ENABLE, 4);
	Chip_I2S_DMA_RxCmd(CODEC_I2S_BUS, I2S_DMA_REQUEST_CHANNEL_2, ENABLE, 4);
	/* Initialize GPDMA controller */
	Chip_GPDMA_Init(LPC_GPDMA);
	/* Setting GPDMA interrupt */
	NVIC_DisableIRQ(DMA_IRQn);
	NVIC_SetPriority(DMA_IRQn, ((0x01 << 3) | 0x01));
	NVIC_EnableIRQ(DMA_IRQn);

	dmaChannelNum_I2S_Rx = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, I2S_DMA_RX_CHAN);

	Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum_I2S_Rx,
					  I2S_DMA_RX_CHAN,
					  I2S_DMA_TX_CHAN,
					  GPDMA_TRANSFERTYPE_P2P_CONTROLLER_SrcPERIPHERAL,
					  1);

DEBUGOUT("I2S DMA mode\r\n");
	while (continue_Flag) {
		bufferUART = 0xFF;
		bufferUART = Con_GetInput();
		switch (bufferUART) {
		case 'x':
			continue_Flag = 0;
			Chip_I2S_DMA_RxCmd(CODEC_I2S_BUS, I2S_DMA_REQUEST_CHANNEL_2, DISABLE, 1);
			Chip_I2S_DMA_TxCmd(CODEC_I2S_BUS, I2S_DMA_REQUEST_CHANNEL_1, DISABLE, 1);

			Chip_GPDMA_Stop(LPC_GPDMA, dmaChannelNum_I2S_Rx);
			NVIC_DisableIRQ(DMA_IRQn);
			DEBUGOUT(WelcomeMenu);
			break;

		case 'm':
			mute_toggle();
			break;

		default:
			break;
		}
	}
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/**
 * @brief	DMA interrupt handler sub-routine
 * @return	Nothing
 */
void DMA_IRQHandler(void)
{
	if (dma_send_receive == 1) {
		if (Chip_GPDMA_Interrupt(LPC_GPDMA, dmaChannelNum_I2S_Rx) == SUCCESS) {
			channelTC++;
		}
		else {
			/* Process error here */
		}
	}
	else {
		if (Chip_GPDMA_Interrupt(LPC_GPDMA, dmaChannelNum_I2S_Tx) == SUCCESS) {
			channelTC++;
		}
		else {
			/* Process error here */
		}
	}
}



/**
 * @brief	I2S0 interrupt handler sub-routine
 * @return	Nothing
 */
void I2S_IRQHandler(void)
{
	while ((ring_buff_get_status(&ring_buffer) != BUFFER_FULL) && (Chip_I2S_GetRxLevel(CODEC_I2S_BUS) > 0)) {
		ring_buffer.buffer[ring_buffer.write_index++] = Chip_I2S_Receive(CODEC_I2S_BUS);
	}
	while ((ring_buff_get_status(&ring_buffer) != BUFFER_EMPTY) && (Chip_I2S_GetTxLevel(CODEC_I2S_BUS) < 8)) {
		Chip_I2S_Send(CODEC_I2S_BUS, ring_buffer.buffer[ring_buffer.read_index++]);
	}
}


void shell_task(void *arg)
{
	RAW_U8 old_priority;

	I2S_AUDIO_FORMAT_T audio_Confg;
	uint8_t bufferUART, continue_Flag = 1;
	audio_Confg.SampleRate = AUDCFG_SAMPLE_RATE;
	/* Select audio data is 2 channels (1 is mono, 2 is stereo) */
	audio_Confg.ChannelNumber = 2;
	/* Select audio data is 16 bits */
	audio_Confg.WordWidth = 16;

	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	#if 0
	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();
	#endif

	/* Sets up DEBUG UART */
	DEBUGINIT();
	
	emc_init();
	sdram_init();

	/*SET I2S_CLOCK to pll1 192M or 204M or whatever*/
	LPC_CGU2->BASE_APB1_CLK = (1UL << 11) | (0x09 << 24);

	/*for our new board enable the following*/
	#if 1
	/* Initialize I2S peripheral ------------------------------------*/
	scu_pinmux(3,0, MD_PLN_FAST, 2);   //I2STX_CLK  OK 
	scu_pinmux(9,1, MD_PLN_FAST, 4);   //I2STX_WS   p9_1 4.13
	scu_pinmux(9,2, MD_PLN_FAST, 4);   //I2STX_SDA  p9_2 4.14


	scu_pinmux(6,0, MD_PLN_FAST, 4);   //I2SRX_CLK
	scu_pinmux(6,1, MD_PLN_FAST, 3);   //I2SRX_WS
	scu_pinmux(6,2, MD_PLN_FAST, 3);   //I2SRX_SDA
	#endif

	Board_Audio_Init(CODEC_I2S_BUS, UDA1380_MIC_IN_LR);
	
	DEBUGOUT(WelcomeMenu);

	Chip_I2S_Init(CODEC_I2S_BUS);
	Chip_I2S_RxConfig(CODEC_I2S_BUS, &audio_Confg);
	Chip_I2S_TxConfig(CODEC_I2S_BUS, &audio_Confg);
	
	Chip_I2S_TxStop(CODEC_I2S_BUS);
	Chip_I2S_DisableMute(CODEC_I2S_BUS);
	Chip_I2S_TxStart(CODEC_I2S_BUS);
	send_flag = 0;
	while (continue_Flag) {
		bufferUART = 0xFF;
		bufferUART = Con_GetInput();
		switch (bufferUART) {
		case '1':
			App_Polling_Test();
			break;

		case '2':
			App_Interrupt_Test();
			break;

		case '3':
			App_DMA_Test();
			break;

		case 'x':
			continue_Flag = 0;
			DEBUGOUT("Thanks for using\r\n");
			break;

		default:
			break;
		}
	}
	
	
	
}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


