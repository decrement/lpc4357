#include <raw_api.h>
#include <string.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/* Data transfer information */
#define TRANSFER_DST_ADDR          0x28500000
#define TRANSFER_SRC_ADDR          0x28100000
#define TRANSFER_SIZE              0x200000

#define TRANSFER_BLOCK_SZ          (4 * 3 * 1024) /* 3K of data transfered per LLI */
#define DMA_DESCRIPTOR_COUNT       256

static uint8_t ch_no;
static DMA_TransferDescriptor_t desc_array[DMA_DESCRIPTOR_COUNT];
static volatile int dma_xfer_complete;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void prepare_src_data(uint32_t *src, int sz)
{
	int i;
	for (i = 0; i < sz; i++) {
		src[i] = i | (~i << (16 - (i & 15))); /* Fill it with some pattern */
	}
}

/* Function to prepare memory to memory transfer descriptors */
static int prepare_dma_desc(uint32_t* dst, const uint32_t* src, uint32_t sz)
{
	int i;
	DMA_TransferDescriptor_t *desc;
	int num_desc = sz / TRANSFER_BLOCK_SZ;

	/* Add one more if size is not in 3K blocks */
	num_desc += sz * TRANSFER_BLOCK_SZ != sz;
	desc = &desc_array[0];
	if (num_desc >= DMA_DESCRIPTOR_COUNT) {
		DEBUGOUT("Transfer needs %d descriptors, but has "
			"%d descriptors only\r\n", num_desc, DMA_DESCRIPTOR_COUNT);
		return 0;
	}

	for (i = 0; i < num_desc; i ++)
	{
		Chip_GPDMA_PrepareDescriptor(LPC_GPDMA, &desc[i],
			(uint32_t) src + (i * TRANSFER_BLOCK_SZ),
			(uint32_t) dst + (i * TRANSFER_BLOCK_SZ),
			(uint32_t) (sz < TRANSFER_BLOCK_SZ ? sz : TRANSFER_BLOCK_SZ),
			GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA,
			(i + 1 == num_desc) ? NULL: &desc[i + 1]);
		sz -= TRANSFER_BLOCK_SZ;
	}
	return num_desc;
}

/* Print the result of a data transfer */
static void print_result(const void *dst, const void *src, int sz, uint32_t stime, uint32_t etime, const char *mode)
{
	uint32_t clk = SystemCoreClock/1000000;
	int invalid;
	invalid = memcmp(dst, src, sz);
	DEBUGOUT("\r\nData transfer results [MODE: %s]\r\n"
		"=========================================\r\n", mode);
	DEBUGOUT("SOURCE  ADDR: 0x%08X\r\n", src);
	DEBUGOUT("DESTN   ADDR: 0x%08X\r\n", dst);
	DEBUGOUT("XFER    SIZE: %lu (Bytes)\r\n", sz);
	DEBUGOUT("CPU    SPEED: %lu.%lu (MHz)\r\n", clk, (SystemCoreClock / 10000) - (clk * 100));
	DEBUGOUT("START   TIME: %lu (ticks)\r\n", stime);
	DEBUGOUT("END     TIME: %lu (ticks)\r\n", etime);
	DEBUGOUT("TIME   TAKEN: %lu (uSec(s))\r\n", (etime - stime) / clk);
	DEBUGOUT("SRC/DST COMP: %s\r\n", invalid ? "NOT MATCHING" : "MATCHING");
}
/*****************************************************************************
 * Public functions
 ****************************************************************************/

/**
 * @brief	RIT interrupt handler
 * @return	Nothing
 */
void DMA_IRQHandler(void)
{
	dma_xfer_complete = 1;
	Chip_GPDMA_Interrupt(LPC_GPDMA, ch_no);
}


void shell_task(void *arg)
{
	RAW_U8 old_priority;
	uint32_t start_time;
	uint32_t end_time;
	uint32_t *dst = (uint32_t *) TRANSFER_DST_ADDR;
	uint32_t *src = (uint32_t *) TRANSFER_SRC_ADDR;
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();

	printf("start dma memery test\r\n");
	
	/* Initialize the DMA */
	Chip_GPDMA_Init(LPC_GPDMA);

	/* Prepare the source buffer for transfer */
	prepare_src_data(src, TRANSFER_SIZE/sizeof(*src));
	DEBUGSTR("***** DATA TRANSFER TEST *******\r\n");
	start_time = Chip_RIT_GetCounter(LPC_RITIMER);
	memcpy(dst, src, TRANSFER_SIZE);
	end_time = Chip_RIT_GetCounter(LPC_RITIMER);
	print_result(dst, src, TRANSFER_SIZE, start_time, end_time, "CPU (memcpy)");
	memset(dst, 0, TRANSFER_SIZE);
	ch_no = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, 0);
	if (prepare_dma_desc(dst, src, TRANSFER_SIZE) <= 0) {
		DEBUGSTR("Unable to create DMA Descriptors\r\n");
		while (1) {}
	}
	start_time = Chip_RIT_GetCounter(LPC_RITIMER);
	Chip_GPDMA_SGTransfer(LPC_GPDMA, ch_no, &desc_array[0], GPDMA_TRANSFERTYPE_M2M_CONTROLLER_DMA);
	NVIC_EnableIRQ(DMA_IRQn);
	while(!dma_xfer_complete); /* Set by ISR */
	end_time = Chip_RIT_GetCounter(LPC_RITIMER);
	print_result(dst, src, TRANSFER_SIZE, start_time, end_time, "DMA");
	/* LED is toggled in interrupt handler */
	while (1) {}
	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


