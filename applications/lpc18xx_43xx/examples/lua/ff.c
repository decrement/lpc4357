#include <raw_api.h>
#include <raw_ff.h>


_FileType stdFiles[3] = {
	{
		.file_no = 0,
	},
	{
		.file_no = 1,
	},
	{
		.file_no = 2,
	}
};

_EnvType __Env = {
	._stdin = &stdFiles[0],
	._stdout = &stdFiles[1],
	._stderr = &stdFiles[2],
};

_EnvType *_EnvPtr = &__Env;


raw_FILE *raw_fopen(const char *path, const char *mode)
{
	RAW_ASSERT(0);
	return 0;
}


RAW_PROCESSOR_UINT  raw_fread(void *ptr, RAW_PROCESSOR_UINT size, RAW_PROCESSOR_UINT nmemb, raw_FILE *stream)
{
	RAW_ASSERT(0);
	return 0;
}


RAW_PROCESSOR_UINT  raw_fwrite(const char *ptr, RAW_PROCESSOR_UINT size, RAW_PROCESSOR_UINT nmemb, raw_FILE *stream)
{
	RAW_ASSERT(0);
	return 0;
}

RAW_S32 raw_fclose(raw_FILE *fp)
{
	RAW_ASSERT(0);
	return 0;
}

RAW_S32 raw_ferror(raw_FILE *fp)
{
	RAW_ASSERT(0);
	return 0;
}


raw_FILE *raw_freopen(const char *pathpath, const char *mode, raw_FILE *stream)

{
	RAW_ASSERT(0);
	return 0;

}


RAW_PROCESSOR_INT raw_fseek(raw_FILE *stream, RAW_PROCESSOR_INT offset,  RAW_PROCESSOR_INT whence)
{
	RAW_ASSERT(0);
	return 0;
}


RAW_PROCESSOR_INT raw_getc(raw_FILE *stream)
{
	RAW_ASSERT(0);
	return 0;

}


int raw_feof(raw_FILE *stream)
{
	RAW_ASSERT(0);
	return 0;

}



long raw_ftell(raw_FILE *stream)
{

	RAW_ASSERT(0);
	return 0;
}

int raw_ungetc(int c, raw_FILE *stream)
{

	RAW_ASSERT(0);
	return 0;
}


int raw_fflush(raw_FILE *fp)
{

	RAW_ASSERT(0);
	return 0;
}


int raw_setvbuf(raw_FILE *stream, char *buf, int type, unsigned size)
{
	RAW_ASSERT(0);
	return 0;
}

