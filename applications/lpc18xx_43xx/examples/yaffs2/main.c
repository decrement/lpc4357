#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>
#include <k9f1g08.h>
#include <mm/raw_malloc.h>
#include <mm/raw_page.h>
#include <yaffsfs.h>


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

#define HEAP_ADDRESS_START 0x2bc00000
#define HEAP_ADDRESS_END   0x2c000000
#define number_bytes_test  4096


int f;
int r;

void ff2(void);


void shell_task(void *arg)
{
	RAW_U8 old_priority;
	
	RAW_U32 i;
	RAW_TICK_TYPE t1, t2;
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();

	k9f_init();

	raw_page_init((RAW_VOID *)HEAP_ADDRESS_START, (RAW_VOID *)HEAP_ADDRESS_END);
	raw_malloc_init();

	ff2();
	
	while (1);

	yaffs_start_up();

	/*if you need format nand , please uncomment the following line!!*/
	//yaffs_format("nand", 0, 0, 0);

	printf("result is %x\r\n", yaffs_mount("nand"));

	raw_memset((RAW_U8 *)0x28000000, 0x0, number_bytes_test);
	raw_memset((RAW_U8 *)0x28100000, 0x18, number_bytes_test);

	f = yaffs_open("/nand/b1", O_CREAT | O_RDWR,S_IREAD | S_IWRITE);

	printf("open /nand/b1 O_CREAT, f=%d\r\n",f);


	r= yaffs_lseek(f,0,SEEK_SET);


	printf("r is %d\r\n", r);

	printf("failure1111 is %d\r\n", yaffsfs_GetLastError());

	printf("writing 6 bytes to nand\r\n");

	t1 = raw_system_time_get();
	
	r = yaffs_write(f,(void *)0x28100000, number_bytes_test);

	t2 = raw_system_time_get();
	
	
	printf("write %d to nand \r\n", r);

	printf("cost time %d ms to write 64M data\r\n", 10 *(t2 - t1));
	
	
	printf("failure2222 is %d\r\n", yaffsfs_GetLastError());
	
	printf("freespace is %d\r\n", yaffs_freespace("nand"));
	printf("totalspace is %d\r\n", yaffs_totalspace("nand"));

	r= yaffs_lseek(f,0,SEEK_SET);

	r= yaffs_read(f, (RAW_U8 *)0x28000000, number_bytes_test + 1024);

	printf("r is %d\r\n", r);

	for (i = 0; i < number_bytes_test; i++) {

		RAW_ASSERT(*(RAW_U8 *)(0x28000000 + i) == 0x18);
	}
		
	r = yaffs_close(f);

	printf("close %d\r\n",r);

	yaffs_unmount("nand");
	
	while (1);
	
	
	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


