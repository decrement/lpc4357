#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>
#include <k9f1g08.h>


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

#define NAND_TEST_BLOCK  1023
#define NAND_TEST_MEM1  0x28400000
#define NAND_TEST_MEM2  0x28500000

static RAW_U8 *test_data;

static RAW_U8 test_data2[2048];
static RAW_U8 test_data3[2048];
RAW_U8 res;
RAW_U32 nand_id;

void shell_task(void *arg)
{
	RAW_U8 old_priority;
	
	RAW_U32 i;
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();

	test_data = (RAW_U8 *)NAND_TEST_MEM1;
	
	raw_memset((RAW_U8 *)NAND_TEST_MEM1, 0x99, 2 * 1024);
	
	k9f_init();
	
	nand_id = k9f_id_read();
	printf("Nand id is 0x%x!\r\n", nand_id);

	/*The following code is for ATO*/
	#if 0
	if (nand_id != 0x9bf1001d) {
		
		printf("Wrong nand flash ID!!!!!!\n");
		RAW_ASSERT(0);
	}
	#endif
	
	res = k9f_bad_block_check_product(NAND_TEST_BLOCK);
	
	if (res == 0) {

		printf("bad block, please change another block to test\r\n");
		RAW_ASSERT(0);

	}


	res = k9f_block_erase(NAND_TEST_BLOCK);
	res = k9f_block_erase(NAND_TEST_BLOCK + 1);



	raw_memset(test_data2, 0xa0, 2048);
	
	res = k9f_page_write(NAND_TEST_BLOCK * 64, test_data2);
	k9f_random_write(NAND_TEST_BLOCK * 64, 2070 + 0, 0xcc);
	res = k9f_random_read(NAND_TEST_BLOCK *64, 2070);
	k9f_page_read(NAND_TEST_BLOCK * 64, (RAW_U8 *)test_data3);
	res = memcmp(test_data2, test_data3, 2048);
	RAW_ASSERT(res == 0);

	test_data2[0] = 0xcc;
	test_data2[1] = 0x11;
	test_data2[2] = 0x22;
	test_data2[3] = 0x33;

	res = k9f_block_erase(NAND_TEST_BLOCK);
	
	k9f_random_continue_write(NAND_TEST_BLOCK * 64, 2070 + 0, test_data2, 1);
	raw_sleep(100);

	res = k9f_block_erase(NAND_TEST_BLOCK);
	
	k9f_random_continue_write(NAND_TEST_BLOCK * 64, 2070 + 1, &test_data2[1], 3);
	
	res = k9f_random_read(NAND_TEST_BLOCK *64, 2070);
	res = k9f_random_read(NAND_TEST_BLOCK *64, 2071);

	res = k9f_random_read(NAND_TEST_BLOCK *64, 2072);
	res = k9f_random_read(NAND_TEST_BLOCK *64, 2073);
	
	res = k9f_block_erase(NAND_TEST_BLOCK);
	
	raw_memset(test_data2, 0xaa, 10);
	k9f_random_continue_write(NAND_TEST_BLOCK * 64 + 1, 2080, test_data2, 10);
	k9f_random_continue_read(NAND_TEST_BLOCK * 64 + 1, 2080, test_data3, 10);

	for (i= 0; i < 10; i++) {

		if (test_data3[i] != (0xaa)) {

			printf("nand test k9f_random_continue_write failed\r\n");
			RAW_ASSERT(0);
		}
			
	}
	
	
	res = k9f_bad_block_check_product(NAND_TEST_BLOCK);
	
	if (res == 0) {

		printf("bad block, please change another block to test\r\n");
		RAW_ASSERT(0);

	}
	
	res = k9f_page_write(NAND_TEST_BLOCK * 64, test_data);
	printf("write result is %d\r\n", res);
	
	k9f_page_read(NAND_TEST_BLOCK * 64, (RAW_U8 *)NAND_TEST_MEM2);

	for (i= 0; i < 2048; i++) {

		if (*((RAW_U8 *)(NAND_TEST_MEM2 + i)) != 0x99) {

			printf("nand test page write and read failed\r\n");
			RAW_ASSERT(0);

		}
			

	}

	printf("nand test all pass\r\n");
	
	while (1);
	
	
	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


