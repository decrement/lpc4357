#include <raw_api.h>
#include <cmsis.h>
#include <board.h>
#include <fifo.h>
#include <lib_string.h>
#include <rsh.h>
#include <sdram.h>
#include "uart1_rs485.h"


#define LPC_UART LPC_USART3
#define UARTx_IRQn  USART3_IRQn


#define SHELL_STACK_SIZE   400

PORT_STACK shell_task_stack[SHELL_STACK_SIZE];

RAW_TASK_OBJ 		shell_task_obj;
RAW_SEMAPHORE uart_sema;
struct raw_fifo uart_fifo;
extern void three_parameters_cmd_register(void);
extern void no_parameters_cmd_register(void);


MEM_POOL rs485_block;


unsigned long g_memBuffer[1024];


uint8_t g_adCmdData[128];
uint32_t g_adCmdDataLen = 0;

uint32_t g_eventAdc;

#define EVENT_ADC_CMD   0x00000001

static void ADC_Init(uint32_t cclk, uint32_t rate)
{
	uint32_t temp, tmpreg;//, ADCbitrate;

	LPC_ADC0->CR = 0;
    
	//Enable PDN bit
	tmpreg = (1UL << 21);


	/* The APB clock (PCLK_ADC0) is divided by (CLKDIV+1) to produce the clock for
	 * A/D converter, which should be less than or equal to 12.4MHz.
	 * A fully conversion requires 31 of these clocks.
	 * ADC clock = PCLK_ADC0 / (CLKDIV + 1);
	 * ADC rate = ADC clock / 31;
	 */
	temp = (cclk /(rate * 31)) - 1;
	tmpreg |=  (temp << 8);	

	LPC_ADC0->CR = tmpreg;

	LPC_ADC0->INTEN &= ~(1UL << 1);
	LPC_ADC0->CR |= (1UL << 1);
}

static uint16_t ADC_GetData(uint8_t channel)
{
	uint32_t temp = 0;

	LPC_ADC0->CR &= ~(7UL << 24);
	LPC_ADC0->CR |= (1 << 24);	    // start now

	while ((temp & (1UL << 31)) == 0)
	{
		temp = LPC_ADC0->DR[channel];
	}

	return ((temp >> 6)& 0x3FF);
}


void UART1_rcv(unsigned char * data, unsigned long len)
{
    if ((g_adCmdDataLen + len) < 128)
    {
        raw_memcpy(g_adCmdData + g_adCmdDataLen, data, len);
        g_adCmdDataLen += len;
    }

    g_eventAdc |= EVENT_ADC_CMD;
}

// AD的事件处理例程
void eventHandleAdc(void)
{
    uint8_t *data;
    void *memory;
    uint8_t srcAddr, cmdLen;
    uint16_t len, value;
    uint8_t answer[8];

    g_eventAdc = 0;

	raw_block_allocate(&rs485_block, &memory);

    if (memory == 0)
    {
        return;
    }

    data = memory;

    UART1_intRxCtl(0);

    raw_memcpy(data, g_adCmdData, g_adCmdDataLen);
    len = g_adCmdDataLen;

    g_adCmdDataLen = 0;

    UART1_intRxCtl(1);

    while (len)
    {
         if (len < 5)
         {
             break;
         }

         // 检测一下前导字节 0X55 0XAA
         if ((data[0] != 0x55) || (data[1] != 0xAA))
         {
             break;
         }

         // 记录源地址
         srcAddr = data[2];

         // 模块RS485从机，地址为2
         if (data[3] != 2)
         {
             break;
         }

         // 记录命令长度
         cmdLen = data[4];

         data += 5;
         len -= 5;

         if (len < cmdLen)
         {
             break;
         }

         if ((cmdLen == 1) && (data[0] == 1))
         {
             // CMD为1，表示查询AD值
             value = ADC_GetData(1);

             answer[0] = 0x55;
             answer[1] = 0xAA;
             answer[2] = 2;
             answer[3] = srcAddr;
             answer[4] = 3;   // LEN
             answer[5] = 0x81;  // GET AD VALUE RESPONE
             answer[6] = (uint8_t)((value >> 8) & 0x000000FF);
             answer[7] = (uint8_t)((value) & 0x000000FF);

             UART1_snd(answer, 8);
         }

         data += cmdLen;
         len -= cmdLen;
    }

	raw_block_release(&rs485_block, memory);
}


void SysTick_Handler(void)
{
	raw_enter_interrupt();
	raw_time_tick();
	raw_finish_int();	
}
	

/* System Clock Frequency (Core Clock) */
extern uint32_t SystemCoreClock;
void  OS_CPU_SysTickInit(void)
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / RAW_TICKS_PER_SECOND);									
}

RAW_U32 test_count;

static RAW_U32 fifo_buffer[32];


int uart_3_recv(void *buf, int cnt, int timeout)
{
	int ret= 0;
	raw_memset((RAW_VOID *)buf, (RAW_U8)0, cnt);
	
	ret = raw_semaphore_get(&uart_sema, timeout);
	
	if (RAW_SUCCESS == ret) {
		ret = fifo_out_all(&uart_fifo, buf);
		RAW_ASSERT(ret <= cnt);
	} else if (RAW_BLOCK_TIMEOUT == ret) {
		ret = 0;
	}

	return ret;
}


static void uart_3_irq(void)
{

	RAW_U8 ch[32];
	RAW_U8 i;

	i = 0;

	while (Chip_UART_ReadLineStatus(LPC_UART) & UART_LSR_RDR) {
	ch[i] = Chip_UART_ReadByte(LPC_UART);
	i++;
	}

	fifo_in(&uart_fifo, ch, i);
	raw_semaphore_put(&uart_sema);




}

void UART3_IRQHandler(void)
{
	raw_enter_interrupt();
	uart_3_irq();
	raw_finish_int();	
	 
}



/* Initialize Interrupt for UART */
static void uart3_int_init(void)
{
	/* Enable UART Rx & line status interrupts */
	/*
	 * Do not enable transmit interrupt here, since it is handled by
	 * UART_Send() function, just to reset Tx Interrupt state for the
	 * first time
	 */
	Chip_UART_IntEnable(LPC_UART, (UART_IER_RBRINT | UART_IER_RLSINT));

	/* Enable Interrupt for UART channel */
	/* Priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART channel */
	NVIC_EnableIRQ(UARTx_IRQn);
}

void register_task_stack_command(void);

void shell_task(void *arg)
{
	RAW_U8 old_priority;
	
	OS_CPU_SysTickInit();
	raw_semaphore_create(&uart_sema, "u_sema", 0);
	fifo_init(&uart_fifo, fifo_buffer, 128);
	
	raw_task_priority_change (raw_task_active, CONFIG_RAW_PRIO_MAX - 2, &old_priority);

	Board_UART_Init(LPC_UART);
	Chip_UART_Init(LPC_UART);
	Chip_UART_SetBaud(LPC_UART, 115200);
	Chip_UART_ConfigData(LPC_UART, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT); /* Default 8-N-1 */

	/* Enable UART Transmit */
	Chip_UART_TXEnable(LPC_UART);
	/* Reset FIFOs, Enable FIFOs and DMA mode in UART */
	Chip_UART_SetupFIFOS(LPC_UART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
							UART_FCR_TX_RS | UART_FCR_DMAMODE_SEL | UART_FCR_TRG_LEV0));

	/* preemption = 1, sub-priority = 1 */
	NVIC_SetPriority(UARTx_IRQn, 1);
	/* Enable Interrupt for UART0 channel */
	NVIC_EnableIRQ(UARTx_IRQn);
	uart3_int_init();

	emc_init();
	sdram_init();

	raw_block_pool_create(&rs485_block, (RAW_U8 *)"485", 256, (RAW_VOID *)0x28000000, 20 * 1024);

	printf("start uart 485  test\r\n");

	UART1_init(180000000, 9600);
 
    ADC_Init(180000000, 50000); 

    while (1)
    {
        if (g_eventAdc)
        {
            eventHandleAdc();
        }
    }
	

}



int main()
{

	raw_os_init();

	raw_task_create(&shell_task_obj, (RAW_U8  *)"task1", 0,
	                         2, 0,  shell_task_stack, 
	                         SHELL_STACK_SIZE ,  shell_task, 1); 
	
	raw_os_start();

	return 0;
	
}


