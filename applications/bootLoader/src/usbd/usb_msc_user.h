
#ifndef USB_HID_USER_H
#define USB_HID_USER_H

//#include "usbd_mscuser.h"
#include "usbd_core.h"
//#include "usbd_hw.h"
//#include "usbd_mscuser.h"
#include "usbd_rom_api.h"

//extern ErrorCode_t usb_hid_init(USBD_HANDLE_T hUsb, USB_INTERFACE_DESCRIPTOR* pIntfDesc, 
//                                 uint32_t* mem_base,  uint32_t* mem_size);
//
//extern uint32_t copy_descriptors(USB_CORE_DESCS_T* pDesc, uint32_t mem_base);
//
//extern ErrorCode_t USB_Configure_Event (USBD_HANDLE_T hUsb);

extern ErrorCode_t usb_msc_mem_init(USBD_HANDLE_T hUsb,	USB_INTERFACE_DESCRIPTOR* pIntfDesc, 
                                     uint32_t* mem_base, uint32_t* mem_size);

extern uint32_t copy_descriptors(USB_CORE_DESCS_T* pDesc, uint32_t mem_base);


#endif


