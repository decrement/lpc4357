#ifndef __DS_FAT16_H
#define __DS_FAT16_H

typedef struct{
    unsigned char jumpInstruction[3];   // 跳转指令(0)
    unsigned char BS_OEMName[8];        // OEM字符串，必须为8个字符，不足以空格填空 MSWIN4.1 (3)
    unsigned char BPB_BytsPerSec[2];    // 每扇区字节数 200h (11)
    unsigned char BPB_SecPerClus;       // 每簇占用的扇区数 (13)
    unsigned char BPB_RsvdSecCnt[2];    // 保留扇区数(14)
    unsigned char BPB_NumFATs;          // FAT表的记录数 2(16) 
    unsigned char BPB_RootEntCnt[2];    // 最大根目录文件数 (17)
    unsigned char BPB_TotSec16[2];      // 逻辑扇区总数 (19)
    unsigned char BPB_Media;            // 媒体描述符 (21)
    unsigned char BPB_FATSz16[2];       // 每个FAT占用扇区数 (22)
    unsigned char BPB_SecPerTrk[2];     // 每个磁道扇区数 (24)
    unsigned char BPB_NumHeads[2];      // 磁头数 (26)
    unsigned char BPB_HiddSec[4];       // 隐藏扇区数(28) 
    unsigned char BPB_TotSec32[4];      // 如果BPB_TotSec16是0，则在这里记录扇区总数 0 (32)

    unsigned char BS_DrvNum;            // 中断13的驱动器号 (36)
    unsigned char S_Reserved1;          // 未使用(37)
    unsigned char BS_BootSig;           // 扩展引导标志 (38)
    unsigned char BS_VolID[4];          // 卷序列号 (39)
    unsigned char BS_VolLab[11];        // 卷标，必须是11个字符，不足以空格填充  (43)
    unsigned char BS_FileSysType[8];    // 文件系统类型，必须是8个字符，不足填充空格(54)

    unsigned char fix[28];
}T_FAT16_BOOT;

// 根据FLASH大小创建一个FAT表
extern void fat16Create(unsigned long flashSize);


#endif
