
#include <string.h>
#include <stdarg.h>
#include "DsFat.h"
#include "fat16.h"
#include "fat.h"

extern unsigned char g_fatLoadSectorImage[];


// 根据FLASH大小创建一个FAT表
void fat16Create(unsigned long flashSize)
{
    unsigned long sectorNum, sectorNumPerFatTable, sectorPerClus;
    unsigned char fatTableFirst[4];

    unsigned long long userClusterNum;

    //fatPrint("\r\nfat16Creat ...");
        
    memcpy((unsigned char *)&g_fatBootUnit.fat16Boot, g_fatLoadSectorImage, sizeof(g_fatBootUnit.fat16Boot));

    //1个fat表
    g_fatBootUnit.fat16Boot.BPB_NumFATs = 1;
    
    sectorNum = (flashSize >> 9);  // 每个扇区512字节

    if (flashSize <= 0x100000)        //1M
    {
        sectorPerClus = 1;   // 目前为每个簇1个扇区
    }
    else if(flashSize <= 0x200000)    //2M
    {
        sectorPerClus = 2;   // 目前为每个簇2个扇区
    }
    else if (flashSize <= 0x1000000) // 16M 
    {
        sectorPerClus = 1;   // 目前为每个簇1个扇区
    }
    else if (flashSize <= 0x8000000) //128
    {
        sectorPerClus = 4;
    }
    else if (flashSize <= 0x10000000) //256
    {
        sectorPerClus = 8;   // 目前为每个簇8个扇区
    }
    else if (flashSize <= 0x20000000)  //512
    {
        sectorPerClus = 16;
    }
    else if (flashSize <= 0x40000000)  //1024
    {
        sectorPerClus = 32;
    }
    else
    {
        sectorPerClus = 64;   // 目前为每个簇64个扇区
    }

    if (flashSize <= 0x200000)
    {
        g_fatSysType = FAT_TYPE_FAT12;
        g_fatFileEndClusterValue = 0x0FF8;
        memcpy(g_fatBootUnit.fat16Boot.BS_FileSysType ,"FAT12", 5);
    }
    else
    {
        g_fatSysType = FAT_TYPE_FAT16;
        g_fatFileEndClusterValue = 0xFFF8;
    }

    g_fatBootUnit.fat16Boot.BPB_SecPerClus = (unsigned char)sectorPerClus;
    
    //fatPrint("\r\nfatCreate totalSize(0x%x), g_fatBootUnit.fat16Boot.BPB_SecPerClus(0x%x)", flashSize, g_fatBootUnit.fat16Boot.BPB_SecPerClus); 

    // 得到用户区的簇数
    userClusterNum =  sectorNum - 2 - 32;  // 除去保留扇区及根目录扇区r外的所有扇区（FAT表及用户区）
    //userClusterNum =  (userClusterNum*(512*sectorPerClus-4)/(512*sectorPerClus))/sectorPerClus;
    userClusterNum =  (userClusterNum*(512*sectorPerClus)/(512*sectorPerClus + 2 * g_fatBootUnit.fat16Boot.BPB_NumFATs))/sectorPerClus;

    // 更新一下FAT表占用的扇区数
    if (g_fatSysType == FAT_TYPE_FAT16)
    {
        sectorNumPerFatTable = (userClusterNum*2 + 511)/512;  // FAT16使用2个字节表示一个簇
    }
    else
    {
        sectorNumPerFatTable = (userClusterNum*3/2 + 511)/512;  // FAT12使用1.5个字节表示一个簇
    }

    // 此时，再重新得到总的扇区数
    sectorNum = 2 + sectorNumPerFatTable*g_fatBootUnit.fat16Boot.BPB_NumFATs + 32 + userClusterNum*sectorPerClus;

    //fatPrint("\r\nfatCreate totalSectorNum(0x%x)", sectorNum); 

    // 更新一下总的扇区数
    if (sectorNum < 0x00010000)
    {   
        g_fatBootUnit.fat16Boot.BPB_TotSec16[0] = (unsigned char)sectorNum; 
        g_fatBootUnit.fat16Boot.BPB_TotSec16[1] = (unsigned char)(sectorNum >> 8);

        g_fatBootUnit.fat16Boot.BPB_TotSec32[0] = 0; 
        g_fatBootUnit.fat16Boot.BPB_TotSec32[1] = 0;
        g_fatBootUnit.fat16Boot.BPB_TotSec32[2] = 0; 
        g_fatBootUnit.fat16Boot.BPB_TotSec32[3] = 0;
    }
    else
    {   
        g_fatBootUnit.fat16Boot.BPB_TotSec16[0] = 0;
        g_fatBootUnit.fat16Boot.BPB_TotSec16[1] = 0;

        g_fatBootUnit.fat16Boot.BPB_TotSec32[0] = (unsigned char)sectorNum; 
        g_fatBootUnit.fat16Boot.BPB_TotSec32[1] = (unsigned char)(sectorNum >> 8);
        g_fatBootUnit.fat16Boot.BPB_TotSec32[2] = (unsigned char)(sectorNum >> 16); 
        g_fatBootUnit.fat16Boot.BPB_TotSec32[3] = (unsigned char)(sectorNum >> 24);
    }

    g_fatBootUnit.fat16Boot.BPB_FATSz16[0] = (unsigned char)sectorNumPerFatTable;
    g_fatBootUnit.fat16Boot.BPB_FATSz16[1] = (unsigned char)(sectorNumPerFatTable >> 8);

    // 在确定FAT后，再重新确定一下簇的数量 
    //sectorNum = (sectorNum -2 - sectorNumPerFatTable*g_fatBootUnit.fat16Boot.BPB_NumFATs)/512; // 得到用户区的扇区数量

    // 写引导扇区
    fatFlashWrite(0, g_fatLoadSectorImage, 512);
    fatFlashWrite(0, (unsigned char *)&g_fatBootUnit.fat16Boot, sizeof(g_fatBootUnit.fat16Boot));

    // 写保留扇区
    fatFlashClear(512, 512);

    fatTableFirst[0] = 0xF8;
    fatTableFirst[1] = 0xFF;
    fatTableFirst[2] = 0xFF;

    if (g_fatSysType == FAT_TYPE_FAT16)
    {
        fatTableFirst[3] = 0xFF;
    }
    else
    {
        fatTableFirst[3] = 0x00;
    }

    // 清FAT表1
    fatFlashWrite(1024, fatTableFirst, 4);
    fatFlashClear(1024+4, sectorNumPerFatTable*512-4);

    // 清FAT表2
    if (g_fatBootUnit.fat16Boot.BPB_NumFATs == 2)
    {
        fatFlashWrite((2 + sectorNumPerFatTable)*512, fatTableFirst, 4);
        fatFlashClear((2 + sectorNumPerFatTable)*512 + 4, sectorNumPerFatTable*512-4);
    }

    // 清根目录
    fatFlashClear((2 + sectorNumPerFatTable*g_fatBootUnit.fat16Boot.BPB_NumFATs)*512, 512*32);
}










