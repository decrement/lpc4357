#ifndef __DS_FAT_DEF_H
#define __DS_FAT_DEF_H

#define FAT_ALLOC_MEM_SUPPORT       0		//支持动态内存分配
#define FAT_CN_SUPPORT              0       //中文支持
#define FAT_VERSION_DEBUG           0		//支持debug

#define FAT_FILE_NAME_LEN_MAX      26       //支持的文件名最大长度         


#define FAT_VERIFY_WORD    0xAA55

typedef unsigned long long ULL;

#define FAT_TYPE_FAT12    1
#define FAT_TYPE_FAT16    2
#define FAT_TYPE_FAT32    3

//#define TEST_VERSION

#ifndef NULL
  #define NULL  0
#endif

typedef struct
{
    unsigned char dirName[8];               //目录名
    unsigned char dirExt[3];                //目录扩展名
    unsigned char attributes;               //目录属性
    unsigned char reserved[2];              //保留
    unsigned char creationTime[2];          //创建时间
    unsigned char creationDate[2];          //创建日期
    unsigned char lastestAccessDate[2];     //最后访问日期
    unsigned char cluster1[2];
    unsigned char lastestModifyTime[2];     //最后修改时间
    unsigned char lastestModifyDate[2];     //最后写日期
    unsigned char cluster[2];               //起始簇
    unsigned char size[4];                  //文件大小
}T_FAT_DIR;
	 
typedef struct
{
    unsigned char attributes;               //属性字节位意义
    unsigned char longname1[10];            //长文件名unicode码1
    unsigned char flag;                     //长文件名目录项标志，取值0F
    unsigned char reserved;                 //系统保留
    unsigned char checkstatus;              //校验值（根据短文件名计算得出）
    unsigned char longname2[12];            //长文件名unicode码2
    unsigned char filestatrtclus[2];        //文件起始簇号（目前为0）
    unsigned char longname3[4];				//长文件名unicode码3
}T_FAT_LONG_DIR;	 


extern unsigned long  g_bytesPerClus; // 每簇的字节数
extern unsigned long  g_clusNum;      // 总的簇数（FAT表能管理的簇数）
extern unsigned long  g_fat1Addr, g_fat2Addr;  // FAT表的地址
extern unsigned long  g_rootDirAddr;  // 根目录地址
extern unsigned long  g_userDataAddr; // 用户数据区地址
extern unsigned short g_rootEntCnt;  // 根目录最大文件数
extern unsigned long  g_clusFreeNum;  // 空闲的簇数


extern unsigned long g_fatFileEndClusterValue;

#define FREE_CLUS_BUF_SIZE   32

extern unsigned long g_freeClusArray[FREE_CLUS_BUF_SIZE];
extern unsigned long g_freeClusFlag;
extern unsigned long g_freeClusNum;

extern unsigned long g_curDirFirstClus;   // 当前目录的首簇号
extern unsigned long g_curDirIndexClus;   // 检索当前目录使用的簇号索引
//extern unsigned long g_curDirIndexAddr;   // 检索当前目录使用的相对地址
extern unsigned long g_curDirPrevClus;    // 检索当前目录使用的前一个簇号（删除目录项时使用）

extern unsigned long g_curDirIndexAddr;   // 检索当前目录使用的相对地址

typedef struct
{
    unsigned long fileSize;        // 文件大小
    unsigned long spaceSize;       // 占用空间大小
    unsigned long firstClus;       // 文件存在的首簇号
    unsigned long clusIndex;       // 文件操作的当前簇
    unsigned long clusOffset;      // 文件操作的当前簇内偏移量
    unsigned long lenToEnd;        // 从当前位置到结束的长度
    unsigned long long dirAddr;         // 目录项所在的地址
    unsigned char openMode;        // 文件的打开模式（读或写）
}T_FILE_CTL;     // 文件操作控制块

#if   FAT_ALLOC_MEM_SUPPORT 
#define FILE_CTL_NUM 32

extern T_FILE_CTL * g_fileCtl[FILE_CTL_NUM];

#else
#define FILE_CTL_NUM 8

extern T_FILE_CTL g_fileCtl[FILE_CTL_NUM];
#endif

extern unsigned long g_fileCtlMask;

extern void fatPrint(char *fmt, ...);
extern void fatLoad(void);

typedef union
{
    //T_FAT32_BOOT  fat32Boot;
    T_FAT16_BOOT  fat16Boot;
}T_FAT_BOOT_UNIT;

extern unsigned char fatMemcmpCaseInsensitive(unsigned char * mem1, unsigned char * mem2, unsigned long len);

extern T_FAT_BOOT_UNIT g_fatBootUnit;

extern unsigned char g_fatSysType;

extern void FatCharToUpper(unsigned char * str, unsigned char * dst, unsigned long len);

extern void fatReadFatTable(unsigned long cluster, unsigned long * nextCluster);

extern void fatWriteFatTable(unsigned long cluster, unsigned long nextCluster);

extern unsigned long fatGetIdleCluster(unsigned long startCluster);

extern unsigned long long fatGetCurDirAddr(void);

extern unsigned long long fatDirectoryListNext(void);

#endif
