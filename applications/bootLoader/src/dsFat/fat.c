#include <string.h>
#include <stdarg.h>
#include "DsFat.h"
#include "fat16.h"
#include "fat.h"

#if FAT_VERSION_DEBUG
extern int vsprintf(char * /*s*/, const char * /*format*/, va_list /*arg*/);
#endif

#define FAT_LONG_DIR_NUM  ((FAT_FILE_NAME_LEN_MAX + 12)/13)
#define FAT_DIR_BUF_LEN   (FAT_LONG_DIR_NUM << 5)

#if !FAT_ALLOC_MEM_SUPPORT
    unsigned char g_fat32LongDirBuf[FAT_DIR_BUF_LEN];
    unsigned char g_fat32FileNameBuf[FAT_FILE_NAME_LEN_MAX + 1];
#endif

const unsigned long g_fatLongDirNum = FAT_LONG_DIR_NUM;
const unsigned long g_fatDirBufLen = FAT_DIR_BUF_LEN;

#define FAT16_FILE_LSNAME_ATTR		 0x0F


unsigned char g_fatSysType;

unsigned long  g_bytesPerClus; // 每簇的字节数
unsigned long  g_clusNum;      // 总的簇数（FAT表能管理的簇数）
unsigned long  g_fat1Addr, g_fat2Addr;  // FAT表的地址
unsigned long  g_rootDirAddr;  // 根目录地址
unsigned long  g_userDataAddr; // 用户数据区地址
unsigned short g_rootEntCnt;  // 根目录最大文件数
unsigned long  g_clusFreeNum;  // 空闲的簇数

unsigned long g_fatFileEndClusterValue;

unsigned long g_freeClusArray[FREE_CLUS_BUF_SIZE];
unsigned long g_freeClusFlag;
unsigned long g_freeClusNum;

unsigned long g_curDirFirstClus;   // 当前目录的首簇号
unsigned long g_curDirIndexClus;   // 检索当前目录使用的簇号索引

unsigned long g_curDirIndexAddr;   // 检索当前目录使用的相对地址

unsigned long g_curDirPrevClus;    // 检索当前目录使用的前一个簇号（删除目录项时使用）

T_FAT_BOOT_UNIT g_fatBootUnit;

#if FAT_ALLOC_MEM_SUPPORT	
	T_FILE_CTL * g_fileCtl[FILE_CTL_NUM];
#else
	
	T_FILE_CTL  g_fileCtl[FILE_CTL_NUM];  
#endif

unsigned long g_fileCtlMask = 0;

#if FAT_VERSION_DEBUG

char g_fatPrintStr[128];

void fatPrint(char *fmt, ...)
{
    va_list ap;
	
    va_start(ap, fmt);
    vsprintf((char *)g_fatPrintStr, fmt, ap);
	
    va_end(ap);

    fatDebugOut(g_fatPrintStr);
}

#else
void fatPrint(char *fmt, ...)
{
    return;
}

#endif

void FatCharToUpper(unsigned char * str, unsigned char * dst, unsigned long len)
{
    unsigned long i;

    //转为大写
    for (i = 0; i < len; i ++)
    {
	    if ((str[i] >= 'a') &&  (str[i] <= 'z'))
        {
            dst[i] = str[i] - 'a' + 'A';
        }
        else
        {
            dst[i] = str[i];
        }          
    }
}

unsigned char fatMemcmpCaseInsensitive(unsigned char * mem1, unsigned char * mem2, unsigned long len)
{
     unsigned long i;
     unsigned char ch[2];

     for (i = 0; i < len; i++)
     {
          ch[0] = (mem1[i] >= 'a' && mem1[i] <= 'z') ? mem1[i] - 'a' + 'A' : mem1[i];
          ch[1] = (mem2[i] >= 'a' && mem2[i] <= 'z') ? mem2[i] - 'a' + 'A' : mem2[i];

          if (ch[0] != ch[1])
          {
             return 1;
          }
     }
     
     return 0;
}

void fatLoad(void)
{
    unsigned short bytesPerSec, rsvdSecCnt, secNumPerFatTable;
    unsigned long totSec;
    unsigned char secPerClus;

    //每扇区字节数
    bytesPerSec = (unsigned short)g_fatBootUnit.fat16Boot.BPB_BytsPerSec[0] | ((unsigned short)g_fatBootUnit.fat16Boot.BPB_BytsPerSec[1] << 8); //每扇区字节数

    secPerClus = g_fatBootUnit.fat16Boot.BPB_SecPerClus;		 //每簇扇区数

    //保留扇区数
    rsvdSecCnt = (unsigned short)g_fatBootUnit.fat16Boot.BPB_RsvdSecCnt[0] | ((unsigned short)g_fatBootUnit.fat16Boot.BPB_RsvdSecCnt[1] << 8);

    //fat表扇区数
    secNumPerFatTable = (unsigned short)g_fatBootUnit.fat16Boot.BPB_FATSz16[0] | ((unsigned short)g_fatBootUnit.fat16Boot.BPB_FATSz16[1] << 8);	//每FAT表占用扇区数

	//fatPrint("#A %d,%d,%d,%d",bytesPerSec, secPerClus, rsvdSecCnt, secNumPerFatTable);
    if (secNumPerFatTable == 0)
    {
	    //fatPrint("#1 ");
        g_fatSysType = FAT_TYPE_FAT32;
        return;
    }

    //每簇字节数
    g_bytesPerClus = bytesPerSec*secPerClus;

    //总扇区数
    totSec = (unsigned short)g_fatBootUnit.fat16Boot.BPB_TotSec16[0] | ((unsigned short)g_fatBootUnit.fat16Boot.BPB_TotSec16[1] << 8);	 //总的扇区数
    
    if (totSec == 0)
    {
        totSec = (unsigned long)g_fatBootUnit.fat16Boot.BPB_TotSec32[0] 
              | ((unsigned long)g_fatBootUnit.fat16Boot.BPB_TotSec32[1] << 8)
              | ((unsigned long)g_fatBootUnit.fat16Boot.BPB_TotSec32[2] << 16)
              | ((unsigned long)g_fatBootUnit.fat16Boot.BPB_TotSec32[3] << 24);
        //fatPrint("\r\ntotalSec %d", totSec);
    }

        //根目录数
        g_rootEntCnt = (unsigned short)g_fatBootUnit.fat16Boot.BPB_RootEntCnt[0] | ((unsigned short)g_fatBootUnit.fat16Boot.BPB_RootEntCnt[1] << 8); //最大根目录文件数
    
        //可用簇数
        g_clusNum = (totSec - rsvdSecCnt - secNumPerFatTable*g_fatBootUnit.fat16Boot.BPB_NumFATs - g_rootEntCnt*32/bytesPerSec)/secPerClus;

        if (g_clusNum < 4085)
        {
            g_fatSysType = FAT_TYPE_FAT12;
            g_fatFileEndClusterValue = 0x0FF8;
            //memcpy(g_fat16Boot.BS_FileSysType ,"FAT12", 5);
        }
        else
        {
            g_fatSysType = FAT_TYPE_FAT16;
            g_fatFileEndClusterValue = 0xFFF8;
        }
    
    g_fat1Addr= rsvdSecCnt*bytesPerSec;	  //fat1表起始地址
	
    if (g_fatBootUnit.fat16Boot.BPB_NumFATs == 1)
    {
        g_fat2Addr = 0;
        g_rootDirAddr = g_fat1Addr + secNumPerFatTable*bytesPerSec;	   //根目录地址
    }
    else
    {
        g_fat2Addr = g_fat1Addr + secNumPerFatTable*bytesPerSec;	  //fat2表起始地址
        g_rootDirAddr = g_fat2Addr + secNumPerFatTable*bytesPerSec;	   //根目录地址
    }

    g_userDataAddr = g_rootDirAddr + g_rootEntCnt*32;

    g_curDirFirstClus = 0;       // 当前目录初始化为根目录

    g_curDirIndexClus = g_curDirFirstClus;
    g_curDirIndexAddr = 0;

    g_curDirPrevClus = 0;

}


FAT_BOOL fatInit(unsigned long long flashSize)
{
    unsigned long addr, i;
    unsigned short state;

    unsigned long long size;
    fatFlashRead(510, (unsigned char *)&state ,2);
    size = flashSize; //<< 10;
    if(state != FAT_VERIFY_WORD)
    {

        fat16Create(size);
    }
    else
    {
        fatFlashRead(0, (unsigned char *)&(g_fatBootUnit.fat16Boot) , sizeof(T_FAT16_BOOT));
    }
    fatLoad();
    g_clusFreeNum = 0;
    g_freeClusFlag = 0;

    g_freeClusNum = 0;

    if (g_fatSysType == FAT_TYPE_FAT16)
    {   
        addr = g_fat1Addr + 4;

        for (i = 0; i < g_clusNum; i++)
        {
            fatFlashRead(addr, (unsigned char *)&state, 2); 
            
            if (state == 0x0000)
            {
                g_clusFreeNum++;
            }
            
            addr+=2;
       }
    }
    else if (g_fatSysType == FAT_TYPE_FAT32)
    {
        return FAT_FALSE;
    } 
    else
    {
        addr = g_fat1Addr + 3;
        for (i = 0; i < g_clusNum; i++)
        {
            fatFlashRead(addr, (unsigned char *)&state, 2);

            if (i & 0x01)
            {
                state = state & 0xFFF0;
                addr += 2;
            }
            else
            {
                state = state & 0x0FFF;
                addr += 1;
            }

            if (state == 0x0000)
            {
                g_clusFreeNum++;
            }
        }
    }       
    g_fileCtlMask = 0;    
	return FAT_TRUE;  
}

unsigned long long fatGetCurDirAddr(void)
{
    unsigned long long addr;

    if (g_curDirFirstClus == 0)
    {
        addr = g_rootDirAddr + g_curDirIndexAddr;
    }
    else
    {
        addr = g_userDataAddr + (g_curDirFirstClus-2)*g_bytesPerClus + g_curDirIndexAddr;
    }
    
    return addr;
}

unsigned long long fatDirectoryListNext()
{
    unsigned long nextClus;

    g_curDirIndexAddr += 32;

    if (g_fatSysType != FAT_TYPE_FAT32)
    {
        if (g_curDirFirstClus == 0)   // 是根目录
        {
            //g_curDirIndexAddr += 32;
            if (g_curDirIndexAddr >= g_rootEntCnt*32)
            {
                 return 0;
            }
            
            return (ULL)(g_rootDirAddr + g_curDirIndexAddr);
        }
    }

    if (g_curDirIndexAddr >= g_bytesPerClus)
    {
        // 进入下一个簇
        fatReadFatTable(g_curDirIndexClus, &nextClus);
        //fatFlashRead(g_fat1Addr + g_curDirIndexClus*2, (unsigned char *)&nextClus, 2);

        if ((nextClus & g_fatFileEndClusterValue) == g_fatFileEndClusterValue)
        {
             return 0;
        }

        g_curDirPrevClus = g_curDirIndexClus;
        g_curDirIndexClus = nextClus;
        g_curDirIndexAddr = 0;
    }
    
    return (ULL)g_userDataAddr + ((ULL)g_curDirIndexClus-2)*(ULL)g_bytesPerClus + (ULL)g_curDirIndexAddr;
}

void fatReadFatTable(unsigned long cluster, unsigned long * nextCluster)
{
    unsigned long addr;
    unsigned long curCluster;

    curCluster = cluster;

    *nextCluster = 0;

    if (g_fatSysType == FAT_TYPE_FAT16)
    {
        addr = curCluster * 2;
        fatFlashRead(g_fat1Addr + addr, (unsigned char *)nextCluster, 2);
    }
    else
    {
        if (curCluster & 0x01)
        {
            addr = ((curCluster - 1) >> 1) * 3 + 1;
            fatFlashRead(g_fat1Addr + addr, (unsigned char *)nextCluster, 2);
            *nextCluster = ((*nextCluster) & 0xFFF0) >> 4;
        }
        else
        {
            addr = (curCluster >> 1) * 3 ;
            fatFlashRead(g_fat1Addr + addr, (unsigned char *)nextCluster, 2);
            *nextCluster = (*nextCluster) & 0x0FFF;
        }
    }
}

void fatGetDateFromDir(T_FAT_DIR * dir, unsigned short * year , unsigned char * mon, unsigned char * day)
{
    unsigned short lastestModifyDate;
    
    lastestModifyDate = (unsigned short)(dir->lastestModifyDate[0])
                    | (((unsigned short)(dir->lastestModifyDate[1])) << 8);


    *year = (lastestModifyDate >> 9) + 1980;
    *mon = (unsigned char)((lastestModifyDate & 0x01E0) >> 5);
    *day = (unsigned char)(lastestModifyDate & 0x001F);
}

void fatGetTimeFromDir(T_FAT_DIR * dir, unsigned char * hour , unsigned char * min, unsigned char * sec)
{
    unsigned short lastestModifyTime;
    
    lastestModifyTime = (unsigned short)(dir->lastestModifyTime[0])
                    | (((unsigned short)(dir->lastestModifyTime[1])) << 8);
                      
    *hour = (unsigned char)(lastestModifyTime >> 11);
    *min = (unsigned char)((lastestModifyTime & 0x07E0) >> 5);
    *sec = (unsigned char)((lastestModifyTime & 0x001F) << 1);
}

void fatGetNameFromDir(T_FAT_DIR * dir, char * name)
{
    int i, j;

    // 去除名字后的空格
    for (i = 8; i; i--)
    {
        if (dir->dirName[i-1] != ' ')
        {
            break;
        }
    }

    memcpy(name, dir->dirName, i);

    // 去除扩展名后的空格
    for (j = 3; j; j--)
    {
        if (dir->dirExt[j-1] != ' ')
        {
            break;
        }
    }

    if (j)
    {
        // 有扩展名    
        name[i] = '.';
    
        memcpy(name+i+1, dir->dirExt, j);
        
        name[i+1+j] = '\0';
    }
    else
    {
        name[i] = '\0';
    }
}

unsigned short fatGetNameFromDir26(T_FAT_LONG_DIR  * dir, unsigned char * name)
{
     unsigned short i, j, word;
         
     for(i=j=0; i<10; i+=2)
	 {
         word = *(unsigned short *)(dir->longname1 + i);

         if (word == 0x0000)
         {
             return j;
         }
         else if(word > 0x7F)
         {

             name[j] = (unsigned char)word;
             name[j + 1] = (unsigned char)(word >> 8);
             j += 2;
         }
         else
         {
             name[j] = (unsigned char)word;
             j += 1;
         }
	 }

	 for(i=0; i<12; i+=2)
	 {
         word = *(unsigned short *)(dir->longname2 + i);

         if (word == 0x0000)
         {
             return j;
         }
         else if(word > 0x7F)
         {
             name[j] = (unsigned char)word;
             name[j + 1] = (unsigned char)(word >> 8);
         }
         else
         {
             name[j] = (unsigned char)word;
             j += 1;
         }
	 }

	 for(i=0; i<4; i+=2)
	 {
         word = *(unsigned short *)(dir->longname3 + i);

         if (word == 0x0000)
         {
             return j;
         }
         else if(word > 0x7F)
         {
             name[j] = (unsigned char)word;
             name[j + 1] = (unsigned char)(word >> 8);
             j += 2;
         }
         else
         {
             name[j] = (unsigned char)word;
             j += 1;
         }

	 }

     return j;
}

FAT_BOOL fatGetDirectoryListFullInfo(T_FAT_DIR * dir, unsigned long longDirNum, FAT_LIST_INFO * listInfo)
{
    unsigned long long addr;
    unsigned long len, i, j;
    T_FAT_LONG_DIR  * longDir;
    unsigned char name[26];

    addr = fatGetCurDirAddr();

    if (longDirNum == 0)//没有长文件名或长文件名超过支持长度显示短文件
    {
        fatGetNameFromDir(dir, listInfo->name); 
        fatGetDateFromDir(dir, &(listInfo->year),  &(listInfo->mon), &(listInfo->day));
        fatGetTimeFromDir(dir, &(listInfo->hour), &(listInfo->min), &(listInfo->sec));
    
        listInfo->type = dir->attributes;
        
        listInfo->size = (unsigned long)dir->size[0]
                       |((unsigned long)dir->size[1] << 8)
                       |((unsigned long)dir->size[2] << 16)
                       |((unsigned long)dir->size[3] << 24);
        return FAT_TRUE;
	}
    else if(longDirNum > g_fatLongDirNum)
    {
        do
        {
            addr = fatDirectoryListNext();
            longDirNum--;
        }while(longDirNum);
        
        if (addr)
        {
            fatFlashRead(addr, (unsigned char *)dir, 32);
        }

        fatGetNameFromDir(dir, listInfo->name); 
        fatGetDateFromDir(dir, &(listInfo->year),  &(listInfo->mon), &(listInfo->day));
        fatGetTimeFromDir(dir, &(listInfo->hour), &(listInfo->min), &(listInfo->sec));
    
        listInfo->type = dir->attributes;
        
        listInfo->size = (unsigned long)dir->size[0]
                       |((unsigned long)dir->size[1] << 8)
                       |((unsigned long)dir->size[2] << 16)
                       |((unsigned long)dir->size[3] << 24);
        return FAT_TRUE;
    }
	else  
	{
         len = 0;
         
         longDir = (T_FAT_LONG_DIR *)dir;

         do
         {
            i = fatGetNameFromDir26(longDir, name);

            len += i;

            if (len >  listInfo->nameLen)
            {
                break;
            }

            for (j = len; j > 0; j--)
            {
                listInfo->name[j] = listInfo->name[j - i];
            }

            memcpy(listInfo->name, name, i); 
		    
            longDirNum--;

            addr = fatDirectoryListNext();

            if (addr)
            {
                fatFlashRead(addr, (unsigned char *)longDir, 32);
            }
                     
         }while(longDirNum);

         if (len > listInfo->nameLen)
         {
             fatGetNameFromDir(dir, listInfo->name); 
         }
         else
         {
             listInfo->name[len] = '\0';
         }

		 fatGetDateFromDir(dir, &(listInfo->year),  &(listInfo->mon), &(listInfo->day));
	     fatGetTimeFromDir(dir, &(listInfo->hour), &(listInfo->min), &(listInfo->sec));
	
	     listInfo->type = dir->attributes;
	    
	     listInfo->size = (unsigned long)dir->size[0]
	                    |((unsigned long)dir->size[1] << 8)
	                    |((unsigned long)dir->size[2] << 16)
	                    |((unsigned long)dir->size[3] << 24);
                        
		return  FAT_TRUE;
	} 
}

// 得到当前目录第一个目录项描述符
FAT_BOOL fatGetDirectoryListFirstDo(T_FAT_DIR * dir, unsigned long *longDirNum)
{
    unsigned long long addr;
    unsigned long nodir = 0;

    g_curDirIndexClus = g_curDirFirstClus;
    g_curDirIndexAddr = 0;
    g_curDirPrevClus = 0;

    addr = fatGetCurDirAddr();

   
    *longDirNum = 0;

    do
    {
		//shellPrint("dsfatlist first addr = 0x%x\r\n", addr);
		
        fatFlashRead(addr, (unsigned char *)dir, 32);
	
		if((dir->dirName[0]&0x40)
         &&(dir->attributes==0x0F)
         &&(dir->dirName[0]!=0xE5))
		{	 
		     *longDirNum = dir->dirName[0]&0x1F;
       
             return FAT_TRUE;
		}

        if (((dir->attributes & 0xC0) == 0)  // 最高两个BIT不应为1
             &&((dir->attributes & (FAT_LIST_TYPE_DOC | FAT_LIST_TYPE_DIR)) != 0)
             &&(dir->dirName[0] != 0xE5)
			 &&(dir->dirName[0] != 0))
        {
            // 读目录项成功
            return FAT_TRUE;
        }

		if (dir->dirName[0] == 0)
		{
            nodir ++;
		}
		else
		{
		    nodir = 0;
		}

		if (nodir > 20)
		{
		    break; 
		}

        addr = fatDirectoryListNext();
    }while (addr);

    // 读目录项失败
    return FAT_FALSE;
}

// 得到当前目录下一个目录项描述符
FAT_BOOL fatGetDirectoryListNextDo(T_FAT_DIR * dir, unsigned long * longDirNum)
{
    unsigned long addr, nodir;

    *longDirNum = 0;

    nodir = 0;

    addr = fatDirectoryListNext();

    while (addr)
    {	   	 
        fatFlashRead(addr, (unsigned char *)dir, 32);

		if((dir->dirName[0]&0x40)
          &&dir->attributes == 0x0F
          &&dir->dirName[0] != 0xE5)
		{	
			 *longDirNum = dir->dirName[0]&0x1F;
		     return FAT_TRUE;
		}

        if (((dir->attributes & 0xC0) == 0)  // 最高两个BIT不应为1
             &&((dir->attributes & (FAT_LIST_TYPE_DOC | FAT_LIST_TYPE_DIR)) != 0)
             &&(dir->dirName[0] != 0xE5)
			 &&(dir->dirName[0] != 0))
        {
            // 读目录项成功
            return FAT_TRUE;
        }

		if (dir->dirName[0] == 0)
		{
            nodir ++;
		}
		else
		{
		    nodir = 0;
		}

		if (nodir > 20)
		{
		    break; 
		}

        addr = fatDirectoryListNext();
    }

    // 读目录项失败
    return FAT_FALSE;
}

// 设置当前目录项
void fatSetDirectoryListDo(T_FAT_DIR * dir)
{
    unsigned long long addr;
    
    if (g_fatSysType != FAT_TYPE_FAT32)
    {
        if (g_curDirIndexClus == 0)
        {
            // 是根目录
            addr = g_rootDirAddr + g_curDirIndexAddr;
            fatFlashWrite(addr, (unsigned char *)dir, 32);
            return;
        }
    }

    // 是普通目录
    addr = (ULL)g_userDataAddr + ((ULL)g_curDirIndexClus-2)*(ULL)g_bytesPerClus + (ULL)g_curDirIndexAddr;
    
    fatFlashWrite(addr, (unsigned char *)dir, 32);
}


// 得到当前目录的第一个项目（子目录或文件）信息
// 返回FALSE：为空目录，没有任何项目
// 返回TRUE：成功获取第1个项目。
FAT_BOOL fatGetDirectoryListFirst(FAT_LIST_INFO * listInfo)
{
    T_FAT_DIR dir;

    unsigned long longDirNum;

    if (listInfo == NULL || listInfo->name == NULL)
    {
        return FAT_FALSE;
    }

    if (listInfo->nameLen < MIN_FILE_NAME_LEN)
    {
        return FAT_FALSE;
    }

	if (fatGetDirectoryListFirstDo(&dir, &longDirNum) == FAT_FALSE)
    {
        return FAT_FALSE;
    }

    return fatGetDirectoryListFullInfo(&dir, longDirNum, listInfo);
}

// 得到当前目录的下一个项目（子目录或文件）信息
// 返回FALSE：为空目录，没有任何项目
// 返回TRUE：成功获取第1个项目。
FAT_BOOL fatGetDirectoryListNext(FAT_LIST_INFO * listInfo)
{
    T_FAT_DIR dir;

	unsigned long longDirNum;//, addr;
    
    if (listInfo == NULL || listInfo->name == NULL)
    {
        return FAT_FALSE;
    }

    if (listInfo->nameLen < MIN_FILE_NAME_LEN)
    {
        return FAT_FALSE;
    }
        	
	if (fatGetDirectoryListNextDo(&dir, &longDirNum) == FAT_FALSE)
    {
       return FAT_FALSE;
    }

    return fatGetDirectoryListFullInfo(&dir, longDirNum, listInfo);
}

// 从起始簇得到空间的大小
unsigned long fatGetSpaceByCluster(unsigned long cluster)
{
    unsigned short clusterNum;

    if (cluster == 0)
    {
        return 0;
    }

    clusterNum = 1;

    fatReadFatTable(cluster, &cluster);

    while ((cluster != 0) && ((cluster & g_fatFileEndClusterValue) != g_fatFileEndClusterValue))
    {
         clusterNum++;

         //fatFlashRead(g_fat1Addr + cluster*2, (unsigned char *)&cluster, 2);
         fatReadFatTable(cluster, &cluster);
    }
    
    return (clusterNum*g_bytesPerClus);
}


FAT_FILE_HANDLE fatFileOpenDo(T_FAT_DIR * dir, unsigned char openMode)
{
    unsigned long i, fileSize; 
    unsigned long firstCluster; 

    fileSize = (unsigned long)dir->size[0]
             |((unsigned long)dir->size[1] << 8)
             |((unsigned long)dir->size[2] << 16)
             |((unsigned long)dir->size[3] << 24);
       

    firstCluster = ((unsigned short)dir->cluster[0])
                  |((unsigned short)dir->cluster[1] <<  8);

    // 检查一下该文件有没有已经打开
    for (i = 0; i < FILE_CTL_NUM; i++)
    {
        if (g_fileCtlMask & (1UL << i))
        {
#if   FAT_ALLOC_MEM_SUPPORT
            if (g_fileCtl[i]->firstClus == firstCluster)
            {
                if ((g_fileCtl[i]->openMode | openMode) & FAT_FILE_OPEN_WRITE)
                {
                    return FAT_FILE_HANDLE_NULL;
                }
            }
#else
            if (g_fileCtl[i].firstClus == firstCluster)
            {
                if ((g_fileCtl[i].openMode | openMode) & FAT_FILE_OPEN_WRITE)
                {
                    return FAT_FILE_HANDLE_NULL;
                }
            }
#endif
        }
    }

    // 申请一个句柄，将文件存入。
    for (i = 0; i < FILE_CTL_NUM; i++)
    {
        if ((g_fileCtlMask & (1UL << i)) == 0)
        {
#if   FAT_ALLOC_MEM_SUPPORT
 	        g_fileCtl[i] = (T_FILE_CTL *)fatAllocMem(sizeof(T_FILE_CTL));

			if (g_fileCtl[i] == 0)
			{
			    return FAT_FILE_HANDLE_NULL;
			}

            g_fileCtl[i]->fileSize = fileSize;
            g_fileCtl[i]->spaceSize = fatGetSpaceByCluster(firstCluster);
            g_fileCtl[i]->lenToEnd = fileSize;
            g_fileCtl[i]->firstClus = firstCluster;
            g_fileCtl[i]->clusIndex = firstCluster;
            g_fileCtl[i]->clusOffset = 0;
       
//            g_fileCtl[i]->dirAddr = g_userDataAddr + (g_curDirIndexClus-2)*g_bytesPerClus + g_curDirIndexAddr;
            g_fileCtl[i]->dirAddr = fatGetCurDirAddr();

            g_fileCtl[i]->openMode = openMode;
#else
            g_fileCtl[i].fileSize = fileSize;
            g_fileCtl[i].spaceSize = fatGetSpaceByCluster(firstCluster);
            g_fileCtl[i].lenToEnd = fileSize;
            g_fileCtl[i].firstClus = firstCluster;
            g_fileCtl[i].clusIndex = firstCluster;
            g_fileCtl[i].clusOffset = 0;
       
//            g_fileCtl[i].dirAddr = g_userDataAddr + (g_curDirIndexClus-2)*g_bytesPerClus + g_curDirIndexAddr;
            g_fileCtl[i].dirAddr = fatGetCurDirAddr();
            g_fileCtl[i].openMode = openMode;
#endif

            g_fileCtlMask |= (1UL << i);

            return i;
        }
    }

    return FAT_FILE_HANDLE_NULL;
}

// 在当前目录中打开指定的文件
// 参数1：fileName 需要打开的文件名
// 参数2：size 用来返回打开文件的实际大小
// 返回打开文件的句柄
// 返回FAT_FILE_HANDLE_NULL：打开文件失败。 
FAT_FILE_HANDLE fatFileOpen(char * fileName, unsigned char openMode)
{
    FAT_LIST_INFO  listInfo;
    T_FAT_DIR dir;
    unsigned char i, dirLen;
    unsigned long longDirNum;

    dirLen = strlen(fileName);

	if(dirLen > FAT_FILE_NAME_LEN_MAX || dirLen == 0)
	{
	    return FAT_FILE_HANDLE_NULL; 
	}
    // 判断一下文件名的合法性，同时做一下大小写转换
    for (i = 0; i < dirLen; i++)
    {
        if ((fileName[i] == '\\')
         || (fileName[i] == '/')
         || (fileName[i] == ':') 
         || (fileName[i] == '*')
         || (fileName[i] == '?')
         || (fileName[i] == '<')
         || (fileName[i] == '>') 
         || (fileName[i] == '|')) 

        {
            return FAT_FILE_HANDLE_NULL;
        }

    }
    
#if !FAT_ALLOC_MEM_SUPPORT
    listInfo.name = (char *)g_fat32FileNameBuf;
    listInfo.nameLen = FAT_FILE_NAME_LEN_MAX;
#else
    listInfo.name = (char *)fatAllocMem(FAT_FILE_NAME_LEN_MAX + 1);
    listInfo.nameLen = FAT_FILE_NAME_LEN_MAX;
#endif
    
    if (listInfo.name == NULL)
    {
       return FAT_FILE_HANDLE_NULL;
    }
    		
	if(fatGetDirectoryListFirstDo(&dir, &longDirNum) == FAT_FALSE)
	{
#if FAT_ALLOC_MEM_SUPPORT
       fatFreeMem((unsigned char *)listInfo.name);
#endif
	   return FAT_FILE_HANDLE_NULL;
	}
    
    do
    {
        if (fatGetDirectoryListFullInfo(&dir, longDirNum, &listInfo) == FAT_FALSE)
        {
           fatPrint("\r\nfat32Open get name fail..");
           continue;
        }

    	if (listInfo.type & FAT_LIST_TYPE_DOC)
    	{	
            if((dirLen == strlen(listInfo.name)) 
             &&(fatMemcmpCaseInsensitive((unsigned char *)listInfo.name, (unsigned char *)fileName, dirLen) == 0))
            {
                longDirNum = fatFileOpenDo(&dir, openMode);
#if FAT_ALLOC_MEM_SUPPORT
                fatFreeMem((unsigned char *)listInfo.name);
#endif
                return longDirNum;
            }
    	}
    }while(fatGetDirectoryListNextDo(&dir, &longDirNum) == FAT_TRUE);


#if FAT_ALLOC_MEM_SUPPORT
    fatFreeMem((unsigned char *)listInfo.name);
#endif
    return FAT_FILE_HANDLE_NULL;
}

// 关闭文件。 
FAT_BOOL fatFileClose(FAT_FILE_HANDLE handle)
{
    if (handle >= FILE_CTL_NUM)
    {
        return FAT_FALSE;
    }

    if ((g_fileCtlMask & (1UL << handle)) == 0)
    {
        return FAT_FALSE;
    }

#if   FAT_ALLOC_MEM_SUPPORT
    fatFreeMem((unsigned char *)g_fileCtl[handle]);
#endif
    g_fileCtlMask &= (~(1UL << handle));
    
    return FAT_TRUE;
}

// 从文件的当前位置读指定长度的内容
// 返回实际读到的内容长度（当文件当前位置到结尾的长度小于buffer空间时）
unsigned long fatFileRead(FAT_FILE_HANDLE handle, unsigned char * buffer, unsigned long size)
{
    unsigned long readLen,toClusEndSize, len;
	T_FILE_CTL * fileCtl;

    if (handle >= FILE_CTL_NUM)
    {
        return 0;
    }

    if ((g_fileCtlMask & (1UL << handle)) == 0)
    {
        return 0;
    }

#if FAT_ALLOC_MEM_SUPPORT
    fileCtl = g_fileCtl[handle];     
#else
    fileCtl = &g_fileCtl[handle];
#endif

    len = 0;

    while (size && fileCtl->lenToEnd)
    {
        if (fileCtl->clusOffset >= g_bytesPerClus)
        {
            // 当前簇往后移一簇
            fatReadFatTable(fileCtl->clusIndex, &fileCtl->clusIndex);
            //fatFlashRead((ULL)g_fat1Addr + (ULL)fileCtl->clusIndex*4, (unsigned char *)&(fileCtl->clusIndex), 4);

            fileCtl->clusOffset = 0; 
        }

        toClusEndSize = g_bytesPerClus - fileCtl->clusOffset;
    
        readLen = size < toClusEndSize ? 
                        (size < fileCtl->lenToEnd ? size : fileCtl->lenToEnd)
                       :(toClusEndSize < fileCtl->lenToEnd ? toClusEndSize : fileCtl->lenToEnd);

        fatFlashRead((ULL)g_userDataAddr + ((ULL)fileCtl->clusIndex -2)*(ULL)g_bytesPerClus + (ULL)fileCtl->clusOffset, 
                     buffer, readLen);

        len+= readLen;

        buffer+=readLen;
        size-=readLen;

        fileCtl->clusOffset+=readLen;
        fileCtl->lenToEnd-=readLen;
    }

    return len;
}


// 得到文件的长度
unsigned long fatFileGetLen(FAT_FILE_HANDLE handle)
{
	T_FILE_CTL * fileCtl;
    if (handle >= FILE_CTL_NUM)
    {
        return 0;
    }

    if ((g_fileCtlMask & (1UL << handle)) == 0)
    {
        return 0;
    }

#if FAT_ALLOC_MEM_SUPPORT
    fileCtl = g_fileCtl[handle];     
#else
    fileCtl = &g_fileCtl[handle];
#endif

    return fileCtl->fileSize;
}


