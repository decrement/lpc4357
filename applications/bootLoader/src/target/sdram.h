
#ifndef LPC18XX_SDRAM_H
#define LPC18XX_SDRAM_H

/* SDRAM Address Base for DYCS0*/
#define SDRAM_ADDR_BASE		0x28000000

#define SDRAM_SIZE          (64ul * 1024ul * 1024ul)

extern void emcInit(void);

extern void sdramInit(void);

#endif

