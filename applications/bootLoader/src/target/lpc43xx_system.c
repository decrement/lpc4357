#include "LPC43xx.h"


/*----------------------------------------------------------------------------
  Define clocks
 *----------------------------------------------------------------------------*/
#define __IRC            (12000000UL)    /* IRC Oscillator frequency          */

/*----------------------------------------------------------------------------
  Clock Variable definitions
 *----------------------------------------------------------------------------*/
uint32_t SystemCoreClock = __IRC;		/*!< System Clock Frequency (Core Clock)*/


/**
 * Initialize the system
 *
 * @param  none
 * @return none
 *
 * @brief  Setup the microcontroller system.
 *         Initialize the System.
 */
void SystemInit(void)
{
	LPC_CGU->XTAL_OSC_CTRL &= ~(1<<2);		  /* 1M -- 20M crystal */
    LPC_CGU->XTAL_OSC_CTRL &= ~(1UL << 0);	  /* Enable crystal */

	/** CGU control clock-source mask bit */
	LPC_CGU->PLL1_CTRL &= ~(0xF<<24);
	LPC_CGU->PLL1_CTRL |= 0x06 <<24;		 /* Clock source: crystal oscillator */

    /** CGU control auto block mask bit */
	LPC_CGU->PLL1_CTRL |= (1<<11);

	/* 
	 * BYPASS = 0, CCO clock send to post-divider 
	 * FBSEL = 1,  PLL output is used as feedback divider 
	 * DIRECT = 0, PLL direct CCO output disable
	 * During this integer mode, the Formula:
	 *       Fout = M * (Fin / N)
	 *       Fcco = Fout * 2          (156M < Fcco < 320M)
	 */
	LPC_CGU->PLL1_CTRL &= ~(  (1 << 6) 
							| (1 << 1) 
							| (1 << 7) 
							| (0x03<<8) 
							| (0xFF<<16) 
							| (0x03<<12));

	/*                      MSEL      NSEL      PSEL            */
    LPC_CGU->PLL1_CTRL |= (0xE<<16) | (0<<12) | (1<<8) | (1 << 6) | (1 << 7);

	LPC_CGU->PLL1_CTRL &= ~(0x01);
    while((LPC_CGU->PLL1_STAT&1) == 0x0);

	LPC_CGU->BASE_M4_CLK = ((0x0009 << 24) | (0x0001 << 11));

	SystemCoreClock = 180000000;	  // 120M 
	
	SysTick_Config(SystemCoreClock / 100); 
}

uint32_t GetCoreClock (void)
{
	return SystemCoreClock;
}


