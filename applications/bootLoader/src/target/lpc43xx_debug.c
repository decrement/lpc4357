

#include <stdarg.h>
#include <stdio.h>
#include "lpc43xx.h"
#include "lpc43xx_debug.h"
#include "lpc43xx_scu.h"

static uint8_t gBuffer[512];

// 根据外设频率和波特率得到寄存器设置需要的除数
static uint16_t uart0GetDivisorByBaudrate(uint32_t clk, uint32_t baudrate)
{
    float divisor;
    uint16_t divisorInt;
    
    divisor = (float)clk/(baudrate*16);
    
    divisorInt = (uint16_t)(divisor/256);
    
    divisor-=(divisorInt*256);
    
    if ((divisor - (uint32_t)divisor) > 0.5F)
    {
        divisorInt = (divisorInt << 8) | ((uint8_t)divisor + 1);
    }
    else
    {
        divisorInt = (divisorInt << 8) | (uint8_t)divisor;
    }
    
    return divisorInt;  // 高八位为DLM 低八位为DLL
}
  
// 入参clk：UART0模块工作的系统频率
// 入参baudrate：UART0工作的波特率
static void UART0_init(unsigned long sysClk, unsigned long baudrate)
{
    uint16_t divisor;
	uint32_t tempReg;

	tempReg = LPC_CGU->BASE_UART0_CLK;
    tempReg &=	~(0xF<<24);
	tempReg |= ((0x0009 << 24) | (0x0001 << 11));
	LPC_CGU->BASE_UART3_CLK = tempReg;

// 	scu_pinmux(0xF ,10 , MD_PDN, FUNC1); 	// PF.10 : UART0_TXD
// 	scu_pinmux(0xF ,11 , MD_PLN|MD_EZI, FUNC1); 	// PF.11 : UART0_RXD
	scu_pinmux(0x2 ,3 , MD_PDN, FUNC2); 	// PF.10 : UART0_TXD
	scu_pinmux(0x2 ,4 , MD_PLN|MD_EZI, FUNC2); 	// PF.11 : UART0_RXD
 
    LPC_USART3->LCR  = 0x83;                      /* 允许设置波特率               */

    // 外设频率60M. 256*DLM+DLL = 3750000/baudrate  
    // baudrate设为9600. 则DLM = 1 DLL = 135

    divisor = uart0GetDivisorByBaudrate(sysClk, baudrate);

    LPC_USART3->DLM  = ((divisor >> 8) & 0x00FF);
    LPC_USART3->DLL  = (divisor & 0x00FF);
    LPC_USART3->LCR  = 0x03;                      /* 锁定波特率                   */
    LPC_USART3->FCR  = 0x87;                      // RX FIFO在收到8个字节时产生中断。 		   
}

void UART0_sendChar (uint8_t data)
{
	while (!(LPC_USART3->LSR & 0x020));
	LPC_USART3->THR = data;
}

void debugComInit(void)
{
	UART0_init(180000000, 115200);
}

void shellPrint(const  char *format, ...)
{
	va_list  vArgs;
	uint8_t i = 0;

	va_start(vArgs, format);
    vsprintf((char *)gBuffer, (char const *)format, vArgs);
    va_end(vArgs);

	while (gBuffer[i] != 0)
	{
		UART0_sendChar(gBuffer[i]);
		i++;
	}

//	UART0_snd(gBuffer, i);
}

void uartPutChar(uint8_t data)
{
	UART0_sendChar(data);
}


