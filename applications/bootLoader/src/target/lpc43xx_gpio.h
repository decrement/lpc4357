
#ifndef LPC18XX_GPIO_H_
#define LPC18XX_GPIO_H_

/* Includes ------------------------------------------------------------------- */
//#include "LPC18xx.h"
//#include "lpc_types.h"

/* GPIO style ------------------------------- */
void GPIO_SetDir(uint8_t portNum, uint32_t bitValue, uint8_t dir);
void GPIO_SetValue(uint8_t portNum, uint32_t bitValue);
void GPIO_ClearValue(uint8_t portNum, uint32_t bitValue);
uint32_t GPIO_ReadValue(uint8_t portNum);


#endif /* LPC18XX_GPIO_H_ */


