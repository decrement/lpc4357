
#include "LPC43xx.h"
#include "lpc43xx_gpio.h"


/*********************************************************************//**
 * @brief		Set Direction for GPIO port.
 * @param[in]	portNum	Port Number value, should be in range from 0 to 4
 * @param[in]	bitValue	Value that contains all bits to set direction,
 * 				in range from 0 to 0xFFFFFFFF.
 * 				example: value 0x5 to set direction for bit 0 and bit 1.
 * @param[in]	dir	Direction value, should be:
 * 					- 0: Input.
 * 					- 1: Output.
 * @return		None
 *
 * Note:
 * All remaining bits that are not activated in bitValue (value '0')
 * will not be effected by this function.
 **********************************************************************/
void GPIO_SetDir(uint8_t portNum, uint32_t bitValue, uint8_t dir)
{
		if (dir)
		{
		LPC_GPIO_PORT->DIR[portNum] |= bitValue;
	} else
		{
		LPC_GPIO_PORT->DIR[portNum] &= ~bitValue;
	}
}


/*********************************************************************//**
 * @brief		Set Value for bits that have output direction on GPIO port.
 * @param[in]	portNum	Port number value, should be in range from 0 to 4
 * @param[in]	bitValue Value that contains all bits on GPIO to set, should
 * 				be in range from 0 to 0xFFFFFFFF.
 * 				example: value 0x5 to set bit 0 and bit 1.
 * @return		None
 *
 * Note:
 * - For all bits that has been set as input direction, this function will
 * not effect.
 * - For all remaining bits that are not activated in bitValue (value '0')
 * will not be effected by this function.
 **********************************************************************/
void GPIO_SetValue(uint8_t portNum, uint32_t bitValue)
{
	LPC_GPIO_PORT->SET[portNum] = bitValue;
}


/*********************************************************************//**
 * @brief		Clear Value for bits that have output direction on GPIO port.
 * @param[in]	portNum	Port number value, should be in range from 0 to 4
 * @param[in]	bitValue Value that contains all bits on GPIO to clear, should
 * 				be in range from 0 to 0xFFFFFFFF.
 * 				example: value 0x5 to clear bit 0 and bit 1.
 * @return		None
 *
 * Note:
 * - For all bits that has been set as input direction, this function will
 * not effect.
 * - For all remaining bits that are not activated in bitValue (value '0')
 * will not be effected by this function.
 **********************************************************************/
void GPIO_ClearValue(uint8_t portNum, uint32_t bitValue)
{
	LPC_GPIO_PORT->CLR[portNum] = bitValue;
}


/*********************************************************************//**
 * @brief		Read Current state on port pin that have input direction of GPIO
 * @param[in]	portNum	Port number to read value, in range from 0 to 4
 * @return		Current value of GPIO port.
 *
 * Note: Return value contain state of each port pin (bit) on that GPIO regardless
 * its direction is input or output.
 **********************************************************************/
uint32_t GPIO_ReadValue(uint8_t portNum)
{
	return LPC_GPIO_PORT->PIN[portNum];
}

