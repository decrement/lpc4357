
#include "lpc43xx.h"
#include "sdram.h"
#include "lpc43xx_scu.h"


#define MHZ           		*1000000ul

#define SYS_FREQ  			90MHZ

#if   SYS_FREQ == (120MHZ)
#define SDRAM_PERIOD          8.33  // 120MHz
#elif     SYS_FREQ == (96MHZ)
#define SDRAM_PERIOD          10.4  // 96MHz
#elif     SYS_FREQ == (90MHZ)
#define SDRAM_PERIOD          11.1  // 90MHz
#elif   SYS_FREQ == (72MHZ)
#define SDRAM_PERIOD          13.8  // 72MHz
#elif   SYS_FREQ == (60MHZ)
#define SDRAM_PERIOD          16.67  // 72MHz
#elif   SYS_FREQ == (57MHZ)
#define SDRAM_PERIOD          17.4  // 57.6MHz
#elif SYS_FREQ == (48MHZ)
#define SDRAM_PERIOD          20.8  // 48MHz
#elif SYS_FREQ == (36MHZ)
#define SDRAM_PERIOD          27.8  // 36MHz
#elif SYS_FREQ == (24MHZ)
#define SDRAM_PERIOD          41.7  // 24MHz
#elif SYS_FREQ == (12MHZ)
#define SDRAM_PERIOD          83.3  // 12MHz
#else
#error Frequency not defined
#endif

#define P2C(Period)           (((Period<SDRAM_PERIOD) ? 1 : (uint32_t)((float)Period/SDRAM_PERIOD))+1)

#define SDRAM_REFRESH         7813	   // 7813 ns
#define SDRAM_TRP             20	   // 20 ns
#define SDRAM_TRAS            45	   // 45 ns
#define SDRAM_TAPR            1		   // 2 cclk
#define SDRAM_TDAL            3		   // 3 cclk
#define SDRAM_TWR             1		   // 2 cclk
#define SDRAM_TRC             65	   // 65 ns
#define SDRAM_TRFC            65	   // 65 ns
#define SDRAM_TXSR            75	   // 75 ns
#define SDRAM_TRRD            1		   // 2 cclk
#define SDRAM_TMRD            1		   // 2 cclk


void emcInit(void)
{
	/* Shared signals : EXTBUS_A[23:0], EXTBUS_D[31:0], EXTBUS_WE*/
	scu_pinmux(	2	,	9	,	MD_PLN_FAST	,	3	);    // EXTBUS_A0
	scu_pinmux(	2	,	10	,	MD_PLN_FAST	,	3	);    // EXTBUS_A1
	scu_pinmux(	2	,	11	,	MD_PLN_FAST	,	3	);    // EXTBUS_A2
	scu_pinmux(	2	,	12	,	MD_PLN_FAST	,	3	);    // EXTBUS_A3
	scu_pinmux(	2	,	13	,	MD_PLN_FAST	,	3	);    // EXTBUS_A4
	scu_pinmux(	1	,	0	,	MD_PLN_FAST	,	2	);	  // EXTBUS_A5
	scu_pinmux(	1	,	1	,	MD_PLN_FAST	,	2	);	  // EXTBUS_A6
	scu_pinmux(	1	,	2	,	MD_PLN_FAST	,	2	);	  // EXTBUS_A7
	scu_pinmux(	2	,	8	,	MD_PLN_FAST	,	3	);    // EXTBUS_A8
	scu_pinmux(	2	,	7	,	MD_PLN_FAST	,	3	);    // EXTBUS_A9
	scu_pinmux(	2	,	6	,	MD_PLN_FAST	,	2	);    // EXTBUS_A10
	scu_pinmux(	2	,	2	,	MD_PLN_FAST	,	2	);    // EXTBUS_A11
	scu_pinmux(	2	,	1	,	MD_PLN_FAST	,	2	);    // EXTBUS_A12
	scu_pinmux(	2	,	0	,	MD_PLN_FAST	,	2	);    // EXTBUS_A13
	scu_pinmux(	6	,	8	,	MD_PLN_FAST	,	1	);    // EXTBUS_A14
#if 1
	scu_pinmux(	6	,	7	,	MD_PLN_FAST	,	1	);    // EXTBUS_A15
	scu_pinmux(	0xD	,	16	,	MD_PLN_FAST	,	2	);    // EXTBUS_A16
	scu_pinmux(	0xD	,	15	,	MD_PLN_FAST	,	2	);    // EXTBUS_A17
	scu_pinmux(	0xE	,	0	,	MD_PLN_FAST	,	3	);    // EXTBUS_A18
	scu_pinmux(	0xE	,	1	,	MD_PLN_FAST	,	3	);    // EXTBUS_A19
	scu_pinmux(	0xE	,	2	,	MD_PLN_FAST	,	3	);    // EXTBUS_A20
	scu_pinmux(	0xE	,	3	,	MD_PLN_FAST	,	3	);    // EXTBUS_A21
	scu_pinmux(	0xE	,	4	,	MD_PLN_FAST	,	3	);    // EXTBUS_A22
	scu_pinmux(	0xA	,	4	,	MD_PLN_FAST	,	3	);    // EXTBUS_A23
#endif

	scu_pinmux(	1	,	7	,	MD_PLN_FAST	,	3	);    // EXTBUS_D0
	scu_pinmux(	1	,	8	,	MD_PLN_FAST	,	3	);    // EXTBUS_D1
	scu_pinmux(	1	,	9	,	MD_PLN_FAST	,	3	);    // EXTBUS_D2
	scu_pinmux(	1	,	10	,	MD_PLN_FAST	,	3	);    // EXTBUS_D3
	scu_pinmux(	1	,	11	,	MD_PLN_FAST	,	3	);    // EXTBUS_D4
	scu_pinmux(	1	,	12	,	MD_PLN_FAST	,	3	);    // EXTBUS_D5
	scu_pinmux(	1	,	13	,	MD_PLN_FAST	,	3	);    // EXTBUS_D6
	scu_pinmux(	1	,	14	,	MD_PLN_FAST	,	3	);    // EXTBUS_D7
	scu_pinmux(	5	,	4	,	MD_PLN_FAST	,	2	);    // EXTBUS_D8
	scu_pinmux(	5	,	5	,	MD_PLN_FAST	,	2	);    // EXTBUS_D9
	scu_pinmux(	5	,	6	,	MD_PLN_FAST	,	2	);    // EXTBUS_D10
	scu_pinmux(	5	,	7	,	MD_PLN_FAST	,	2	);    // EXTBUS_D11
	scu_pinmux(	5	,	0	,	MD_PLN_FAST	,	2	);    // EXTBUS_D12
	scu_pinmux(	5	,	1	,	MD_PLN_FAST	,	2	);    // EXTBUS_D13
	scu_pinmux(	5	,	2	,	MD_PLN_FAST	,	2	);    // EXTBUS_D14
	scu_pinmux(	5	,	3	,	MD_PLN_FAST	,	2	);    // EXTBUS_D15
#if 1
	scu_pinmux(	0xD	,	2	,	MD_PLN_FAST	,	2	);    // EXTBUS_D16
	scu_pinmux(	0xD	,	3	,	MD_PLN_FAST	,	2	);    // EXTBUS_D17
	scu_pinmux(	0xD	,	4	,	MD_PLN_FAST	,	2	);    // EXTBUS_D18
	scu_pinmux(	0xD	,	5	,	MD_PLN_FAST	,	2	);    // EXTBUS_D19
	scu_pinmux(	0xD	,	6	,	MD_PLN_FAST	,	2	);    // EXTBUS_D20
	scu_pinmux(	0xD	,	7	,	MD_PLN_FAST	,	2	);    // EXTBUS_D21
	scu_pinmux(	0xD	,	8	,	MD_PLN_FAST	,	2	);    // EXTBUS_D22
	scu_pinmux(	0xD	,	9	,	MD_PLN_FAST	,	2	);    // EXTBUS_D23
	scu_pinmux(	0xE	,	5	,	MD_PLN_FAST	,	3	);    // EXTBUS_D24
	scu_pinmux(	0xE	,	6	,	MD_PLN_FAST	,	3	);    // EXTBUS_D25
	scu_pinmux(	0xE	,	7	,	MD_PLN_FAST	,	3	);    // EXTBUS_D26
	scu_pinmux(	0xE	,	8	,	MD_PLN_FAST	,	3	);    // EXTBUS_D27
	scu_pinmux(	0xE	,	9	,	MD_PLN_FAST	,	3	);    // EXTBUS_D28
	scu_pinmux(	0xE	,	10	,	MD_PLN_FAST	,	3	);    // EXTBUS_D29
	scu_pinmux(	0xE	,	11	,	MD_PLN_FAST	,	3	);    // EXTBUS_D30
	scu_pinmux(	0xE	,	12	,	MD_PLN_FAST	,	3	);    // EXTBUS_D31
#endif
	
	scu_pinmux(	1	,	6	,	MD_PLN_FAST	,	3	);	  // EXTBUS_WE	

	/* Static memory signals : EXTBUS_OE, EXTBUS_BLS[3:0], EXTBUS_CS[3:0] */
	scu_pinmux(	1	,	3	,	MD_PLN_FAST	,	3	);	  // EXTBUS_OE
	scu_pinmux(	1	,	4	,	MD_PLN_FAST	,	3	);	  // EXTBUS_BLS0
	scu_pinmux(	6	,	6	,	MD_PLN_FAST	,	1	);	  // EXTBUS_BLS1
//	scu_pinmux(	0xD	,	13	,	MD_PLN_FAST	,	2	);	  // EXTBUS_BLS2
//	scu_pinmux(	0xD	,	10	,	MD_PLN_FAST	,	2	);	  // EXTBUS_BLS3
	scu_pinmux(	1	,	5	,	MD_PLN_FAST	,	3	);	  // EXTBUS_CS0
	scu_pinmux(	6	,	3	,	MD_PLN_FAST	,	3	);    // EXTBUS_CS1
	scu_pinmux(	0xD	,	12	,	MD_PLN_FAST	,	2	);    // EXTBUS_CS2
//	scu_pinmux(	0xD	,	11	,	MD_PLN_FAST	,	2	);    // EXTBUS_CS3

	/* Dynamic memory signals : EXTBUS_DYCS[3:0], EXTBUS_CAS, EXTBUS_RAS, EXTBUS_CLK[3:0], EXTBUS_CLKOUT[3:0], EXTBUS_DQMOUT[3:0]*/
	scu_pinmux(	6	,	9	,	MD_PLN_FAST	,	3	);    // EXTBUS_DYCS0
//	scu_pinmux(	6	,	1	,	MD_PLN_FAST	,	1	);    // EXTBUS_DYCS1
//	scu_pinmux(	0xD	,	14	,	MD_PLN_FAST	,	2	);    // EXTBUS_DYCS2
//	scu_pinmux(	0xE	,	14	,	MD_PLN_FAST	,	3	);    // EXTBUS_DYCS3
	scu_pinmux(	6	,	4	,	MD_PLN_FAST	,	3	);    // EXTBUS_CAS
	scu_pinmux(	6	,	5	,	MD_PLN_FAST	,	3	);    // EXTBUS_RAS
	LPC_SCU_CLK(0) = 0 + (MD_PLN_FAST);					  // EXTBUS_CLK0
	LPC_SCU_CLK(1) = 0 + (MD_PLN_FAST);					  // EXTBUS_CLK1
	LPC_SCU_CLK(2) = 0 + (MD_PLN_FAST);					  // EXTBUS_CLK2
	LPC_SCU_CLK(3) = 0 + (MD_PLN_FAST);					  // EXTBUS_CLK3
	scu_pinmux(	6	,	11	,	MD_PLN_FAST	,	3	);    // EXTBUS_CKEOUT0
//	scu_pinmux(	6	,	2	,	MD_PLN_FAST	,	1	);    // EXTBUS_CKEOUT1
//	scu_pinmux(	0xD	,	1	,	MD_PLN_FAST	,	2	);    // EXTBUS_CKEOUT2
//	scu_pinmux(	0xE	,	15	,	MD_PLN_FAST	,	3	);    // EXTBUS_CKEOUT3
	scu_pinmux(	6	,	12	,	MD_PLN_FAST	,	3	);    // EXTBUS_DQMOUT0
	scu_pinmux(	6	,	10	,	MD_PLN_FAST	,	3	);    // EXTBUS_DQMOUT1
	scu_pinmux(	0xD	,	0	,	MD_PLN_FAST	,	2	);    // EXTBUS_DQMOUT2
	scu_pinmux(	0xE	,	13	,	MD_PLN_FAST	,	3	);    // EXTBUS_DQMOUT3

	/* The EMC clock is half of the core clock */
	LPC_CCU1->CLK_M4_EMCDIV_CFG = 0x01 | (1 << 5);
	LPC_CREG->CREG6 |= (1 << 16);
}

void sdramInit(void)
{
	volatile uint32_t i;
	volatile unsigned long Dummy;

	LPC_SCU->EMCDELAYCLK = 0x7777;

	LPC_EMC->CONTROL =1;				              /* Disable Address mirror */

	LPC_EMC->DYNAMICRP = P2C(SDRAM_TRP) - 1;			 /* Precharge command period -- Trp */
	LPC_EMC->DYNAMICRAS = P2C(SDRAM_TRAS) - 1;			 /* Active to precharge command period -- Tras */
	LPC_EMC->DYNAMICSREX = P2C(SDRAM_TXSR) - 1;			 /* Self-refresh exit time -- Txsr */
	LPC_EMC->DYNAMICRC = P2C(SDRAM_TRC) - 1;			 /* Active to active command period -- Trc */
	LPC_EMC->DYNAMICRFC = P2C(SDRAM_TRFC) - 1;			 /* Auto-refresh period and auto-refresh to active command period -- Trfc */
	LPC_EMC->DYNAMICXSR = P2C(SDRAM_TXSR) - 1;			 /* Exit self-refresh to active command time -- Txsr */
	LPC_EMC->DYNAMICRRD = SDRAM_TRRD;				     /* Active bank A to active bank B latency -- Trrd */
	LPC_EMC->DYNAMICAPR = SDRAM_TAPR;				     /* Last-data-out to active command time -- Tapr */
	LPC_EMC->DYNAMICDAL = SDRAM_TDAL+P2C(SDRAM_TRP) - 1; /* Data-in to active command -- Tdal */
	LPC_EMC->DYNAMICWR = SDRAM_TWR;					     /* Write recovery time -- Twr*/
	LPC_EMC->DYNAMICMRD = SDRAM_TMRD;				     /* Load mode register to active command time -- Tmrd */

	LPC_EMC->DYNAMICREADCONFIG = 1;					/* Command delayed strategy */
	LPC_EMC->DYNAMICRASCAS0 = (3 << 0) |(3 << 8);	/* RAS latency 3 CCLKs, CAS latenty 3 CCLKs. */

//	LPC_EMC->DYNAMICCONFIG0 = 0<<14 | 3<<9 | 1<<7;			
	LPC_EMC->DYNAMICCONFIG0 = 1<<14 | 3<<9 | 1<<7;		/* 256MB, 16Mx16, 4 banks, row=13, column=9 */

	LPC_EMC->DYNAMICCONTROL = 0x0183;				   /* Mem clock enable, CLKOUT runs, send command: NOP */
	for(i= 200*30; i;i--);
	
	LPC_EMC->DYNAMICCONTROL = 0x0103;				  /* PRECHARGE-ALL, shortest possible refresh period */
	LPC_EMC->DYNAMICREFRESH = 2;					  /* set 32 CCLKs between SDRAM refresh cycles */
	for(i= 256; i; --i);       				           /* wait 128 AHB clock cycles */
	LPC_EMC->DYNAMICREFRESH = P2C(SDRAM_REFRESH) >> 4; /* 7.813us between SDRAM refresh cycles */
	

    LPC_EMC->DYNAMICCONTROL    = 0x00000083;        /* Mem clock enable, CLKOUT runs, send command: MODE */
	Dummy = *((volatile uint32_t *)(SDRAM_ADDR_BASE | (0x32<<13)));	  /* Set Mode regitster */
//	Dummy = *((volatile uint32_t *)(SDRAM_ADDR_BASE | (0x33<<12)));
	
	LPC_EMC->DYNAMICCONTROL = 0x0000;				 /* Issue NORMAL command */
	LPC_EMC->DYNAMICCONFIG0 |=(1<<19);				 /* Enable buffer */
	for(i = 100000; i;i--);
}




