
#ifndef LPC18XX_DEBUG_H
#define LPC18XX_DEBUG_H

extern void debugComInit(void);

extern void shellPrint(const char *format, ...);

extern void uartPutChar(uint8_t data);

#endif


