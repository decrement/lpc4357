
#ifndef __LED_H
#define __LED_H

#define LED_NUM 4

extern unsigned long g_eventLed;

#define EVENT_LED_TICK  0x00000001   // LED模块的TICK事件

// 对lED使用的GPIO管脚进行初始化
extern void ledInit(void);

// 将灯点亮，对应GPIP管脚置高电平
extern void ledOn(int led);

// 将灯熄灭，对应GPIP管脚置低电平
extern void ledOff(int led);

// LED模块的事件处理函数
extern void ledEventHandle(void);

#endif
