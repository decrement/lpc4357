#include <stdio.h>
#include <string.h>
#include "LPC43xx.h"
#include "led.h"
#include "key.h"
#include "dsFat.h"
#include "lpc43xx_debug.h"
#include "sdram.h"
#include "usbd_usr.h"
#include "iap.h"
#include "w25x32.h"


// #define KEY_IS_DOWN   ((((LPC_GPIO_TypeDef *)LPC_GPIO2_BASE)->PIN & 1UL<<10) == 0)
#define KEY_IS_DOWN   (keyScan() != 0)

#define KEY_JITTER_TICK  5  // 判断铵键抖动的时间

unsigned char g_tickFlag;

char g_keyState;

unsigned long g_sysTick;

unsigned long g_keyDownTick;  // 键按下时的TICK，用来去抖动

#define APP_ADDRESS   0x1A008000



#define LED_MODE_CONTINUE  0x55555555   // 连续快闪模式
#define LED_MODE_INTERVEL  0x55005500   // 间隔快闪模块

unsigned long g_ledMode;   // 当前闪灯模式
unsigned char g_ledIndex;  // 当前闪灯位置


__asm void boot_jump( uint32_t address )
{
	LDR SP, [R0]		;Load new stack pointer address
    LDR PC, [R0, #4]	;Load new program counter address
}

void ExecuteUserCode(uint32_t addr)
{
	SysTick->CTRL = 0;
	SCB->VTOR = addr & 0xFFFFFF80;
	boot_jump(addr);
}

unsigned long g_readBuffer[256];

#define FAT_LIST_NAME_LEN                32												 
FAT_LIST_INFO g_fatListInfo;
char g_fatListName[FAT_LIST_NAME_LEN];

// 升级应用程序固件
int updateFirmware(void)
{
    int fileLen, readLen, fileNameLen, len, ret;
    FAT_FILE_HANDLE fp;
    FAT_BOOL fatRet;
    
    USB_Connect(0);

    if (0 == fatInit(U_DISK_SIZE))
    {
        return 0;
    }
	
	iap_init(BANK0);

    g_fatListInfo.name = g_fatListName;
    g_fatListInfo.nameLen = FAT_LIST_NAME_LEN;

    fatRet = fatGetDirectoryListFirst(&g_fatListInfo);

    while (fatRet != FAT_FALSE)
    {
        // 找文件后缀名为bin的文件
        
        fileNameLen = strlen(g_fatListInfo.name);

        if (fileNameLen < 5)
        {
            fatRet = fatGetDirectoryListNext(&g_fatListInfo);
            continue;
        }

        if (   (g_fatListInfo.name[fileNameLen - 4] != '.')
            || (g_fatListInfo.name[fileNameLen - 3] != 'b' && g_fatListInfo.name[fileNameLen - 3] != 'B')
            || (g_fatListInfo.name[fileNameLen - 2] != 'i' && g_fatListInfo.name[fileNameLen - 2] != 'I')
            || (g_fatListInfo.name[fileNameLen - 1] != 'n' && g_fatListInfo.name[fileNameLen - 1] != 'N'))
        {
            fatRet = fatGetDirectoryListNext(&g_fatListInfo);
            continue;
        }

        fp = fatFileOpen(g_fatListInfo.name, FAT_FILE_OPEN_READ);

        fileLen = fatFileGetLen(fp);

        if (fileLen >= 480*1024)
        {
            // BIN文件不能超过480K
            fatFileClose(fp);
            return 0;
        }

        // 擦除FLASH
        ret = iap_erase_flash((uint32_t)APP_ADDRESS, fileLen);
        if (ret != 0)
        {
            fatFileClose(fp);
            return 0;
        }

		__disable_irq();

        readLen = 0;

        while (readLen < fileLen)
        {
            len = fatFileRead(fp, (unsigned char*)g_readBuffer, 1024);

            //复制到内部Flash中
            if (iap_write_flash((uint32_t)APP_ADDRESS + readLen , (unsigned char *)g_readBuffer, len) != 0)
            {
                __enable_irq();
                fatFileClose(fp);
                return 0;
            }

            readLen += len;
        }

        __enable_irq();
        
        fatFileClose(fp);

        return 1;
    }
    
    return 0;
}

void runApp(void)
{
    ExecuteUserCode(APP_ADDRESS);
}


void checkFlash(void)
{
    unsigned short word;
    unsigned long len, copyLen, ret, dst, src;
	
	SSPInit();
	iap_init(BANK0);
    
	flash_read_data(0, (unsigned char *)g_readBuffer, 6);
    //查一下norflash上有没有要更新的固件
    memcpy((char *)&word, (char*)g_readBuffer, 2);
	
// 	shellPrint("word 0x%x\r\n", word);
       
    //把norflash的固件拷贝到片内flash上
    if (word != 0x55AA)
    {
        return;
    }

    memcpy((char *)&len, (char*)((char *)g_readBuffer + 2), 4);
//     shellPrint("len %d\r\n", len);

    //判断上norflash上的固件大小
    if (len > (0x78000 - 6))
    {
        return;
    }
 
    copyLen = 0;    
    ret = iap_erase_flash(APP_ADDRESS, len);   
    if (ret != 0)
    {
        shellPrint("iap_erase_flash fail.\r\n");
        return;
    }
    
    dst = APP_ADDRESS;  
    src = 6;
    
    __disable_irq();
    while (len)
    {
        copyLen = (len >= 1024) ? 1024 : len;
              
		flash_read_data(src, (unsigned char *)g_readBuffer, copyLen);

        if ((ret = iap_write_flash(dst , (unsigned char *)g_readBuffer, copyLen)) != 0)
        {
            __enable_irq();
            shellPrint("iap_write_flash fail %x, %d.\r\n", dst, ret);
            return;
        }
         
        src += copyLen;
        dst += copyLen;
        
        len -= copyLen;
    }
    __enable_irq();
    shellPrint("copy Ok.\r\n");
	
    //更新完,擦除nor上的第一个扇区
	flash_sector_erase(0);
}

// 在上电1秒时间，会检测按键是否动作，
//   如按过按键，则更新应用程序，否则直接引导应用程序

// 按键状态
// 0：初始状态
// 1：键按下
// 2：键释放（点击），等待用户加载程序
// 3：更新状态（键按下）
// 4：更新状态（键释放[点击]），更新应用程序

// 状态迁移说明
// 在上电后1秒内，有键按下，则进入状态1
// 在状态1，有键释放，则进入状态2
// 在状态2，不再采集按键动作。上电1秒时，会进入状态3。
// 在状态3，键按下，则进入状态4。
// 在状态4，键释放，更新BootLoader，成功则进入状态5，失败进入状态3。
// 在状态5，不再采集按键动作。修改LED闪烁模式。

int main (void)
{		
//	int res;
// 	uint32_t FlashID;

    g_keyState = 0;
    g_sysTick = 0;;

    g_ledMode = LED_MODE_INTERVEL;
    g_ledIndex = 0;
	
	SystemInit();
    
	ledInit();
	keyInit();
	debugComInit();
	//shellPrint("hello\r\n");
		
	emcInit();
	sdramInit();
	
// 	FlashID = ReadDeviceId();
	//debugPrint("Get NOR FLASH ID 0x%x \r\n", FlashID);
    
   	memset((char*)SDRAM_ADDR_BASE, 0, U_DISK_SIZE);	
	
	fatInit(U_DISK_SIZE);
    
    checkFlash();

	while (1)
	{
// 		shellPrint("(%d)", g_keyState);
        // 不断检测按键状态
        switch (g_keyState)
        {
        case 0:
            if (KEY_IS_DOWN)
            {
				shellPrint("key down\r\n");
                g_keyDownTick = g_sysTick;
                g_keyState = 1;
            }
            break;
        case 1:
            if (!KEY_IS_DOWN)
            {
				shellPrint("key up\r\n");
                if ((g_sysTick - g_keyDownTick) > KEY_JITTER_TICK)
                {
                    USB_Init();                  /* USB Initialization */
                    USB_Connect(1);              /* USB Connect */

                    g_ledMode = LED_MODE_CONTINUE;
                    g_ledIndex = 0;
                    
                    g_keyState = 2;
                }
                else
                {
                    g_keyState = 0;
                }
                
            }
            break;
        case 2:
            if (KEY_IS_DOWN)
            {
                g_keyDownTick = g_sysTick;
                g_keyState = 3;
            }
            break;            
        case 3:
            if (!KEY_IS_DOWN)
            {
                if ((g_sysTick - g_keyDownTick) > KEY_JITTER_TICK)
                {
                    if (updateFirmware())
                    {
                        g_ledMode = LED_MODE_INTERVEL;
                        g_ledIndex = 0;

                        // 进入App
                        runApp();
                    }
                    else
                    {
                        USB_Connect(1);

                        g_keyState = 2;
                    }
                }
                else
                {
                    g_keyState = 2;
                }
            }
            break;
        default:
            break;
        }
        
        // 周期性事件
        if (g_tickFlag)
        {
            g_tickFlag = 0;

            g_sysTick++;
            
            if ((g_sysTick & 0x07) == 0)
            {
                if (g_ledMode & (1UL << g_ledIndex))
                {
                    ledOn(1);
                }
                else
                {
                    ledOff(1);
                }
                
                g_ledIndex = (g_ledIndex >= 31 ? 0 : (g_ledIndex + 1));
            }
            
            if (g_sysTick == 100) // 1秒钟
            {
                if (g_keyState != 2)
                {
                    // 进入App
                    runApp();
                }
            }                
        }
	}
}

void SysTick_Handler(void)
{
    g_tickFlag = 1;        
}

