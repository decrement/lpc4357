#include <string.h>
#include "usbd_usr.h"
#include "usb_msc_user.h"
#include "lpc43xx.h"
#include "lpc43xx_scu.h"

extern void shellPrint(const  char *format, ...);

#define  USB_A            0 
#define  USB_B            1 
#define  USBDEVICSTATE    USB_A 


static USBD_HANDLE_T hUsb;

void USB0_IRQHandler(void)
{	
  	USBD_API->hw->ISR(hUsb);
}

void USB1_IRQHandler(void)
{	
  	USBD_API->hw->ISR(hUsb);
}

void InitUsbClock (void)
{   
    if(USBDEVICSTATE == USB_A)
	{
		LPC_CGU->PLL0USB_CTRL |= 1; 	// Power down PLL
							//	P			N
		LPC_CGU->PLL0USB_NP_DIV = (98<<0) | (514<<12);
							//	SELP	SELI	SELR	MDEC
		LPC_CGU->PLL0USB_MDIV = (0xB<<17)|(0x18<<22)|(0<<28)|(0x7FFA<<0);
		LPC_CGU->PLL0USB_CTRL =(0x06<<24) | (0x3<<2) | (1<<4);
	
		LPC_CGU->PLL0USB_CTRL &= ~(0x01);
	    while((LPC_CGU->PLL0USB_STAT&1) == 0x0);
	
		LPC_CGU->BASE_USB0_CLK = (0x07 << 24) | (1 << 11);
	}
    if(USBDEVICSTATE == USB_B)
	{
		LPC_CGU->IDIVA_CTRL = (2UL << 2)|(1UL << 11)|(0x00000009 << 24);
		LPC_CGU->BASE_USB1_CLK = (1UL << 11)|(0x0000000C << 24);
	
		LPC_CREG->CREG0 &= ~(1 << 5);
		LPC_SCU->SFSUSB = 0x12;
		LPC_USB1->PORTSC1_D |= (1<<24);
	
		/* enable USB1_VBUS */
	    scu_pinmux(0x2, 5, MD_PLN | MD_EZI | MD_ZI, FUNC2);	
	}
}


void USB_Init (void)
{
	USBD_API_INIT_PARAM_T usb_param;
  	USB_CORE_DESCS_T desc;
	ErrorCode_t ret;
  	USB_INTERFACE_DESCRIPTOR* pIntfDesc;

  	InitUsbClock();
	
	/* initilize call back structures */
  	memset((void*)&usb_param, 0, sizeof(USBD_API_INIT_PARAM_T));
    
    if(USBDEVICSTATE == USB_A)
	{
	  	usb_param.usb_reg_base = LPC_USB0_BASE;
	}
    if(USBDEVICSTATE == USB_B)
	{
		usb_param.usb_reg_base = LPC_USB1_BASE;
	}	
  	usb_param.max_num_ep = 6;
  	usb_param.mem_base = 0x20004000;
  	usb_param.mem_size = 0x2000;

  	/* for eagle/raptor the local SRAM is not accesable to USB
  	 * so copy the descriptors to USB accessable memory
  	 */
  	copy_descriptors(&desc, usb_param.mem_base + usb_param.mem_size);
	
	if(USBDEVICSTATE == USB_A)
	{
		/* Turn on the phy */
		LPC_CREG->CREG0 &= ~(1<<5);
	
	  	scu_pinmux(0x08, 1, MD_PUP, 1);
	  	LPC_USB0->PORTSC1_D &= ~(0x03 << 14);
	  	LPC_USB0->PORTSC1_D |= (1 << 14);
	}
    if(USBDEVICSTATE == USB_B)	
	{
		scu_pinmux(9, 3, MD_PUP, 2);
	  	LPC_USB1->PORTSC1_D &= ~(0x03 << 14);
	  	LPC_USB1->PORTSC1_D |= (1 << 14);	
	}
	
	/* USB Initialization */
  	ret = USBD_API->hw->Init(&hUsb, &desc, &usb_param);	
  	if (ret == LPC_OK) 
	{
        if(USBDEVICSTATE == USB_A)	
	    {	
		    pIntfDesc = (USB_INTERFACE_DESCRIPTOR*)((uint32_t)desc.high_speed_desc + USB_CONFIGUARTION_DESC_SIZE);
		    ret = usb_msc_mem_init(hUsb, pIntfDesc, &usb_param.mem_base, &usb_param.mem_size);
		    if (ret != LPC_OK)
			{
				shellPrint("\r\nusb_hid_init error!!!\r\n");
			}
			
		    if (ret == LPC_OK) 
			{
		      	NVIC_EnableIRQ(USB0_IRQn); //  enable USB0 interrrupts
		      	/* now connect */
// 		      	USBD_API->hw->Connect(hUsb, 1);
		    }
		}
        if(USBDEVICSTATE == USB_B)	
	    {
		    pIntfDesc = (USB_INTERFACE_DESCRIPTOR*)((uint32_t)desc.full_speed_desc + USB_CONFIGUARTION_DESC_SIZE);
		    ret = usb_msc_mem_init(hUsb, pIntfDesc, &usb_param.mem_base, &usb_param.mem_size);
		    if (ret != LPC_OK)
			{
				shellPrint("\r\nusb_hid_init error!!!\r\n");
			}
			
		    if (ret == LPC_OK) 
			{
		      	NVIC_EnableIRQ(USB1_IRQn); //  enable USB0 interrrupts
		      	/* now connect */
// 		      	USBD_API->hw->Connect(hUsb, 1);
		    }		
		}		
  	} else 
	{
		shellPrint("usb init error!!!\r\n");
  	}
}

void USB_Connect (char on)
{
	USBD_API->hw->Connect(hUsb, on);
}



