#include "LPC43xx.h"
#include "lpc43xx_gpio.h"
#include "lpc43xx_scu.h"
#include "led.h"

unsigned long g_eventLed;

typedef struct
{
    uint8_t pin;
    uint8_t mode;
    uint8_t func;
    uint8_t gpioPort;       // 对应GPIO的口
    uint32_t gpioPin;        // 对应的PIN脚掩码 
}T_LED;     // LED灯的描述信息

#define LED_NUM      4

// 定义各个灯的数据
const T_LED g_led[LED_NUM]= {
	{0xEE, MD_PUP, FUNC4, 7, 1UL << 14},
	{0xEF, MD_PUP, FUNC4, 7, 1UL << 15},
	{0xF1, MD_PUP, FUNC4, 7, 1UL << 16},
	{0xF3, MD_PUP, FUNC4, 7, 1UL << 18}
};

// 对lED使用的GPIO管脚进行初始化
void ledInit(void)
{
    uint32_t i;

    for (i = 0; i < LED_NUM; i++)
    {
        scu_pinmux(g_led[i].pin >> 4, g_led[i].pin & 0x0F, g_led[i].mode, g_led[i].func);

        GPIO_SetDir(g_led[i].gpioPort, g_led[i].gpioPin, 1);
    }

    g_eventLed = 0;
}

// 将灯点亮，对应GPIP管脚置高电平
void ledOn(int led)
{
    if (led >= LED_NUM)
    {
        return;
    }

    GPIO_ClearValue(g_led[led].gpioPort, g_led[led].gpioPin);
}

// 将灯熄灭，对应GPIP管脚置低电平
void ledOff(int led)
{
    if (led >= LED_NUM)
    {
        return;
    }

    GPIO_SetValue(g_led[led].gpioPort, g_led[led].gpioPin);
}

// 跑马灯函数，每调用一次，灯后移一位。
void ledRolling(void)
{
    static uint8_t index = 0;

    if (index & 0x01)
    {
        // Turn off led
        ledOff(index>>1);
    }
    else
    {
        // Turn on led
        ledOn(index>>1);
    }

	index++;                     /* Count value plus one */       
	if (index >= (LED_NUM*2))        /* If counter reach MAX, clear counter */
	{
		index = 0;
	}
}

void ledEventHandle(void)
{
    static int tick = 0;

    g_eventLed = 0;

    tick++;

    if (tick >= 50)
    {
        tick = 0;

        ledRolling();
    }
}
