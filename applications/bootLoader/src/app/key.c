#include "lpc43xx.h"
#include "key.h"	
#include "lpc43xx_scu.h"
#include "lpc43xx_gpio.h"

typedef struct __PIN_INFO
{
	uint32_t port;
	uint32_t pinMask;
}PIN_INFO;


PIN_INFO gkeyX[KEY_NUM] = {{3, 1ul << 2}, {4, 1ul << 12}, {6, 1ul << 25}, {7, 1ul << 25},{6, 1ul << 28}};


// 对lED使用的GPIO管脚进行初始化
void keyInit(void)
{
    uint8_t i;

	scu_pinmux(0x06, 3, MD_PUP| MD_EZI, FUNC0);		              // PD_1   ---> keyport1  p6_3  gpio3.2
	scu_pinmux(0x09, 0, MD_PUP| MD_EZI, FUNC0);		              // P4_0   ---> keyport2 p9_0 gpio4.12
	scu_pinmux(0x0D, 11, MD_PUP| MD_EZI, FUNC4);		          // PD_11  ---> keyport3    pgio 6.25 *
	scu_pinmux(0x0f, 11, MD_PUP| MD_EZI, FUNC4);		  		  // PD_13  ---> keyport4    pf11 gpio 7.25
	scu_pinmux(0x0D, 14, MD_PUP| MD_EZI, FUNC4);		  // PD_14  ---> keyport5    pgio6.28  *

	for (i = 0; i < KEY_NUM; i++)						    
	{
		GPIO_SetDir(gkeyX[i].port, gkeyX[i].pinMask, 0);
	}
}

void DelayMs (uint32_t ms)
{
	uint32_t i, j;
	
	for (i = 0; i < ms; i++)
	{
		for (j = 0; j < 0x1000; j++);
	}
}

// 对键盘进行扫描，获得按键值。返回的BIT0-15分别对应16个按键是否按下。1为按下。
uint32_t keyScan(void)
{
    uint32_t i, ret;

    ret = 0;

    for (i = 0; i < KEY_NUM; i++)						   /* Raw scan                         */
    {       
		if ((GPIO_ReadValue(gkeyX[i].port) & gkeyX[i].pinMask) == 0)
        {											   /* Get key state                    */
			DelayMs(10);						       /* Debounce                         */
			if ((GPIO_ReadValue(gkeyX[i].port) & gkeyX[i].pinMask) == 0)
			{										   /* Get key state again              */
                ret |= 1UL << i;	       /* Mark key state                   */
			}
        }
    }													   /* Restore pins state               */

    return ret;
}

