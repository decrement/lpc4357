
#ifndef _KEY_H
#define _KEY_H

#define KEY_NUM                               5		  /* Raw number of matrix         */

extern void DelayMs(uint32_t ms);

extern void keyInit(void);

extern uint32_t keyScan(void);


#endif

